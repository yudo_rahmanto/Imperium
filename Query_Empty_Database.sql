DELETE FROM cetak_sk;
DELETE FROM cetak_sk_tdp;
DELETE FROM dokumentasi_lapangan;
DELETE FROM history_permohonan;
DELETE FROM log_reset_password;
DELETE FROM m_alamat_perusahaan;
DELETE FROM m_permohonan;
DELETE FROM m_perusahaan;
DELETE FROM m_retribusi;
DELETE FROM m_signature;
UPDATE m_sk SET `status` = 0;
DELETE FROM m_tanda_terima;
DELETE FROM pembayaran_retribusi;
DELETE FROM pemenuhan_syarat_pemohon;
DELETE FROM m_izin_access;
DELETE FROM m_alur_izin;
DELETE FROM m_menu_access;