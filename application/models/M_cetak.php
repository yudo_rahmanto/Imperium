<?php
class M_cetak extends CI_Model{
	function getCetakSK($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT
						a.sk_id,
						a.no_sk,
						g.code AS no_permohonan,
						g.qr_code AS qr_code_permohonan,
						b.first_name,
						b.last_name,
						c.nama_perusahaan,
						c.nama_cabang,
						d.name AS izin_category,
						e.izin_id,
						e.izin_name,
						f.type_name AS izin_type_name
					FROM
						m_sk a
					INNER JOIN m_user_management b ON b.user_management_id = a.user_management_id AND b.activation = 'Y'
					INNER JOIN (	SELECT 
								aa.perusahaan_id,
								aa.perusahaan_type_id,
								aa.nama_perusahaan,
								ab.alamat_perusahaan_id,
								ab.nama_cabang
							FROM 
								m_perusahaan aa 
							INNER JOIN m_alamat_perusahaan ab ON ab.perusahaan_id = aa.perusahaan_id AND ab.activation = 'Y'
						) c ON c.alamat_perusahaan_id = a.alamat_perusahaan_id
					INNER JOIN m_izin_category d ON d.izin_category_id = a.izin_category_id AND d.activation = 'Y'
					INNER JOIN m_izin e ON e.izin_id = a.izin_id AND e.activation = 'Y'
					INNER JOIN m_izin_type f ON f.izin_type_id = a.izin_type_id AND f.activation = 'Y'
					INNER JOIN m_permohonan g ON g.permohonan_id = a.permohonan_id AND g.status_sk = 1 AND g.activation = 'Y'
					Where	
						a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.created_date asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDetailSK($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT
						a.sk_id,
						a.no_sk,
						a.path_location_barcode as qr_code_sk,
						g.permohonan_id,
						g.code AS no_permohonan,
						g.qr_code AS qr_code_permohonan,
						(select retribusi_id from m_retribusi where permohonan_id = g.permohonan_id and activation = 'Y') as retribusi_id,
						(select tgl_posting from m_retribusi where permohonan_id = g.permohonan_id and activation = 'Y') as tgl_retribusi,
						b.user_management_id,
						b.first_name,
						b.last_name,
						(select path_location from pemenuhan_syarat_pemohon where syarat_id = 8 and permohonan_id = a.permohonan_id and izin_id = a.izin_id and izin_type_id = a.izin_type_id order by syarat_id asc limit 0,1) as photo_pemohon,
						b.ktp,
						b.npwp,
						b.no_tlp as no_tlp_user,
						b.address as alamat_user,
						b.rt as rt_user,
						b.rw as rw_user,
						(Select nama from provinsi where id = b.provinsi_id) as provinsi_user,
						(Select nama from kabupaten where id = b.kabupaten_id) as kabupaten_user,
						(Select nama from kecamatan where id = b.kecamatan_id) as kecamatan_user,
						(Select nama from kelurahan where id = b.kelurahan_id) as kelurahan_user,
						c.*,
						d.izin_category_id,
						d.name AS izin_category,
						e.izin_id,
						e.izin_name,
						e.keterangan as keterangan_izin,
						f.type_name AS izin_type_name,
						(Select data_detail from m_sk_detail where sk_id = a.sk_id) as sk_detail
					FROM
						m_sk a
					INNER JOIN m_user_management b ON b.user_management_id = a.user_management_id AND b.activation = 'Y'
					INNER JOIN (	SELECT 
										aa.perusahaan_id,
										aa.perusahaan_type_id,
										(Select initial from m_perusahaan_type where perusahaan_type_id = aa.perusahaan_type_id) as type_perusahaan,
										aa.nama_perusahaan,
										ab.alamat_perusahaan_id,
										ab.nama_cabang,
										(Select nama from kecamatan where id = ab.kecamatan_id) as kecamatan_perusahaan,
										(Select nama from kelurahan where id = ab.kelurahan_id) as kelurahan_perusahaan,
										(select aab.nama from kecamatan aaa inner join kabupaten aab on aab.id = aaa.id_kabupaten where aaa.id = ab.kecamatan_id) as kabupaten_perusahaan,
										(select aac.nama from kecamatan aaa inner join kabupaten aab on aab.id = aaa.id_kabupaten inner join provinsi aac on aac.id = aab.id_prov where aaa.id = ab.kecamatan_id) as provinsi_perusahaan,
										(Select name from m_jenis_usaha where jenis_usaha_id = ab.jenis_usaha_id) as jenis_usaha,
										(Select name from m_lokasi_perusahaan where lokasi_perusahaan_id = ab.lokasi_perusahaan_id) as lokasi_perusahaan,
										(Select name from m_lingkungan_perusahaan where lingkungan_perusahaan_id = ab.lingkungan_perusahaan_id) as lingkungan_perusahaan,
										(Select name from m_index_gangguan where index_gangguan_id = ab.index_gangguan_id) as index_gangguan,
										(Select name from m_alat_kerja where alat_kerja_id = ab.alat_kerja_id) as alat_kerja,
										ab.luas as luas_perusahaan,
										ab.kegiatan_usaha as kegiatan_usaha_perusahaan,
										ab.modal_usaha as modal_usaha_perusahaan,
										ab.kelembagaan as kelembagaan_perusahaan,
										ab.product as product_perusahaan,
										ab.alamat AS alamat_perusahaan,
										ab.tlp as no_tlp_perusahaan,
										ab.rt AS rt_perusahaan,
										ab.rw AS rw_perusahaan
									FROM 
										m_perusahaan aa 
							INNER JOIN m_alamat_perusahaan ab ON ab.perusahaan_id = aa.perusahaan_id AND ab.activation = 'Y'
						) c ON c.alamat_perusahaan_id = a.alamat_perusahaan_id
					INNER JOIN m_izin_category d ON d.izin_category_id = a.izin_category_id AND d.activation = 'Y'
					INNER JOIN m_izin e ON e.izin_id = a.izin_id AND e.activation = 'Y'
					INNER JOIN m_izin_type f ON f.izin_type_id = a.izin_type_id AND f.activation = 'Y'
					INNER JOIN m_permohonan g ON g.permohonan_id = a.permohonan_id AND g.status_sk = 1 AND g.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.created_date asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getSPM($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT
						a.retribusi_id,
						a.tgl_posting AS tgl_terbit,
						b.permohonan_id,
						b.code,
						c.nama_perusahaan,
						c.nama_cabang,
						f.ktp,
						f.first_name,
						f.last_name,
						d.izin_id,
						d.izin_name,
						e.type_name AS izin_type_name
					FROM
						m_retribusi a
					INNER JOIN m_permohonan b ON b.permohonan_id = a.permohonan_id AND b.activation = 'Y'
					INNER JOIN (	SELECT 
								aa.perusahaan_id,
								aa.perusahaan_type_id,
								aa.nama_perusahaan,
								ab.alamat_perusahaan_id,
								ab.nama_cabang
							FROM 
								m_perusahaan aa 
							INNER JOIN m_alamat_perusahaan ab ON ab.perusahaan_id = aa.perusahaan_id AND ab.activation = 'Y'
						) c ON c.alamat_perusahaan_id = b.alamat_perusahaan_id
					INNER JOIN m_izin d ON d.izin_id = b.izin_id AND d.activation = 'Y'
					INNER JOIN m_izin_type e ON e.izin_type_id = b.izin_type_id AND e.activation = 'Y'
					INNER JOIN m_user_management f ON f.user_management_id = b.user_management_id AND f.activation = 'Y'
					WHERE	
						a.activation = 'Y' And DATE(a.tgl_posting) >= DATE(NOW() - INTERVAL 3 MONTH)
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.created_date asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getSKRD($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT
						a.retribusi_id,
						a.tgl_posting AS tgl_terbit,
						b.permohonan_id,
						b.code,
						c.nama_perusahaan,
						c.nama_cabang,
						f.ktp,
						f.first_name,
						f.last_name,
						d.izin_id,
						d.izin_name,
						e.type_name AS izin_type_name
					FROM
						m_retribusi a
					INNER JOIN m_permohonan b ON b.permohonan_id = a.permohonan_id AND b.activation = 'Y'
					INNER JOIN (	SELECT 
								aa.perusahaan_id,
								aa.perusahaan_type_id,
								aa.nama_perusahaan,
								ab.alamat_perusahaan_id,
								ab.nama_cabang
							FROM 
								m_perusahaan aa 
							INNER JOIN m_alamat_perusahaan ab ON ab.perusahaan_id = aa.perusahaan_id AND ab.activation = 'Y'
						) c ON c.alamat_perusahaan_id = b.alamat_perusahaan_id
					INNER JOIN m_izin d ON d.izin_id = b.izin_id AND d.activation = 'Y'
					INNER JOIN m_izin_type e ON e.izin_type_id = b.izin_type_id AND e.activation = 'Y'
					INNER JOIN m_user_management f ON f.user_management_id = b.user_management_id AND f.activation = 'Y'
					WHERE	
						a.activation = 'Y' And DATE(a.tgl_posting) >= DATE(NOW() - INTERVAL 3 MONTH)
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.created_date asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPermohonanDetail($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						(
							Select 
								level_name
							From
								m_level 
							Where
								activation = 'Y' and
								level_id = f.level_id
						) as posisi_alur,
						b.nama_perusahaan,
						c.first_name,
						c.last_name,
						d.izin_name,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join m_perusahaan b on b.perusahaan_id = a.perusahaan_id and b.activation = 'Y'
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					Inner Join m_alur_izin f on f.izin_id = d.izin_id and f.izin_type_id = e.izin_type_id and f.no_urut = a.status_level
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						b.name as syarat_name
					FROM m_syarat a 
					Inner Join m_persyaratan b on b.persyaratan_id = a.persyaratan_id and b.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql			= $sql." Order By a.syarat_id Asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPemenuhanSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.nama_perusahaan,
						d.first_name,
						d.last_name,
						e.izin_name,
						f.type_name as izin_type_name,
						h.name as syarat_name
					FROM m_permohonan a 
					Inner Join pemenuhan_syarat_pemohon b on b.permohonan_id = a.permohonan_id and b.activation = 'Y'
					Inner Join m_perusahaan c on c.perusahaan_id = b.perusahaan_id and c.activation = 'Y'
					Inner Join m_user_management d on d.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin e on e.izin_id = b.izin_id and e.activation = 'Y'
					Inner Join m_izin_type f on f.izin_type_id = b.izin_type_id and f.activation = 'Y'
					Left Join m_syarat g on g.syarat_id = b.syarat_id and g.activation = 'Y'
					Left Join m_persyaratan h on h.persyaratan_id = g.persyaratan_id and h.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.syarat_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataPerusahaan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.id,
						c.nama as kecamatan_name,
						d.id,
						d.nama as kelurahan_name,
						e.name as jenis_usaha,
						f.name as lokasi_perusahaan,
						g.name as lingkungan_perusahaan,
						h.name as index_gangguan,
						i.name as alat_kerja,
						j.perusahaan_type as jenis_perusahaan
					FROM m_permohonan a 
					Inner Join m_perusahaan b on b.perusahaan_id = a.perusahaan_id and b.activation = 'Y'
					Left Join kecamatan c on c.id = b.kecamatan_id
					Left Join kelurahan d on d.id = b.kelurahan_id
					Left Join m_jenis_usaha e on e.jenis_usaha_id = b.jenis_usaha_id
					Left Join m_lokasi_perusahaan f on f.lokasi_perusahaan_id = b.lokasi_perusahaan_id
					Left Join m_lingkungan_perusahaan g on g.lingkungan_perusahaan_id = b.lingkungan_perusahaan_id
					Left Join m_index_gangguan h on h.index_gangguan_id = b.index_gangguan_id
					Left Join m_alat_kerja i on i.alat_kerja_id = b.alat_kerja_id
					Left Join m_perusahaan_type j on j.perusahaan_type_id = b.perusahaan_type_id and j.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.perusahaan_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataUser($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.level_name,
						d.nama as provinsi_name,
						e.nama as kabupaten_name,
						f.nama as kecamatan_name,
						g.nama as kelurahan_name
					FROM m_permohonan a 
					Inner Join m_user_management b on b.user_management_id = a.user_management_id and b.activation = 'Y'
					Inner Join m_level c on c.level_id = b.level_id and c.activation = 'Y'
					Left Join provinsi d on d.id = b.provinsi_id
					Left Join kabupaten e on e.id = b.kabupaten_id
					Left Join kecamatan f on f.id = b.kecamatan_id
					Left Join kelurahan g on g.id = b.kelurahan_id
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.user_management_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function dataSignature($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= '';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 0){$operation = ' And ';}
				$filter_array	.= $operation.$row_left." = '".$row_right."' ";
			$i++;
			}
		}
		
		$sql	= "	Select 
						b.permohonan_id,
						d.nip,
						d.fullname_with_gelar,
						e.jabatan_name,
						c.path_location as signature
					From 
						m_sk a
					Inner Join m_permohonan b on b.permohonan_id = a.permohonan_id and b.status_finish = 1 and b.activation = 'Y'
					Inner Join m_signature c on c.permohonan_id = b.permohonan_id 
					Inner Join m_admin d on d.admin_id = c.created_user and d.activation = 'Y'
					Inner Join m_jabatan e on e.jabatan_id = d.jabatan_id and e.activation = 'Y'
					Where 
						a.activation = 'Y'
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataNoSK(){
		$sql	= "	SELECT 
						a.*
					FROM m_sk a 
					WHERE a.status = '0'
					Order By a.sk_id asc
					";
		$sql		= $sql;
		$query		= $this->db->query($sql);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null,$field = null,$value = null,$order_by = null,$sort = null){
		$filter	= '';
		if(($field) && ($value)){
			$filter	= " Where ".$field." = '".$value."' ";
		}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		if(($order_by) && ($sort)){
			$orderby	= " Order By ".$order_by." ".$sort;
			$sql		= $sql.$orderby;
		}
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getIdTandaTerima(){
		$sql	= "	Select case when max(tanda_terima_id) is not null then (max(tanda_terima_id) + 1) else '1' end as id from m_tanda_terima ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getId($table = null,$field = null){
		$sql	= "	Select case when max(".$field.") is not null then (max(".$field.") + 1) else '1' end as id from ".$table." ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function add($table,$data){	
		if($this->db->insert($table, $data)){
			if($table == 'm_permohonan'){
				$insert_id = $this->db->insert_id();
				$sql			= "Select * from m_permohonan Where permohonan_id = ".$insert_id."";
				$query			= $this->db->query($sql);
				$data			= $query->result_array();
				
				if($data){
					$result['temp']	= $data[0];
					$this->session->set_userdata($result);
				}
			}
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_permohonan where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		$sql	= "	Delete from pemenuhan_syarat_pemohon where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>