<?php
class M_finance extends CI_Model{
	function getFinanceRetribusi($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						b.*,
						c.nama_perusahaan,
						d.ktp,
						d.npwp,
						d.first_name,
						d.last_name,
						d.email,
						d.no_tlp as no_tlp_user,
						d.address as alamat,
						e.izin_name,
						f.type_name as izin_type_name
					FROM m_retribusi a
					Inner Join m_permohonan b on b.permohonan_id = a.permohonan_id and b.activation = 'Y'
					Inner Join (Select aa.perusahaan_id,aa.nama_perusahaan,ab.alamat_perusahaan_id From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y' Where aa.activation = 'Y') c on c.alamat_perusahaan_id = b.alamat_perusahaan_id
					Inner Join m_user_management d on d.user_management_id = b.user_management_id and d.activation = 'Y'
					Inner Join m_izin e on e.izin_id = a.izin_id and e.activation = 'Y'
					Inner Join m_izin_type f on f.izin_type_id = a.izin_type_id and f.activation = 'Y'
					WHERE a.activation = 'Y' and a.status_pembayaran = 0
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null){
		$sql			= "
							Select * From ".$table." 
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getData($find = null,$id = null){
		if($find == 'kecamatan'){$field = 'id_kabupaten';}
		else if($find == 'kelurahan'){$field = 'id_kecamatan';}
		$sql			= "
							Select * From ".$find." where ".$field." = ".$id."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getId(){
		$sql	= "	Select case when max(jenis_usaha_id) is not null then (max(jenis_usaha_id) + 1) else '1' end as id from m_jenis_usaha ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function add($table,$data){	
				if($this->db->insert($table, $data)){
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_jenis_usaha where jenis_usaha_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>