<?php
class M_permohonan extends CI_Model{
	function getPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						(select path_location_qr_code from m_tanda_terima where tanda_terima_id = a.tanda_terima_id and activation = 'Y') as path_location_qr_code,
						(Select initial from m_perusahaan_type where perusahaan_type_id = b.perusahaan_type_id and activation = 'Y') as jenis_perusahaan,
						b.nama_perusahaan,
						b.alamat as alamat_perusahaan,
						b.rt as rt_perusahaan,
						b.rw as rw_perusahaan,
						(select ac.nama from kecamatan aa inner join kabupaten ab on ab.id = aa.id_kabupaten inner join provinsi ac on ac.id = ab.id_prov where aa.id = b.kecamatan_id) as provinsi_perusahaan,
						(select ab.nama from kecamatan aa inner join kabupaten ab on ab.id = aa.id_kabupaten where aa.id = b.kecamatan_id) as kota_perusahaan,
						(select nama from kecamatan where id = b.kecamatan_id) as kecamatan_perusahaan,
						(select nama from kelurahan where id = b.kelurahan_id) as kelurahan_perusahaan,
						c.ktp,
						c.npwp,
						c.first_name,
						c.last_name,
						c.email,
						c.no_tlp as no_tlp_user,
						c.address as alamat_user,
						c.rt as rt_user,
						c.rw as rw_user,
						(select nama from provinsi where id = c.provinsi_id) as provinsi_user,
						(select nama from kabupaten where id = c.kabupaten_id) as kota_user,
						(select nama from kecamatan where id = c.kecamatan_id) as kecamatan_user,
						(select nama from kelurahan where id = c.kelurahan_id) as kelurahan_user,
						d.izin_name,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Inner Join m_user_management c on c.user_management_id = a.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPermohonanDetail($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						(
							SELECT 
								ac.level_name
							FROM
								m_jabatan aa
								INNER JOIN m_admin ab ON ab.jabatan_id = aa.jabatan_id AND ab.activation = 'Y'	
								INNER JOIN m_level ac ON ac.level_id = ab.level_id AND ac.activation = 'Y'
								INNER JOIN m_izin_access ad ON ad.level_id = ac.level_id AND ad.activation = 'Y'
							WHERE
								aa.activation = 'Y' AND
								aa.jabatan_id = f.jabatan_id AND
								FIND_IN_SET(a.izin_id,ad.izin_id) > 0  
							Order By
								ac.level_id Asc
							Limit 0,1
						) as posisi_alur,
						(Select initial from m_perusahaan_type where perusahaan_type_id = b.perusahaan_type_id and activation = 'Y') as jenis_perusahaan,
						b.nama_perusahaan,
						c.first_name,
						c.last_name,
						d.izin_name,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					Inner Join m_alur_izin f on f.izin_id = d.izin_id and f.izin_type_id = e.izin_type_id and f.no_urut = a.status_level
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						b.name as syarat_name
					FROM m_syarat a 
					Inner Join m_persyaratan b on b.persyaratan_id = a.persyaratan_id and b.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql			= $sql." Order By a.mandatory Desc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPemenuhanSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.nama_perusahaan,
						d.first_name,
						d.last_name,
						e.izin_name,
						f.type_name as izin_type_name,
						h.name as syarat_name
					FROM m_permohonan a 
					Inner Join pemenuhan_syarat_pemohon b on b.permohonan_id = a.permohonan_id and b.activation = 'Y'
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') c on c.alamat_perusahaan_id = b.alamat_perusahaan_id and c.activation = 'Y'
					Inner Join m_user_management d on d.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin e on e.izin_id = b.izin_id and e.activation = 'Y'
					Inner Join m_izin_type f on f.izin_type_id = b.izin_type_id and f.activation = 'Y'
					Left Join m_syarat g on g.syarat_id = b.syarat_id and g.activation = 'Y'
					Left Join m_persyaratan h on h.persyaratan_id = g.persyaratan_id and h.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.syarat_id Asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null,$field = null,$value = null,$order_by = null,$sort = null){
		$filter	= '';
		if(($field) && ($value)){
			$filter	= " Where ".$field." = '".$value."' ";
		}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		if(($order_by) && ($sort)){
			$orderby	= " Order By ".$order_by." ".$sort;
			$sql		= $sql.$orderby;
		}
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getIdHistory(){
		$sql	= "	Select case when max(id) is not null then (max(id) + 1) else '1' end as id from history_permohonan ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getIdTandaTerima(){
		$sql	= "	Select case when max(tanda_terima_id) is not null then (max(tanda_terima_id) + 1) else '1' end as id from m_tanda_terima ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getId(){
		$sql	= "	Select case when max(permohonan_id) is not null then (max(permohonan_id) + 1) else '1' end as id from m_permohonan ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function add($table,$data){	
		if($this->db->insert($table, $data)){
			if($table == 'm_permohonan'){
				$insert_id = $this->db->insert_id();
				$sql			= "Select * from m_permohonan Where permohonan_id = ".$insert_id."";
				$query			= $this->db->query($sql);
				$data			= $query->result_array();
				
				if($data){
					$result['temp']	= $data[0];
					$this->session->set_userdata($result);
				}
			}
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_permohonan where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		$sql	= "	Delete from pemenuhan_syarat_pemohon where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		$sql	= "	Delete from history_permohonan where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>