<?php
class M_approval extends CI_Model{
	function getApproval($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						(select path_location_qr_code from m_tanda_terima where tanda_terima_id = a.tanda_terima_id and activation = 'Y') as path_location_qr_code,
						b.nama_perusahaan,
						b.alamat as alamat_perusahaan,
						(select ac.nama from kecamatan aa inner join kabupaten ab on ab.id = aa.id_kabupaten inner join provinsi ac on ac.id = ab.id_prov where aa.id = b.kecamatan_id) as provinsi_perusahaan,
						(select ab.nama from kecamatan aa inner join kabupaten ab on ab.id = aa.id_kabupaten where aa.id = b.kecamatan_id) as kota_perusahaan,
						(select nama from kecamatan where id = b.kecamatan_id) as kecamatan_perusahaan,
						(select nama from kelurahan where id = b.kelurahan_id) as kelurahan_perusahaan,
						(Select initial from m_perusahaan_type where perusahaan_type_id = b.perusahaan_type_id and activation = 'Y') as jenis_perusahaan,
						c.ktp,
						c.npwp,
						c.first_name,
						c.last_name,
						c.email,
						c.no_tlp as no_tlp_user,
						c.address as alamat,
						c.rt as rt_user,
						c.rw as rw_user,
						(select nama from kelurahan where id = c.kelurahan_id) as kel_user,
						(select nama from kecamatan where id = c.kecamatan_id) as kec_user,
						(select nama from kabupaten where id = c.kabupaten_id) as kab_user,
						(select nama from provinsi where id = c.provinsi_id) as prov_user,
						d.izin_name,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					Inner Join m_alur_izin f on f.izin_id = d.izin_id and f.izin_type_id = e.izin_type_id and f.no_urut = a.status_level
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.created_date Desc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getInbox($filter = null,$limit = null){
		$dataSession					= $this->session->userdata('user_data');
		$izin_access					= $dataSession['izin_id'];
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		if($izin_access){
			$filter_array	.= " And c.izin_id In (".$izin_access.") ";
		}
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						(Select initial from m_perusahaan_type where perusahaan_type_id = b.perusahaan_type_id and activation = 'Y') as jenis_perusahaan,
						b.nama_perusahaan,
						b.alamat AS alamat_perusahaan,
						c.izin_name,
						c.durasi_pekerjaan,
						d.type_name AS izin_type_name,
						e.jabatan_id,
						f.tgl_diterima
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Inner Join m_izin c on c.izin_id = a.izin_id and c.activation = 'Y'
					Inner Join m_izin_type d on d.izin_type_id = a.izin_type_id and d.activation = 'Y'
					Inner Join m_alur_izin e on e.izin_id = c.izin_id and e.izin_type_id = d.izin_type_id and e.no_urut = a.status_level
					LEFT JOIN history_permohonan f ON f.permohonan_id = a.permohonan_id AND f.jabatan_id_penerima = e.jabatan_id 
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.created_date Desc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getOnProgress($filter = null,$limit = null){
		$dataSession					= $this->session->userdata('user_data');
		$izin_access					= $dataSession['izin_id'];
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		if($izin_access){
			$filter_array	.= " And d.izin_id In (".$izin_access.") ";
		}
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT
						b.*,
						(SELECT initial FROM m_perusahaan_type WHERE perusahaan_type_id = c.perusahaan_type_id AND activation = 'Y') AS jenis_perusahaan,
						c.nama_perusahaan,
						c.alamat AS alamat_perusahaan,
						d.izin_name,
						d.durasi_pekerjaan,
						e.type_name AS izin_type_name,
						a.jabatan_id_penerima,
						a.tgl_diterima
					FROM
						history_permohonan a
					INNER JOIN m_permohonan b ON b.permohonan_id = a.permohonan_id AND b.activation = 'Y'
					INNER JOIN (SELECT aa.nama_perusahaan,aa.perusahaan_type_id,ab.* FROM m_perusahaan aa INNER JOIN m_alamat_perusahaan ab ON ab.perusahaan_id = aa.perusahaan_id AND ab.activation = 'Y') c ON c.alamat_perusahaan_id = b.alamat_perusahaan_id AND c.activation = 'Y'
					INNER JOIN m_izin d ON d.izin_id = b.izin_id AND d.activation = 'Y'
					INNER JOIN m_izin_type e ON e.izin_type_id = b.izin_type_id AND e.activation = 'Y'
					WHERE a.status_approval = 0 AND a.tgl_diterima IS NOT NULL
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." GROUP BY b.permohonan_id ORDER BY b.created_date ASC ";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getFinish($filter = null,$limit = null){
		$dataSession					= $this->session->userdata('user_data');
		$izin_access					= $dataSession['izin_id'];
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		if($izin_access){
			$filter_array	.= " And d.izin_id In (".$izin_access.") ";
		}
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT
						b.*,
						(SELECT initial FROM m_perusahaan_type WHERE perusahaan_type_id = c.perusahaan_type_id AND activation = 'Y') AS jenis_perusahaan,
						c.nama_perusahaan,
						c.alamat AS alamat_perusahaan,
						d.izin_name,
						e.type_name AS izin_type_name,
						a.jabatan_id_penerima,
						a.tgl_diterima
					FROM
						history_permohonan a
					INNER JOIN m_permohonan b ON b.permohonan_id = a.permohonan_id AND b.activation = 'Y'
					INNER JOIN (SELECT aa.nama_perusahaan,aa.perusahaan_type_id,ab.* FROM m_perusahaan aa INNER JOIN m_alamat_perusahaan ab ON ab.perusahaan_id = aa.perusahaan_id AND ab.activation = 'Y') c ON c.alamat_perusahaan_id = b.alamat_perusahaan_id AND c.activation = 'Y'
					INNER JOIN m_izin d ON d.izin_id = b.izin_id AND d.activation = 'Y'
					INNER JOIN m_izin_type e ON e.izin_type_id = b.izin_type_id AND e.activation = 'Y'
					WHERE a.status_approval = 1
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." GROUP BY b.permohonan_id Order By b.created_date Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPermohonanDetail($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						(
							Select 
								jabatan_name
							From
								m_jabatan 
							Where
								activation = 'Y' and
								jabatan_id = f.jabatan_id
						) as posisi_alur,
						b.nama_perusahaan,
						c.first_name,
						c.last_name,
						d.izin_category_id,
						d.izin_name,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					Inner Join m_alur_izin f on f.izin_id = d.izin_id and f.izin_type_id = e.izin_type_id and f.no_urut = a.status_level
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						b.name as syarat_name
					FROM m_syarat a 
					Inner Join m_persyaratan b on b.persyaratan_id = a.persyaratan_id and b.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql			= $sql." Order By a.syarat_id Asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPemenuhanSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.nama_perusahaan,
						d.first_name,
						d.last_name,
						e.izin_name,
						f.type_name as izin_type_name,
						h.name as syarat_name
					FROM m_permohonan a 
					Inner Join pemenuhan_syarat_pemohon b on b.permohonan_id = a.permohonan_id and b.activation = 'Y'
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') c on c.alamat_perusahaan_id = b.alamat_perusahaan_id and c.activation = 'Y'
					Inner Join m_user_management d on d.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin e on e.izin_id = b.izin_id and e.activation = 'Y'
					Inner Join m_izin_type f on f.izin_type_id = b.izin_type_id and f.activation = 'Y'
					Left Join m_syarat g on g.syarat_id = b.syarat_id and g.activation = 'Y'
					Left Join m_persyaratan h on h.persyaratan_id = g.persyaratan_id and h.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.syarat_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataPerusahaan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.id,
						c.nama as kecamatan_name,
						d.id,
						d.nama as kelurahan_name,
						e.name as jenis_usaha,
						f.name as lokasi_perusahaan,
						g.name as lingkungan_perusahaan,
						h.name as index_gangguan,
						i.name as alat_kerja,
						j.perusahaan_type as jenis_perusahaan
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Left Join kecamatan c on c.id = b.kecamatan_id
					Left Join kelurahan d on d.id = b.kelurahan_id
					Left Join m_jenis_usaha e on e.jenis_usaha_id = b.jenis_usaha_id
					Left Join m_lokasi_perusahaan f on f.lokasi_perusahaan_id = b.lokasi_perusahaan_id
					Left Join m_lingkungan_perusahaan g on g.lingkungan_perusahaan_id = b.lingkungan_perusahaan_id
					Left Join m_index_gangguan h on h.index_gangguan_id = b.index_gangguan_id
					Left Join m_alat_kerja i on i.alat_kerja_id = b.alat_kerja_id
					Left Join m_perusahaan_type j on j.perusahaan_type_id = b.perusahaan_type_id and j.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.perusahaan_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataUser($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.level_name,
						d.nama as provinsi_name,
						e.nama as kabupaten_name,
						f.nama as kecamatan_name,
						g.nama as kelurahan_name
					FROM m_permohonan a 
					Inner Join m_user_management b on b.user_management_id = a.user_management_id and b.activation = 'Y'
					Inner Join m_level c on c.level_id = b.level_id and c.activation = 'Y'
					Left Join provinsi d on d.id = b.provinsi_id
					Left Join kabupaten e on e.id = b.kabupaten_id
					Left Join kecamatan f on f.id = b.kecamatan_id
					Left Join kelurahan g on g.id = b.kelurahan_id
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.user_management_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataHistoryPermohonan($filter = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		$sql	= "	SELECT 
						a.*
					FROM history_permohonan a 
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.id Desc Limit 0,1";
		$query		= $this->db->query($sql);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataSuratTugas($filter = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		$sql	= "	
					SELECT 
						a.permohonan_id,
						b.nama_perusahaan,
						b.alamat as alamat_perusahaan,
						b.tlp as no_tlp_perusahaan,
						c.ktp,
						c.npwp,
						c.first_name,
						c.last_name,
						c.email,
						c.no_tlp as no_tlp_user,
						c.address as alamat_user,
						d.izin_id,
						d.izin_name,
						e.izin_type_id,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc Limit 0,1";
		$query		= $this->db->query($sql);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataRetribusi($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						b.code,
						b.qr_code,
						c.izin_name,
						c.keterangan as keterangan_izin,
						d.type_name,
						e.name as index_gangguan,
						f.name as lingkungan_perusahaan,
						g.name as lokasi_perusahaan,
						h.name as alat_kerja,
						h.bobot as bobot_alat_kerja
					FROM m_retribusi a 
					Inner Join m_permohonan b on b.permohonan_id = a.permohonan_id and b.activation = 'Y'
					Inner Join m_izin c on c.izin_id = a.izin_id and c.activation = 'Y'
					Inner Join m_izin_type d on d.izin_type_id = a.izin_type_id and d.activation = 'Y'
					Inner Join m_index_gangguan e on e.index_gangguan_id = a.index_gangguan_id and e.activation = 'Y'
					Inner Join m_lingkungan_perusahaan f on f.lingkungan_perusahaan_id = a.lingkungan_perusahaan_id and f.activation = 'Y'
					Inner Join m_lokasi_perusahaan g on g.lokasi_perusahaan_id = a.lokasi_perusahaan_id and g.activation = 'Y'
					Left Join m_alat_kerja h on h.alat_kerja_id = a.alat_kerja_id and h.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.retribusi_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null,$field = null,$value = null,$order_by = null,$sort = null){
		$filter	= '';
		if(($field) && ($value)){
			$filter	= " Where ".$field." = '".$value."' ";
		}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		if(($order_by) && ($sort)){
			$orderby	= " Order By ".$order_by." ".$sort;
			$sql		= $sql.$orderby;
		}
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getIdSignature(){
		$sql	= "	Select case when max(signature_id) is not null then (max(signature_id) + 1) else '1' end as id from m_signature ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getIdDokumentasi(){
		$sql	= "	Select case when max(dokumentasi_lapangan_id) is not null then (max(dokumentasi_lapangan_id) + 1) else '1' end as id from dokumentasi_lapangan ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getIdRetribusi(){
		$sql	= "	Select case when max(retribusi_id) is not null then (max(retribusi_id) + 1) else '1' end as id from m_retribusi ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getId(){
		$sql	= "	Select case when max(permohonan_id) is not null then (max(permohonan_id) + 1) else '1' end as id from m_permohonan ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function add($table,$data){	
		if($this->db->insert($table, $data)){
			if($table == 'm_permohonan'){
				$insert_id = $this->db->insert_id();
				$sql			= "Select * from m_permohonan Where permohonan_id = ".$insert_id."";
				$query			= $this->db->query($sql);
				$data			= $query->result_array();
				
				if($data){
					$result['temp']	= $data[0];
					$this->session->set_userdata($result);
				}
			}
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_permohonan where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		$sql	= "	Delete from pemenuhan_syarat_pemohon where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>