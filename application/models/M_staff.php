<?php
class M_staff extends CI_Model{
	function getStaff($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= ' Where ';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 1){$operation = ' And ';}
				$filter_array	.= $operation.$row_left." = '".$row_right."' ";
			$i++;
			}
		}
		
		$sql	= "	Select 
						a.*,
						b.level_name,
						c.jabatan_name
					From m_admin a
					Inner Join m_level b on b.level_id = a.level_id and b.activation = 'Y'
					Inner Join m_jabatan c on c.jabatan_id = a.jabatan_id and c.activation = 'Y'
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getData($find = null,$id = null){
		if($find == 'kabupaten'){$field = 'id_prov';}
		else if($find == 'kecamatan'){$field = 'id_kabupaten';}
		else if($find == 'kelurahan'){$field = 'id_kecamatan';}
		$sql			= "
							Select * From ".$find." where ".$field." = ".$id."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null,$field = null,$value = null,$order_by = null,$sort = null){
		$filter	= '';
		if(($field) && ($value)){
			$filter	= " Where ".$field." = '".$value."' ";
		}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		if(($order_by) && ($sort)){
			$orderby	= " Order By ".$order_by." ".$sort;
			$sql		= $sql.$orderby;
		}
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function cekEmail($email = null){
		$sql	= "Select * from m_user_management where email = '".$email."'";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
		}
	}
	function getId(){
		$sql	= "	Select case when max(user_management_id) is not null then (max(user_management_id) + 1) else '1' end as id from m_user_management ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	
	function add($table,$data){	
		if($this->db->insert($table, $data)){
			if($table == 'm_admin'){
				$insert_id = $this->db->insert_id();
				$sql			= "Select * from m_admin Where admin_id = ".$insert_id."";
				$query			= $this->db->query($sql);
				$data			= $query->result_array();
				
				if($data){
					$result['temp']	= $data[0];
					$this->session->set_userdata($result);
				}
			}
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_admin where admin_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>