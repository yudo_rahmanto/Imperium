<?php
class M_penolakan extends CI_Model{
	function getPenolakan($filter = null){
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		$sql	= "	SELECT 
						a.*,
						(select path_location_qr_code from m_tanda_terima where tanda_terima_id = a.tanda_terima_id and activation = 'Y') as path_location_qr_code,
						b.nama_perusahaan,
						b.alamat as alamat_perusahaan,
						(select ac.nama from kecamatan aa inner join kabupaten ab on ab.id = aa.id_kabupaten inner join provinsi ac on ac.id = ab.id_prov where aa.id = b.kecamatan_id) as provinsi_perusahaan,
						(select ab.nama from kecamatan aa inner join kabupaten ab on ab.id = aa.id_kabupaten where aa.id = b.kecamatan_id) as kota_perusahaan,
						(select nama from kecamatan where id = b.kecamatan_id) as kecamatan_perusahaan,
						(select nama from kelurahan where id = b.kelurahan_id) as kelurahan_perusahaan,
						(Select initial from m_perusahaan_type where perusahaan_type_id = b.perusahaan_type_id and activation = 'Y') as jenis_perusahaan,
						c.ktp,
						c.npwp,
						c.first_name,
						c.last_name,
						c.email,
						c.no_tlp as no_tlp_user,
						c.address as alamat,
						(select nama from kelurahan where id = c.kelurahan_id) as kel_user,
						(select nama from kecamatan where id = c.kecamatan_id) as kec_user,
						(select nama from kabupaten where id = c.kabupaten_id) as kab_user,
						(select nama from provinsi where id = c.provinsi_id) as prov_user,
						d.izin_name,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join (Select aa.nama_perusahaan,aa.perusahaan_type_id,ab.* From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id and b.activation = 'Y'
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					Inner Join m_alur_izin f on f.izin_id = d.izin_id and f.izin_type_id = e.izin_type_id and f.no_urut = a.status_level
					WHERE a.activation = 'Y' and a.permohonan_id not in (Select permohonan_id from m_signature)
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql			= $sql." Order By a.created_date Desc";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPenolakanAfterTtd($filter = null){
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		$sql	= "	SELECT 
						a.*,
						(SELECT path_location_qr_code FROM m_tanda_terima WHERE tanda_terima_id = a.tanda_terima_id AND activation = 'Y') AS path_location_qr_code,
						(SELECT initial FROM m_perusahaan_type WHERE perusahaan_type_id = b.perusahaan_type_id AND activation = 'Y') AS initial,
						b.nama_perusahaan,
						b.alamat AS alamat_perusahaan,
						(SELECT ac.nama FROM kecamatan aa INNER JOIN kabupaten ab ON ab.id = aa.id_kabupaten INNER JOIN provinsi ac ON ac.id = ab.id_prov WHERE aa.id = b.kecamatan_id) AS provinsi_perusahaan,
						(SELECT ab.nama FROM kecamatan aa INNER JOIN kabupaten ab ON ab.id = aa.id_kabupaten WHERE aa.id = b.kecamatan_id) AS kabupaten_perusahaan,
						(SELECT nama FROM kecamatan WHERE id = b.kecamatan_id) AS kecamatan_perusahaan,
						(SELECT nama FROM kelurahan WHERE id = b.kelurahan_id) AS kelurahan_perusahaan,
						(SELECT initial FROM m_perusahaan_type WHERE perusahaan_type_id = b.perusahaan_type_id AND activation = 'Y') AS jenis_perusahaan,
						c.ktp,
						c.npwp,
						c.first_name,
						c.last_name,
						c.email,
						c.no_tlp AS no_tlp_user,
						c.address AS alamat_user,
						c.rt AS rt_user,
						c.rw AS rw_user,
						(SELECT nama FROM provinsi WHERE id = c.provinsi_id) AS provinsi_user,
						(SELECT nama FROM kabupaten WHERE id = c.kabupaten_id) AS kabupaten_user,
						(SELECT nama FROM kecamatan WHERE id = c.kecamatan_id) AS kecamatan_user,
						(SELECT nama FROM kelurahan WHERE id = c.kelurahan_id) AS kelurahan_user,
						d.izin_name,
						e.type_name AS izin_type_name,
						h.id AS no_rekomendasi,
						h.tgl_tolak AS tgl_rekomendasi,
						h.catatan_penolakan AS catatan_rekomendasi
					FROM m_permohonan a 
					INNER JOIN (SELECT aa.nama_perusahaan,aa.perusahaan_type_id,ab.* FROM m_perusahaan aa INNER JOIN m_alamat_perusahaan ab ON ab.perusahaan_id = aa.perusahaan_id AND ab.activation = 'Y') b ON b.alamat_perusahaan_id = a.alamat_perusahaan_id AND b.activation = 'Y'
					INNER JOIN m_user_management c ON c.user_management_id = b.user_management_id AND c.activation = 'Y'
					INNER JOIN m_izin d ON d.izin_id = a.izin_id AND d.activation = 'Y'
					INNER JOIN m_izin_type e ON e.izin_type_id = a.izin_type_id AND e.activation = 'Y'
					INNER JOIN m_alur_izin f ON f.izin_id = d.izin_id AND f.izin_type_id = e.izin_type_id AND f.no_urut = a.status_level
					INNER JOIN m_signature g ON g.permohonan_id = a.permohonan_id
					INNER JOIN (SELECT id,permohonan_id,jabatan_id_penerima,tgl_tolak,catatan_penolakan FROM history_permohonan WHERE kategori_penolakan = 1) h ON h.permohonan_id = a.permohonan_id
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql			= $sql." Order By a.created_date Desc";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function dataSignature($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= '';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 0){$operation = ' And ';}
				$filter_array	.= $operation.$row_left." = '".$row_right."' ";
			$i++;
			}
		}
		
		$sql	= "	Select 
						a.permohonan_id,
						c.nip,
						c.fullname_with_gelar,
						d.jabatan_name,
						b.path_location as signature
					From 
						m_permohonan a
					Inner Join m_signature b on b.permohonan_id = a.permohonan_id 
					Inner Join m_admin c on c.admin_id = b.created_user and c.activation = 'Y'
					Inner Join m_jabatan d on d.jabatan_id = c.jabatan_id and d.activation = 'Y'
					Where 
						a.activation = 'Y' and a.reject = 1
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function inboxPengaduan($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= '';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 0){$operation = ' And ';}
				$filter_array	.= $operation.$row_left." = '".$row_right."' ";
			$i++;
			}
		}
		
		$sql	= "	Select 
						a.*,
						b.jabatan_name
					From m_pengaduan a
					Inner Join m_jabatan b on b.jabatan_id = a.jabatan_id_penerima and b.activation = 'Y'
					Where 
						a.activation = 'Y' and a.tgl_diterima is null
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function processPengaduan($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= '';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 0){$operation = ' And ';}
				$filter_array	.= $operation.$row_left." = '".$row_right."' ";
			$i++;
			}
		}
		
		$sql	= "	Select 
						a.*,
						b.jabatan_name
					From m_pengaduan a
					Inner Join m_jabatan b on b.jabatan_id = a.jabatan_id_penerima and b.activation = 'Y'
					Where 
						a.activation = 'Y' and a.tgl_diterima is not null and a.tgl_selesai is null and a.status_selesai = 0
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function finishPengaduan($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= '';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 0){$operation = ' And ';}
				$filter_array	.= $operation.$row_left." = '".$row_right."' ";
			$i++;
			}
		}
		
		$sql	= "	Select 
						a.*,
						b.jabatan_name
					From m_pengaduan a
					Inner Join m_jabatan b on b.jabatan_id = a.jabatan_id_penerima and b.activation = 'Y'
					Where 
						a.activation = 'Y' and a.status_selesai = 1
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getData($find = null,$id = null){
		if($find == 'kabupaten'){$field = 'id_prov';}
		else if($find == 'kecamatan'){$field = 'id_kabupaten';}
		else if($find == 'kelurahan'){$field = 'id_kecamatan';}
		$sql			= "
							Select * From ".$find." where ".$field." = ".$id."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null,$field = null,$value = null){
		$filter	= '';
		if(($field) && ($value)){
			$filter	= " Where ".$field." = '".$value."' ";
		}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getId(){
		$sql	= "	Select case when max(template_surat_id) is not null then (max(template_surat_id) + 1) else '1' end as id from m_template_surat ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	
	function add($table,$data){	
		if($this->db->insert($table, $data)){
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_pengaduan where pengaduan_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>