<?php
class M_default_access extends CI_Model{
	function menu(){
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$sql	= "
					SELECT 
						a.*,
						(SELECT IF(COUNT(sub_menu_id) > 0,'Y','N') FROM m_sub_menu WHERE menu_id = a.menu_id AND sub_menu_id = b.sub_menu_id and activation = 'Y') AS parent_menu
					FROM m_menu a 
					INNER JOIN m_menu_access b ON b.menu_id = a.menu_id
					WHERE 
						a.activation = 'Y' AND 
						b.level_user = ".$LevelId_Session."
					GROUP BY a.menu_id
					ORDER BY a.no_urut ASC
					";
		$query				= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $query->num_rows();
		if($data){
			return $data;
		} else {
			return false;
		}			
	}
	function parents(){
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		
		$sql	= "SELECT 
						b.menu_id,b.menu_name,a.* 
					FROM m_sub_menu a 
					LEFT JOIN m_menu b ON b.menu_id = a.menu_id 
					INNER JOIN m_menu_access c ON c.menu_id = b.menu_id AND c.sub_menu_id = a.sub_menu_id
					WHERE 
						a.activation = 'Y' AND 
						c.level_user = ".$LevelId_Session."
					";
		$query				= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $query->num_rows();
		if($data){
			return $data;
		} else {
			return false;
		}			
	}
	function date1($date){
		if(!empty($date)){
		$date		= explode('/',$date);
		$tgl		= $date[0];
		$bln		= $date[1];
		$thn		= $date[2];
		$date		= $thn.'-'.$bln.'-'.$tgl;
		} else {
		$date		= '';
		}
		if(!empty($date)){
			return $date;
			} else {
			return false;
			}
	}
	function date2($date){
		if(!empty($date)){
		$date		= explode('-',$date);
		$tgl		= $date[2];
		$bln		= $date[1];
		$thn		= $date[0];
		$date		= $tgl.'/'.$bln.'/'.$thn;
		} else {
		$date		= '';
		}
		if(!empty($date)){
			return $date;
			} else {
			return false;
			}
	}
	
}
?>