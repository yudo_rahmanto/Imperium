<?php
class M_surat extends CI_Model{
	function getSuratSK($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						b.nama_perusahaan,
						c.ktp,
						c.npwp,
						c.first_name,
						c.last_name,
						c.email,
						c.no_tlp as no_tlp_user,
						c.address as alamat,
						d.izin_name,
						e.type_name as izin_type_name,
						if(a.status_finish = '1','Finish','On Progress') as status_permohonan
					FROM m_permohonan a 
					Inner Join (Select aa.perusahaan_id,aa.user_management_id,aa.nama_perusahaan,ab.alamat_perusahaan_id From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y' Where aa.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					WHERE a.activation = 'Y' And a.status_sk = 0
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPermohonanDetail($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						(
							Select 
								level_name
							From
								m_level 
							Where
								activation = 'Y' and
								level_id = f.jabatan_id
						) as posisi_alur,
						b.nama_perusahaan,
						c.first_name,
						c.last_name,
						d.izin_category_id,
						d.izin_name,
						e.type_name as izin_type_name
					FROM m_permohonan a 
					Inner Join (Select aa.perusahaan_id,aa.user_management_id,aa.nama_perusahaan,ab.alamat_perusahaan_id From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y' Where aa.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id
					Inner Join m_user_management c on c.user_management_id = b.user_management_id and c.activation = 'Y'
					Inner Join m_izin d on d.izin_id = a.izin_id and d.activation = 'Y'
					Inner Join m_izin_type e on e.izin_type_id = a.izin_type_id and e.activation = 'Y'
					Inner Join m_alur_izin f on f.izin_id = d.izin_id and f.izin_type_id = e.izin_type_id and f.no_urut = a.status_level
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By a.permohonan_id Desc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						b.name as syarat_name
					FROM m_syarat a 
					Inner Join m_persyaratan b on b.persyaratan_id = a.persyaratan_id and b.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql			= $sql." Order By a.syarat_id Asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getPemenuhanSyaratPermohonan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.nama_perusahaan,
						d.first_name,
						d.last_name,
						e.izin_name,
						f.type_name as izin_type_name,
						h.name as syarat_name
					FROM m_permohonan a 
					Inner Join pemenuhan_syarat_pemohon b on b.permohonan_id = a.permohonan_id and b.activation = 'Y'
					Inner Join (Select aa.perusahaan_id,aa.user_management_id,aa.nama_perusahaan,ab.alamat_perusahaan_id From m_perusahaan aa Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y' Where aa.activation = 'Y') c on c.alamat_perusahaan_id = a.alamat_perusahaan_id
					Inner Join m_user_management d on d.user_management_id = b.user_management_id and d.activation = 'Y'
					Inner Join m_izin e on e.izin_id = b.izin_id and e.activation = 'Y'
					Inner Join m_izin_type f on f.izin_type_id = b.izin_type_id and f.activation = 'Y'
					Left Join m_syarat g on g.syarat_id = b.syarat_id and g.activation = 'Y'
					Left Join m_persyaratan h on h.persyaratan_id = g.persyaratan_id and h.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.syarat_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataPerusahaan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.id,
						c.nama as kecamatan_name,
						d.id,
						d.nama as kelurahan_name,
						e.name as jenis_usaha,
						f.name as lokasi_perusahaan,
						g.name as lingkungan_perusahaan,
						h.name as index_gangguan,
						i.name as alat_kerja,
						j.perusahaan_type as jenis_perusahaan
					FROM m_permohonan a 
					Inner Join (
								Select 
									aa.perusahaan_id,
									aa.user_management_id,
									aa.nama_perusahaan,
									ab.alamat_perusahaan_id,
									ab.kecamatan_id,
									ab.kelurahan_id,
									ab.jenis_usaha_id,
									ab.lokasi_perusahaan_id,
									ab.lingkungan_perusahaan_id,
									ab.index_gangguan_id,
									ab.alat_kerja_id,
									ab.kegiatan_usaha,
									ab.modal_usaha,
									ab.kelembagaan,
									ab.product,
									ab.alamat,
									ab.luas,
									ab.rt,
									ab.rw,
									aa.perusahaan_type_id
								From m_perusahaan aa 
								Inner Join m_alamat_perusahaan ab on ab.perusahaan_id = aa.perusahaan_id and ab.activation = 'Y' 
								Where aa.activation = 'Y') b on b.alamat_perusahaan_id = a.alamat_perusahaan_id
					Left Join kecamatan c on c.id = b.kecamatan_id
					Left Join kelurahan d on d.id = b.kelurahan_id
					Left Join m_jenis_usaha e on e.jenis_usaha_id = b.jenis_usaha_id
					Left Join m_lokasi_perusahaan f on f.lokasi_perusahaan_id = b.lokasi_perusahaan_id
					Left Join m_lingkungan_perusahaan g on g.lingkungan_perusahaan_id = b.lingkungan_perusahaan_id
					Left Join m_index_gangguan h on h.index_gangguan_id = b.index_gangguan_id
					Left Join m_alat_kerja i on i.alat_kerja_id = b.alat_kerja_id
					Left Join m_perusahaan_type j on j.perusahaan_type_id = b.perusahaan_type_id and j.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.perusahaan_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataUser($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						b.*,
						c.level_name,
						d.nama as provinsi_name,
						e.nama as kabupaten_name,
						f.nama as kecamatan_name,
						g.nama as kelurahan_name
					FROM m_permohonan a 
					Inner Join m_user_management b on b.user_management_id = a.user_management_id and b.activation = 'Y'
					Inner Join m_level c on c.level_id = b.level_id and c.activation = 'Y'
					Left Join provinsi d on d.id = b.provinsi_id
					Left Join kabupaten e on e.id = b.kabupaten_id
					Left Join kecamatan f on f.id = b.kecamatan_id
					Left Join kelurahan g on g.id = b.kelurahan_id
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.user_management_id Asc";
		$query		= $this->db->query($sql.$limit);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataNoSK(){
		$sql	= "	SELECT 
						a.*
					FROM m_sk a 
					WHERE a.status = '0'
					Order By a.sk_id asc
					";
		$sql		= $sql;
		$query		= $this->db->query($sql);
		$data		= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null,$field = null,$value = null,$order_by = null,$sort = null){
		$filter	= '';
		if(($field) && ($value)){
			$filter	= " Where ".$field." = '".$value."' ";
		}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		if(($order_by) && ($sort)){
			$orderby	= " Order By ".$order_by." ".$sort;
			$sql		= $sql.$orderby;
		}
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getIdTandaTerima(){
		$sql	= "	Select case when max(tanda_terima_id) is not null then (max(tanda_terima_id) + 1) else '1' end as id from m_tanda_terima ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getIdSK($id = null){
		$sql	= "	Select count(sk_id) + 1 as id from m_sk where izin_category_id = ".$id;
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function add($table,$data){	
		if($this->db->insert($table, $data)){
			if($table == 'm_permohonan'){
				$insert_id = $this->db->insert_id();
				$sql			= "Select * from m_permohonan Where permohonan_id = ".$insert_id."";
				$query			= $this->db->query($sql);
				$data			= $query->result_array();
				
				if($data){
					$result['temp']	= $data[0];
					$this->session->set_userdata($result);
				}
			}
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_permohonan where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		$sql	= "	Delete from pemenuhan_syarat_pemohon where permohonan_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>