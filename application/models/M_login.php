<?php
class M_login extends CI_Model{
	function getStaff($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= ' Where ';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 1){$operation = ' And ';}
				if($row_left == 'a.username'){
					$filter_array	.= $operation." ( ".$row_left." = '".$row_right."' or a.nip = '".$row_right."' or a.email = '".$row_right."' ) ";
				} else {
					$filter_array	.= $operation.$row_left." = '".$row_right."' ";	
				}
			$i++;
			}
		}
		
		$sql	= "	Select 
						'Staff' as login_as,
						a.admin_id as user_id,
						a.jabatan_id,
						a.level_id,
						a.nip,
						a.avatar,
						a.first_name,
						a.last_name,
						a.email,
						a.created_date,
						b.level_name,
						c.jabatan_name,
						d.izin_id
					From m_admin a	
					Inner Join m_level b on b.level_id = a.level_id and b.activation = 'Y'
					Left Join m_jabatan c on c.jabatan_id = a.jabatan_id and b.activation = 'Y'
					Left Join m_izin_access d on d.level_id = b.level_id and d.activation = 'Y'
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getUser($filter = null){
		
		if($filter){
			$i 				= 1;
			$filter_array	= ' Where ';
			$operation		= '';
			foreach($filter as $row_left=>$row_right){
				if($i > 1){$operation = ' And ';}
				if($row_left == 'a.username'){
					$filter_array	.= $operation." ( ".$row_left." = '".$row_right."' or a.ktp = '".$row_right."' or a.email = '".$row_right."' ) ";
				} else {
					$filter_array	.= $operation.$row_left." = '".$row_right."' ";	
				}
			$i++;
			}
		}
		
		$sql	= "	Select 
						'User' as login_as,
						a.user_management_id as user_id,
						'7' as jabatan_id,
						a.level_id,
						a.ktp,
						a.npwp,
						a.avatar,
						a.first_name,
						a.last_name,
						a.email,
						a.created_date,
						b.level_name
					From m_user_management a	
					Inner Join m_level b on b.level_id = a.level_id and b.activation = 'Y'
					";
					
		if($filter_array){ $sql	= $sql.$filter_array;}
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataUser($token = null){
		$sql	= "
					SELECT
						a.token,
						IF(a.jabatan_id != 7,(SELECT jabatan_id FROM m_admin WHERE admin_id = a.user_id),'7') AS jabatan_id,
						IF(a.jabatan_id != 7,(SELECT admin_id FROM m_admin WHERE admin_id = a.user_id),(SELECT user_management_id FROM m_user_management WHERE user_management_id = a.user_id)) AS user_id,
						IF(a.jabatan_id != 7,(SELECT first_name FROM m_admin WHERE admin_id = a.user_id),(SELECT first_name FROM m_user_management WHERE user_management_id = a.user_id)) AS first_name,
						IF(a.jabatan_id != 7,(SELECT last_name FROM m_admin WHERE admin_id = a.user_id),(SELECT last_name FROM m_user_management WHERE user_management_id = a.user_id)) AS last_name,
						IF(a.jabatan_id != 7,(SELECT email FROM m_admin WHERE admin_id = a.user_id),(SELECT email FROM m_user_management WHERE user_management_id = a.user_id)) AS email
					FROM
						log_reset_password a
					WHERE 
						a.token = '".$token."'";
						
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
		}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($right){
					if($i > 1){$operation = ' and ';}
					$filter		.= $operation.$left." = '".$right."'";
					$i++;
				}
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function cekEmail($email = null){
		$sql	= "Select * from m_user_management where email = '".$email."'";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
		}
	}
	function add($table,$data){	
				if($this->db->insert($table, $data)){
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($table= null,$data= null,$real= null){
		if(!empty($real)){
			foreach($real['field'] as $left1=>$right1){
					foreach($real['id'] as $left2=>$right2){
						if($left1 == $left2){ 
							$this->wms->where($right1,$right2);
						}
					}
				}
			}
		$this->db->update($table,$data);
	}
}
?>