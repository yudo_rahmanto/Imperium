<?php
class M_syarat extends CI_Model{
	function getSyarat($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
						a.*,
						c.izin_name,
						d.type_name as izin_type_name,
						b.name as persyaratan_name
					FROM m_syarat a 
					Inner Join m_persyaratan b on b.persyaratan_id = a.persyaratan_id and b.activation = 'Y'
					Inner Join m_izin c on c.izin_id = a.izin_id and c.activation = 'Y'
					Left Join m_izin_type d on d.izin_type_id = a.izin_type_id and d.activation = 'Y'
					WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By c.izin_name,b.name Asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null){
		$sql			= "
							Select * From ".$table." 
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	
	function getIdSyarat(){
		$sql	= "	Select case when max(syarat_id) is not null then (max(syarat_id) + 1) else '1' end as id from m_syarat ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function add($table,$data){	
				if($this->db->insert($table, $data)){
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$sql	= "	Delete from m_syarat where syarat_id = ".$code." ";
		$query	= $this->db->query($sql);
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>