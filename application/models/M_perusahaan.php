<?php
class M_perusahaan extends CI_Model{
	function getPerusahaan($filter = null,$limit = null){
		
		$filter_array	= '';
		if(!empty($filter)){
			$filter_array	= '';
			foreach($filter as $row_left => $row_right){
				if($row_right){
					if(is_numeric($row_right)){
					$filter_array	.= " And ".$row_left." = ".$row_right."";
					} else {
					$filter_array	.= " And ".$row_left." like '%".$row_right."%'";
					}
				}
			}
		}
		
		if(!empty($limit)){
			$awal = (($limit[0]-1)*$limit[1]);
			if($limit[0] > 1){
			$awal = (($limit[0]-1)*$limit[1]+1);
			}
			$akhir = $limit[1];
			$limit	= ' Limit '.$awal.','.$akhir;
			}
		$sql	= "	SELECT 
							a.*,
							b.perusahaan_type_id,
							b.nama_perusahaan,
							prov.id AS provinsi_id,
							prov.nama AS provinsi_name,
							kab.id AS kabupaten_id,
							kab.nama AS kabupaten_name,
							c.id AS kecamatan_id,
							c.nama AS kecamatan_name,
							d.id AS kelurahan_id,
							d.nama AS kelurahan_name,
							e.name AS jenis_usaha,
							f.name AS lokasi_perusahaan,
							g.name AS lingkungan_perusahaan,
							h.name AS index_gangguan,
							i.name AS alat_kerja,
							j.perusahaan_type AS jenis_perusahaan
						FROM m_alamat_perusahaan a
						LEFT JOIN m_perusahaan b ON b.perusahaan_id = a.perusahaan_id AND b.activation = 'Y'
						LEFT JOIN provinsi prov ON prov.id = a.provinsi_id
						LEFT JOIN kabupaten kab ON kab.id = a.kabupaten_id
						LEFT JOIN kecamatan c ON c.id = a.kecamatan_id
						LEFT JOIN kelurahan d ON d.id = a.kelurahan_id
						LEFT JOIN m_jenis_usaha e ON e.jenis_usaha_id = a.jenis_usaha_id
						LEFT JOIN m_lokasi_perusahaan f ON f.lokasi_perusahaan_id = a.lokasi_perusahaan_id
						LEFT JOIN m_lingkungan_perusahaan g ON g.lingkungan_perusahaan_id = a.lingkungan_perusahaan_id
						LEFT JOIN m_index_gangguan h ON h.index_gangguan_id = a.index_gangguan_id
						LEFT JOIN m_alat_kerja i ON i.alat_kerja_id = a.alat_kerja_id
						LEFT JOIN m_perusahaan_type j ON j.perusahaan_type_id = b.perusahaan_type_id AND j.activation = 'Y'
						WHERE a.activation = 'Y'
					";
		if($filter_array){ $sql	= $sql.$filter_array;}
		$sql		= $sql." Order By b.nama_perusahaan Asc";
		$query			= $this->db->query($sql.$limit);
		$queryNoLimit	= $this->db->query($sql);
		$data['data']		= $query->result_array();
		$data['jml_row']	= $queryNoLimit->num_rows();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function checkData($table = null,$filter_array = null){
		$filter			= '';
		if($filter_array){
			$i			= 1;
			$operation	= '';
			foreach($filter_array as $left=>$right){
				if($i > 1){$operation = ' and ';}
				$filter		.= $operation.$left." = '".$right."'";
				$i++;
			}
		}
		if($filter){$filter	= " Where ".$filter;}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getDataCombo($table = null,$field = null,$value = null,$order_by = null,$sort = null){
		$filter	= '';
		if(($field) && ($value)){
			$filter	= " Where ".$field." = '".$value."' ";
		}
		$sql			= "
							Select * From ".$table." ".$filter."
						";
		if(($order_by) && ($sort)){
			$orderby	= " Order By ".$order_by." ".$sort;
			$sql		= $sql.$orderby;
		}
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getData($find = null,$id = null){
		if($find == 'provinsi'){$field = 'id';}
		else if($find == 'kabupaten'){$field = 'id_prov';}
		else if($find == 'kecamatan'){$field = 'id_kabupaten';}
		else if($find == 'kelurahan'){$field = 'id_kecamatan';}
		$sql			= "
							Select * From ".$find." where ".$field." = ".$id." Order By nama Asc
						";
		$query			= $this->db->query($sql);
		$data			= $query->result_array();
		if($data){
			return $data;
		} else {
			return false;
			}
	}
	function getIdJenisUsaha(){
		$sql	= "	Select case when max(jenis_usaha_id) is not null then (max(jenis_usaha_id) + 1) else '1' end as id from m_jenis_usaha ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function getIdPerusahaan(){
		$sql	= "	Select case when max(perusahaan_id) is not null then (max(perusahaan_id) + 1) else '1' end as id from m_perusahaan ";
		$query	= $this->db->query($sql);
		$data	= $query->result_array();
		if($data){
			return $data[0]['id'];
			} else {
			return false;
			}
	}
	function add($table,$data){	
				if($this->db->insert($table, $data)){
			return true;
		}else {			
			return false;	
		}
		
				
	}
	
	function edit($table = null,$data= null,$field= null,$id= null){	
		
		$this->db->where($field,$id);
		if($this->db->update($table,$data)){
			return true;
		}else {			
			return false;	
		}
	}
	
	function delete($code = null){
		$id_perusahaan	= "";
		$dataAlamat		= singleFilter('m_alamat_perusahaan','alamat_perusahaan_id',$code);
		if($dataAlamat){
			$id_perusahaan	= $dataAlamat[0]['perusahaan_id'];
		}
		$sql	= "	Delete from m_alamat_perusahaan where alamat_perusahaan_id = ".$code." ";
		$query	= $this->db->query($sql);
		$findAlamat		= singleFilter('m_alamat_perusahaan','perusahaan_id',$id_perusahaan);
		if(!$findAlamat){
			$sql	= "	Delete from m_perusahaan where perusahaan_id = ".$id_perusahaan." ";
			$query	= $this->db->query($sql);
		}
		if($query){
			return true;
			} else {
			return false;
			}
	}
}
?>