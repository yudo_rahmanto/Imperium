<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_perusahaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_jenis_perusahaan');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Jenis_perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Jenis Perusahaan'
										);
		$search							= array(
											"activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Jenis Perusahaan';
		$data['title2']			= 'Search Data Jenis Perusahaan';
		$data['data_search']	= $search;	
		$data['getData']		= ManyFilter('m_perusahaan_type',$search);
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Jenis_perusahaan/autocomplete');
		$data['print'] 			= site_url('Jenis_perusahaan/cetak/print');
		$data['excell'] 		= site_url('Jenis_perusahaan/cetak/excell');
		$data['add'] 			= site_url('Jenis_perusahaan/add/');
		$data['edit'] 			= site_url('Jenis_perusahaan/edit');
		$data['detail'] 		= site_url('Jenis_perusahaan/detail');
		$data['page_action']	= site_url('Jenis_perusahaan/');
		$data['back_action']	= site_url('Jenis_perusahaan/');
		$data['delete']			= site_url('Jenis_perusahaan/delete/');
		$this->load->view('master/jenis_perusahaan/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Jenis_perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Jenis Perusahaan';
		$data['title2']			= 'Create New Jenis Perusahaan';
		$data['action']			= site_url('Jenis_perusahaan/saveForm/add');
		$data['back']			= site_url('Jenis_perusahaan/');
		$this->load->view('master/jenis_perusahaan/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Jenis_perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'perusahaan_type_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Jenis Perusahaan';
		$data['title2']			= 'Edit Jenis Perusahaan';
		$data['getData']		= ManyFilter('m_perusahaan_type',$search);
		//debug($data['getData']);exit;
		$data['action']			= site_url('Jenis_perusahaan/saveForm/edit');
		$data['back']			= site_url('Jenis_perusahaan/');
		
		$this->load->view('master/jenis_perusahaan/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Jenis_perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'perusahaan_type_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Jenis Perusahaan';
		$data['title2']			= 'Data Detail Jenis Perusahaan';
		$data['getData']		= ManyFilter('m_perusahaan_type',$search);
		//debug($data['getData']);exit;
		$data['back']			= site_url('Jenis_perusahaan/');
		$this->load->view('master/jenis_perusahaan/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Jenis_perusahaan/');
		$filter					= array(
										"perusahaan_type" => $this->input->post('jenis_perusahaan')
										);
		$dataPost				= $this->input->post();
		$checkData				= ManyFilter('m_perusahaan_type',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_perusahaan_type','perusahaan_type_id');
			$data	 				= array(
										'perusahaan_type_id'	=> $id,
										'initial' 				=> $this->input->post('initial'),
										'perusahaan_type' 		=> $this->input->post('jenis_perusahaan'),
										'activation' 			=> 'Y'
										);
			$save					= Add('m_perusahaan_type',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Jenis_perusahaan/');
		$id						= $this->input->post('perusahaan_type_id');
		$data	 				= array(
										'initial' 				=> $this->input->post('initial'),
										'perusahaan_type' 		=> $this->input->post('jenis_perusahaan'),
										);
		$update					= Edit('m_perusahaan_type',$data,'perusahaan_type_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_jenis_perusahaan->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
