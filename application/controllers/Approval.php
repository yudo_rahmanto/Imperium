<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_approval');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$JabatanId_Session					= $dataSession['jabatan_id'];
		$LevelId_Session					= $dataSession['level_id'];
		// debug($dataSession);exit;
		$data['path_info']				= 'Approval';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Tgl Pendaftaran',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search					= array(
											"a.activation"		=> 'Y',
											"a.status"			=> 'On Progress',
											"e.jabatan_id"		=> $JabatanId_Session
										);
		$searchOnProgress			= array(
											"a.activation"				=> 'Y',
											"a.jabatan_id_penerima"		=> $JabatanId_Session
										);
		$searchFinish			= array(
											"a.activation"				=> 'Y',
											"a.jabatan_id_penerima"		=> $JabatanId_Session
										);
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Approval';
		$data['title2']			= 'Search Data Approval';
		$data['data_search']	= $search;	
		$data['getDataInbox']		= $this->M_approval->getInbox($search,'');
		// $data['getDataOnProgress']	= $this->M_approval->getOnProgress($searchOnProgress,'');
		$data['getDataFinish']		= $this->M_approval->getFinish($searchFinish,'');
		// debug($data);exit;
		$data['autocomplete'] 	= site_url('Approval/autocomplete');
		$data['print'] 			= site_url('Approval/cetak/print');
		$data['excell'] 		= site_url('Approval/cetak/excell');
		$data['add'] 			= site_url('Approval/add/');
		$data['edit'] 			= site_url('Approval/edit');
		$data['detail'] 		= site_url('Approval/detail');
		$data['cetak_resi'] 	= site_url('Approval/cetak_resi');
		$data['terim_berkas'] 	= site_url('Approval/saveForm/terimaberkas');
		$data['page_action']	= site_url('Approval/');
		$data['back_action']	= site_url('Approval/');
		$data['delete']			= site_url('Approval/delete');
		$this->load->view('approval/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$this->load->model('M_archive');
		$this->load->model('M_alur_izin');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Approval';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Approval';
		$data['title2']			= 'Edit Approval';
		$data['getDataPermohonan']		= $this->M_approval->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_approval->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_approval->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_approval->getPemenuhanSyaratPermohonan($search);
		$data['getDataHistory']			= $this->M_archive->getDataHistoryPermohonan($search);
		$kabupaten_id					= 1671;
		$kecamatan_id					= '';
		if($data['getDataPerusahaan']){
			$kabupaten_id	= $data['getDataPerusahaan'][0]['kabupaten_id'];
			$kecamatan_id	= $data['getDataPerusahaan'][0]['kecamatan_id'];
		}
		$data['getJabatan']		= SingleFilter('m_jabatan','','','jabatan_name','asc');
		$data['getJenisUsaha']	= SingleFilter('m_jenis_usaha','','','name','asc');
		$data['getLingkungan']	= SingleFilter('m_lingkungan_perusahaan','','','name','asc');
		$data['getLokasi']		= SingleFilter('m_lokasi_perusahaan','','','name','asc');
		$data['getIndexGangguan']	= SingleFilter('m_index_gangguan','','','name','asc');
		$data['getAlatKerja']	= SingleFilter('m_alat_kerja','','','name','asc');
		$data['getKecamatan']	= SingleFilter('kecamatan','id_kabupaten',$kabupaten_id,'nama','asc');
		$data['getKelurahan']	= SingleFilter('kelurahan','id_kecamatan',$kecamatan_id,'nama','asc');
		$data['getAdmin']		= SingleFilter('m_admin','','','first_name','asc');
		$searchHistory			= array(
											"a.permohonan_id"			=> $id
										);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($searchHistory);
		$getIdHistor			= '';
		$tgl_diterima			= '';
		$getIdHistory			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
			$tgl_diterima		= $getHistoryPermohonan[0]['tgl_diterima'];
		}
		if(!$tgl_diterima){
		$dataHistory	 		= array(
									'tgl_diterima'			=> date('Y-m-d H:i:s'),
									);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		}
		$data['getRetribusi']	= $this->M_approval->getDataRetribusi($search);
		$data['getAlurIzin']	= '';
		if($data['getDataPermohonan']){
			$izin_id			= $data['getDataPermohonan'][0]['izin_id'];
			$izin_type_id		= $data['getDataPermohonan'][0]['izin_type_id'];
			$status_level		= $data['getDataPermohonan'][0]['status_level'];
		$param					= array(
										'a.izin_id'			=> $izin_id,
										'a.izin_type_id'	=> $izin_type_id
										);
		$data['getAlurIzin']	= $this->M_alur_izin->getAlurIzin($param);
		$data['getAlurIzin']	= $data['getAlurIzin']['data'];
			if($data['getAlurIzin']){
				$alur_izin		= '';
				$i = 0;
				$ii = 1;
				$array_field	= array();
				foreach($data['getAlurIzin'] as $row){
					foreach($data['getAlurIzin'][$i] as $row_left=>$row_right){
						$alur_izin[$ii][$row_left]	= $row_right;
					}
				$i++;
				$ii++;
				}
			}
		}
		$data['dataAlurIzin']	= $alur_izin;
		// echo '<pre>';debug($alur_izin);exit;
		$data['getDataSelect']	= site_url('Approval/saveForm/getDataSelect');
		$data['getData']		= site_url('Perusahaan/saveForm/getData');
		$data['update']			= site_url('Approval/saveForm/edit');
		$data['batal']			= site_url('Approval/saveForm/batal');
		$data['tolak']			= site_url('Approval/saveForm/tolak');
		$data['setuju']			= site_url('Approval/saveForm/setuju');
		$data['uploadData']		= site_url('Approval/saveForm/uploadData');
		$data['cetak_surat_kerja']		= site_url('Approval/cetak_surat_kerja');
		$data['compare_log_kegiatan']	=	site_url('Approval/saveForm/compare_log_kegiatan');
		$data['back']			= site_url('Approval/');
		$data['sendEmail']		= site_url('Approval/pdf/');
		
		$this->load->view('approval/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Approval';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Approval';
		$data['title2']			= 'Data Detail Approval';
		$data['default']		= $this->M_approval->getPermohonanDetail($search);
		$data['getData']		= $data['default']['data'];
		$data['dataSyarat']		= $this->M_approval->getPemenuhanSyaratPermohonan($search);
		$data['getDataSyarat']	= $data['dataSyarat']['data'];
		$data['back']			= site_url('Approval/');
		$this->load->view('approval/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function signature(){
		$permohonan_id		= $this->uri->segment(3);
		$izin_category_id	= $this->uri->segment(4);
		$data['action']				= site_url('Approval/saveForm/saveSignature');
		$data['back']				= site_url('Approval/');
		$data['permohonan_id']		= $permohonan_id;
		$data['izin_category_id']	= $izin_category_id;
		$this->load->view('approval/signature',$data);
	}
	public function cetak_surat_kerja()
	{
		##Default##
		
		$this->load->library('pdf');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Approval';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		##Default##
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title']			= 'Cetak Surat Tugas';
		$data['getPetugas']		= SingleFilter('m_admin','admin_id',$UserId_Session);
		$data['getData']		= $this->M_approval->getDataSuratTugas($search);
		
		$this->pdf->load_view('template/cetak_surat_kerja',$data);
		$this->pdf->render();
		$this->pdf->stream($data['title'].".pdf");
		
		// $this->load->view('template/cetak_sk',$data);
		
		
	}
	public function pdf($id = null)
	{
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$data['path_info']				= 'Approval';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		
		
		##Default##
		
		##Costumize##
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_approval->getApproval($search);
		$data['getData']		= $data['default']['data'];
		$data['getDataRetribusi']	= $this->M_approval->getDataRetribusi($search);
		$data['getDataSignature']	= SingleFilter('m_admin','jabatan_id',5);
		$data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		$data['back']			= site_url('Permohonan/');
		
		$this->load->view('template/cetak_spm',$data);
		$this->email($id);
	}
	function email($id = null){
		$search					= array(
										'a.permohonan_id'	=>	$id
										);
		$data['default']		= $this->M_approval->getApproval($search);
		$getData				= $data['default']['data'];
		$getDataRetribusi		= $this->M_approval->getDataRetribusi($search);
		if($getData){
			$user_management_id	= $getData[0]['user_management_id'];
			$email				= $getData[0]['email'];
			$permohonan_id		= $getData[0]['permohonan_id'];
			$izin_id			= $getData[0]['izin_id'];
			$izin_type_id		= $getData[0]['izin_type_id'];
			$nama_pemohon		= $getData[0]['first_name'].' '.$getData[0]['last_name'];
			$alamat_pemohon		= $getData[0]['alamat'];
			$nama_perusahaan	= $getData[0]['nama_perusahaan'];
			$qr_code			= $getData[0]['qr_code'];
		} else {
			$user_management_id	='';
			$email				= '';
			$permohonan_id	= '';
			$izin_id		= '';
			$izin_type_id	= '';
			$nama_pemohon	= '';
			$alamat_pemohon	= '';
			$nama_perusahaan	= '';
			$qr_code		= '';
		}
			$total 	= 0;
			$html	= '';
			if($getDataRetribusi){
					$i = 1;
					foreach($getDataRetribusi as $row){
						$total	= $total + $row['total_akhir'];
			$html	.='
						<tr>
							<td valign="top" width="10%" align="center">'.$i.'</td>
							<td align="center" width="20%"><p>'.$row['code'].'</p></td>
							<td align="left" width="30%"><p>'.$row['keterangan_izin'].'</p></td>
							<td align="center" width="20%"><p>1.16.1.16.01.4.1.2.03.03</p></td>
							<td align="right" width="20%"><p>Rp '.number_format($row['total_akhir']).'</p></td>
						</tr>
					';
					}
			}
		$subject	= "Surat Pemberitahuan Retribusi";
		$body		= '
								<div align="center"><img src="'.base_url('assets/images/logo/logo.png').'"></div>
								<table align="center" style="width:100%;text-align:center;border-bottom: 1px solid black;">
									<tr>
										<td style="height:30px;"><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td>BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
									<tr>
										<td>Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
									</tr>
									<tr>
										<td>Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td>PALEMBANG</td>
									</tr>
								</table>
								<p>&nbsp;</p>
								<table align="center" style="width:100%;text-align:center;">
									<tr>
										<td width="10%">&nbsp;</td>
										<td width="5%">&nbsp;</td>
										<td width="39%">&nbsp;</td>
										<td width="15%">&nbsp;</td>
										<td width="30%"><div align="left">Palembang, '.date('d F Y').'</div></td>
									</tr>
									<tr>
										<td width="10%"><div align="left">Nomor</div></td>
										<td width="5%"><div align="center">:</div></td>
										<td width="39%"><div align="left">'.$permohonan_id.'/IG-SP/KPPT/'.date('Y').'</div></td>
										<td width="15%">&nbsp;</td>
										<td width="30%"><div align="left">Kepada :</div></td>
									</tr>
									<tr>
										<td width="10%"><div align="left">Perihal</div></td>
										<td width="5%"><div align="center">:</div></td>
										<td width="39%"><div align="left">Pemberitahuan Retribusi Perijinan</div></td>
										<td width="15%">&nbsp;</td>
										<td width="30%"><div align="left">Yth. '.$nama_pemohon.'</div></td>
									</tr>
									<tr>
										<td width="10%"><div align="left">Lampiran</div></td>
										<td width="5%"><div align="center">:</div></td>
										<td width="39%"><div align="left">Slip Setoran Pembayaran Retribusi</div></td>
										<td width="15%">&nbsp;</td>
										<td width="30%"><div align="left">Nama Perusahaan</div></td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td>
										<td width="5%">&nbsp;</td>
										<td width="39%">&nbsp;</td>
										<td width="15%">&nbsp;</td>
										<td width="30%"><div align="left">di alamat perusahaan</div></td>
									</tr>
								</table>
								<p>&nbsp;</p>
								<p>Menunjuk surat permohonan ijin saudara tanggal '.date('d F Y').', bersama ini kami beritahukan bahwa retribusi yang harus saudara bayar sebesar :</p>
								<p>&nbsp;</p>
								<table align="left" border="1" style="width:100%;text-align:left;font-size: 80%;">
									<tr>
										<td align="center" width="10%"><b>No</b></td>
										<td align="center" width="20%"><b>Nomor Pendaftaran</b></td>
										<td align="center" width="30%"><b>Jenis Permohonan Ijin</b></td>
										<td align="center" width="20%"><b>Kode Rekening</b></td>
										<td align="center" width="20%"><b>Jumlah Retribusi</b></td>
									</tr>
									'.$html.'
									<tr>
										<td valign="top" width="10%">&nbsp;</td>
										<td width="20%">&nbsp;</td>
										<td align="left" width="30%"><p>Denda</p></td>
										<td align="center" width="20%"><p>1.20.1.20.03.4.1.4.14.03</p></td>
										<td align="right" width="20%"><p>Rp 0</p></td>
									</tr>
									<tr>
										<td colspan="4">JUMLAH TOTAL</td>
										<td align="right">Rp '.number_format($total).'</td>
									</tr>
									<tr>
										<td colspan="5">TOTAL : '.ucwords(strtolower(terbilang($total))).'</td>
									</tr>
								</table>
								<p>&nbsp;</p>
								<table align="center" style="width:100%;font-size: 80%;">
									<tr>
										<td colspan="2"><div align="left"><u><b>Catatan :</b></u></div></td>
									</tr>
									<tr>
										<td width="5%"><div align="left">1</div></td>
										<td width="95%">
											<div align="left">
												Pembayaran dilakukan di loket pembayaran Kantor Kas Bank Sumselbabel Badan Penanaman Modal Dan Pelayanan Terpadu Satu Pintu Kota 
												 Palembang Jl. Merdeka No.1 Palembang pada jam kerja dengan nomor rekening 150.300.0001 a.n PEMKOT PALEMBANG, 
												 selambat-lambatnya 3 (tiga) bulan sejak diterbitkannya Surat Pemberitahuan Retribusi.
											</div>
										</td>
									</tr>
									<tr>
										<td width="5%"><div align="left">2</div></td>
										<td width="95%">
											<div align="left">
												Apabila Melebihi waktu 3 (tiga) bulan sejak diterbitkannya Surat Pemberitahuan Retribusi belum dilakukan pembayaran, 
												maka permohonan perijinan dibatalkan dan dapat melakukan pendaftaran kembali. 
											</div>
										</td>
									</tr>
								</table>
								<p>&nbsp;</p>
								<p>IMPERIUM © "'.date('Y').'". All Rights Reserved. Privacy | Terms</p>
								
							';
		$attach			= $_SERVER['DOCUMENT_ROOT'].root_folder.'/'.path_view_user_upload.'user_'.$user_management_id.'/document/spm_'.$qr_code.'.pdf';
		$sendEmail		= sendEmail($email,$subject,$body,$attach);
		
	}
	public function pdfSKRD($id = null)
	{
		$this->load->model('M_approval');
		$data['path_info']				= 'Approval';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		
		
		##Default##
		
		##Costumize##
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_approval->getApproval($search);
		$data['getData']		= $data['default']['data'];
		$data['getDataRetribusi']	= $this->M_approval->getDataRetribusi($search);
		$data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$id);
		$data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		$data['back']			= site_url('Permohonan/');
		
		$this->load->view('template/cetak_skrd',$data);
		$email				= $this->emailSKRD($id);
		$directto			= site_url('Approval');
		redirect($directto);
		exit;		
			
	}
	function emailSKRD($id = null){
		
		$getDataPermohonan				= SingleFilter('m_permohonan','permohonan_id',$id);
		$email 							= '';
		if($getDataPermohonan){
			$user_management_id			= $getDataPermohonan[0]['user_management_id'];
			$qr_code					= $getDataPermohonan[0]['qr_code'];
			$getDataUser				= SingleFilter('m_user_management','user_management_id',$user_management_id);
			if($getDataUser){
				$email					= $getDataUser[0]['email'];
			}
		}
		
		$subject		= "Surat Pemberitahuan Surat Keterangan Sudah Siap";
		$body			= "
								<div align='center'><img src='".base_url('assets/images/logo/logo.png')."'></div>
								<table align='center' style='width:100%;text-align:center;border-bottom: 1px solid black;'>
									<tr>
										<td style='height:30px;'><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td>DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
									<tr>
										<td>Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
									</tr>
									<tr>
										<td>Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td>PALEMBANG</td>
									</tr>
								</table>
								<p>&nbsp;</p>
								<p>Bersama dengan email ini, kami memberitahukan bahwa surat keterangan anda sudah siap dicetak</p>
								<p>&nbsp;</p>
								<p>IMPERIUM © ".date('Y').". All Rights Reserved. Privacy | Terms</p>
								
							";
		$attach			= $_SERVER['DOCUMENT_ROOT'].root_folder.'/'.path_view_user_upload.'user_'.$user_management_id.'/document/skrd_'.$qr_code.'.pdf';
		$sendEmail		= sendEmail($email,$subject,$body,$attach);
		if($sendEmail){
			return "sukses";
		} else {
			return "gagal";
		}
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'batal'){$directto	= $this->saveBatal();} 
		if($action == 'tolak'){$directto	= $this->saveTolak();} 
		if($action == 'setuju'){$directto	= $this->saveSetuju();}
		if($action == 'compare_log_kegiatan'){$directto	= $this->compare_log_kegiatan();} 
		if($action == 'uploadData'){$directto	= $this->uploadFile();} 
		if($action == 'saveSignature'){$directto	= $this->saveSignature();} 
		if($action == 'getDataSelect'){$directto	= $this->getDataSelect();} 
		if($action == 'terimaberkas'){$directto	= $this->terimaBerkas();} 
	}
	private function terimaBerkas(){
		$dataSession			= $this->session->userdata('user_data');
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$permohonan_id			= $this->input->post('permohonan_id');
		$return					= "Gagal";
		$getData	= '';
		if($permohonan_id){
			$getLevel			= SingleFilter('m_level','level_id',$LevelId_Session);
			$nama_level			= "";
			if($getLevel){
				$nama_level		= $getLevel[0]['level_name'];
			}
			$param		= array(
								"permohonan_id"			=> $permohonan_id,
								"jabatan_id_penerima"	=> $JabatanId_Session
								);
			$getData	= ManyFilter('history_permohonan',$param,'id','Desc');
			if($getData){
				$idHistory		= $getData[0]['id'];
				$updateData		= array(
										"tgl_diterima"	=> date('Y-m-d H:i:s'),
										"nama_penerima"	=> $nama_level
										);
				$edit			= Edit('history_permohonan',$updateData,'id',$idHistory);
				$return			= "Ok";
			}
		} 
		echo $return;
	}
	private function compare_log_kegiatan(){
		$data			= $this->input->post();
		$permohonan_id	= $data['permohonan_id'];
		$search			= array(
								"a.permohonan_id"			=> $permohonan_id
								);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistory			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
		}
		
		$getDataHistory	= SingleFilter('history_permohonan','id',$getIdHistory);
		$dataCompare	= '';
		if(($data) && ($getDataHistory)){
			$log_kegiatan_compare	= json_decode($getDataHistory[0]['log_kegiatan_compare'],true);
			$i = 1;
			if($log_kegiatan_compare){
				foreach($data as $row_left=>$row_right){
					foreach($log_kegiatan_compare as $row_left1=>$row_right1){
						if(($row_left == $row_left1) && ($row_right != $row_right1)){
						if($row_left == 'izin_category_id'){$getTable	= SingleFilter('m_izin_category','izin_category_id',$row_right); if($getTable){$row_right	= $getTable[0]['name'];}}
						if($row_left == 'izin_id'){$getTable	= SingleFilter('m_izin','izin_id',$row_right); if($getTable){$row_right	= $getTable[0]['izin_name'];}}
						if($row_left == 'izin_type_id'){$getTable	= SingleFilter('m_izin_type','izin_type_id',$row_right); if($getTable){$row_right	= $getTable[0]['type_name'];}}
						if($row_left == 'perusahaan_id'){$getTable	= SingleFilter('m_perusahaan','perusahaan_id',$row_right); if($getTable){$row_right	= $getTable[0]['nama_perusahaan'];}}
						if($row_left == 'alamat_perusahaan_id'){$getTable	= SingleFilter('m_alamat_perusahaan','alamat_perusahaan_id',$row_right); if($getTable){$row_right	= $getTable[0]['nama_cabang'];}}
						if($row_left == 'jenis_usaha'){$getTable	= SingleFilter('m_jenis_usaha','jenis_usaha_id',$row_right); if($getTable){$row_right	= $getTable[0]['name'];}}
						if($row_left == 'lingkungan'){$getTable	= SingleFilter('m_lingkungan_perusahaan','lingkungan_perusahaan_id',$row_right); if($getTable){$row_right	= $getTable[0]['name'];}}
						if($row_left == 'lokasi_perusahaan'){$getTable	= SingleFilter('m_lokasi_perusahaan','lokasi_perusahaan_id',$row_right); if($getTable){$row_right	= $getTable[0]['name'];}}
						
						
						if($row_left1 == 'izin_category_id'){$getTable	= SingleFilter('m_izin_category','izin_category_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['name'];}}
						if($row_left1 == 'izin_id'){$getTable	= SingleFilter('m_izin','izin_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['izin_name'];}}
						if($row_left1 == 'izin_type_id'){$getTable	= SingleFilter('m_izin_type','izin_type_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['type_name'];}}
						if($row_left1 == 'perusahaan_id'){$getTable	= SingleFilter('m_perusahaan','perusahaan_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['nama_perusahaan'];}}
						if($row_left1 == 'alamat_perusahaan_id'){$getTable	= SingleFilter('m_alamat_perusahaan','alamat_perusahaan_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['nama_cabang'];}}
						if($row_left1 == 'jenis_usaha'){$getTable	= SingleFilter('m_jenis_usaha','jenis_usaha_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['name'];}}
						if($row_left1 == 'lingkungan'){$getTable	= SingleFilter('m_lingkungan_perusahaan','lingkungan_perusahaan_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['name'];}}
						if($row_left1 == 'lokasi_perusahaan'){$getTable	= SingleFilter('m_lokasi_perusahaan','lokasi_perusahaan_id',$row_right1); if($getTable){$row_right1	= $getTable[0]['name'];}}
							if($row_left1 != 'catatan'){
								$dataCompare[$i]	= str_replace('_',' ',str_replace('_id','',$row_left1)).' sebelumnya : '.$row_right1.' => menjadi : '.$row_right;
							$i++;
							}
						}
					}
					
				}
			}
		}
		
		if($dataCompare){
			$dataHistory	 		= array(
											'log_kegiatan'	=> json_encode($dataCompare)
											);
			$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		}
		$dataHistory	 		= array(
									'log_kegiatan_compare'	=> json_encode($data)
									);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		
	}
	private function getDataSelect(){
		$find		= $this->input->post('find');
		$field		= $this->input->post('field');
		$id			= $this->input->post('id');
		$getData	= '';
		if($id){
		$getData	= SingleFilter($find,$field,$id);
		} 
		echo json_encode($getData);
	}
	private function saveSignature(){
		$this->load->model('M_permohonan');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Approval/');
		$id						= Id('m_signature','signature_id');
		$permohonan_id			= $this->input->post('permohonan_id');
		$izin_category_id		= $this->input->post('izin_category_id');
		
		#Create File Signature#
		if(!file_exists(path_user_upload.'signature/')) {
			$create_folder			= mkdir(path_user_upload.'signature/', 0777);
			}
		
		$nmfile 					= random_string(5);
		$dataImage = $this->input->post('signature');
		
		list($type, $dataImage) = explode(';', $dataImage);
		list(, $dataImage)      = explode(',', $dataImage);
		$dataImage = base64_decode($dataImage);

		file_put_contents(path_user_upload.'signature/'.$nmfile.'.png', $dataImage);
		#Create File Signature#
		
		$data	 				= array(
										'signature_id' 		=> $id,
										'permohonan_id' 	=> $this->input->post('permohonan_id'),
										'signature' 		=> $this->input->post('signature'),
										'path_location' 	=> str_replace('.','',path_user_upload).'signature/'.$nmfile.'.png',
										'level_id' 			=> $LevelId_Session	,
										'created_date' 		=> date('Y-m-d H:i:s'),
										'created_user' 		=> $UserId_Session
										);
		$save					= $this->M_approval->add('m_signature',$data);
		
		
		
		$getIdHistory			= Id('history_permohonan','id');
		$sendPengirim			= SingleFilter('m_jabatan','jabatan_id',$JabatanId_Session);
		$nama_pengirim			= "";
		if($sendPengirim){
		$nama_pengirim			= $sendPengirim[0]['jabatan_name'];
		}
		$jabatan_id_penerima	= 1;
		$nama_penerima			= 'Fo';
		$getDataPermohonan		= SingleFilter('m_permohonan','permohonan_id',$this->input->post('permohonan_id'));
		if($getDataPermohonan){
			$izin_id			= $getDataPermohonan[0]['izin_id'];
			$izin_type_id		= $getDataPermohonan[0]['izin_type_id'];
		}
		$getLevel				= SingleFilter('m_level','level_id',$LevelId_Session);
		$nama_level				= "";
		if($getLevel){
			$nama_level			= $getLevel[0]['level_name'];
		}
			$dataHistory	 		= array(
											'id'					=> $getIdHistory,
											'permohonan_id'			=> $this->input->post('permohonan_id'),
											'izin_id'				=> $izin_id,
											'izin_type_id'			=> $izin_type_id,
											'tgl_mulai'				=> date('Y-m-d H:i:s'),
											'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
											'keterangan_pengirim'	=> 'Mengirim berkas ketingkat '.$nama_penerima.' untuk melakukan cetak SK',
											'jabatan_id_pengirim' 	=> $JabatanId_Session	,
											'nama_pengirim' 		=> $nama_level,
											'jabatan_id_penerima' 	=> $jabatan_id_penerima,
											'nama_penerima' 		=> $nama_penerima,
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session,
											'modified_date' 		=> date('Y-m-d H:i:s'),
											'modified_user' 		=> $UserId_Session,
											'activation' 			=> 'Y'
											);
			
			$save					= Add('history_permohonan',$dataHistory);
		$this->pdfSKRD($permohonan_id);
		$directto			= site_url('Approval');
		redirect($directto);
		exit;
		
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Approval/');
		$id						= $this->input->post('alamat_perusahaan_id');
		$izin_category_id		= $this->input->post('izin_category_id');
		$izin_id				= $this->input->post('izin_id');
		$data	 				= array(
										'nama_perusahaan' 			=> $this->input->post('nama_perusahaan')
										);
		$update					= Edit('m_perusahaan',$data,'perusahaan_id',$this->input->post('perusahaan_id'));
		$data	 				= array(
										'jenis_usaha_id' 			=> $this->input->post('jenis_usaha'),
										'lingkungan_perusahaan_id' 	=> $this->input->post('lingkungan'),
										'lokasi_perusahaan_id' 		=> $this->input->post('lokasi_perusahaan'),
										'luas' 						=> $this->input->post('luas'),
										'alamat' 					=> $this->input->post('alamat'),
										'rt' 						=> $this->input->post('rt'),
										'rw' 						=> $this->input->post('rw'),
										'kecamatan_id' 				=> $this->input->post('kecamatan'),
										'kelurahan_id' 				=> $this->input->post('kelurahan'),
										'tlp' 						=> $this->input->post('tlp'),
										'lat' 						=> $this->input->post('lat'),
										'lng' 						=> $this->input->post('lng'),
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
		$update					= Edit('m_alamat_perusahaan',$data,'alamat_perusahaan_id',$id);
		if(($JabatanId_Session	 == 3) && ($izin_category_id == 2)){
			$data	 				= array(
										'index_gangguan_id'			=> $this->input->post('index_gangguan'),
										'alat_kerja_id'				=> $this->input->post('alat_kerja'),
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
			$update					= Edit('m_alamat_perusahaan',$data,'alamat_perusahaan_id',$id);
		}
		$this->compare_log_kegiatan();
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
	}
	private function saveBatal(){
		$this->load->model('M_permohonan');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Approval/');
		$id						= $this->input->post('permohonan_id');
		$data	 				= array(
										'reject' 					=> 1,
										'status' 					=> 'Reject',
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
		$update					= Edit('m_permohonan',$data,'permohonan_id',$id);
		
		$search							= array(
											"a.permohonan_id"			=> $id
										);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistory			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
		}
		$dataHistory	 		= array(
									'status_approval'		=> 2,
									'tgl_tolak'				=> date('Y-m-d H:i:s'),
									'kategori_penolakan'	=> 1,
									'catatan_penolakan'		=> $this->input->post('catatan'),
									'modified_date' 		=> date('Y-m-d H:i:s'),
									'modified_user' 		=> $UserId_Session,
									);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		
		
		$getIdHistory			= Id('history_permohonan','id');
		$sendPengirim			= SingleFilter('m_jabatan','jabatan_id',$JabatanId_Session);
		$nama_pengirim			= "";
		if($sendPengirim){
		$nama_pengirim			= $sendPengirim[0]['jabatan_name'];
		}
		$getLevel				= SingleFilter('m_level','level_id',$LevelId_Session);
		$nama_level				= "";
		if($getLevel){
			$nama_level			= $getLevel[0]['level_name'];
		}
		$jabatan_id_penerima	= 5;
		$nama_penerima			= "Kepala Dinas";
		$dataHistory	 		= array(
										'id'					=> $getIdHistory,
										'permohonan_id'			=> $id,
										'izin_id'				=> $this->input->post('izin_id'),
										'izin_type_id'			=> $this->input->post('izin_type_id'),
										'tgl_mulai'				=> date('Y-m-d H:i:s'),
										'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
										'keterangan_pengirim'	=> 'Menandatangani Permohonan yang telah ditolak kepada kepala dinas',
										'jabatan_id_pengirim' 	=> $JabatanId_Session,
										'nama_pengirim' 		=> $nama_level,
										'jabatan_id_penerima' 	=> $jabatan_id_penerima,
										'nama_penerima' 		=> $nama_penerima,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'created_user' 			=> $UserId_Session,
										'modified_date' 		=> date('Y-m-d H:i:s'),
										'modified_user' 		=> $UserId_Session,
										'activation' 			=> 'Y'
										);
		$save					= $this->M_permohonan->add('history_permohonan',$dataHistory);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
	}
	private function saveTolak(){
		$this->load->model('M_permohonan');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Approval/');
		$id						= $this->input->post('permohonan_id');
		$status_level			= $this->input->post('status_level');
		$data	 				= array(
										'status_level' 				=> $this->input->post('status_level'),
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
		$update					= Edit('m_permohonan',$data,'permohonan_id',$id);
		
		$search							= array(
											"a.permohonan_id"			=> $id
										);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistor			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
		}
		$dataHistory	 		= array(
									'status_approval'		=> 2,
									'tgl_tolak'				=> date('Y-m-d H:i:s'),
									'kategori_penolakan'	=> 1,
									'catatan_penolakan'		=> $this->input->post('catatan'),
									'modified_date' 		=> date('Y-m-d H:i:s'),
									'modified_user' 		=> $UserId_Session,
									);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		
		$getIdHistory			= Id('history_permohonan','id');
		$sendPengirim			= SingleFilter('m_jabatan','jabatan_id',$JabatanId_Session	);
		$nama_pengirim			= "";
		if($sendPengirim){
		$nama_pengirim			= $sendPengirim[0]['jabatan_name'];
		}
		$getAlurIzin			= grabDataAlurIzin($this->input->post('izin_id'),$this->input->post('izin_type_id'));
		$jabatan_id_penerima	= $getAlurIzin[$status_level]['jabatan_id'];
		$nama_penerima			= $getAlurIzin[$status_level]['jabatan_name'];
		$getLevel				= SingleFilter('m_level','level_id',$LevelId_Session);
		$nama_level				= "";
		if($getLevel){
			$nama_level			= $getLevel[0]['level_name'];
		}
		$dataHistory	 		= array(
										'id'					=> $getIdHistory,
										'permohonan_id'			=> $id,
										'izin_id'				=> $this->input->post('izin_id'),
										'izin_type_id'			=> $this->input->post('izin_type_id'),
										'tgl_mulai'				=> date('Y-m-d H:i:s'),
										'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
										'keterangan_pengirim'	=> 'Mengembalikan Permohonan',
										'jabatan_id_pengirim' 	=> $JabatanId_Session,
										'nama_pengirim' 		=> $nama_level,
										'jabatan_id_penerima' 	=> $jabatan_id_penerima,
										'nama_penerima' 		=> $nama_penerima,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'created_user' 			=> $UserId_Session,
										'modified_date' 		=> date('Y-m-d H:i:s'),
										'modified_user' 		=> $UserId_Session,
										'activation' 			=> 'Y'
										);
		$save					= $this->M_permohonan->add('history_permohonan',$dataHistory);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
	}
	private function saveSetuju(){
		$this->load->model('M_permohonan');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Approval/');
		$id						= $this->input->post('permohonan_id');
		$status_level			= $this->input->post('status_level') + 1;
		$izin_category_id		= $this->input->post('izin_category_id');
		$izin_id				= $this->input->post('izin_id');
		$search							= array(
											"a.permohonan_id"			=> $id
										);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistor			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
		}
		$getAlurIzin			= grabDataAlurIzin($this->input->post('izin_id'),$this->input->post('izin_type_id'));
		$jabatan_id_penerima	= $getAlurIzin[1]['jabatan_id'];
		$nama_penerima			= $getAlurIzin[1]['jabatan_name'];
		$dataHistory	 				= array(
												'tgl_selesai'		=> date('Y-m-d H:i:s'),
												'catatan_approval'	=> $this->input->post('catatan'),
												'status_approval'	=> 1,
												'modified_date' 	=> date('Y-m-d H:i:s'),
												'modified_user' 	=> $UserId_Session,
												);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		
		
		
		$getAlurIzin			= grabDataAlurIzin($this->input->post('izin_id'),$this->input->post('izin_type_id'));
		$jml_alur_izin			= count($getAlurIzin);
		$level_akhir			= $getAlurIzin[$jml_alur_izin]['jabatan_id'];
		
		if($JabatanId_Session != $level_akhir){
		$data	 				= array(
										'status_level' 				=> $status_level,
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
		$update					= Edit('m_permohonan',$data,'permohonan_id',$id);
		
		$getIdHistory			= Id('history_permohonan','id');
		$sendPengirim			= SingleFilter('m_jabatan','jabatan_id',$JabatanId_Session);
		$nama_pengirim			= "";
		if($sendPengirim){
			$nama_pengirim			= $sendPengirim[0]['jabatan_name'];
		}
		$jabatan_id_penerima	= $getAlurIzin[$status_level]['jabatan_id'];
		$nama_penerima			= $getAlurIzin[$status_level]['jabatan_name'];
		$getLevel				= SingleFilter('m_level','level_id',$LevelId_Session);
		$nama_level				= "";
		if($getLevel){
			$nama_level			= $getLevel[0]['level_name'];
		}
		$dataHistory	 		= array(
										'id'					=> $getIdHistory,
										'permohonan_id'			=> $id,
										'izin_id'				=> $this->input->post('izin_id'),
										'izin_type_id'			=> $this->input->post('izin_type_id'),
										'tgl_mulai'				=> date('Y-m-d H:i:s'),
										'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
										'keterangan_pengirim'	=> 'Mengirim berkas ketingkat selanjutnya '.$nama_penerima,
										'jabatan_id_pengirim' 	=> $JabatanId_Session	,
										'nama_pengirim' 		=> $nama_level,
										'jabatan_id_penerima' 	=> $jabatan_id_penerima,
										'nama_penerima' 		=> $nama_penerima,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'created_user' 			=> $UserId_Session,
										'modified_date' 		=> date('Y-m-d H:i:s'),
										'modified_user' 		=> $UserId_Session,
										'activation' 			=> 'Y'
										);
		
		$save					= $this->M_approval->add('history_permohonan',$dataHistory);
			if(($JabatanId_Session	 == 3) && ($izin_category_id == 2)){
				$getIdRetribusi			= Id('m_retribusi','retribusi_id');
				$dataRetribusi	 		= array(
											'retribusi_id'			=> $getIdRetribusi,
											'permohonan_id'			=> $id,
											'izin_id'				=> $this->input->post('izin_id'),
											'izin_type_id'			=> $this->input->post('izin_type_id'),
											'tgl_posting'			=> date('Y-m-d H:i:s'),
											'index_gangguan_id'		=> $this->input->post('index_gangguan'),
											'lingkungan_perusahaan_id'	=> $this->input->post('lingkungan'),
											'lokasi_perusahaan_id'		=> $this->input->post('lokasi_perusahaan'),
											'golongan'				=> $this->input->post('golongan'),
											'luas'					=> $this->input->post('luas'),
											'total_rigb'			=> $this->input->post('total_rigb'),
											'alat_kerja_id'			=> $this->input->post('alat_kerja'),
											'total_akhir'			=> $this->input->post('total'),
											'status_pembayaran'		=> 0,
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session,
											'modified_date' 		=> date('Y-m-d H:i:s'),
											'modified_user' 		=> $UserId_Session,
											'activation' 			=> 'Y'
											);
			
			$save					= Add('m_retribusi',$dataRetribusi);
			$data	 				= array(
											'index_gangguan_id'			=> $this->input->post('index_gangguan'),
											'alat_kerja_id'				=> $this->input->post('alat_kerja'),
											'modified_date' 			=> date('Y-m-d H:i:s'),
											'modified_user' 			=> $UserId_Session
											);
			$update					= Edit('m_alamat_perusahaan',$data,'alamat_perusahaan_id',$this->input->post('alamat_perusahaan_id'));
			}
		} else {
		$data	 				= array(
										'status_finish' 			=> 1,
										'status' 					=> 'Finish',
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
		$update					= Edit('m_permohonan',$data,'permohonan_id',$id);
			
		}
		$this->compare_log_kegiatan();
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
	}
	private function uploadFile(){
		
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		
		$id						= $this->input->post('permohonan_id');
		$status_level			= $this->input->post('status_level');
		$izin_category_id		= $this->input->post('izin_category_id');
		$izin_id				= $this->input->post('izin_id');
		$izin_type_id			= $this->input->post('izin_type_id');
		$getAlurIzin			= grabDataAlurIzin($this->input->post('izin_id'),$this->input->post('izin_type_id'));
		if(($JabatanId_Session != 5) && ($getAlurIzin[$status_level+1]['jabatan_id'] == 6)){
			$data	 				= array(
										'status' 					=> 'Waiting Payment',
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
			$update					= Edit('m_permohonan',$data,'permohonan_id',$id);
			$this->pdf($id);
		}
		if(($JabatanId_Session	 == 3) && ($izin_category_id == 2)){
			if(!file_exists(path_user_upload.'user_'.$UserId_Session)) {
			$create_folder			= mkdir(path_user_upload.'user_'.$UserId_Session, 0777);
			}
			if(!file_exists(path_user_upload.'user_'.$UserId_Session.'/document')) {
			$create_folder			= mkdir(path_user_upload.'user_'.$UserId_Session.'/document', 0777);
			}
			
			
			$jml_row_syarat				= $this->input->post('jml_row');
			for($i = 1;$i <= $jml_row_syarat; $i++){
				$nmfile 					= random_string(5); //nama file saya beri nama langsung dan diikuti fungsi time
				$config['upload_path'] 		= path_user_upload.'user_'.$UserId_Session.'/document/'; //path folder
				$config['allowed_types'] 	= '*'; //type yang dapat diakses bisa anda sesuaikan
				$config['max_size'] 		= 5100; //maksimum besar file 2M
				$config['file_name'] 		= $nmfile; //nama yang terupload nantinya
				$config['overwrite'] 		= TRUE;
				$this->load->library('upload');
				$this->upload->initialize($config);
			
				if($this->upload->do_upload('upload_file'.$i)){	
					$gbr		= $this->upload->data();
					$image_name	= $nmfile.$gbr['file_ext'];
					$dokumentasi_lapangan_id = Id('dokumentasi_lapangan','dokumentasi_lapangan_id');
					$data 		= array(
										'dokumentasi_lapangan_id'	=> $dokumentasi_lapangan_id,
										'permohonan_id'				=> $id	,
										'izin_id'					=> $izin_id,
										'izin_type_id'				=> $izin_type_id,
										'title'						=> $this->input->post('title_file'.$i),
										'file_name'					=> $image_name,
										'path_location'				=> path_user_upload.'user_'.$UserId_Session.'/document/'.$image_name,
										'created_user'				=> $UserId_Session,
										'activation'			=> 'Y'
									   );
					$update		= Add('dokumentasi_lapangan',$data);
				} 
			}
		}
		$jml_alur_izin			= count($getAlurIzin);
		$level_akhir			= $getAlurIzin[$jml_alur_izin]['jabatan_id'];
		if($JabatanId_Session	 != $level_akhir){
			$directto			= site_url('Approval/');
			redirect($directto);
			exit;
		} else {
			$directto			= site_url('Approval/signature/'.$id.'/'.$izin_category_id);
			redirect($directto);
			exit;	
		}
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_approval->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
