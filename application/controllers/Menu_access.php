<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_access extends CI_Controller {

	function __construct(){
		parent::__construct();	
		$this->cekSession 	= $this->authlogin->check_admin_session();
		$this->load->model('M_menu_access');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Menu_access';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Level Name'
										);
		$search							= array(
											"a.level_name"			=> $this->input->post('name')
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Menu Access';
		$data['title2']			= 'Search Data Menu Access';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_menu_access->getMenuAccess($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Menu_access/autocomplete');
		$data['print'] 			= site_url('Menu_access/cetak/print');
		$data['excell'] 		= site_url('Menu_access/cetak/excell');
		$data['add'] 			= site_url('Menu_access/add/');
		$data['edit'] 			= site_url('Menu_access/edit/');
		$data['detail'] 		= site_url('Menu_access/detail/');
		$data['page_action']	= site_url('Menu_access/');
		$data['back_action']	= site_url('Menu_access/');
		$data['delete']			= site_url('Menu_access/delete/');
		$this->load->view('setting/menu_access/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Menu_access';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.level_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Menu Access';
		$data['title2']			= 'Edit Menu Access';
		$data['default1']		= $this->M_menu_access->getMenuAccess($search);
		$data['default2']		= $this->M_menu_access->dataAccess($search);
		$data['getLevel']		= $data['default1']['data'];
		$data['getData']		= $data['default2']['data'];
		$data['getIzin']		= singleFilter('m_izin','','','izin_name','asc');
		$data['getMenu']		= singleFilter('m_menu','','','menu_id','asc');
		$data['getParentMenu']	= singleFilter('m_sub_menu','','','no_urut','asc');
		$getIzinAccess			= singleFilter('m_izin_access','level_id',$id);
		$data['IzinAccess']	= "";
		if($getIzinAccess){
		$data['IzinAccess']	= explode(',',$getIzinAccess[0]['izin_id']);
		}
		$data['action']			= site_url('Menu_access/saveForm/edit');
		$data['back']			= site_url('Menu_access/');
		
		$this->load->view('setting/menu_access/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Menu_access';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'b.parent_menu_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Menu Access';
		$data['title2']			= 'Data Detail Parent_menu';
		$data['default']		= $this->M_menu_access->getMenuAccess($search);
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Menu_access/');
		$this->load->view('setting/menu_access/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$directto				= site_url('Menu_access/');
		$filter					= array(
										"menu_id" 			=> $this->input->post('nama_menu'),
										"parent_menu_name" 	=> $this->input->post('nama_parent_menu')
										);
		$dataPost				= $this->input->post();
		$checkData				= $this->M_menu_access->checkData('m_parent_menu',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= $this->M_menu_access->getIdParentMenu();
			$no_urut				= $this->M_menu_access->getNoUrut($this->input->post('nama_menu'));
			$data	 				= array(
										'parent_menu_id'	=> $id,
										'menu_id'			=> $this->input->post('nama_menu'),
										'parent_menu_name' 	=> $this->input->post('nama_parent_menu'),
										'link' 				=> $this->input->post('link'),
										'no_urut' 			=> $no_urut,
										'activation' 		=> 'Y'
										);
			$save					= $this->M_menu_access->add('m_parent_menu',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		
	}
	private function saveEdit(){
		$directto				= site_url('access/');
		$delete					= $this->M_menu_access->delete($this->input->post('level_id'));
		$getMenu				= singleFilter('m_menu');
		$getParentMenu			= singleFilter('m_sub_menu');
		$getIzin				= singleFilter('m_izin','','','izin_name','asc');
		$error					= 0;
		if($getMenu){
			$i = 1;
			foreach($getMenu as $row_menu){
				if($getParentMenu){
					$ii = 1;
					foreach($getParentMenu as $row_parent_menu){
						if($row_menu['menu_id'] == $row_parent_menu['menu_id']){
							
								$id						= Id('m_menu_access','menu_access_id');
								$view					= $this->input->post('isi_view'.$i.'_'.$ii);
							if($view){
								$data	 				= array(
															'menu_access_id'	=> $id,
															'level_user'		=> $this->input->post('level_id'),
															'menu_id' 			=> $this->input->post('menu_id'.$i),
															'sub_menu_id' 	=> $this->input->post('sub_menu_id'.$i.'_'.$ii)
															);
															
								$save					= Add('m_menu_access',$data);
								if(!$save){
									$error++;
								}
							}
						$ii++;	
						}
					}
				}
			$i++;
			}
		}
		if($getIzin){
			$i = 1;
			$izin_id	= array();
			foreach($getIzin as $row){
				$izin			= $this->input->post('izin'.$i);
				if($izin){
				$izin_id[$i]	= $this->input->post('izin'.$i);
				}
			$i++;
			}
			$izinAccess				= singleFilter('m_izin_access','level_id',$this->input->post('level_id'));
			if(!$izinAccess){
				$data	 				= array(
												'izin_access_id'	=> Id('m_izin_access','izin_access_id'),
												'level_id'			=> $this->input->post('level_id'),
												'izin_id' 			=> implode(',',$izin_id),
												'activation' 		=> 'Y'
												);
				$save					= Add('m_izin_access',$data);
				if(!$save){
					$error++;
				}
			} else {
				$data	 				= array(
												'izin_id' 			=> implode(',',$izin_id)
												);
				$save					= Edit('m_izin_access',$data,'level_id',$this->input->post('level_id'));	
				if(!$save){
					$error++;
				}
			}
		}
		if($error == 0){
			echo "sukses";
		} else {
			echo "gagal";
		}
		
	}
	public function delete(){
		$directto				= site_url('Menu_access');
		$data['getData']		= $this->M_menu_access->getMenuAccess();
		$jml_row				= $this->input->post('rows');
		if($data['getData']){
			for($i = 1;$i <= $jml_row;$i++){
				$id			= $this->input->post('id'.$i);
				if($id){
				$delete					= $this->M_menu_access->delete($id);
				}
				
			}
		}
		if($delete){
		$error				= 6;
		$this->session->set_flashdata('error', $error);				
		redirect($directto);
		exit;
		} else {
		$error				= 7;
		$this->session->set_flashdata('error', $error);					
		redirect($directto);
		exit;
		}
	}
	
}
