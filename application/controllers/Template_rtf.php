<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_rtf extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_template_rtf');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Template'
										);
		$search							= array(
											"a.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Template RTF';
		$data['title2']			= 'Search Data Template RTF';
		$data['data_search']	= $search;	
		$data['getData']		= $this->M_template_rtf->getTemplate($search,'');
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Template_rtf/autocomplete');
		$data['print'] 			= site_url('Template_rtf/cetak/print');
		$data['excell'] 		= site_url('Template_rtf/cetak/excell');
		$data['add'] 			= site_url('Template_rtf/add/');
		$data['edit'] 			= site_url('Template_rtf/edit');
		$data['detail'] 		= site_url('Template_rtf/detail');
		$data['page_action']	= site_url('Template_rtf/');
		$data['back_action']	= site_url('Template_rtf/');
		$data['delete']			= site_url('Template_rtf/delete/');
		$this->load->view('master/Template_rtf/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Template RTF';
		$data['title2']			= 'Create New Template RTF';
		$data['getTemplate']	= $this->M_template_rtf->getDataCombo('m_template_rtf','template_rtf_id',1);
		$data['uploadFile']		= site_url('Template_rtf/uploadFile');
		$data['action']			= site_url('Template_rtf/saveForm/add');
		$data['back']			= site_url('Template_rtf/');
		$this->load->view('master/template_rtf/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'template_rtf_id'	=>	$id
										);
		
		$data['title1']			= 'Data Template RTF';
		$data['title2']			= 'Edit Template RTF';
		$data['getData']		= ManyFilter('m_template_rtf',$search);
		
		$data['dataTemplate']	= array();
		if($data['getData']){
			foreach($data['getData'][0] as $row_left=>$row_right){
				$data['dataTemplate'][$row_left]		= $row_right;
				if($row_left == 'content'){
					$data['dataTemplate'][$row_left]	= json_decode($row_right,true);
				}
			}
		}
		$data['action']			= site_url('Template_rtf/saveForm/edit');
		$data['back']			= site_url('Template_rtf/');
		$data['uploadFile']		= site_url('Template_rtf/uploadFile');
		$this->load->view('master/template_rtf/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	
	public function _detail(){
		require_once('./assets/phpword/PHPWord.php');	
		$PHPWord = new PHPWord();
			
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'template_rtf_id'	=>	$id
										);
		
		$data['title1']			= 'Data Template RTF';
		$data['title2']			= 'Edit Template RTF';
		$data['getData']		= ManyFilter('m_template_rtf',$search);
		if($data['getData']){
			$title				= $data['getData'][0]['title'];
			$path_location		= $data['getData'][0]['path_location'];
		}
		
			$document = $PHPWord->loadTemplate($_SERVER["DOCUMENT_ROOT"].'imperium_new/'.$path_location);
			// debug($document);exit;
			// $document->setValue('{nama_pemohon}', 'Yudo Rahmanto ');
			$filename = str_replace(' ','_',$title).'_view.docx'; 
			
			$document->save($_SERVER["DOCUMENT_ROOT"].'imperium_new/'.path_view_user_upload.'template_doc/'.$filename);
			
			header('location:' . base_url(path_view_user_upload.'template_doc/'. $filename));
			
			//$this->debug->show($surat, 1);die();
		}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'template_surat_id'	=>	$id
										);
		
		$data['title1']			= 'Data Template';
		$data['title2']			= 'Edit Template';
		$data['getData']		= ManyFilter('m_template_surat',$search);
		
		$data['dataTemplate']	= file_get_contents(base_url('assets/SURAT_IJIN_WALIKOTA_PALEMBANG.html'));
		
		debug($data['dataTemplate']);exit;
		$data['back']			= site_url('Template/');
		$this->load->view('template/print_rtf_to_pdf',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'uploadFile'){$directto	= $this->uploadFile();} 
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Template_rtf/');
		$filter					= array(
										"title" => $this->input->post('title')
										);
		$dataPost				= $this->input->post();
		$checkData				= $this->M_template_rtf->checkData('m_template_rtf',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_template_rtf','template_rtf_id');
			$data	 				= array(
										'template_rtf_id'			=> $id,
										'title' 					=> $this->input->post('title'),
										'path_location' 			=> '',
										'activation' 				=> 'Y'
										);
			$save					= $this->M_template_rtf->add('m_template_rtf',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Template/');
		$id						= $this->input->post('template_rtf_id');
		$dataPost				= $this->input->post();
		
		if($dataPost){
			$data	 				= array(
										'title' 					=> $this->input->post('title'),
										'path_location' 			=> '',
										'activation' 				=> 'Y'
										);
			$update					= Edit('m_template_rtf',$data,'template_rtf_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		}
	}
	function uploadFile(){
		$dataSession 			= $this->session->userdata('temp');
		if(!$dataSession){
			$template_rtf_id 		= $this->input->post('template_rtf_id');
		} else { 
			$template_rtf_id		= $dataSession['template_rtf_id'];
		}
		if(!file_exists(path_user_upload.'template_doc')) {
		$create_folder			= mkdir(path_user_upload.'template_doc', 0777);
		}
		$title 	= $this->input->get('title');
		$title 	= str_replace(' ','_',str_base64_decode($title));
		
		if($_FILES){
			$namaFileAsli				= $_FILES['file']['name'];
			$extensi 					= substr($namaFileAsli, strpos($namaFileAsli, ".") + 1);
			if(in_array($extensi,array("doc","docx"))){
				
				move_uploaded_file($_FILES['file']['tmp_name'], path_user_upload.'template_doc/'.$title.'.'.$extensi);
				$data 		= array(
									'path_location'	=> path_view_user_upload.'template_doc/'.$title.'.'.$extensi
								   );
						   
				$update		= $this->M_template_rtf->edit('m_template_rtf',$data,'template_rtf_id',$template_rtf_id);
					if($update){
						echo 'Ok';
					} else {
						echo 'Not Ok';
					}
			} else {
				echo 'Not Ok';
			}
		} else {
			echo 'Not Ok';
		}
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_template->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
