<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_template');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Template'
										);
		$search							= array(
											"a.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Template';
		$data['title2']			= 'Search Data Template';
		$data['data_search']	= $search;	
		$data['getData']		= $this->M_template->getTemplate($search,'');
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Template/autocomplete');
		$data['print'] 			= site_url('Template/cetak/print');
		$data['excell'] 		= site_url('Template/cetak/excell');
		$data['add'] 			= site_url('Template/add/');
		$data['edit'] 			= site_url('Template/edit');
		$data['detail'] 		= site_url('Template/detail');
		$data['page_action']	= site_url('Template/');
		$data['back_action']	= site_url('Template/');
		$data['delete']			= site_url('Template/delete/');
		$this->load->view('master/template/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Template';
		$data['title2']			= 'Create New Template';
		$data['getTemplate']	= $this->M_template->getDataCombo('m_template_surat','template_surat_id',1);
		$data['action']			= site_url('Template/saveForm/add');
		$data['back']			= site_url('Template/');
		$this->load->view('master/template/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'template_surat_id'	=>	$id
										);
		
		$data['title1']			= 'Data Template';
		$data['title2']			= 'Edit Template';
		$data['getData']		= ManyFilter('m_template_surat',$search);
		
		$data['dataTemplate']	= array();
		if($data['getData']){
			foreach($data['getData'][0] as $row_left=>$row_right){
				$data['dataTemplate'][$row_left]		= $row_right;
				if($row_left == 'content'){
					$data['dataTemplate'][$row_left]	= json_decode($row_right,true);
				}
			}
		}
		$data['action']			= site_url('Template/saveForm/edit');
		$data['back']			= site_url('Template/');
		
		$this->load->view('master/template/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Template';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'template_surat_id'	=>	$id
										);
		
		$data['title1']			= 'Data Template';
		$data['title2']			= 'Edit Template';
		$data['getData']		= ManyFilter('m_template_surat',$search);
		
		$data['dataTemplate']	= array();
		if($data['getData']){
			foreach($data['getData'][0] as $row_left=>$row_right){
				$data['dataTemplate'][$row_left]		= $row_right;
				if($row_left == 'content'){
					$data['dataTemplate'][$row_left]	= json_decode($row_right,true);
				}
			}
		}
		//debug($data['getData']);exit;
		$data['back']			= site_url('Template/');
		$this->load->view('template/print_pdf',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Template/');
		$filter					= array(
										"title" => $this->input->post('title')
										);
		$dataPost				= $this->input->post();
		$checkData				= $this->M_template->checkData('m_template_surat',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_template_surat','template_surat_id');
			$jml_row				= $this->input->post('jml_halaman');
			for($i=1;$i <= $jml_row;$i++){
				$dataJson[$i]['editor']			= $this->input->post('editor'.$i);
			}
			$getJson				= json_encode($dataJson);
			$data	 				= array(
										'template_surat_id'			=> $id,
										'title' 					=> $this->input->post('title'),
										'content' 					=> $getJson,
										'activation' 				=> 'Y'
										);
			$save					= Add('m_template_surat',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Template/');
		$id						= $this->input->post('template_surat_id');
		$dataPost				= $this->input->post();
		if($dataPost){
			$jml_row				= $this->input->post('jml_halaman');
			for($i=1;$i <= $jml_row;$i++){
				$dataJson[$i]['editor']			= $this->input->post('editor'.$i);
			}
			
			$getJson				= json_encode($dataJson);
			$data	 				= array(
										'title' 					=> $this->input->post('title'),
										'content' 					=> $getJson
										);
			$update					= Edit('m_template_surat',$data,'template_surat_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		}
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_template->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
