<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();	
		$this->load->model('M_login');
		
	}
	
	public function index()
	{
		$this->load->model('M_user');
		$this->session->sess_destroy();
		$data['path_info']			= 'Login';
		$data['assets_css']			= assets_css($data['path_info']);
		$data['assets_js']			= assets_js($data['path_info']);
		$data['getProvinsi']		= $this->M_user->getDataCombo('provinsi','','','nama','asc');
		$data['validation']			= 'username,password';
		$data['cekEmail']			= site_url('Login/action/cekEmail');
		$data['action']				= site_url('Login/action/cekLogin');
		$data['getData']			= site_url('Login/action/getData');
		$data['resetPassword']		= site_url('Login/action/resetPassword');
		$data['resetCaptcha']		= site_url('Login/action/resetCaptcha');
		$data['actionRegister']		= site_url('Login/action/cekRegister');
		$data['mycaptcha']			= $this->captcha();
		$this->load->view('common/head',$data);
		$this->load->view('common/popup_message');
		$this->load->view('login/login',$data);
	}
	public function formActivation()
	{
		$key						= $this->input->get('key');
		
		if($key){
			$filter	= array(
							'token_activation'	=>	$key
							);
			$cekToken		= $this->M_login->checkData("m_user_management",$filter);
			if($cekToken){
				$data	 				= array(
										'token_activation' 		=> '',
										'activation'			=> 'Y'
										);
										
				$update					= $this->M_login->edit('m_user_management',$data,'token_activation',$key);
				$error				= 15;
				$this->session->set_flashdata('error', $error);	
				$directto			= site_url('Login');
				redirect($directto);
				exit;
			} else {
				$directto			= site_url('Page404');
				redirect($directto);
				exit;
			}
		} else {
			$directto			= site_url('Page404');
			redirect($directto);
			exit;
		}
		
	}
	
	public function formReset()
	{
		$data['assets_css']		= assets_css('Login');
		$data['assets_js']		= assets_js('Login');
		$this->load->view('common/head',$data);
		$token					= $this->input->get('key');
		$getDataUser			= $this->M_login->getDataUser($token);
		if(($token) && ($getDataUser)){
		$data['type_save']		= 'processResetPassword';
		$data['validation']		= 'new_password';
		$data['getData']		= $getDataUser;
		$data['action']			= site_url('Login/action/actionResetPassword');
		$this->load->view('login/reset_password',$data);
		} 
		else if(($token) && (!$getDataUser)){
		$directto				= site_url('Page404?msg=Token Not Valid');
		redirect($directto);
		exit;	
		} else {
		$directto				= site_url('Login/');
		redirect($directto);
		exit;	
		}
	}
	
	public function logout()
	{
			$this->session->sess_destroy();
		
			$directto			= site_url('Login');
			$error				= 7;
			$this->session->set_flashdata('error', $error);		
			redirect($directto);
			exit;
			
	}
	public function action(){
		$action		= $this->uri->segment(3);
		if($action == 'cekLogin'){$this->validasiLogin();}
		if($action == 'cekEmail'){$this->validasiEmail();}
		if($action == 'cekRegister'){$this->saveRegister();}
		if($action == 'getData'){$this->getData();}
		if($action == 'resetCaptcha'){$this->resetCaptcha();}
		if($action == 'resetPassword'){$this->resetPassword();}
		if($action == 'actionResetPassword'){$this->actionResetPassword();}
	}
	private function getData(){
		$this->load->model('M_user');
		$find		= $this->input->post('find');
		$id			= $this->input->post('id');
		$getData	= '';
		if($id){
		$getData	= $this->M_user->getData($find,$id);
		} 
		echo json_encode($getData);
	}
	private function createSession($data_user){
		$data_sessio	= "";
		if($data_user){
			foreach($data_user[0] as $row_left=>$row_right){
			$data_session['user_data'][$row_left]	= $row_right;
			}
		}
		
		$this->session->set_userdata($data_session);
	}
	private function validasiEmail(){
		$email			= $this->input->post('email');
		$result			= 'Not Ok';
		$checkData		= emailChecking($email);
		if($checkData){
				$result = 'Ok';
			}
		echo $result;
	}
	private function validasiLogin(){
		$username		= $this->input->post('username');
		$password		= md5(md5($this->input->post('password')));
		$filter			= array(
								'a.username'	=>	$username,
								'a.password'	=>	$password
								);
		$cekCharacter	= $this->cekCharacter($filter);
		if(!$cekCharacter){
			echo 'disallow character';
			$directto			= site_url('Login');
			$error				= 14;
			$this->session->set_flashdata('error', $error);		
			redirect($directto);
			exit;
		} else {
			$getDataStaff	= $this->M_login->getStaff($filter);
			$getDataUser	= $this->M_login->getUser($filter);
			if($getDataStaff){
				$this->createSession($getDataStaff);
				$directto			= site_url('Home');
				$error				= 5;
				$this->session->set_flashdata('error', $error);		
				redirect($directto);
				exit;
			}
			else if($getDataUser){
				$this->createSession($getDataUser);
				$directto			= site_url('Home');
				$error				= 5;
				$this->session->set_flashdata('error', $error);		
				redirect($directto);
				exit;
			} else {
				$directto			= site_url('Login');
				$error				= 3;
				$this->session->set_flashdata('error', $error);		
				redirect($directto);
				exit;
			}
		}
	}
	private function resetPassword(){
		$email		= $this->input->post('email');
		
		$cekEmail	= emailChecking($email);
		/* Jika status $cekEmail = 'Ok' berarti email already exist */
		if($cekEmail != 'Ok'){
			$serch			= array(
									"a.email"	=> $email
									);
			$getDataStaff	= $this->M_login->getStaff($serch);
			$getDataUser	= $this->M_login->getUser($serch);
			$token				= '';
			$full_name			= '';
			if($getDataStaff){
				foreach($getDataStaff as $row){
					$token				= md5(md5($row['jabatan_id'].':'.$row['user_id'].':'.$row['email'].':'.date('Y-m-d H:i:s')));
					$full_name			= $row['first_name'].' '.$row['last_name'];
					$data	 			= array(
												'date_request' 		=> date('Y-m-d H:i:s'),
												'jabatan_id' 		=> $row['jabatan_id'],
												'user_id' 			=> $row['user_id'],
												'email'				=> $row['email'],
												'token'				=> $token,
												'date_expire'		=> date('Y-m-d H:i:s', strtotime('+1 hour')),
												'activation'		=> 'Y'
												);
											
					$saveLog			= Add('log_reset_password',$data);
				}
			}
			else if($getDataUser){
				foreach($getDataUser as $row){
					$token				= md5(md5($row['jabatan_id'].':'.$row['user_id'].':'.$row['email'].':'.date('Y-m-d H:i:s')));
					$full_name			= $row['first_name'].' '.$row['last_name'];
					$data	 			= array(
												'date_request' 		=> date('Y-m-d H:i:s'),
												'jabatan_id' 		=> $row['jabatan_id'],
												'user_id' 			=> $row['user_id'],
												'email'				=> $row['email'],
												'token'				=> $token,
												'date_expire'		=> date('Y-m-d H:i:s', strtotime('+1 hour')),
												'activation'		=> 'Y'
												);
											
					$saveLog			= Add('log_reset_password',$data);
				}
			}
		$link		= site_url('Login/formReset?key='.$token);
		$sendEmail	= $this->emailReset($email,$link,$full_name);
		echo $sendEmail;
		} else {
		echo "Email not found"; 
		}
	}
	private function actionResetPassword(){
		
		$directto				= site_url('Login/');
		$jabatan_id				= $this->input->post('jabatan_id');
		$user_id				= $this->input->post('user_id');
		$token					= $this->input->post('token');
		
		$data_log	 			= array(
										'activation' 		=> 'N'
										);
										
		$update_log				= Edit('log_reset_password',$data_log,'token',$token);
		if($jabatan_id == 7){
			$data	 				= array(
											'password' 		=> md5(md5($this->input->post('new_password')))
											);
											
			$update					= Edit('m_user_management',$data,'user_management_id',$user_id);
		} else {
			$data	 				= array(
											'password' 		=> md5(md5($this->input->post('new_password')))
											);
											
			$update					= Edit('m_admin',$data,'admin_id',$user_id);
		}
		if($update){
		$error				= 12;
		$this->session->set_flashdata('error', $error);	
		redirect($directto);
		exit;
		} else {
		$error				= 2;
		$this->session->set_flashdata('error', $error);	
		redirect($directto);
		exit;
		}
		
	}
	function cekCharacter($data = null){
		$i = 0;
		if($data){
			$validasi	= "/[^a-zA-Z0-9_@~!$*.#&]/";
			foreach($data as $row_left=>$row_right){
				if(preg_match($validasi, $row_right)){
					$i++;
				}
			}
		}
		if($i == 0){
			return true ;
		} else {
			return false ;
		}
	}
	
	private function resetCaptcha(){
		$getData[0]	= $this->captcha();
		echo json_encode($getData);
	}
	function captcha(){
		$this->load->helper('captcha');
		$string_value	= random_string(5);
		$vals = array(
			'word'          => $string_value,
			'img_path'  	=> './assets/upload/image/captcha/',
			'font_path'     => './assets/font/Fibel_Nord.ttf',
			'img_url'  		=> base_url('assets/upload/image/captcha/'),
			'img_width'  	=> '150',
			'img_height' 	=> 30,
            'font_size'  	=> 15,
			'border' 		=> 0, 
			'expiration' 	=> 7200,
                        'colors'   => array(
                                  'background' => array(255, 255, 255),
                                  'border' => array(255, 255, 255),
                                  'text' => array(0, 0, 255),
                                  'grid' => array(190, 190, 255)
                                         )
                     );
	 
		// create captcha image
		$cap = create_captcha($vals);
	 
		// store image html code in a variable
		$data['img'] = $cap['image'];
		$getData	 = '';
		if($cap){
		$getData	 = array(
							'text' => $cap['word'],
							'filename' => $cap['filename']
							);
		}
		return $getData;
	}
	public function saveRegister(){
		$filter			= array(
								"ktp" 			=> $this->input->post('ktp'),
								"email" 		=> $this->input->post('email')
								);
		$checkData		= $this->M_login->checkData("m_user_management",$filter);
		if(!$checkData){
			$save				= '';
			$token				= $this->input->post('email').'|'.date('Y-m-d H:i:s');
			$token				= random_string(10,$token);
			$email				= $this->input->post('email');
			$fullname			= $this->input->post('first_name').' '.$this->input->post('last_name');
			$password			= $this->input->post('new_password');
			$data	 			= array(
										'level_id' 				=> 8,
										'ktp' 					=> $this->input->post('ktp'),
										'npwp' 					=> $this->input->post('npwp'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'username' 				=> $this->input->post('first_name').date('s'),
										'password' 				=> md5(md5($this->input->post('new_password'))),
										'no_tlp' 				=> $this->input->post('no_tlp'),
										'address' 				=> $this->input->post('alamat'),
										'rt' 					=> $this->input->post('rt'),
										'rw' 					=> $this->input->post('rw'),
										'provinsi_id' 			=> $this->input->post('provinsi'),
										'kabupaten_id' 			=> $this->input->post('kabupaten'),
										'kecamatan_id' 			=> $this->input->post('kecamatan'),
										'kelurahan_id' 			=> $this->input->post('kelurahan'),
										'token_activation' 		=> $token,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'activation' 			=> 'N'
										);
										
			$save					= $this->M_login->add('m_user_management',$data);
			if($save){
				$link		= site_url('Login/formActivation?key='.$token);
				$sendEmail	= $this->email($email,$link,$fullname,$password);
				if($sendEmail){
					$directto			= site_url('Login');
					$error				= 16;
					$this->session->set_flashdata('error', $error);		
					redirect($directto);
					exit;
				} else {
					$directto			= site_url('Login');
					$error				= 17;
					$this->session->set_flashdata('error', $error);		
					redirect($directto);
					exit;
				}
			} else {
				$directto			= site_url('Login');
				$error				= 17;
				$this->session->set_flashdata('error', $error);		
				redirect($directto);
				exit;
			}
		} else {
			$directto			= site_url('Login');
			$error				= 9;
			$this->session->set_flashdata('error', $error);		
			redirect($directto);
			exit;
		}
		 
	}
	function email($email = null,$link = null,$fullname = null,$password = null){
		 
		$subject	= "REGISTRATION";
		$body		= "
					<p>Terimakasih Telah Melakukan Pendaftaran </p>
								<table>
									<tr>
										<td colspan='3'>Berikut Adalah Info Akun Anda </td>
									</tr>
									<tr>
										<td>Full Name</td><td>:</td><td>".$fullname."</td>
									</tr>
									<tr>
										<td>Email</td><td>:</td><td>".$email."</td>
									</tr>
									<tr>
										<td>Password</td><td>:</td><td>".$password."</td>
									</tr>
									<tr>
										<td colspan='3'>Silahkan Ikuti Langkah aktivasi Akun <a href='".$link."'>Aktifkan Akun</a> Sekarang</td>
									</tr>
									<tr>
										<td colspan='3'>Jika Link Diatas Tidak Berfungsi , copy dan paste link berikut </td>
									</tr>
									<tr>
										<td colspan='3'>".$link."</td>
									</tr>
								</table>
					<p>IMPERIUM © ".date('Y').". All Rights Reserved. Privacy | Terms</p>
								
							";
		$sendEmail		= sendEmail($email,$subject,$body,'');
		if($this->email->send()){
			return "sukses";
		} else {
			return "gagal";
		}
	}
	function emailReset($email = null,$link = null,$full_name = null){
		 
		$subject	="RESET PASSWORD";
		$body		= "
					<p>Terimakasih Telah Melakukan Reset Password </p>
								<table>
									<tr>
										<td colspan='3'>Berikut Adalah Info Akun Anda </td>
									</tr>
									<tr>
										<td>Full Name</td><td>:</td><td>".$full_name."</td>
									</tr>
									<tr>
										<td>Email</td><td>:</td><td>".$email."</td>
									</tr>
									<tr>
										<td colspan='3'>Silahkan Klik <a href='".$link."'>Reset Password</a> untuk pergantian password </td>
									</tr>
									<tr>
										<td colspan='3'>Jika Link Diatas Tidak Berfungsi , copy dan paste link berikut </td>
									</tr>
									<tr>
										<td colspan='3'>".$link."</td>
									</tr>
								</table>
					<p>IMPERIUM © ".date('Y').". All Rights Reserved. Privacy | Terms</p>
								
							";
		$sendEmail		= sendEmail($email,$subject,$body,'');
		if($sendEmail){
			return "sukses";
		} else {
			return "gagal";
		}
	}
}
