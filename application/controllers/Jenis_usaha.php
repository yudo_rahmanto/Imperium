<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_usaha extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_jenis_usaha');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Jenis_usaha';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Jenis Usaha'
										);
		$search							= array(
											"activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Jenis Usaha';
		$data['title2']			= 'Search Data Jenis Usaha';
		$data['data_search']	= $search;	
		$data['getData']		= ManyFilter('m_jenis_usaha',$search);
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Jenis_usaha/autocomplete');
		$data['print'] 			= site_url('Jenis_usaha/cetak/print');
		$data['excell'] 		= site_url('Jenis_usaha/cetak/excell');
		$data['add'] 			= site_url('Jenis_usaha/add/');
		$data['edit'] 			= site_url('Jenis_usaha/edit');
		$data['detail'] 		= site_url('Jenis_usaha/detail');
		$data['page_action']	= site_url('Jenis_usaha/');
		$data['back_action']	= site_url('Jenis_usaha/');
		$data['delete']			= site_url('Jenis_usaha/delete/');
		$this->load->view('master/jenis_usaha/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Jenis_usaha';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Jenis Usaha';
		$data['title2']			= 'Create New Jenis Usaha';
		$data['action']			= site_url('Jenis_usaha/saveForm/add');
		$data['back']			= site_url('Jenis_usaha/');
		$this->load->view('master/jenis_usaha/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Jenis_usaha';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'jenis_usaha_id'	=>	$id
										);
		
		$data['title1']			= 'Data Jenis Usaha';
		$data['title2']			= 'Edit Jenis Usaha';
		$data['getData']		= ManyFilter('m_jenis_usaha',$search);
		//debug($data['getData']);exit;
		$data['action']			= site_url('Jenis_usaha/saveForm/edit');
		$data['back']			= site_url('Jenis_usaha/');
		
		$this->load->view('master/jenis_usaha/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Jenis_usaha';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'jenis_usaha_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Jenis Usaha';
		$data['title2']			= 'Data Detail Jenis Usaha';
		$data['getData']		= ManyFilter('m_jenis_usaha',$search);
		//debug($data['getData']);exit;
		$data['back']			= site_url('Jenis_usaha/');
		$this->load->view('master/jenis_usaha/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Jenis_usaha/');
		$filter					= array(
										"name" => $this->input->post('jenis_usaha')
										);
		$dataPost				= $this->input->post();
		$checkData				= ManyFilter('m_jenis_usaha',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_jenis_usaha','jenis_usaha_id');
			$data	 				= array(
										'jenis_usaha_id'	=> $id,
										'name' 				=> $this->input->post('jenis_usaha'),
										'activation' 		=> 'Y'
										);
			$save					= Add('m_jenis_usaha',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Jenis_usaha/');
		$id						= $this->input->post('jenis_usaha_id');
		$data	 				= array(
										'name' 		=> $this->input->post('jenis_usaha'),
										);
		$update					= Edit('m_jenis_usaha',$data,'jenis_usaha_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_jenis_usaha->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
