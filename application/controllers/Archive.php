<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archive extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_archive');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Archive';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Perusahaan',
										'Tgl Pendaftaran',
										'Code Permohonan',
										'Nama Izin',
										'Jenis Permohonan',
										'Posisi Permohonan',
										'Status Akhir'
										);
		$search							= array(
											"a.activation"		=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Search Data Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_archive->getArchive($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Archive/autocomplete');
		$data['print'] 			= site_url('Archive/cetak/print');
		$data['excell'] 		= site_url('Archive/cetak/excell');
		$data['add'] 			= site_url('Archive/add/');
		$data['edit'] 			= site_url('Archive/edit');
		$data['detail'] 		= site_url('Archive/detail');
		$data['cetak_resi'] 	= site_url('Archive/cetak_resi');
		$data['page_action']	= site_url('Archive/');
		$data['back_action']	= site_url('Archive/');
		$data['delete']			= site_url('Archive/delete');
		$this->load->view('archive/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$JabatanId_Session				= $dataSession['jabatan_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Archive';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Detail Pemohon';
		$data['getDataPermohonan']		= $this->M_archive->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_archive->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_archive->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_archive->getPemenuhanSyaratPermohonan($search);
		$data['getDataHistory']			= $this->M_archive->getDataHistoryPermohonan($search);
		$data['getJenisUsaha']	= SingleFilter('m_jenis_usaha');
		$data['getLingkungan']	= SingleFilter('m_lingkungan_perusahaan');
		$data['getLokasi']		= SingleFilter('m_lokasi_perusahaan');
		$data['getKecamatan']	= SingleFilter('kecamatan','id_kabupaten',1671);
		$kecamatan_id = '';
		if($data['getKecamatan']){
			foreach($data['getKecamatan'] as $row){
				$kecamatan_id = $row['id'];
			}
		}
		$data['getKelurahan']	= SingleFilter('kelurahan','id_kecamatan',$kecamatan_id);
		
		$searchHistory			= array(
											"a.permohonan_id"			=> $id,
											"a.jabatan_id_penerima"		=> $JabatanId_Session	
										);
		$getHistoryPermohonan	= $this->M_archive->getDataHistoryPermohonan($searchHistory);
		$getIdHistory			= '';
		$tgl_diterima			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
			$tgl_diterima		= $getHistoryPermohonan[0]['tgl_diterima'];
		}
		if(!$tgl_diterima){
		$dataHistory	 		= array(
									'tgl_diterima'			=> date('Y-m-d H:i:s'),
									);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		}
		$data['getData']		= site_url('Perusahaan/saveForm/getData');
		$data['update']			= site_url('Archive/saveForm/edit');
		$data['reject']			= site_url('Archive/saveForm/reject');
		$data['approv']			= site_url('Archive/saveForm/approv');
		$data['uploadFile']		= site_url('Archive/uploadImage');
		$data['back']			= site_url('Archive/');
		$this->load->view('archive/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'setuju'){$directto	= $this->saveSetuju();} 
	}
	
}
