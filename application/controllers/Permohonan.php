<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permohonan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_permohonan');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$JabatanId_Session				= $dataSession['jabatan_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Permohonan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan',
										'Status'
										);
		$search							= array(
											"a.activation"			=> 'Y',
											"a.user_management_id"	=> $UserId_Session
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Search Data Permohonan';
		$data['data_search']	= $search;	
		if($JabatanId_Session == 7){
		$data['default']		= $this->M_permohonan->getPermohonan($search,'');
		$data['getData']		= $data['default']['data'];
		} else {
		$data['getData']		= "";	
		}
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Permohonan/autocomplete');
		$data['print'] 			= site_url('Permohonan/cetak/print');
		$data['excell'] 		= site_url('Permohonan/cetak/excell');
		$data['add'] 			= site_url('Permohonan/add/');
		$data['edit'] 			= site_url('Permohonan/edit');
		$data['detail'] 		= site_url('Permohonan/detail');
		$data['cetak_tanda_terima'] 	= site_url('Permohonan/cetak_tanda_terima');
		$data['cetak_sk'] 		= site_url('Permohonan/cetak_sk');
		$data['page_action']	= site_url('Permohonan/');
		$data['back_action']	= site_url('Permohonan/');
		$data['delete']			= site_url('Permohonan/delete');
		$this->load->view('master/permohonan/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Permohonan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Create Permohonan';
		if($LevelId_Session == 8){
		$data['getPerusahaan']	= SingleFilter('m_perusahaan','user_management_id',$UserId_Session);
		$data['getIzin']		= SingleFilter('m_izin','','','izin_name','Asc');
		$data['getIzinValidasi']= site_url('Permohonan/izinValidasi');
		$data['getTypeIzin']	= SingleFilter('m_izin_type','','','type_name','Asc');
		$data['action']			= site_url('Permohonan/saveForm/add');
		$data['getData']		= site_url('Perusahaan/saveForm/getData');
		$data['linkPemohon']	= site_url('Permohonan/saveForm/getPemohon');
		$data['linkPerusahaan']	= site_url('Permohonan/saveForm/getPerusahaan');
		$data['linkAlamatPerusahaan']	= site_url('Permohonan/saveForm/getAlamatPerusahaan');
		$data['linkSyarat']		= site_url('Permohonan/saveForm/getSyarat');
		$data['uploadImage']	= site_url('Permohonan/uploadImage');
		$data['addPerusahaan']	= site_url('Perusahaan/add');
		$data['back']			= site_url('Permohonan/');
		$this->load->view('master/permohonan/add',$data);
		} else {
		$data['getLevel']				= SingleFilter('m_level','','','level_name','asc');
		$data['getProvinsi']			= SingleFilter('provinsi','','','nama','asc');
		$data['getKabupatenPerusahaan']	= singleFilter('kabupaten','','','nama','asc');
		
		$data['getPerusahaan']			= singleFilter('m_perusahaan','user_management_id',$UserId_Session,'nama_perusahaan','asc');
		$data['getJenisPerusahaan']		= singleFilter('m_perusahaan_type','','','perusahaan_type','asc');
		$data['getJenisUsaha']			= singleFilter('m_jenis_usaha','','','name','asc');
		$data['getLingkungan']			= singleFilter('m_lingkungan_perusahaan','','','name','asc');
		$data['getLokasi']				= singleFilter('m_lokasi_perusahaan','','','name','asc');
		$data['getIndexGangguan']		= singleFilter('m_index_gangguan','','','name','asc');
		$data['getAlatKerja']			= singleFilter('m_alat_kerja');
		
		$data['getTypeIzin']		= SingleFilter('m_izin_type','','','type_name','Asc');
		$data['getIzin']			= SingleFilter('m_izin','','','izin_name','Asc');
		$data['getIzinValidasi']	= site_url('Permohonan/izinValidasi');
		$data['getData']		= site_url('User/saveForm/getData');
		$data['getDataSelect']	= site_url('Perusahaan/saveForm/getDataSelect');
		$data['cekEmail']		= site_url('User/saveForm/cekEmail');
		$data['linkSyarat']		= site_url('Permohonan/saveForm/getSyarat');
		$data['uploadImage']	= site_url('Permohonan/uploadImage');
		$data['action']			= site_url('Permohonan/saveForm/add');
		$data['back']			= site_url('Permohonan/');
		$this->load->view('master/permohonan/add_fo',$data);	
		}
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Permohonan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Edit Permohonan';
		$data['default']		= $this->M_permohonan->getPermohonan($search);
		$data['getData']		= $data['default']['data'];
		$data['getPerusahaan']	= SingleFilter('m_perusahaan','user_management_id',$UserId_Session,'nama_perusahaan','asc');
		if($data['getData']){
			foreach($data['getData'] as $row){
				$id_izin		= $row['izin_id'];
				$izin_type_id	= $row['izin_type_id'];
			}
		}
		$search_syarat			= array(
										'a.izin_id'			=>	$id_izin,
										'a.izin_type_id'	=>	$izin_type_id
										);
		$data['getSyaratPermohonan']	= $this->M_permohonan->getSyaratPermohonan($search_syarat);
		$data['getDataSyarat']			= $data['getSyaratPermohonan']['data'];
		
		$data['getUploadSyaratPermohonan']	= $this->M_permohonan->getPemenuhanSyaratPermohonan($search);
		$data['getDataUploadSyarat']		= $data['getUploadSyaratPermohonan']['data'];
		$data['action']			= site_url('Permohonan/saveForm/edit');
		$data['linkPemohon']	= site_url('Permohonan/saveForm/getPemohon');
		$data['linkPerusahaan']	= site_url('Permohonan/saveForm/getPerusahaan');
		$data['linkSyarat']		= site_url('Permohonan/saveForm/getSyarat');
		$data['uploadFile']		= site_url('Permohonan/uploadImage');
		$data['addPerusahaan']	= site_url('Perusahaan/add');
		$data['back']			= site_url('Permohonan/');
		
		$this->load->view('master/permohonan/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Permohonan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_permohonan->getPermohonanDetail($search);
		$data['getData']		= $data['default']['data'];
		$data['dataSyarat']		= $this->M_permohonan->getPemenuhanSyaratPermohonan($search);
		$data['getDataSyarat']	= $data['dataSyarat']['data'];
		$data['back']			= site_url('Permohonan/');
		$this->load->view('master/permohonan/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function cetak_tanda_terima()
	{
		##Default##
		$data['path_info']				= 'Permohonan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		
		
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_permohonan->getPermohonan($search);
		$data['getData']		= $data['default']['data'];
		$data['dataSyarat']		= $this->M_permohonan->getPemenuhanSyaratPermohonan($search);
		$data['getDataSyarat']	= $data['dataSyarat']['data'];
		$data['back']			= site_url('Permohonan/');
		$this->load->view('master/permohonan/cetak_tanda_terima',$data);
		
	}
	public function cetak_sk()
	{
		##Default##
		$data['path_info']				= 'Permohonan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		
		$sk['siup']			= SingleFilter('cetak_sk','permohonan_id',$id);
		$sk['tdp']			= SingleFilter('cetak_sk_tdp','permohonan_id',$id);
		
		if($sk['siup']){
			foreach($sk['siup'] as $row){
				$sk_id				= $row['sk_id'];
				$izin_id			= $row['izin_id'];
				$izin_category_id	= SingleFilter('m_izin','izin_id',$izin_id);
				if($izin_category_id){
					$izin_category_id	= $izin_category_id[0]['izin_category_id'];
					$izin_category	= SingleFilter('m_izin_category','izin_category_id',$izin_category_id);
					if($izin_category){
						$izin_category_name	= strtolower($izin_category[0]['name']);
					}
				}
			}
			$directto			= site_url('Cetak/cetak_sk_'.$izin_category_name.'/'.$sk_id);
			redirect($directto);
			exit;
		} 
		else if($sk['tdp']){
			foreach($sk['tdp'] as $row){
				$sk_id			= $row['sk_id'];
				$izin_id		= $row['izin_id'];
				$izin_category_id	= SingleFilter('m_izin','izin_id',$izin_id);
				if($izin_category_id){
					$izin_category_id	= $izin_category_id[0]['izin_category_id'];
					$izin_category	= SingleFilter('m_izin_category','izin_category_id',$izin_category_id);
					if($izin_category){
						$izin_category_name	= strtolower($izin_category[0]['name']);
					}
				}
			}
			$directto			= site_url('Cetak/cetak_sk_'.$izin_category_name.'/'.$sk_id);
			redirect($directto);
			exit;
		}
		
	}
	public function pdf($id = null)
	{
		$data['path_info']				= 'Permohonan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		
		
		##Default##
		
		##Costumize##
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_permohonan->getPermohonan($search);
		$data['getData']		= $data['default']['data'];
		$data['dataSyarat']		= $this->M_permohonan->getPemenuhanSyaratPermohonan($search);
		$data['getDataSyarat']	= $data['dataSyarat']['data'];
		$data['back']			= site_url('Permohonan/');
		$this->load->view('template/cetak_tanda_terima',$data);
		$this->email($id);
	}
	function email($id = null){
		$getDataPermohonan				= SingleFilter('m_permohonan','permohonan_id',$id);
		$email 							= '';
		if($getDataPermohonan){
			$user_management_id			= $getDataPermohonan[0]['user_management_id'];
			$qr_code					= $getDataPermohonan[0]['qr_code'];
			$getDataUser				= SingleFilter('m_user_management','user_management_id',$user_management_id);
			if($getDataUser){
			$email						= $getDataUser[0]['email'];
			}
		}
		$subject	= "Tanda Terima Pendaftaran";
		$body			= "
								<div align='center'><img src='".base_url('assets/images/logo/logo.png')."'></div>
								<table align='center' style='width:100%;text-align:center;border-bottom: 1px solid black;'>
									<tr>
										<td style='height:30px;'><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td>DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
									<tr>
										<td>Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
									</tr>
									<tr>
										<td>Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td>PALEMBANG</td>
									</tr>
								</table>
								<p>&nbsp;</p>
								<p>Terima kasih, permohonan anda akan kami proses.</p>
								<p>Kami lampirkan bukti tanda terima atas permohonan yang telah anda ajukan.</p>
								<p>&nbsp;</p>
								<p>IMPERIUM © ".date('Y').". All Rights Reserved. Privacy | Terms</p>
								
							";
		$attach			= $_SERVER['DOCUMENT_ROOT'].root_folder.'/'.path_view_user_upload.'user_'.$user_management_id.'/document/'.$qr_code.'.pdf';
		$sendEmail		= sendEmail($email,$subject,$body,$attach);
		if($sendEmail){
			return "sukses";
		} else {
			return "gagal";
		}
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'getPemohon'){$directto	= $this->getDataPemohon();} 
		if($action == 'getPerusahaan'){$directto	= $this->getDataPerusahaan();} 
		if($action == 'getAlamatPerusahaan'){$directto	= $this->getDataAlamatPerusahaan();} 
		if($action == 'getSyarat'){$directto	= $this->getDataSyarat();} 
		if($action == 'googleMaps'){$directto	= $this->googleMaps();} 
	}
	private function googleMaps(){
		$this->load->library('googlemaps');
		
		$data['getJenisPerusahaan']		= SingleFilter('m_perusahaan_type','','');
		$data['getJenisUsaha']			= SingleFilter('m_jenis_usaha','','');
		$data['getLingkungan']			= SingleFilter('m_lingkungan_perusahaan','','');
		$data['getKecamatan']			= SingleFilter('kecamatan','','');
		$this->load->view('common/google_maps',$data);
	}
	public function izinValidasi(){
		$izin_id		= $this->input->post('izin_id');
		$getData		= '';
		$getData		= singleFilter('m_izin_validasi','izin_id',$izin_id,'izin_id','asc');
		if($getData){
			$value		= $getData[0]['value'];
			echo $value;
		}
	}
	private function getDataAlamatPerusahaan(){
		$perusahaan_id	= $this->input->post('perusahaan_id');
		$getData		= '';
		$getData		= singleFilter('m_alamat_perusahaan','perusahaan_id',$perusahaan_id,'nama_cabang','asc');
		
		if($getData){
			echo json_encode($getData);
		}
	}
	private function getDataPemohon(){
		$this->load->model('M_user');
		$id					= $this->input->post('id');
		$getData			= '';
		$filter				= array(
									'a.user_management_id'	=>	$id
									);
		$getData	= $this->M_user->getUser($filter);
		if($getData){
			echo json_encode($getData);
		}
	}
	private function getDataPerusahaan(){
		$this->load->model('M_perusahaan');
		$alamat_perusahaan_id		= $this->input->post('alamat_perusahaan');
		$getData	= '';
		$filter			= array(
										'a.alamat_perusahaan_id'	=>	$alamat_perusahaan_id
										);
		$getData	= $this->M_perusahaan->getPerusahaan($filter);
		
		if($getData['data']){
			echo json_encode($getData['data']);
		}
	}
	private function getDataSyarat(){
		$izin		= $this->input->post('izin_id');
		$izin_type	= $this->input->post('izin_type_id');
		$getData	= '';
		$filter		= array(
							"izin_id" 		=> $izin,
							"izin_type_id" 	=> $izin_type
							);
		$getData	= $this->M_permohonan->getSyaratPermohonan($filter);
		if($getData['data']){
			echo json_encode($getData['data']);
		} else {
			echo "";
		}
	}
	function QrCode($code = null){
		$this->load->library('ciqrcode');
		$params['data'] = $code;
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = path_upload_qrcode.$code.'.png';
		$qrCode	= $this->ciqrcode->generate($params);
	}
	function uploadImage(){
		$dataSession 			= $this->session->userdata('temp');
		if($dataSession){
		$user_management_id		= $dataSession['user_management_id'];
		$permohonan_id			= $dataSession['permohonan_id'];
		$alamat_perusahaan_id	= $dataSession['alamat_perusahaan_id'];
		$izin_id				= $dataSession['izin_id'];
		$izin_type_id			= $dataSession['izin_type_id'];
		} else {
		$user_management_id		= $this->input->post('user_management_id');
		$permohonan_id			= $this->input->post('permohonan_id');
		$alamat_perusahaan_id	= $this->input->post('alamat_perusahaan_id');
		$izin_id				= $this->input->post('izin_id');
		$izin_type_id			= $this->input->post('izin_type_id');
		}
		if(!file_exists(path_user_upload.'user_'.$user_management_id)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id, 0777);
		}
		if(!file_exists(path_user_upload.'user_'.$user_management_id.'/file_manager')) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id.'/file_manager', 0777);
		}
		
		
		$jml_row_syarat				= $this->input->post('jml_row_syarat');
		for($i = 1;$i <= $jml_row_syarat; $i++){
			$syarat_id					= $this->input->post('syarat_id'.$i);
			$max_size					= $this->input->post('max_size'.$i);
			$format_file				= $this->input->post('format_file'.$i);
			$nmfile 					= random_string(5); //nama file saya beri nama langsung dan diikuti fungsi time
			$config['upload_path'] 		= path_user_upload.'user_'.$user_management_id.'/file_manager/'; //path folder
			$config['allowed_types'] 	= strtolower($format_file); //type yang dapat diakses bisa anda sesuaikan
			$config['max_size'] 		= $max_size * 1000; //maksimum besar file 2M
			$config['file_name'] 		= $nmfile; //nama yang terupload nantinya
			$config['overwrite'] 		= TRUE;
			$this->load->library('upload');
			$this->upload->initialize($config);
		
			if($this->upload->do_upload('upload_file'.$i)){	
				$file		= $this->upload->data();
				$image_name	= str_replace(' ','',$file['raw_name']).$file['file_ext'];
				$data 		= array(
									'permohonan_id'			=> $permohonan_id,
									'alamat_perusahaan_id'	=> $alamat_perusahaan_id,
									'user_management_id'	=> $user_management_id,
									'izin_id'				=> $izin_id,
									'izin_type_id'			=> $izin_type_id,
									'syarat_id'				=> $syarat_id,
									'nama_file'				=> $image_name,
									'ukuran_file'			=> $file['file_size'],
									'path_location'			=> $config['upload_path'].$image_name,
									'activation'			=> 'Y'
								   );
				$update		= Add('pemenuhan_syarat_pemohon',$data);
			} 
		}
			if($jml_row_syarat > 0){
				if(!$update){
					$error				= 13;
					$this->session->set_flashdata('error', $error);	
				}
				else if($update){$this->pdf($permohonan_id);}
			}
				$this->session->unset_userdata('temp');
				$directto			= site_url('Permohonan/');
				redirect($directto);
				exit;
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$izin_category_id		= $this->input->post('izin_category_id');
		if($JabatanId_Session == 7){
			$filter					= array(
											"user_management_id"	=> $UserId_Session,
											"alamat_perusahaan_id" 	=> $this->input->post('pilih_alamat_perusahaan'),
											"izin_id" 				=> $this->input->post('izin'),
											"izin_type_id" 			=> $this->input->post('izin_type')
											);
			$dataPost				= $this->input->post();
			$checkData				= ManyFilter('m_permohonan',$filter);
			if((!$checkData) && ($dataPost)){
				$id_tanda_terima		= Id('m_tanda_terima','tanda_terima_id');
				$code_tanda_terima		= random_string(10);
				$data	 				= array(
											'tanda_terima_id'		=> $id_tanda_terima,
											'code_tanda_terima'		=> $code_tanda_terima,
											'path_location_qr_code'	=> path_view_qrcode.$code_tanda_terima.'.png',
											'activation'			=> 'Y'
											);
				$save					= Add('m_tanda_terima',$data);
				$qr_code				= $this->QrCode($code_tanda_terima);
				$id						= Id('m_permohonan','permohonan_id');
				$code					= $this->input->post('izin_category_id').'.'.$this->input->post('izin').'.'.$this->input->post('izin_type').'.'.$id;
				$dataPermohonan	 				= array(
											'permohonan_id'			=> $id,
											'tanda_terima_id'		=> $id_tanda_terima,
											'code'					=> $code,
											'qr_code'				=> $code_tanda_terima,
											'user_management_id'	=> $UserId_Session,
											'alamat_perusahaan_id'	=> $this->input->post('pilih_alamat_perusahaan'),
											'izin_id' 				=> $this->input->post('izin'),
											'izin_type_id' 			=> $this->input->post('izin_type'),
											'status_level' 			=> '1',
											'status' 				=> 'On Progress',
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session,
											'modified_date' 		=> date('Y-m-d H:i:s'),
											'modified_user' 		=> $UserId_Session,
											'activation' 			=> 'Y'
											);
				$save					= $this->M_permohonan->add('m_permohonan',$dataPermohonan);
				if($izin_category_id == 1){
				$data	 				= array(
											'modal_usaha'		=> $this->input->post('modal_usaha'),
											'kelembagaan'		=> $this->input->post('kelembagaan'),
											'kegiatan_usaha'	=> $this->input->post('kegiatan_usaha'),
											'product'			=> $this->input->post('product')
											);
				$save					= Edit('m_alamat_perusahaan',$data,'alamat_perusahaan_id',$this->input->post('pilih_alamat_perusahaan'));
				}
				$getIdHistory			= Id('history_permohonan','id');
				
				$sendPengirim			= singleFilter('m_jabatan','jabatan_id',$JabatanId_Session);
				$nama_pengirim			= "";
				if($sendPengirim){
				$nama_pengirim			= $sendPengirim[0]['jabatan_name'];
				}
				$getAlurIzin			= grabDataAlurIzin($this->input->post('izin'),$this->input->post('izin_type'));
				$jabatan_id_penerima	= $getAlurIzin[1]['jabatan_id'];
				$nama_penerima			= $getAlurIzin[1]['jabatan_name'];
				$dataHistory	 				= array(
											'id'					=> $getIdHistory,
											'permohonan_id'			=> $id,
											'izin_id'				=> $this->input->post('izin'),
											'izin_type_id'			=> $this->input->post('izin_type'),
											'tgl_mulai'				=> date('Y-m-d H:i:s'),
											'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
											'keterangan_pengirim'	=> 'Mengajukan Permohonan',
											'jabatan_id_pengirim' 	=> $JabatanId_Session	,
											'nama_pengirim' 		=> $nama_pengirim,
											'jabatan_id_penerima' 	=> $jabatan_id_penerima,
											'nama_penerima' 		=> $nama_penerima,
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session,
											'modified_date' 		=> date('Y-m-d H:i:s'),
											'modified_user' 		=> $UserId_Session,
											'activation' 			=> 'Y'
											);
											// echo '<pre>';print_r($dataHistory);exit;
				$save					= Add('history_permohonan',$dataHistory);
				if($save){
					echo "sukses";
				} else {
					$delete					= $this->M_permohonan->delete($id);
					echo "gagal";
				}
			} else {
				echo "duplikat";
			}
		} else {
			$dataPost				= $this->input->post();
			$filterUser					= array(
											"ktp"		=> $this->input->post('ktp'),
											"email" 	=> $this->input->post('email')
											);
			$checkDataUser				= ManyFilter('m_user_management',$filterUser);
			$filterPerusahaan			= array(
											"perusahaan_type_id" 	=> $this->input->post('jenis_perusahaan'),
											"nama_perusahaan" 		=> $this->input->post('nama_perusahaan')
											);
			$checkDataPerusahaan		= ManyFilter('m_perusahaan',$filterPerusahaan);
			if($checkDataUser){
				echo "duplikat user";
			}
			else if($checkDataPerusahaan){
				echo "duplikat perusahaan";
			}
			else if((!$checkDataUser) && (!$checkDataPerusahaan)){
				$idUser					= Id('m_user_management','user_management_id');
				$dataUser	 			= array(
										'user_management_id'	=> $idUser,
										'level_id' 				=> $this->input->post('level_id'),
										'ktp' 					=> $this->input->post('ktp'),
										'npwp' 					=> $this->input->post('npwp'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'username' 				=> $this->input->post('username'),
										'password' 				=> md5(md5($this->input->post('password'))),
										'no_tlp' 				=> $this->input->post('tlp'),
										'address' 				=> $this->input->post('alamat'),
										'rt' 					=> $this->input->post('rt'),
										'rw' 					=> $this->input->post('rw'),
										'provinsi_id' 			=> $this->input->post('provinsi'),
										'kabupaten_id' 			=> $this->input->post('kabupaten'),
										'kecamatan_id' 			=> $this->input->post('kecamatan'),
										'kelurahan_id' 			=> $this->input->post('kelurahan'),
										'created_date' 			=> date('Y-m-d H:i:s'),
										'activation' 			=> 'Y'
										);
				$save					= Add('m_user_management',$dataUser);
				
				$idPerusahaan			= Id('m_perusahaan','perusahaan_id');
				$dataPerusahaan	 		= array(
										'perusahaan_id'			=> $idPerusahaan,
										'user_management_id' 	=> $idUser,
										'perusahaan_type_id' 	=> $this->input->post('jenis_perusahaan'),
										'nama_perusahaan' 		=> $this->input->post('nama_perusahaan'),
										'activation' 			=> 'Y'
										);
				$save					= Add('m_perusahaan',$dataPerusahaan);
				
				$jenis_usaha			= $this->input->post('jenis_usaha');
				if($jenis_usaha){
					$jenis_usaha_lainnya	= $this->input->post('jenis_usaha_lainnya');
					if($jenis_usaha_lainnya){
					$jenis_usaha_id			= Id('m_jenis_usaha','jenis_usaha_id');
					$jenis_usaha			= $jenis_usaha_id;
					
					$data_jenis_usaha	= array(
													'jenis_usaha_id'	=> $jenis_usaha_id,
													'name'				=> strtoupper($jenis_usaha_lainnya),
													'activation'		=> 'Y'
												);
					$save				= Add('m_jenis_usaha',$data_jenis_usaha);
					}
				
				}
				$idAlamatPerusahaan		= Id('m_alamat_perusahaan','alamat_perusahaan_id');
				$type_perusahaan		= $this->input->post('type_perusahaan');
				$nama_cabang			= "";
				if($type_perusahaan == 1){
					$nama_cabang		= "Kantor Pusat";
				}
				else if($type_perusahaan == 2){
					$nama_cabang		= $this->input->post('nama_cabang');
				}
				$dataAlamatPerusahaan	= array(
											'alamat_perusahaan_id'		=> $idAlamatPerusahaan,
											'user_management_id'		=> $idUser,
											'perusahaan_id'				=> $idPerusahaan,
											'status' 					=> $type_perusahaan,
											'nama_cabang' 				=> $nama_cabang,
											'jenis_usaha_id' 			=> $jenis_usaha,
											'lingkungan_perusahaan_id' 	=> $this->input->post('lingkungan_perusahaan'),
											'lokasi_perusahaan_id' 		=> $this->input->post('lokasi_perusahaan'),
											'panjang' 					=> $this->input->post('panjang_perusahaan'),
											'lebar' 					=> $this->input->post('lebar_perusahaan'),
											'luas' 						=> round(($this->input->post('panjang_perusahaan') * $this->input->post('lebar_perusahaan')),1),
											'alamat' 					=> $this->input->post('alamat_perusahaan'),
											'rt' 						=> $this->input->post('rt_perusahaan'),
											'rw' 						=> $this->input->post('rw_perusahaan'),
											'provinsi_id' 				=> $this->input->post('provinsi_perusahaan'),
											'kabupaten_id' 				=> $this->input->post('kabupaten_perusahaan'),
											'kecamatan_id' 				=> $this->input->post('kecamatan_perusahaan'),
											'kelurahan_id' 				=> $this->input->post('kelurahan_perusahaan'),
											'tlp' 						=> $this->input->post('tlp_perusahaan'),
											'lat' 						=> $this->input->post('lat'),
											'lng' 						=> $this->input->post('lng'),
											'created_date' 				=> date('Y-m-d H:i:s'),
											'created_user' 				=> $UserId_Session,
											'modified_date' 			=> date('Y-m-d H:i:s'),
											'modified_user' 			=> $UserId_Session,
											'activation' 				=> 'Y'
											);
				$save					= Add('m_alamat_perusahaan',$dataAlamatPerusahaan);
				
				$id_tanda_terima		= Id('m_tanda_terima','tanda_terima_id');
				$code_tanda_terima		= random_string(10);
				$dataTandaTerima		= array(
											'tanda_terima_id'		=> $id_tanda_terima,
											'code_tanda_terima'		=> $code_tanda_terima,
											'path_location_qr_code'	=> path_view_qrcode.$code_tanda_terima.'.png',
											'activation'			=> 'Y'
											);
				$save					= Add('m_tanda_terima',$dataTandaTerima);
				
				$qr_code				= $this->QrCode($code_tanda_terima);
				$idPermohonan			= Id('m_permohonan','permohonan_id');
				$code					= $this->input->post('izin_category_id').'.'.$this->input->post('izin').'.'.$this->input->post('izin_type').'.'.$idPermohonan;
				$dataPermohonan	 				= array(
											'permohonan_id'			=> $qr_code,
											'tanda_terima_id'		=> $id_tanda_terima,
											'code'					=> $code,
											'qr_code'				=> $code_tanda_terima,
											'user_management_id'	=> $idUser,
											'alamat_perusahaan_id'	=> $idAlamatPerusahaan,
											'izin_id' 				=> $this->input->post('izin'),
											'izin_type_id' 			=> $this->input->post('izin_type'),
											'status_level' 			=> '1',
											'status' 				=> 'On Progress',
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session,
											'modified_date' 		=> date('Y-m-d H:i:s'),
											'modified_user' 		=> $UserId_Session,
											'activation' 			=> 'Y'
											);
				$save					= $this->M_permohonan->add('m_permohonan',$dataPermohonan);
				
				if($izin_category_id == 1){
				$dataExtend	 				= array(
											'modal_usaha'		=> $this->input->post('modal_usaha'),
											'kelembagaan'		=> $this->input->post('kelembagaan'),
											'kegiatan_usaha'	=> $this->input->post('kegiatan_usaha'),
											'product'			=> $this->input->post('product')
											);
				$save					= Edit('m_alamat_perusahaan',$dataExtend,'alamat_perusahaan_id',$idAlamatPerusahaan);
				}
				$getIdHistory			= Id('history_permohonan','id');
				
				$sendPengirim			= singleFilter('m_jabatan','jabatan_id',7);
				$nama_pengirim			= "";
				if($sendPengirim){
				$nama_pengirim			= $sendPengirim[0]['jabatan_name'];
				}
				$getAlurIzin			= grabDataAlurIzin($this->input->post('izin'),$this->input->post('izin_type'));
				$jabatan_id_penerima	= $getAlurIzin[1]['jabatan_id'];
				$nama_penerima			= $getAlurIzin[1]['jabatan_name'];
				$dataHistory	 				= array(
											'id'					=> $getIdHistory,
											'permohonan_id'			=> $idPermohonan,
											'izin_id'				=> $this->input->post('izin'),
											'izin_type_id'			=> $this->input->post('izin_type'),
											'tgl_mulai'				=> date('Y-m-d H:i:s'),
											'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
											'keterangan_pengirim'	=> 'Mengajukan Permohonan',
											'jabatan_id_pengirim' 	=> 7,
											'nama_pengirim' 		=> $nama_pengirim,
											'jabatan_id_penerima' 	=> $jabatan_id_penerima,
											'nama_penerima' 		=> $nama_penerima,
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session,
											'modified_date' 		=> date('Y-m-d H:i:s'),
											'modified_user' 		=> $UserId_Session,
											'activation' 			=> 'Y'
											);
											// echo '<pre>';print_r($dataHistory);exit;
				$save					= Add('history_permohonan',$dataHistory);
				if($save){
					echo "sukses";
				} else {
					$delete					= $this->M_permohonan->delete($id);
					echo "gagal";
				}
			}
		}
		
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Permohonan/');
		$id						= $this->input->post('syarat_id');
		$data	 				= array(
										'izin_id' 			=> $this->input->post('izin'),
										'izin_type_id' 		=> $this->input->post('izin_type_id'),
										'syarat_name' 		=> $this->input->post('syarat_name'),
										'max_size' 			=> $this->input->post('max_size'),
										'activation' 		=> 'Y'
										);
		$update					= Edit('m_syarat',$data,'syarat_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_permohonan->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
