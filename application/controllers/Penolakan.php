<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penolakan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_penolakan');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Penolakan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Tgl Pendaftaran',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search							= array(
											"a.reject"			=> 1
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Penolakan';
		$data['title2']			= 'Search Data Penolakan';
		$data['data_search']	= $search;	
		$data['getData']		= $this->M_penolakan->getPenolakan($search,'');
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Penolakan/autocomplete');
		$data['print'] 			= site_url('Penolakan/cetak/print');
		$data['excell'] 		= site_url('Penolakan/cetak/excell');
		$data['add'] 			= site_url('Penolakan/add/');
		$data['edit'] 			= site_url('Penolakan/edit');
		$data['detail'] 		= site_url('Penolakan/detail');
		$data['page_action']	= site_url('Penolakan/');
		$data['back_action']	= site_url('Penolakan/');
		$data['delete']			= site_url('Penolakan/delete/');
		$this->load->view('penolakan/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function afterTtd()
	{
		##Default##
		
		$data['path_info']				= 'Penolakan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Tgl Pendaftaran',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search							= array(
											"a.reject"			=> 1
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Penolakan';
		$data['title2']			= 'Search Data Penolakan';
		$data['data_search']	= $search;	
		$data['getData']		= $this->M_penolakan->getPenolakanAfterTtd($search,'');
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Penolakan/autocomplete');
		$data['print'] 			= site_url('Penolakan/cetak/print');
		$data['excell'] 		= site_url('Penolakan/cetak/excell');
		$data['add'] 			= site_url('Penolakan/add/');
		$data['edit'] 			= site_url('Penolakan/edit');
		$data['detail'] 		= site_url('Penolakan/detail');
		$data['page_action']	= site_url('Penolakan/');
		$data['back_action']	= site_url('Penolakan/');
		$data['delete']			= site_url('Penolakan/delete/');
		$this->load->view('penolakan/view_penolakan',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$this->load->model('M_approval');
		$this->load->model('M_archive');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Penolakan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Penolakan';
		$data['title2']			= 'Edit Penolakan';
		$data['getDataPermohonan']		= $this->M_approval->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_approval->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_approval->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_approval->getPemenuhanSyaratPermohonan($search);
		$data['getDataHistory']			= $this->M_archive->getDataHistoryPermohonan($search);
		$data['getJenisUsaha']	= SingleFilter('m_jenis_usaha','','','name','asc');
		$data['getLingkungan']	= SingleFilter('m_lingkungan_perusahaan','','','name','asc');
		$data['getLokasi']		= SingleFilter('m_lokasi_perusahaan','','','name','asc');
		$data['getIndexGangguan']	= SingleFilter('m_index_gangguan','','','name','asc');
		$data['getAlatKerja']	= SingleFilter('m_alat_kerja','','','name','asc');
		$data['getKecamatan']	= SingleFilter('kecamatan','id_kabupaten',1671,'nama','asc');
		$kecamatan_id = '';
		if($data['getKecamatan']){
			foreach($data['getKecamatan'] as $row){
				$kecamatan_id = $row['id'];
			}
		}
		$data['getKelurahan']	= SingleFilter('kelurahan','id_kecamatan',$kecamatan_id,'nama','asc');
		
		$searchHistory			= array(
											"a.permohonan_id"			=> $id
										);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($searchHistory);
		$getIdHistor			= '';
		$tgl_diterima			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
			$tgl_diterima		= $getHistoryPermohonan[0]['tgl_diterima'];
		}
		if(!$tgl_diterima){
		$dataHistory	 		= array(
									'tgl_diterima'			=> date('Y-m-d H:i:s'),
									);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		}
		$data['getRetribusi']	= $this->M_approval->getDataRetribusi($search);
		// echo '<pre>';print_r($data['getRetribusi']);exit;
		$data['getDataSelect']	= site_url('Penolakan/saveForm/getDataSelect');
		$data['getData']		= site_url('Perusahaan/saveForm/getData');
		$data['update']			= site_url('Penolakan/saveForm/edit');
		$data['batal']			= site_url('Penolakan/saveForm/batal');
		$data['tolak']			= site_url('Penolakan/saveForm/tolak');
		$data['setuju']			= site_url('Penolakan/saveForm/setuju');
		$data['signature']		= site_url('Penolakan/signature');
		$data['back']			= site_url('Penolakan/');
		
		$this->load->view('penolakan/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Penolakan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']					= 'Data Penolakan';
		$data['title2']					= 'Edit Penolakan';
		
		$data['dataPenolakanDetail']	= $this->M_penolakan->getPenolakanAfterTtd($search,'');
		$data['getData']				= SingleFilter('m_template_surat','template_surat_id',1);
		$data['dataSignature']			= $this->M_penolakan->dataSignature($search);
		$data['dataTemplate']	= array();
		if($data['getData']){
			foreach($data['getData'][0] as $row_left=>$row_right){
				$data['dataTemplate'][$row_left]		= $row_right;
				if($row_left == 'content'){
					$data['dataTemplate'][$row_left]	= json_decode($row_right,true);
				}
			}
		}
		
		$isiDataDetail		= array();
		$isiDataSignature	= array();
		if($data['dataPenolakanDetail']){
			foreach($data['dataPenolakanDetail'][0] as $row_left=>$row_right){
				if($row_left == 'tgl_rekomendasi'){
					$row_right = date("d F Y", strtotime($row_right));
				}
				$isiDataDetail[$row_left]	= $row_right;
			}
		
		}
		if($data['dataSignature']){
			foreach($data['dataSignature'][0] as $row_left=>$row_right){
				$isiDataSignature[$row_left]	= $row_right;
			}
		
		}
		// debug($data['dataTemplate']);exit;
		$paramArray		= array(
								"{logo}"				=> '<img src="'.base_url('assets/images/logo/logo.png').'" width="auto" height="70px">',
								"{tgl_sekarang}"		=> date("d F Y", strtotime(date('Y-m-d'))),
								"{nomor}"				=> $isiDataDetail['no_rekomendasi']."/".$isiDataDetail['izin_id']."/DPMPTSP-PPK/2017",
								"{sifat}"				=> "Biasa",
								"{lampiran}"			=> "1 (satu) Berkas",
								"{perihal}"				=> "Pengembalian ".$isiDataDetail['izin_name']." <br> ".$isiDataDetail['initial'].". ".$isiDataDetail['nama_perusahaan'],
								"{no_tanda_terima}"		=> $isiDataDetail['permohonan_id'],
								"{nama_izin}"			=> $isiDataDetail['izin_name'],
								"{nama_pemohon}"		=> $isiDataDetail['first_name'].' '.$isiDataDetail['last_name'],
								"{alamat_pemohon_lengkap}"	=> strtoupper($isiDataDetail['alamat_user']).' RT. '.$isiDataDetail['rt_user'].' RW. '.$isiDataDetail['rw_user'].' KEL. '.$isiDataDetail['kelurahan_user'].' KEC. '.$isiDataDetail['kecamatan_user'].' '.$isiDataDetail['kabupaten_user'].' - '.$isiDataDetail['provinsi_user'],
								"{nama_perusahaan}"		=> $isiDataDetail['nama_perusahaan'],
								"{alamat_perusahaan}"	=> $isiDataDetail['alamat_perusahaan'],
								"{nomor_rekomendasi}"	=> $isiDataDetail['no_rekomendasi'],
								"{tanggal_rekomendasi}"	=> $isiDataDetail['tgl_rekomendasi'],
								"{keterangan}"			=> $isiDataDetail['catatan_rekomendasi'],
								"{nip}"					=> $isiDataSignature['nip'],
								"{nama_pejabat}"		=> $isiDataSignature['fullname_with_gelar'],
								"{nama_jabatan}"		=> $isiDataSignature['jabatan_name'],
								"{signature}"			=> '<img src="'.base_url($isiDataSignature['signature']).'" width="80px" height="80px">'
								);
		if($paramArray){
			foreach($paramArray as $row_left=>$row_right){
				if($data['dataTemplate']['content']){
					$i = 1;
					foreach($data['dataTemplate']['content'] as $row){
						$data['dataTemplate']['content'][$i]['editor']	= str_replace($row_left,$row_right,$row['editor']);
					$i++;
					}
				}
				
			}
		}
		// debug($data['dataTemplate']);exit;
		$data['back']			= site_url('Template/');
		$this->load->view('template/print_pdf',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function signature(){
		$permohonan_id		= $this->uri->segment(3);
		$izin_category_id	= $this->uri->segment(4);
		$data['action']				= site_url('Penolakan/saveForm/saveSignature');
		$data['back']				= site_url('Penolakan/');
		$data['permohonan_id']		= $permohonan_id;
		$data['izin_category_id']	= $izin_category_id;
		$this->load->view('approval/signature',$data);
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'setuju'){$directto	= $this->saveSetuju();} 
		if($action == 'saveSignature'){$directto	= $this->saveSignature();} 
	}
	private function saveSignature(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Approval/');
		$id						= Id('m_signature','signature_id');
		$permohonan_id			= $this->input->post('permohonan_id');
		$izin_category_id		= $this->input->post('izin_category_id');
		
		#Create File Signature#
		if(!file_exists(path_user_upload.'signature/')) {
			$create_folder			= mkdir(path_user_upload.'signature/', 0777);
			}
		
		$nmfile 					= random_string(5);
		$dataImage = $this->input->post('signature');
		
		list($type, $dataImage) = explode(';', $dataImage);
		list(, $dataImage)      = explode(',', $dataImage);
		$dataImage = base64_decode($dataImage);

		file_put_contents(path_user_upload.'signature/'.$nmfile.'.png', $dataImage);
		#Create File Signature#
		
		$data	 				= array(
										'signature_id' 		=> $id,
										'permohonan_id' 	=> $this->input->post('permohonan_id'),
										'signature' 		=> $this->input->post('signature'),
										'path_location' 	=> str_replace('.','',path_user_upload).'signature/'.$nmfile.'.png',
										'level_id' 			=> $LevelId_Session	,
										'created_date' 		=> date('Y-m-d H:i:s'),
										'created_user' 		=> $UserId_Session
										);
		$save					= Add('m_signature',$data);
		
		$directto				= site_url('Penolakan');
		redirect($directto);
		exit;
		
	}
	private function saveSetuju(){
		$this->load->model('M_approval');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Approval/');
		$id						= $this->input->post('permohonan_id');
		$search							= array(
											"a.permohonan_id"			=> $id
										);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistory			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
		}
		$dataHistory	 		= array(
									'tgl_selesai'		=> date('Y-m-d H:i:s'),
									'status_approval'	=> 1,
									'catatan_approval'	=> $this->input->post('catatan'),
									'modified_date' 	=> date('Y-m-d H:i:s'),
									'modified_user' 	=> $UserId_Session,
									);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		
		$getIdHistory			= Id('history_permohonan','id');
		$sendPengirim			= SingleFilter('m_jabatan','jabatan_id',$JabatanId_Session);
		$nama_pengirim			= "";
		if($sendPengirim){
		$nama_pengirim			= $sendPengirim[0]['jabatan_name'];
		}
		$jabatan_id_penerima	= 1;
		$nama_penerima			= "Fo";
		$dataHistory	 		= array(
										'id'					=> $getIdHistory,
										'permohonan_id'			=> $id,
										'izin_id'				=> $this->input->post('izin_id'),
										'izin_type_id'			=> $this->input->post('izin_type_id'),
										'tgl_mulai'				=> date('Y-m-d H:i:s'),
										'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
										'keterangan_pengirim'	=> 'Berkas penolakan telah ditandatangan, dan siap dicetak oleh '.$nama_penerima,
										'jabatan_id_pengirim' 	=> $JabatanId_Session	,
										'nama_pengirim' 		=> $nama_pengirim,
										'jabatan_id_penerima' 	=> $jabatan_id_penerima,
										'nama_penerima' 		=> $nama_penerima,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'created_user' 			=> $UserId_Session,
										'modified_date' 		=> date('Y-m-d H:i:s'),
										'modified_user' 		=> $UserId_Session,
										'activation' 			=> 'Y'
										);
		
		$save					= Add('history_permohonan',$dataHistory);
		
		if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Penolakan/');
		$id						= $this->input->post('pengaduan_id');
		$data	 				= array(
									'fullname' 					=> $this->input->post('nama'),
									'email' 					=> $this->input->post('email'),
									'subject' 					=> $this->input->post('subject'),
									'content' 					=> $this->input->post('content')
									);
		$update					= Edit('m_pengaduan',$data,'pengaduan_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		
	}
	private function sendReplay(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Penolakan/');
		$dataPost				= $this->input->post();
		if($dataPost){
			$id						= Id('m_pengaduan_replay','pengaduan_replay_id');
			$data	 				= array(
										'pengaduan_replay_id'		=> $id,
										'pengaduan_id' 				=> $this->input->post('pengaduan_id'),
										'jabatan_id' 				=> $JabatanId_Session,
										'admin_id' 					=> $UserId_Session,
										'content' 					=> $this->input->post('replay'),
										'tgl_replay' 				=> date('Y-m-d H:i:s')
										);
			$save					= Add('m_pengaduan_replay',$data);
			$email					= sendEmail($this->input->post('email'),"Balasan Penolakan dengan code penolakan : ".$this->input->post('code'),$this->input->post('replay'));
			if(($save) && ($email)) {
				echo "sukses";
			} else {
				echo "gagal";
			}
		} 
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_penolakan->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
