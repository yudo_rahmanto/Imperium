<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Finance extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_finance');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function Retribusi()
	{
		##Default##
		
		$data['path_info']				= 'Finance';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Perusahaan',
										'Code Permohonan',
										'Nama Izin',
										'Jenis Permohonan',
										'Total Retribusi'
										);
		$search							= array(
											"e.izin_category_id"			=> 2
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Retribusi';
		$data['title2']			= 'Search Data Retribusi';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_finance->getFinanceRetribusi($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Finance/Retribusi/autocomplete');
		$data['print'] 			= site_url('Finance/Retribusi/cetak/print');
		$data['excell'] 		= site_url('Finance/Retribusi/cetak/excell');
		$data['edit'] 			= site_url('Finance/editRetribusi');
		$data['detail'] 		= site_url('Finance/detailRetribusi');
		$data['page_action']	= site_url('Finance/Retribusi/');
		$data['back_action']	= site_url('Finance/Retribusi/');
		$data['delete']			= site_url('Finance/Retribusi/delete/');
		$this->load->view('finance/retribusi/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Finance';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Retribusi';
		$data['title2']			= 'Create New Retribusi';
		$data['action']			= site_url('Finance/saveForm/addRetribusi');
		$data['back']			= site_url('Finance/Retribusi');
		$this->load->view('finance/retribusi/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function editRetribusi()
	{
		##Default##
		$this->load->model('M_approval');
		$data['path_info']				= 'Finance';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.retribusi_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Retribusi';
		$data['title2']			= 'Edit Retribusi';
		
		$data['default']		= $this->M_finance->getFinanceRetribusi($search);
		$data['getData']		= $data['default']['data'];
		$permohonan_id			= '';
		if($data['getData']){
			foreach($data['getData'] as $row){
				$permohonan_id	= $row['permohonan_id'];
			}
		}
		if($permohonan_id){
			$search			= array(
											'a.permohonan_id'	=>	$permohonan_id
											);
			$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
			$getIdHistor			= '';
			$tgl_diterima			= '';
			if($getHistoryPermohonan){
				$getIdHistory		= $getHistoryPermohonan[0]['id'];
				$tgl_diterima		= $getHistoryPermohonan[0]['tgl_diterima'];
			}
			if(!$tgl_diterima){
			$dataHistory	 		= array(
										'tgl_diterima'			=> date('Y-m-d H:i:s'),
										);
			$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
			}
		}
		//debug($data['getData']);exit;
		$data['action']			= site_url('Finance/saveForm/editRetribusi');
		$data['back']			= site_url('Finance/Retribusi');
		
		$this->load->view('finance/retribusi/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Finance';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.jenis_usaha_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Finance';
		$data['title2']			= 'Data Detail Jenis Usaha';
		$data['default']		= $this->M_finance->getFinanceRetribusi($search);
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Finance/');
		$this->load->view('finance/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'editRetribusi'){$directto	= $this->saveEditRetribusi();} 
	}
	private function saveEditRetribusi(){
		$this->load->model('M_approval');
		$this->load->model('M_permohonan');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Finance/');
		$id						= $this->input->post('retribusi_id');
		$permohonan_id			= $this->input->post('permohonan_id');
		$original_tgl_bayar 	=  $this->input->post('tgl_bayar');
		$tgl_bayar 				= date("Y-m-d", strtotime($original_tgl_bayar));
		$data	 				= array(
										'tgl_bayar' 			=> $tgl_bayar,
										'no_ref' 				=> $this->input->post('no_ref'),
										'status_pembayaran' 	=> 1
										);
		$update					= Edit('m_retribusi',$data,'retribusi_id',$id);
		
		
		$search					= array(
										"a.permohonan_id"			=> $permohonan_id
										);
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistor			= '';
		$jabatan_id_pengirim	= '';
		$nama_pengirim			= '';
		$status_level			= '';
		if($getHistoryPermohonan){
			$getIdHistory				= $getHistoryPermohonan[0]['id'];
			$jabatan_id_pengirim		= $getHistoryPermohonan[0]['jabatan_id_penerima'];
			$nama_pengirim				= $getHistoryPermohonan[0]['nama_penerima'];
		}
		$getLevel				= SingleFilter('m_level','level_id',$LevelId_Session);
		$nama_level				= "";
		if($getLevel){
			$nama_level			= $getLevel[0]['level_name'];
		}
		$dataHistory	 				= array(
												'nama_penerima'		=> $nama_level,
												'tgl_selesai'		=> date('Y-m-d H:i:s'),
												'catatan_approval'	=> 'Pembayaran Sudah diterima oleh bendahara',
												'status_approval'	=> 1,
												'modified_date' 	=> date('Y-m-d H:i:s'),
												'modified_user' 	=> $UserId_Session,
												);
		$update					= Edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		
		$getIdHistory			= Id('history_permohonan','id');
		$sendPengirim			= SingleFilter('m_jabatan','jabatan_id',$JabatanId_Session);
		
		$getDataPermohonan		= SingleFilter('m_permohonan','permohonan_id',$this->input->post('permohonan_id'));
		if($getDataPermohonan){
			$izin_id			= $getDataPermohonan[0]['izin_id'];
			$izin_type_id		= $getDataPermohonan[0]['izin_type_id'];
			$status_level		= $getDataPermohonan[0]['status_level'] + 1;
		}
		$getAlurIzin			= grabDataAlurIzin($this->input->post('izin_id'),$this->input->post('izin_type_id'));
		$jabatan_id_penerima	= $getAlurIzin[$status_level]['jabatan_id'];
		$nama_penerima			= $getAlurIzin[$status_level]['jabatan_name'];
		$dataPermohonan	 		= array(
										'status' 		=> 'On Progress',
										'status_level' 	=> $status_level
										);
		$update					= Edit('m_permohonan',$dataPermohonan,'permohonan_id',$permohonan_id);
		
		$dataHistory	 		= array(
										'id'					=> $getIdHistory,
										'permohonan_id'			=> $permohonan_id,
										'izin_id'				=> $izin_id,
										'izin_type_id'			=> $izin_type_id,
										'tgl_mulai'				=> date('Y-m-d H:i:s'),
										'tgl_berkas_dikirim'	=> date('Y-m-d H:i:s'),
										'keterangan_pengirim'	=> 'Mengirim berkas ketingkat '.$nama_penerima.' untuk tanda tangan',
										'jabatan_id_pengirim' 	=> $jabatan_id_pengirim	,
										'nama_pengirim' 		=> $nama_level,
										'jabatan_id_penerima' 	=> $jabatan_id_penerima,
										'nama_penerima' 		=> $nama_penerima,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'created_user' 			=> $UserId_Session,
										'modified_date' 		=> date('Y-m-d H:i:s'),
										'modified_user' 		=> $UserId_Session,
										'activation' 			=> 'Y'
										);
		
		$save					= Add('history_permohonan',$dataHistory);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_finance->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
