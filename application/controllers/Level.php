<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_level');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Level';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Level Name'
										);
		$search							= array(
											"activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Level';
		$data['title2']			= 'Search Data Level';
		$data['data_search']	= $search;	
		$data['getData']		= ManyFilter('m_level',$search);
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Level/autocomplete');
		$data['print'] 			= site_url('Level/cetak/print');
		$data['excell'] 		= site_url('Level/cetak/excell');
		$data['add'] 			= site_url('Level/add/');
		$data['edit'] 			= site_url('Level/edit');
		$data['detail'] 		= site_url('Level/detail');
		$data['page_action']	= site_url('Level/');
		$data['back_action']	= site_url('Level/');
		$data['delete']			= site_url('Level/delete/');
		$this->load->view('setting/level/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Level';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Level';
		$data['title2']			= 'Create New Level';
		$data['action']			= site_url('Level/saveForm/add');
		$data['back']			= site_url('Level/');
		$this->load->view('setting/level/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Level';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'level_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Level';
		$data['title2']			= 'Edit Level';
		$data['getData']		= ManyFilter('m_level',$search);
		//debug($data['getData']);exit;
		$data['action']			= site_url('Level/saveForm/edit');
		$data['back']			= site_url('Level/');
		
		$this->load->view('setting/level/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Level';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'level_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Level';
		$data['title2']			= 'Data Detail Level';
		$data['getData']		= ManyFilter('m_level',$search);
		//debug($data['getData']);exit;
		$data['back']			= site_url('Level/');
		$this->load->view('setting/level/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$directto				= site_url('Level/');
		$filter					= array(
										"level_name" => $this->input->post('level_name')
										);
		$dataPost				= $this->input->post();
		$checkData				= ManyFilter('m_level',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_level','level_id');
			$data	 				= array(
										'level_id'			=> $id,
										'level_name' 		=> $this->input->post('level_name'),
										'activation' 		=> 'Y'
										);
			$save					= Add('m_level',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Level/');
		$id						= $this->input->post('level_id');
		$data	 				= array(
										'level_name' 		=> $this->input->post('level_name'),
										);
		$update					= Edit('m_level',$data,'level_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_level->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
