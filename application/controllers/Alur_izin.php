<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alur_izin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_alur_izin');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Alur_izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Izin Name',
										'Izin Type Name'
										);
		$search							= array();
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Alur Izin';
		$data['title2']			= 'Search Data Alur Izin';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_alur_izin->getGroupAlurIzin($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Alur_izin/autocomplete');
		$data['print'] 			= site_url('Alur_izin/cetak/print');
		$data['excell'] 		= site_url('Alur_izin/cetak/excell');
		$data['add'] 			= site_url('Alur_izin/add/');
		$data['edit'] 			= site_url('Alur_izin/edit');
		$data['detail'] 		= site_url('Alur_izin/detail');
		$data['page_action']	= site_url('Alur_izin/');
		$data['back_action']	= site_url('Alur_izin/');
		$data['delete']			= site_url('Alur_izin/delete/');
		$this->load->view('master/alur_izin/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Alur_izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Alur izin';
		$data['title2']			= 'Create New Alur Izin';
		$data['getIzin']		= SingleFilter('m_izin','','','izin_name','Asc');
		$data['getIzinType']	= SingleFilter('m_izin_type','','','type_name','Asc');
		$data['getJabatan']		= SingleFilter('m_jabatan','','','jabatan_name','Asc');
		$data['action']			= site_url('Alur_izin/saveForm/add');
		$data['back']			= site_url('Alur_izin/');
		$this->load->view('master/alur_izin/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Alur_izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$izin_id				= $this->uri->segment(3);
		$izin_type_id			= $this->uri->segment(4);
		$search			= array(
										'a.izin_id'			=>	$izin_id,
										'a.izin_type_id'	=>	$izin_type_id
										);
		
		$data['title1']			= 'Data Alur Izin';
		$data['title2']			= 'Edit Alur Izin';
		$data['default']		= $this->M_alur_izin->getAlurIzin($search);
		$data['getData']		= $data['default']['data'];
		$data['getIzin']		= SingleFilter('m_izin');
		$data['getIzinType']	= SingleFilter('m_izin_type');
		$data['getJabatan']		= SingleFilter('m_jabatan','','','jabatan_name','Asc');
		// debug($data['getData']);exit;
		$data['action']			= site_url('Alur_izin/saveForm/edit');
		$data['back']			= site_url('Alur_izin/');
		
		$this->load->view('master/alur_izin/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Alur_izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$izin_id				= $this->uri->segment(3);
		$izin_type_id			= $this->uri->segment(4);
		$search			= array(
										'a.izin_id'			=>	$izin_id,
										'a.izin_type_id'	=>	$izin_type_id
										);
		
		$data['title1']			= 'Data Alur Izin';
		$data['title2']			= 'Data Detail Alur Izin';
		$data['default']		= $this->M_alur_izin->getAlurIzin($search);
		$data['getData']		= $data['default']['data'];
		$data['getIzin']		= SingleFilter('m_izin');
		$data['getIzinType']	= SingleFilter('m_izin_type');
		$data['getJabatan']		= SingleFilter('m_jabatan','','','jabatan_name','Asc');
		//debug($data['getData']);exit;
		$data['back']			= site_url('Alur_izin/');
		$this->load->view('master/alur_izin/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Alur_izin/');
		$dataPost				= $this->input->post();
		if($dataPost){
			$jml_row				= $this->input->post('jml_row');
			for($i = 1; $i <= $jml_row; $i++){
				$filter					= array(
										"izin_id" => $this->input->post('izin'),
										"izin_type_id" => $this->input->post('izin_type'),
										"jabatan_id" => $this->input->post('jabatan_'.$i)
										);
				$checkData				= ManyFilter('m_alur_izin',$filter);
				if(!$checkData){
					$id						= Id('m_alur_izin','alur_izin_id');
					$data	 				= array(
												'alur_izin_id'		=> $id,
												'izin_id' 			=> $this->input->post('izin'),
												'izin_type_id' 		=> $this->input->post('izin_type'),
												'jabatan_id' 			=> $this->input->post('jabatan_'.$i),
												'no_urut' 			=> $i,
												'created_date' 		=> date('Y-m-d H:i:s'),
												'created_user' 		=> $UserId_Session,
												'activation' 		=> 'Y'
												);
					$save					= Add('m_alur_izin',$data);
				}
			}
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Syarat/');
		$jml_row				= $this->input->post('jml_row');
		$deleteIzin				= $this->M_alur_izin->delete($this->input->post('izin'),$this->input->post('izin_type'));
			for($i = 1; $i <= $jml_row; $i++){
					$id						= $this->input->post('alur_izin_id_'.$i);
					if(!$id){
					$id						= Id('m_alur_izin','alur_izin_id');
					}
					$data	 				= array(
												'alur_izin_id'		=> $id,
												'izin_id' 			=> $this->input->post('izin'),
												'izin_type_id' 		=> $this->input->post('izin_type'),
												'jabatan_id' 		=> $this->input->post('jabatan_'.$i),
												'no_urut' 			=> $i,
												'created_date' 		=> date('Y-m-d H:i:s'),
												'created_user' 		=> $UserId_Session,
												'activation' 		=> 'Y'
												);
					$save					= Add('m_alur_izin',$data);
			
			}
		
		if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		
	}
	public function delete(){
		$izin_id			= $this->input->post('izin_id');
		$izin_type_id		= $this->input->post('izin_type_id');
		$result		= "Failed";
		if(($izin_id) && ($izin_type_id)){
		$delete					= $this->M_alur_izin->delete($izin_id,$izin_type_id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
