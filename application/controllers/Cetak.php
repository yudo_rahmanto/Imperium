<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require_once('./assets/PHPWord-master/src/PhpWord/Autoloader.php');	
// use PhpOffice\PhpWord\Autoloader;
// use PhpOffice\PhpWord\Settings;
// Autoloader::register();
// Settings::loadConfig();
class Cetak extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_cetak');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function sk_siup()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'No SK',
										'Nama Pemilik',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search							= array(
											"d.izin_category_id" 	=> 1,
											"b.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Cetak SK Siup Pemohon';
		$data['title2']			= 'Search Data SK Siup Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_cetak->getCetakSK($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		
		$data['cetak'] 			= site_url('Cetak/cetak_sk_siup');
		$data['back'] 			= site_url('Cetak/sk_siup');
		$data['delete'] 		= site_url('Cetak/sk_siup');
		$this->load->view('cetak/sk/siup/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function sk_ig()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'No SK',
										'Nama Pemilik',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search							= array(
											"d.izin_category_id" 	=> 2,
											"b.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Cetak SK IG Pemohon';
		$data['title2']			= 'Search Data SK IG Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_cetak->getCetakSK($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		
		$data['cetak'] 			= site_url('Cetak/cetak_sk_ig');
		$data['back'] 			= site_url('Cetak/sk_ig');
		$data['delete'] 		= site_url('Cetak/sk_ig');
		$this->load->view('cetak/sk/ig/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function sk_tdp()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'No SK',
										'Nama Pemilik',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search							= array(
											"d.izin_category_id" 	=> 3,
											"b.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Cetak SK TDP Pemohon';
		$data['title2']			= 'Search Data SK TDP Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_cetak->getCetakSK($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		
		$data['cetak'] 			= site_url('Cetak/cetak_sk_tdp');
		$data['back'] 			= site_url('Cetak/sk_tdp');
		$data['delete'] 		= site_url('Cetak/sk_tdp');
		$this->load->view('cetak/sk/tdp/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function spm()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$JabatanId_Session				= $dataSession['jabatan_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Code Permohonan',
										'KTP Pemilik',
										'Nama Pemilik',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search							= array();
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Cetak SPM';
		$data['title2']			= 'Search Data SPM Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_cetak->getSPM($search,'');
		$data['getData']		= $data['default']['data'];
		// debug($data['getData']);exit;
		
		$data['cetak'] 			= site_url('Cetak/cetak_SPM');
		$data['back'] 			= site_url('Cetak/spm');
		$data['delete'] 		= site_url('Cetak/spm');
		$this->load->view('cetak/spm/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function skrd()
	{
		$this->load->model('M_approval');
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$JabatanId_Session				= $dataSession['jabatan_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Code Permohonan',
										'KTP Pemilik',
										'Nama Pemilik',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search					= array();
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Cetak SKRD';
		$data['title2']			= 'Search Data SKRD Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_approval->getApproval($search,'');
		$data['getData']		= $data['default']['data'];
		// debug($data['getData']);exit;
		
		$data['cetak'] 			= site_url('Cetak/cetak_SKRD');
		$data['back'] 			= site_url('Cetak/skrd');
		$data['delete'] 		= site_url('Cetak/skrd');
		$this->load->view('cetak/skrd/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function old_cetak_sk_siup()
	{
		##Default##
		$this->load->model('M_approval');
		$this->load->library('pdf');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.sk_id'	=>	$id
										);
		
		$data['getData']			= $this->M_cetak->getDetailSK($search,'');
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							} else {
							$row_right = date("d F Y", strtotime($row_right));
							$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
		
		}
		
		$data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		$data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		$this->pdf->load_view('template/cetak_sk_siup',$data);
		$this->pdf->render();
		$this->pdf->stream("Surat SK SIUP.pdf");
	}
	public function cetak_sk_siup()
	{
		##Default##
		$this->load->model('M_approval');
		$this->load->library('pdf');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.sk_id'	=>	$id
										);
		
		$data['getTemplate']	= SingleFilter('m_template_surat','template_surat_id',3);
		$data['getData']		= $this->M_cetak->getDetailSK($search,'');
		$data['dataSignature']	= $this->M_cetak->dataSignature($search);
		$data['dataTemplate']	= array();
		if($data['getTemplate']){
			foreach($data['getTemplate'][0] as $row_left=>$row_right){
				$data['dataTemplate'][$row_left]		= $row_right;
				if($row_left == 'content'){
					$data['dataTemplate'][$row_left]	= json_decode($row_right,true);
				}
			}
		}
		$isiDataDetail		= array();
		$isiDataSignature	= array();
		
		if($data['dataSignature']){
			foreach($data['dataSignature'][0] as $row_left=>$row_right){
				$isiDataSignature[$row_left]	= $row_right;
			}
		
		}
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							} else {
							$row_right = date("d F Y", strtotime($row_right));
							$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
				$isiDataDetail	= $data['getData'][0];
		}
		// debug($isiDataDetail);exit;
		$paramArray		= array(
								"{logo}"				=> '<img src="'.base_url('assets/images/logo/logo.png').'" width="auto" height="70px">',
								"{tanggal_sekarang}"	=> date("d F Y", strtotime(date('Y-m-d'))),
								"{nama_izin}"			=> $isiDataDetail['izin_name'],
								"{no_sk}"				=> $isiDataDetail['no_sk'],
								"{year}"				=> date('Y'),
								"{nama_pemohon}"		=> $isiDataDetail['first_name'].' '.$isiDataDetail['last_name'],
								"{alamat_pemohon_lengkap}"	=> strtoupper($isiDataDetail['alamat_user']).' RT. '.$isiDataDetail['rt_user'].' RW. '.$isiDataDetail['rw_user'].' KEL. '.$isiDataDetail['kelurahan_user'].' KEC. '.$isiDataDetail['kecamatan_user'].' '.$isiDataDetail['kabupaten_user'].' - '.$isiDataDetail['provinsi_user'],
								"{no_tlp_pemohon}"		=> $isiDataDetail['no_tlp_user'],
								"{no_npwp_pemohon}"		=> $isiDataDetail['npwp'],
								"{nama_perusahaan}"		=> $isiDataDetail['nama_perusahaan'],
								"{alamat_perusahaan_lengkap}"	=> strtoupper($isiDataDetail['alamat_perusahaan']).' RT. '.$isiDataDetail['rt_perusahaan'].' RW. '.$isiDataDetail['rw_perusahaan'].' KEL. '.$isiDataDetail['kelurahan_perusahaan'].' KEC. '.$isiDataDetail['kecamatan_perusahaan'].' '.$isiDataDetail['kabupaten_perusahaan'].' - '.$isiDataDetail['provinsi_perusahaan'],
								"{modal_usaha_perusahaan}"		=> number_format($isiDataDetail['modal_usaha_perusahaan']),
								"{kegiatan_usaha_perusahaan}"		=> $isiDataDetail['kegiatan_usaha_perusahaan'],
								"{kelembagaan_perusahaan}"		=> $isiDataDetail['kelembagaan_perusahaan'],
								"{jenis_usaha}"			=> $isiDataDetail['jenis_usaha'],
								"{product_perusahaan}"		=> $isiDataDetail['product_perusahaan'],
								"{masa_berlaku}"		=> $isiDataDetail['periode'],
								"{photo_pemohon}"		=> '<img src="'.base_url($isiDataDetail['photo_pemohon']).'" width="3cm" height="4cm">',
								"{qr_code_sk}"			=> '<img src="'.base_url($isiDataDetail['qr_code_sk']).'" width="80px" height="80px">',
								"{nip}"					=> $isiDataSignature['nip'],
								"{nama_pejabat}"		=> $isiDataSignature['fullname_with_gelar'],
								"{nama_jabatan}"		=> $isiDataSignature['jabatan_name'],
								"{signature}"			=> '<img src="'.base_url($isiDataSignature['signature']).'" width="80px" height="80px">'
								);
		if($paramArray){
			foreach($paramArray as $row_left=>$row_right){
				if($data['dataTemplate']['content']){
					$i = 1;
					foreach($data['dataTemplate']['content'] as $row){
						$data['dataTemplate']['content'][$i]['editor']	= str_replace($row_left,$row_right,$row['editor']);
					$i++;
					}
				}
				
			}
		}
		// $data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		// $data['getKepalaDinas']		= SingleFilter('m_admin','jabatan_id',5);
		// $this->pdf->load_view('template/cetak_sk_siup',$data);
		// $this->pdf->render();
		// $this->pdf->stream("Surat SK SIUP.pdf");
		$this->load->view('template/print_pdf',$data);
	}
	public function phpword_cetak_sk_ig()
	{
		##Default##
		
		
		$this->load->model('M_approval');
		
		##Costumize##
		
		$id				= $this->uri->segment(3);
		$search			= array(
								'a.sk_id'	=>	$id
								);
		
		$data['getData']			= $this->M_cetak->getDetailSK($search,'');
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							} else {
							$row_right = date("d F Y", strtotime($row_right));
							$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
		
		}
		$data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		$data['getKepalaDinas']		= SingleFilter('m_admin','jabatan_id',5);
		$code_izin = '';
		if($data['getData']){
			$isiData	= "";
			foreach($data['getData'][0] as $row_left=>$row_right){
				$isiData[$row_left]	= $row_right;
				if($row_left == "izin_name"){
					if(preg_match('/RINGAN/',strtoupper($row_right))){$code_izin = 'IG.R';}
					if(preg_match('/BERAT/',strtoupper($row_right))){$code_izin = 'IG.B';}
					
				}
			}
		}
		if($data['getDataSignature']){
			$isiDataSignature	= "";
			foreach($data['getDataSignature'][0] as $row_left=>$row_right){
				$isiDataSignature[$row_left]	= $row_right;
			}
		}
		if($data['getKepalaDinas']){
			$isiDataKepala	= "";
			foreach($data['getKepalaDinas'][0] as $row_left=>$row_right){
				$isiDataKepala[$row_left]	= $row_right;
			}
			$getLevel		= SingleFilter('m_level','level_id',$isiDataKepala['level_id']);
			if($getLevel){
				$level_name = $getLevel[0]['level_name'];
			}
		}
		
		$data['getDataTemplate']		= SingleFilter('m_template_rtf','template_rtf_id',1);
		if($data['getDataTemplate']){
			$isiDataTemplate	= "";
			foreach($data['getDataTemplate'][0] as $row_left=>$row_right){
				$isiDataTemplate[$row_left]	= $row_right;
			}
		}
		// debug($_SERVER["DOCUMENT_ROOT"].root_folder.'/'.$isiDataTemplate['path_location']);exit;
		$phpWord = new \PhpOffice\PhpWord\PhpWord($_SERVER["DOCUMENT_ROOT"].root_folder.'/'.$isiDataTemplate['path_location']);
		$section = $phpWord->addSection();
		$section->addImage($_SERVER['DOCUMENT_ROOT'].root_folder.'/'.$isiData['qr_code_sk'], array('align' => 'center','width'=>200, 'height'=>200));
		// $section->save($_SERVER["DOCUMENT_ROOT"].root_folder.'/'.path_view_user_upload.'user_'.$isiData['user_management_id'].'/document/'.$filename);
		debug($phpWord);exit;
		
		// echo $_SERVER['DOCUMENT_ROOT'].root_folder.'/'.$isiData['qr_code_sk'];exit;
		$document->setValue("{tgl_sekarang}",date("d F Y", strtotime(date('Y-m-d'))));
		$document->setValue('{code_izin}',$code_izin);
		$document->setValue('{year}',date('Y'));
		$document->setValue("{no_sk}",$isiData['no_sk']);
		$document->setValue("{nama_pemohon}",$isiData['first_name'].' '.$isiData['last_name']);
		$document->setValue("{no_permohonan}",$isiData['no_permohonan']);
		$document->setValue("{tgl_retribusi}",date("d F Y", strtotime($isiData['tgl_retribusi'])));
		$document->setValue("{alamat_pemohon_lengkap}",strtoupper($isiData['alamat_user']).' RT. '.$isiData['rt_user'].' RW. '.$isiData['rw_user'].' KEL. '.$isiData['kelurahan_user'].' KEC. '.$isiData['kecamatan_user'].' '.$isiData['kabupaten_user'].' - '.$isiData['provinsi_user']);
		$document->setValue("{nama_perusahaan}",$isiData['nama_perusahaan']);
		$document->setValue("{alamat_perusahaan_lengkap}",strtoupper($isiData['alamat_perusahaan']).' RT. '.$isiData['rt_perusahaan'].' RW. '.$isiData['rw_perusahaan'].' KEL. '.$isiData['kelurahan_perusahaan'].' KEC. '.$isiData['kecamatan_perusahaan'].' '.$isiData['kabupaten_perusahaan'].' - '.$isiData['provinsi_perusahaan']);
		$document->setValue("{jenis_usaha}",$isiData['jenis_usaha']);
		$document->setValue("{luas_usaha}",number_format($isiData['luas_perusahaan']));
		$document->setValue("{periode}",$isiData['periode']);
		// $section->addImage($_SERVER['DOCUMENT_ROOT'].root_folder.'/'.$isiData['qr_code_sk'], array('width'=>80, 'height'=>80));
		// $document->SetValue("{qr_code_sk}",$section);
		// $document->setValue("{qr_code_sk}",'<img src="'.base_url($isiData['qr_code_sk']).'" width="80px" height="80px">');
		$document->save_image("{qr_code_sk}", $_SERVER['DOCUMENT_ROOT'].root_folder.'/'.$isiData['qr_code_sk']);
		$document->setValue("{signature}",'<img src="'.base_url($isiDataSignature['path_location']).'" width="80px" height="80px">');
		$document->setValue("{periode}",$isiData['periode']);
		$document->setValue("{nama_kepala}",$isiDataKepala['fullname_with_gelar']);
		$document->setValue("{level_kepala}",$level_name);
		$document->setValue("{nip_kepala}",$isiDataKepala['nip']);
		
		// debug($document);exit;
		
		
		$filename = 'IG-'.$isiData['qr_code_permohonan'].'.docx'; 
			
		$document->save($_SERVER["DOCUMENT_ROOT"].root_folder.'/'.path_view_user_upload.'user_'.$isiData['user_management_id'].'/document/'.$filename);
			
		header('location:' . base_url(path_view_user_upload.'user_'.$isiData['user_management_id'].'/document/'.$filename));
		
	}
	public function phpword_basic_cetak_sk_ig()
	{
		##Default##
		
		require_once('./assets/phpword/PHPWord.php');	
		$PHPWord = new PHPWord();
		$section = $PHPWord->createSection();
		$this->load->model('M_approval');
		
		##Costumize##
		
		$id				= $this->uri->segment(3);
		$search			= array(
								'a.sk_id'	=>	$id
								);
		
		$data['getData']			= $this->M_cetak->getDetailSK($search,'');
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							} else {
							$row_right = date("d F Y", strtotime($row_right));
							$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
		
		}
		$data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		$data['getKepalaDinas']		= SingleFilter('m_admin','jabatan_id',5);
		$code_izin = '';
		if($data['getData']){
			$isiData	= "";
			foreach($data['getData'][0] as $row_left=>$row_right){
				$isiData[$row_left]	= $row_right;
				if($row_left == "izin_name"){
					if(preg_match('/RINGAN/',strtoupper($row_right))){$code_izin = 'IG.R';}
					if(preg_match('/BERAT/',strtoupper($row_right))){$code_izin = 'IG.B';}
					
				}
			}
		}
		if($data['getDataSignature']){
			$isiDataSignature	= "";
			foreach($data['getDataSignature'][0] as $row_left=>$row_right){
				$isiDataSignature[$row_left]	= $row_right;
			}
		}
		if($data['getKepalaDinas']){
			$isiDataKepala	= "";
			foreach($data['getKepalaDinas'][0] as $row_left=>$row_right){
				$isiDataKepala[$row_left]	= $row_right;
			}
			$getLevel		= SingleFilter('m_level','level_id',$isiDataKepala['level_id']);
			if($getLevel){
				$level_name = $getLevel[0]['level_name'];
			}
		}
		
		$data['getDataTemplate']		= SingleFilter('m_template_rtf','template_rtf_id',1);
		if($data['getDataTemplate']){
			$isiDataTemplate	= "";
			foreach($data['getDataTemplate'][0] as $row_left=>$row_right){
				$isiDataTemplate[$row_left]	= $row_right;
			}
		}
		// debug($_SERVER["DOCUMENT_ROOT"].root_folder.'/'.$isiDataTemplate['path_location']);exit;
		$document = $PHPWord->loadTemplate($_SERVER["DOCUMENT_ROOT"].root_folder.'/'.$isiDataTemplate['path_location']);
		
		// echo $_SERVER['DOCUMENT_ROOT'].root_folder.'/'.$isiData['qr_code_sk'];exit;
		$document->setValue("{tgl_sekarang}",date("d F Y", strtotime(date('Y-m-d'))));
		$document->setValue('{code_izin}',$code_izin);
		$document->setValue('{year}',date('Y'));
		$document->setValue("{no_sk}",$isiData['no_sk']);
		$document->setValue("{nama_pemohon}",$isiData['first_name'].' '.$isiData['last_name']);
		$document->setValue("{no_permohonan}",$isiData['no_permohonan']);
		$document->setValue("{tgl_retribusi}",date("d F Y", strtotime($isiData['tgl_retribusi'])));
		$document->setValue("{alamat_pemohon_lengkap}",strtoupper($isiData['alamat_user']).' RT. '.$isiData['rt_user'].' RW. '.$isiData['rw_user'].' KEL. '.$isiData['kelurahan_user'].' KEC. '.$isiData['kecamatan_user'].' '.$isiData['kabupaten_user'].' - '.$isiData['provinsi_user']);
		$document->setValue("{nama_perusahaan}",$isiData['nama_perusahaan']);
		$document->setValue("{alamat_perusahaan_lengkap}",strtoupper($isiData['alamat_perusahaan']).' RT. '.$isiData['rt_perusahaan'].' RW. '.$isiData['rw_perusahaan'].' KEL. '.$isiData['kelurahan_perusahaan'].' KEC. '.$isiData['kecamatan_perusahaan'].' '.$isiData['kabupaten_perusahaan'].' - '.$isiData['provinsi_perusahaan']);
		$document->setValue("{jenis_usaha}",$isiData['jenis_usaha']);
		$document->setValue("{luas_usaha}",number_format($isiData['luas_perusahaan']));
		$document->setValue("{periode}",$isiData['periode']);
		// $section->addImage($_SERVER['DOCUMENT_ROOT'].root_folder.'/'.$isiData['qr_code_sk'], array('width'=>80, 'height'=>80));
		// $document->SetValue("{qr_code_sk}",$section);
		// $document->setValue("{qr_code_sk}",'<img src="'.base_url($isiData['qr_code_sk']).'" width="80px" height="80px">');
		// $document->save_image("{qr_code_sk}", $_SERVER['DOCUMENT_ROOT'].root_folder.'/'.$isiData['qr_code_sk']);
		$document->setValue("{signature}",'<img src="'.base_url($isiDataSignature['path_location']).'" width="80px" height="80px">');
		$document->setValue("{periode}",$isiData['periode']);
		$document->setValue("{nama_kepala}",$isiDataKepala['fullname_with_gelar']);
		$document->setValue("{level_kepala}",$level_name);
		$document->setValue("{nip_kepala}",$isiDataKepala['nip']);
		
		// debug($document);exit;
		
		
		$filename = 'IG-'.$isiData['qr_code_permohonan'].'.docx'; 
			
		$document->save($_SERVER["DOCUMENT_ROOT"].root_folder.'/'.path_view_user_upload.'user_'.$isiData['user_management_id'].'/document/'.$filename);
			
		header('location:' . base_url(path_view_user_upload.'user_'.$isiData['user_management_id'].'/document/'.$filename));
		
	}
	
	public function old_cetak_sk_ig()
	{
		##Default##
		$this->load->model('M_approval');
		$this->load->library('pdf');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
								'a.sk_id'	=>	$id
								);
		
		$data['getData']			= $this->M_cetak->getDetailSK($search,'');
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							} else {
							$row_right = date("d F Y", strtotime($row_right));
							$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
		
		}
		// debug($data['getData']);exit;
		$data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		$data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		$this->pdf->load_view('template/cetak_sk_ig',$data);
		$this->pdf->render();
		$this->pdf->stream("Surat SK IG.pdf");
	}
	public function cetak_sk_ig()
	{
		##Default##
		$this->load->model('M_approval');
		$this->load->library('pdf');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
								'a.sk_id'	=>	$id
								);
		$data['getTemplate']	= SingleFilter('m_template_surat','template_surat_id',2);
		$data['getData']		= $this->M_cetak->getDetailSK($search,'');
		$data['dataSignature']	= $this->M_cetak->dataSignature($search);
		
		$data['dataTemplate']	= array();
		if($data['getTemplate']){
			foreach($data['getTemplate'][0] as $row_left=>$row_right){
				$data['dataTemplate'][$row_left]		= $row_right;
				if($row_left == 'content'){
					$data['dataTemplate'][$row_left]	= json_decode($row_right,true);
				}
			}
		}
		$isiDataDetail		= array();
		$isiDataSignature	= array();
		
		if($data['dataSignature']){
			foreach($data['dataSignature'][0] as $row_left=>$row_right){
				$isiDataSignature[$row_left]	= $row_right;
			}
		
		}
		
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							} else {
							$row_right = date("d F Y", strtotime($row_right));
							$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
			$isiDataDetail	= $data['getData'][0];
		}
		if(preg_match('/RINGAN/',$isiDataDetail['izin_name'])){$code_izin = 'IG.R';}
		else if(preg_match('/BERAT/',$isiDataDetail['izin_name'])){$code_izin = 'IG.B';}
		
		$paramArray		= array(
								"{logo}"				=> '<img src="'.base_url('assets/images/logo/logo.png').'" width="auto" height="70px">',
								"{tanggal_sekarang}"	=> date("d F Y", strtotime(date('Y-m-d'))),
								"{code_izin}"			=> $code_izin,
								"{izin_id}"				=> $isiDataDetail['izin_id'],
								"{no_sk}"				=> $isiDataDetail['no_sk'],
								"{year}"				=> date('Y'),
								"{nama_pemohon}"		=> $isiDataDetail['first_name'].' '.$isiDataDetail['last_name'],
								"{alamat_pemohon_lengkap}"	=> strtoupper($isiDataDetail['alamat_user']).' RT. '.$isiDataDetail['rt_user'].' RW. '.$isiDataDetail['rw_user'].' KEL. '.$isiDataDetail['kelurahan_user'].' KEC. '.$isiDataDetail['kecamatan_user'].' '.$isiDataDetail['kabupaten_user'].' - '.$isiDataDetail['provinsi_user'],
								"{nama_perusahaan}"		=> $isiDataDetail['nama_perusahaan'],
								"{alamat_perusahaan_lengkap}"	=> strtoupper($isiDataDetail['alamat_perusahaan']).' RT. '.$isiDataDetail['rt_perusahaan'].' RW. '.$isiDataDetail['rw_perusahaan'].' KEL. '.$isiDataDetail['kelurahan_perusahaan'].' KEC. '.$isiDataDetail['kecamatan_perusahaan'].' '.$isiDataDetail['kabupaten_perusahaan'].' - '.$isiDataDetail['provinsi_perusahaan'],
								"{jenis_usaha}"			=> $isiDataDetail['jenis_usaha'],
								"{luas_perusahaan}"		=> $isiDataDetail['luas_perusahaan'],
								"{masa_berlaku}"		=> $isiDataDetail['periode'],
								"{qr_code_sk}"			=> '<img src="'.base_url($isiDataDetail['qr_code_sk']).'" width="80px" height="80px">',
								"{nip}"					=> $isiDataSignature['nip'],
								"{nama_pejabat}"		=> $isiDataSignature['fullname_with_gelar'],
								"{nama_jabatan}"		=> $isiDataSignature['jabatan_name'],
								"{signature}"			=> '<img src="'.base_url($isiDataSignature['signature']).'" width="80px" height="80px">'
								);
		if($paramArray){
			foreach($paramArray as $row_left=>$row_right){
				if($data['dataTemplate']['content']){
					$i = 1;
					foreach($data['dataTemplate']['content'] as $row){
						$data['dataTemplate']['content'][$i]['editor']	= str_replace($row_left,$row_right,$row['editor']);
					$i++;
					}
				}
				
			}
		}
		
		// $data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		// $data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		// $this->pdf->load_view('template/cetak_sk_ig',$data);
		// $this->pdf->render();
		// $this->pdf->stream("Surat SK IG.pdf");
		$this->load->view('template/print_pdf',$data);
	}
	public function old_cetak_sk_tdp()
	{
		##Default##
		$this->load->model('M_approval');
		$this->load->library('pdf');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
								'a.sk_id'	=>	$id
								);
		
		$data['getData']			= $this->M_cetak->getDetailSK($search,'');
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							}
							else if(in_array($row_left,array('awal_masa_berlaku','akhir_masa_berlaku','tgl_pengesahan'))){
								$row_right = date("d F Y", strtotime($row_right));
								$data['getData'][0][$row_left]	= $row_right;
							} else {
								$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
			
		}
		// debug($data['getData']);exit;
		$data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		$data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		$this->pdf->load_view('template/cetak_sk_tdp',$data);
		$this->pdf->render();
		$this->pdf->stream("Surat SK TDP.pdf");
	}
	public function cetak_sk_tdp()
	{
		##Default##
		$this->load->model('M_approval');
		$this->load->library('pdf');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
								'a.sk_id'	=>	$id
								);
		$data['getTemplate']	= SingleFilter('m_template_surat','template_surat_id',4);
		$data['getData']		= $this->M_cetak->getDetailSK($search,'');
		$data['dataSignature']	= $this->M_cetak->dataSignature($search);
		$data['dataTemplate']	= array();
		if($data['getTemplate']){
			foreach($data['getTemplate'][0] as $row_left=>$row_right){
				$data['dataTemplate'][$row_left]		= $row_right;
				if($row_left == 'content'){
					$data['dataTemplate'][$row_left]	= json_decode($row_right,true);
				}
			}
		}
		$isiDataDetail		= array();
		$isiDataSignature	= array();
		
		if($data['dataSignature']){
			foreach($data['dataSignature'][0] as $row_left=>$row_right){
				$isiDataSignature[$row_left]	= $row_right;
			}
		
		}
		if($data['getData']){
				foreach($data['getData'] as $row){
					$permohonan_id	= $row['permohonan_id'];
					$sk_detail		= json_decode($row['sk_detail'],true);
					if($sk_detail){
						foreach($sk_detail as $row_left=>$row_right){
							if($row_left == 'is_permanen'){
								$data['getData'][0][$row_left]	= $row_right;
							}
							else if(in_array($row_left,array('awal_masa_berlaku','akhir_masa_berlaku','tgl_pengesahan'))){
								$row_right = date("d F Y", strtotime($row_right));
								$data['getData'][0][$row_left]	= $row_right;
							} else {
								$data['getData'][0][$row_left]	= $row_right;
							}
						}
						if($data['getData'][0]['is_permanen']){
							$data['getData'][0]['periode']	= "Permanen";
						} else {
							$data['getData'][0]['periode']	= $data['getData'][0]['awal_masa_berlaku'].' s/d '.$data['getData'][0]['akhir_masa_berlaku'];
						}
					}
				}
			$isiDataDetail	= $data['getData'][0];
		}
		// debug($isiDataDetail);exit;
		$catatan_izin	= '';
		if($isiDataDetail['izin_category_id'] == 3){
			$catatan_izin	= " <br> DAN UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 25 TAHUN 1992 TENTANG PERKOPERASIAN";
		}
		$paramArray		= array(
								"{logo}"				=> '<img src="'.base_url('assets/images/logo/logo.png').'" width="auto" height="70px">',
								"{tanggal_sekarang}"	=> date("d F Y", strtotime(date('Y-m-d'))),
								"{keterangan_izin}"		=> strtoupper(str_replace("TANDA DAFTAR PERUSAHAAN","",$isiDataDetail['keterangan_izin'])),
								"{catatan_izin}"		=> $catatan_izin,
								"{no_sk}"				=> $isiDataDetail['no_sk'],
								"{d1}"					=> $isiDataDetail['d1'],
								"{d2}"					=> $isiDataDetail['d2'],
								"{nama_pemohon}"		=> $isiDataDetail['first_name'].' '.$isiDataDetail['last_name'],
								"{nama_perusahaan}"		=> $isiDataDetail['nama_perusahaan'],
								"{status}"				=> $isiDataDetail['status'],
								"{no_tlp_perusahaan}"	=> $isiDataDetail['no_tlp_perusahaan'],
								"{alamat_perusahaan_lengkap}"	=> strtoupper($isiDataDetail['alamat_perusahaan']).' RT. '.$isiDataDetail['rt_perusahaan'].' RW. '.$isiDataDetail['rw_perusahaan'].' KEL. '.$isiDataDetail['kelurahan_perusahaan'].' KEC. '.$isiDataDetail['kecamatan_perusahaan'].' '.$isiDataDetail['kabupaten_perusahaan'].' - '.$isiDataDetail['provinsi_perusahaan'],
								"{kegiatan_usaha_perusahaan}"			=> $isiDataDetail['kegiatan_usaha_perusahaan'],
								"{klui}"				=> $isiDataDetail['klui'],
								"{no_pengesahan_menkop}" => $isiDataDetail['no_pengesahan_menkop'],
								"{tgl_pengesahan}" 		=> $isiDataDetail['tgl_pengesahan'],
								"{masa_berlaku}"		=> $isiDataDetail['periode'],
								"{qr_code_sk}"			=> '<img src="'.base_url($isiDataDetail['qr_code_sk']).'" width="80px" height="80px">',
								"{nip}"					=> $isiDataSignature['nip'],
								"{nama_pejabat}"		=> $isiDataSignature['fullname_with_gelar'],
								"{nama_jabatan}"		=> $isiDataSignature['jabatan_name'],
								"{signature}"			=> '<img src="'.base_url($isiDataSignature['signature']).'" width="80px" height="80px">'
								);
		if($paramArray){
			foreach($paramArray as $row_left=>$row_right){
				if($data['dataTemplate']['content']){
					$i = 1;
					foreach($data['dataTemplate']['content'] as $row){
						$data['dataTemplate']['content'][$i]['editor']	= str_replace($row_left,$row_right,$row['editor']);
					$i++;
					}
				}
				
			}
		}
		// debug($data['getData']);exit;
		// $data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$permohonan_id);
		// $data['getKepalaDinas']		= SingleFilter('m_admin','jabatan_id',5);
		// $this->pdf->load_view('template/cetak_sk_tdp',$data);
		// $this->pdf->render();
		// $this->pdf->stream("Surat SK TDP.pdf");
		$this->load->view('template/print_pdf',$data);
	}
	public function cetak_SPM($id = null)
	{
		$this->load->model('M_approval');
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		$getRetribusi			= SingleFilter('m_retribusi','permohonan_id',$id);
		$retribusi_id			= "";	 
		$jml_print				= 1;
		if($getRetribusi){
			$retribusi_id		= $getRetribusi[0]['retribusi_id'];
			$params				= array(
										"jenis_print"	=> "SPM",
										"id"			=> $retribusi_id
										);
			$getLog				= ManyFilter('log_print',$params);
			if($getLog){
				$log_id			= $getLog[0]['log_id'];
				$jml_print		= $getLog[0]['jml_print'] + 1;
				$dataLog		= array(
										"jml_print"		=> $jml_print
										);
				$edit			= Edit('log_print',$dataLog,'log_id',$log_id);
			} else {
				$dataLog		= array(
										"log_id"		=> Id("log_print","log_id"),
										"jenis_print"	=> "SPM",
										"id"			=> $retribusi_id,
										"jml_print"		=> $jml_print
										);
				$add			= Add('log_print',$dataLog);
			}
		}
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_approval->getApproval($search);
		$data['getData']		= $data['default']['data'];
		$data['getDataRetribusi']	= $this->M_approval->getDataRetribusi($search);
		$data['getDataSignature']	= SingleFilter('m_admin','jabatan_id',5);
		$data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		$data['back']			= site_url('Permohonan/');
		
		$this->load->view('cetak/spm/cetak_spm',$data);
	}
	public function cetak_SKRD($id = null)
	{
		$this->load->model('M_approval');
		
		$search			= array(
								'a.permohonan_id'	=>	$id
								);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_approval->getApproval($search);
		$data['getData']		= $data['default']['data'];
		$data['getDataRetribusi']	= $this->M_approval->getDataRetribusi($search);
		$data['getKepalaDinas']	= SingleFilter('m_admin','jabatan_id',5);
		$data['getDataSignature']	= SingleFilter('m_signature','permohonan_id',$id);
		
		$this->load->view('cetak/skrd/cetak_skrd',$data);
	}
	public function edit()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Edit Pemohon';
		$data['getDataPermohonan']		= $this->M_cetak->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_cetak->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_cetak->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_cetak->getPemenuhanSyaratPermohonan($search);
		$data['getDataNoSK']			= $this->M_cetak->getDataNoSK();
		$data['cetak_sk']		= site_url('Surat/saveForm/cetak_sk');
		$data['action']			= site_url('Surat/saveForm/edit');
		$data['back']			= site_url('Surat/sk/');
		$this->load->view('surat/sk/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	
	public function detail()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Edit Pemohon';
		$data['getDataSK']				= $this->M_cetak->getDataSK($search);
		$data['getDataPermohonan']		= $this->M_cetak->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_cetak->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_cetak->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_cetak->getPemenuhanSyaratPermohonan($search);
		$data['getDataNoSK']			= $this->M_cetak->getDataNoSK();
		$data['cetak_sk']		= site_url('Surat/saveForm/cetak_sk');
		$data['action']			= site_url('Approval/saveForm/edit');
		$data['back']			= site_url('Surat/');
		$this->load->view('surat/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'setuju'){$directto	= $this->saveSetuju();} 
	}
	function QrCode($name = null,$code = null){
		$this->load->library('ciqrcode');
		$params['data'] = $code;
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = path_upload_qrcode.$name.'.png';
		$qrCode	= $this->ciqrcode->generate($params);
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Surat/');
		$permohonan_id			= $this->input->post('permohonan_id');
		$perusahaan_id			= $this->input->post('perusahaan_id');
		$getDataSK				= grabDataDetail('m_sk','sk_id',$this->input->post('no_sk'));
		$codeSK					= '';
		$nameBarcode			= random_string(10);
		if($getDataSK){
			$codeSK				= $getDataSK['no_sk'];
		}
		$data	 				= array(
										'kegiatan_usaha' 			=> $this->input->post('kegiatan_usaha'),
										'modal_usaha' 				=> $this->input->post('modal_usaha'),
										'kelembagaan' 				=> $this->input->post('kelembagaan'),
										'product' 					=> $this->input->post('product'),
										'modified_date' 			=> date('Y-m-d H:i:s'),
										'modified_user' 			=> $UserId_Session
										);
		$update					= $this->M_cetak->edit('m_perusahaan',$data,'perusahaan_id',$perusahaan_id);
		$data	 				= array(
										'status_sk' 				=> 1
										);
		$update					= $this->M_cetak->edit('m_permohonan',$data,'permohonan_id',$permohonan_id);
		
		$data	 				= array(
										'status' 				=> 1
										);
		$update					= $this->M_cetak->edit('m_sk',$data,'sk_id',$this->input->post('no_sk'));
		
		$cetak_sk_id			= Id('cetak_sk','cetak_sk_id');
		
		$original_start_date =  $this->input->post('start_date');
		$start_date = date("Y-m-d", strtotime($original_start_date));
		
		$original_end_date =  $this->input->post('end_date');
		$end_date = date("Y-m-d", strtotime($original_end_date));
		$data	 				= array(
										'cetak_sk_id' 				=> $cetak_sk_id,
										'permohonan_id' 			=> $permohonan_id,
										'izin_id' 					=> $this->input->post('izin_id'),
										'izin_type_id' 				=> $this->input->post('izin_type_id'),
										'user_management_id' 		=> $this->input->post('user_management_id'),
										'sk_id' 					=> $this->input->post('no_sk'),
										'awal_masa_berlaku' 		=> $start_date,
										'akhir_masa_berlaku' 		=> $end_date,
										'path_location_barcode' 	=> path_view_qrcode.$nameBarcode.'.png',
										);
		$update					= $this->M_cetak->add('cetak_sk',$data);
		$qr_code				= $this->QrCode($nameBarcode,$codeSK);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
	}
	
}
