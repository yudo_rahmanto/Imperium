<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_surat');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function sk_siup()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Perusahaan',
										'Code Permohonan',
										'Nama Izin',
										'Jenis Permohonan',
										'Status Permohonan'
										);
		$search							= array(
											"d.izin_category_id"=> 1,
											"a.status_finish"	=> 1,
											"a.activation"		=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data SK Siup Pemohon';
		$data['title2']			= 'Search Data SK Siup Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_surat->getSuratSK($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		
		$data['add'] 		= site_url('Surat/add');
		$data['edit'] 		= site_url('Surat/edit_siup');
		$data['delete'] 	= site_url('Surat/delete');
		$data['back'] 		= site_url('Surat/edit');
		$this->load->view('surat/sk/siup/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function sk_ig()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Perusahaan',
										'Code Permohonan',
										'Nama Izin',
										'Jenis Permohonan',
										'Status Permohonan'
										);
		$search							= array(
											"d.izin_category_id"=> 2,
											"a.status_finish"	=> 1,
											"a.status"			=> 'Finish',
											"a.activation"		=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data SK IG Pemohon';
		$data['title2']			= 'Search Data SK IG Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_surat->getSuratSK($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		
		$data['add'] 		= site_url('Surat/add');
		$data['edit'] 		= site_url('Surat/edit_ig');
		$data['delete'] 	= site_url('Surat/delete');
		$data['back'] 		= site_url('Surat/edit');
		$this->load->view('surat/sk/ig/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function sk_tdp()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Nama Perusahaan',
										'Code Permohonan',
										'Nama Izin',
										'Jenis Permohonan',
										'Status Permohonan'
										);
		$search							= array(
											"d.izin_category_id" => 3,
											"a.status_finish"	 => 1,
											"a.activation"		 => 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data SK TDP Pemohon';
		$data['title2']			= 'Search Data SK TDP Pemohon';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_surat->getSuratSK($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		
		$data['add'] 		= site_url('Surat/add');
		$data['edit'] 		= site_url('Surat/edit_tdp');
		$data['delete'] 	= site_url('Surat/delete');
		$data['back'] 		= site_url('Surat/edit');
		$this->load->view('surat/sk/ig/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function add()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Detail Pemohon';
		$data['getDataPermohonan']		= $this->M_surat->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_surat->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_surat->getDataUser($search);
		
		$data['cetak_sk']		= site_url('Surat/saveForm/cetak_sk');
		$data['setuju']			= site_url('Approval/saveForm/setuju');
		$data['back']			= site_url('Surat/');
		
		$this->load->view('surat/sk/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit_siup()
	{
		##Default##
		$this->load->model('M_approval');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Edit Pemohon';
		
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistor			= '';
		$tgl_diterima			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
			$tgl_diterima		= $getHistoryPermohonan[0]['tgl_diterima'];
		}
		if(!$tgl_diterima){
		$dataHistory	 		= array(
									'tgl_diterima'			=> date('Y-m-d H:i:s'),
									);
		$update					= $this->M_approval->edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		}
		
		
		$data['getDataPermohonan']		= $this->M_surat->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_surat->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_surat->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_surat->getPemenuhanSyaratPermohonan($search);
		$data['getDataNoSK']			= $this->M_surat->getDataNoSK();
		$data['cetak_sk']		= site_url('Surat/saveForm/cetak_sk');
		$data['action']			= site_url('Surat/saveForm/edit_siup');
		$data['back']			= site_url('Surat/sk_siup/');
		$this->load->view('surat/sk/siup/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit_ig()
	{
		##Default##
		$this->load->model('M_approval');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Edit Pemohon';
		
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistor			= '';
		$tgl_diterima			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
			$tgl_diterima		= $getHistoryPermohonan[0]['tgl_diterima'];
		}
		if(!$tgl_diterima){
		$dataHistory	 		= array(
									'tgl_diterima'			=> date('Y-m-d H:i:s'),
									);
		$update					= $this->M_approval->edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		}
		$data['getDataPermohonan']		= $this->M_surat->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_surat->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_surat->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_surat->getPemenuhanSyaratPermohonan($search);
		$data['getDataNoSK']			= $this->M_surat->getDataNoSK();
		$data['cetak_sk']		= site_url('Surat/saveForm/cetak_ig');
		$data['action']			= site_url('Surat/saveForm/edit_ig');
		$data['back']			= site_url('Surat/sk_ig/');
		$data['sendPdf']		= site_url('Surat/pdf/');
		$this->load->view('surat/sk/ig/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit_tdp()
	{
		##Default##
		$this->load->model('M_approval');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Edit Pemohon';
		
		$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
		$getIdHistor			= '';
		$tgl_diterima			= '';
		if($getHistoryPermohonan){
			$getIdHistory		= $getHistoryPermohonan[0]['id'];
			$tgl_diterima		= $getHistoryPermohonan[0]['tgl_diterima'];
		}
		if(!$tgl_diterima){
		$dataHistory	 		= array(
									'tgl_diterima'			=> date('Y-m-d H:i:s'),
									);
		$update					= $this->M_approval->edit('history_permohonan',$dataHistory,'id',$getIdHistory);
		}
		$data['getDataPermohonan']		= $this->M_surat->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_surat->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_surat->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_surat->getPemenuhanSyaratPermohonan($search);
		$data['getDataNoSK']			= $this->M_surat->getDataNoSK();
		$data['cetak_sk']		= site_url('Surat/saveForm/cetak_tdp');
		$data['action']			= site_url('Surat/saveForm/edit_tdp');
		$data['back']			= site_url('Surat/sk_tdp/');
		$this->load->view('surat/sk/tdp/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Surat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.permohonan_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Pemohon';
		$data['title2']			= 'Edit Pemohon';
		$data['getDataSK']				= $this->M_surat->getDataSK($search);
		$data['getDataPermohonan']		= $this->M_surat->getPermohonanDetail($search);
		$data['getDataPerusahaan']		= $this->M_surat->getDataPerusahaan($search);
		$data['getDataUser']			= $this->M_surat->getDataUser($search);
		$data['getDataPersyaratan']		= $this->M_surat->getPemenuhanSyaratPermohonan($search);
		$data['getDataNoSK']			= $this->M_surat->getDataNoSK();
		$data['cetak_sk']		= site_url('Surat/saveForm/cetak_sk');
		$data['action']			= site_url('Approval/saveForm/edit');
		$data['back']			= site_url('Surat/');
		$this->load->view('surat/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function pdf($id = null)
	{
		$this->load->model('M_approval');
		$data['path_info']				= 'Approval';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		
		
		##Default##
		
		##Costumize##
		$search			= array(
										'a.permohonan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Permohonan';
		$data['title2']			= 'Data Detail Permohonan';
		$data['default']		= $this->M_approval->getApproval($search);
		$data['getData']		= $data['default']['data'];
		$data['getDataRetribusi']	= $this->M_approval->getDataRetribusi($search);
		$data['getDataSignature']	= $this->M_approval->getDataCombo('m_signature','permohonan_id',$id);
		$data['back']			= site_url('Permohonan/');
		
		$this->load->view('template/cetak_skrd',$data);
		$email		= $this->email($id);
		$directto			= site_url('Surat/sk_ig');
		redirect($directto);
		exit;		
			
	}
	function email($id = null){
		
		$getDataPermohonan				= $this->M_surat->getDataCombo('m_permohonan','permohonan_id',$id);
		$email 							= '';
		if($getDataPermohonan){
			$user_management_id			= $getDataPermohonan[0]['user_management_id'];
			$qr_code					= $getDataPermohonan[0]['qr_code'];
			$getDataUser				= $this->M_surat->getDataCombo('m_user_management','user_management_id',$user_management_id);
			if($getDataUser){
				$email					= $getDataUser[0]['email'];
			}
		}
		 
		$subject		= "Surat Pemberitahuan Surat Keterangan Sudah Siap";
		$body			= "
								<div align='center'><img src='".base_url('assets/images/logo/logo.png')."'></div>
								<table align='center' style='width:100%;text-align:center;border-bottom: 1px solid black;'>
									<tr>
										<td style='height:30px;'><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td>DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
									<tr>
										<td>Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
									</tr>
									<tr>
										<td>Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td>PALEMBANG</td>
									</tr>
								</table>
								<p>&nbsp;</p>
								<p>Bersama dengan email ini, kami memberitahukan bahwa surat keterangan anda sudah siap dicetak</p>
								<p>&nbsp;</p>
								<p>IMPERIUM © ".date('Y').". All Rights Reserved. Privacy | Terms</p>
								
							";
		$attach			= $_SERVER['DOCUMENT_ROOT'].'imperium_new/'.path_view_user_upload.'user_'.$user_management_id.'/document/skrd_'.$qr_code.'.pdf';
		$sendEmail		= sendEmail($email,$subject,$body,$attach);
		if($this->email->send()){
			return "sukses";
		} else {
			return "gagal";
		}
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit_siup'){$directto	= $this->saveEdit_siup();} 
		if($action == 'edit_ig'){$directto	= $this->saveEdit_ig();} 
		if($action == 'edit_tdp'){$directto	= $this->saveEdit_tdp();} 
		if($action == 'setuju'){$directto	= $this->saveSetuju();} 
	}
	function QrCode($name = null,$code = null){
		$this->load->library('ciqrcode');
		$params['data'] = $code;
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = path_upload_qrcode.$name.'.png';
		$qrCode	= $this->ciqrcode->generate($params);
	}
	private function saveEdit_siup(){
		$this->load->model('M_approval');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Surat/');
		$params					= array(
										"user_management_id"	=> $this->input->post('user_management_id'),
										"alamat_perusahaan_id"	=> $this->input->post('alamat_perusahaan_id'),
										"permohonan_id"			=> $this->input->post('permohonan_id'),
										"izin_category_id"		=> $this->input->post('izin_category_id'),
										"izin_id"				=> $this->input->post('izin_id'),
										"izin_type_id"			=> $this->input->post('izin_type_id')
									);
		$checkDataSK			= ManyFilter('m_sk',$params);
		if(!$checkDataSK){
			$permohonan_id			= $this->input->post('permohonan_id');
			$perusahaan_id			= $this->input->post('perusahaan_id');
			$codeSK					= $this->M_surat->getIdSK($this->input->post('izin_category_id'));
			$nameBarcode			= random_string(10);
			$qr_code				= $this->QrCode($nameBarcode,$codeSK);
			$data	 				= array(
											'status_sk' 				=> 1
											);
			$update					= $this->M_surat->edit('m_permohonan',$data,'permohonan_id',$permohonan_id);
			$sk_id					= Id('m_sk','sk_id');
			$dataSK	 				= array(
											'sk_id' 				=> $sk_id,
											'user_management_id' 	=> $this->input->post('user_management_id'),
											'izin_category_id' 		=> $this->input->post('izin_category_id'),
											'izin_id' 				=> $this->input->post('izin_id'),
											'izin_type_id' 			=> $this->input->post('izin_type_id'),
											'permohonan_id' 		=> $permohonan_id,
											'alamat_perusahaan_id' 	=> $this->input->post('alamat_perusahaan_id'),
											'no_sk' 				=> $codeSK,
											'status' 				=> 0,
											'path_location_barcode' => path_view_qrcode.$nameBarcode.'.png',
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session
											);
			$addSK					= Add('m_sk',$dataSK);
			
			$idSKDetail				= Id('m_sk_detail','sk_detail_id');
			
			$original_start_date =  $this->input->post('start_date');
			$start_date = date("Y-m-d", strtotime(str_replace('/', '-',$original_start_date)));
			
			$original_end_date =  $this->input->post('end_date');
			$end_date = date("Y-m-d", strtotime(str_replace('/', '-', $original_end_date)));
			
			$dataToJs	 			= array(
											'is_permanen' 				=> $this->input->post('permanen'),
											'awal_masa_berlaku' 		=> $start_date,
											'akhir_masa_berlaku' 		=> $end_date,
											
											);
											
			$dataSKDetail			= array(
											"sk_detail_id"	=> Id('m_sk_detail','sk_detail_id'),
											'sk_id'			=> $sk_id,
											'data_detail'	=> json_encode($dataToJs)
											);
			$addSKDetail			= Add('m_sk_detail',$dataSKDetail);
			$search					= array(
											"a.permohonan_id"			=> $permohonan_id
											);
			$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
			$getIdHistor			= '';
			if($getHistoryPermohonan){
				$getIdHistory		= $getHistoryPermohonan[0]['id'];
			}
			$dataIzin				= SingleFilter('m_izin','izin_id',$this->input->post('izin_id'));
			$izin_name				= '';
			if($dataIzin){
				$izin_name			= $dataIzin[0]['izin_name'];
			}
			$dataHistory	 				= array(
													'tgl_selesai'		=> date('Y-m-d H:i:s'),
													'catatan_approval'	=> 'SK '.$izin_name.' Sudah Siap dicetak',
													'status_approval'	=> 1,
													'modified_date' 	=> date('Y-m-d H:i:s'),
													'modified_user' 	=> $UserId_Session,
													);
			$update					= $this->M_approval->edit('history_permohonan',$dataHistory,'id',$getIdHistory);
			
			if($update){
					$this->email($permohonan_id);
					echo "sukses";
				} else {
					echo "gagal";
				}
		} else {
			echo "duplikat";
		}
	}
	private function saveEdit_ig(){
		$this->load->model('M_approval');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Surat/');
		
		$params					= array(
										"user_management_id"	=> $this->input->post('user_management_id'),
										"alamat_perusahaan_id"	=> $this->input->post('alamat_perusahaan_id'),
										"permohonan_id"			=> $this->input->post('permohonan_id'),
										"izin_category_id"		=> $this->input->post('izin_category_id'),
										"izin_id"				=> $this->input->post('izin_id'),
										"izin_type_id"			=> $this->input->post('izin_type_id')
									);
		$checkDataSK			= ManyFilter('m_sk',$params);
		if(!$checkDataSK){
			$permohonan_id			= $this->input->post('permohonan_id');
			$perusahaan_id			= $this->input->post('perusahaan_id');
			$codeSK					= $this->M_surat->getIdSK($this->input->post('izin_category_id'));
			$nameBarcode			= random_string(10);
			$qr_code				= $this->QrCode($nameBarcode,$codeSK);
			$data	 				= array(
											'status_sk' 				=> 1
											);
			$update					= $this->M_surat->edit('m_permohonan',$data,'permohonan_id',$permohonan_id);
			$sk_id					= Id('m_sk','sk_id');
			$dataSK	 				= array(
											'sk_id' 				=> $sk_id,
											'user_management_id' 	=> $this->input->post('user_management_id'),
											'izin_category_id' 		=> $this->input->post('izin_category_id'),
											'izin_id' 				=> $this->input->post('izin_id'),
											'izin_type_id' 			=> $this->input->post('izin_type_id'),
											'permohonan_id' 		=> $permohonan_id,
											'alamat_perusahaan_id' 	=> $this->input->post('alamat_perusahaan_id'),
											'no_sk' 				=> $codeSK,
											'status' 				=> 0,
											'path_location_barcode' => path_view_qrcode.$nameBarcode.'.png',
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session
											);
			$addSK					= Add('m_sk',$dataSK);
			
			$idSKDetail				= Id('m_sk_detail','sk_detail_id');
			
			$original_start_date =  $this->input->post('start_date');
			$start_date = date("Y-m-d", strtotime(str_replace('/', '-',$original_start_date)));
			
			$original_end_date =  $this->input->post('end_date');
			$end_date = date("Y-m-d", strtotime(str_replace('/', '-', $original_end_date)));
			
			$dataToJs	 			= array(
											'is_permanen' 				=> $this->input->post('permanen'),
											'awal_masa_berlaku' 		=> $start_date,
											'akhir_masa_berlaku' 		=> $end_date,
											
											);
			$dataSKDetail			= array(
											"sk_detail_id"	=> Id('m_sk_detail','sk_detail_id'),
											'sk_id'			=> $sk_id,
											'data_detail'	=> json_encode($dataToJs)
											);
			$addSKDetail			= Add('m_sk_detail',$dataSKDetail);
			
			
			$search					= array(
											"a.permohonan_id"			=> $permohonan_id
											);
			$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
			$getIdHistor			= '';
			if($getHistoryPermohonan){
				$getIdHistory		= $getHistoryPermohonan[0]['id'];
			}
			$dataIzin				= SingleFilter('m_izin','izin_id',$this->input->post('izin_id'));
			$izin_name				= '';
			if($dataIzin){
				$izin_name			= $dataIzin[0]['izin_name'];
			}
			$dataHistory	 				= array(
													'tgl_selesai'		=> date('Y-m-d H:i:s'),
													'catatan_approval'	=> 'SK '.$izin_name.' Sudah Siap dicetak',
													'status_approval'	=> 1,
													'modified_date' 	=> date('Y-m-d H:i:s'),
													'modified_user' 	=> $UserId_Session,
													);
			$update					= $this->M_approval->edit('history_permohonan',$dataHistory,'id',$getIdHistory);
			
			if($update){
					echo "sukses";
				} else {
					echo "gagal";
				}
		} else {
			echo "duplikat";
		}
	}
	private function saveEdit_tdp(){
		$this->load->model('M_approval');
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Surat/');
		$params					= array(
										"user_management_id"	=> $this->input->post('user_management_id'),
										"alamat_perusahaan_id"	=> $this->input->post('alamat_perusahaan_id'),
										"permohonan_id"			=> $this->input->post('permohonan_id'),
										"izin_category_id"		=> $this->input->post('izin_category_id'),
										"izin_id"				=> $this->input->post('izin_id'),
										"izin_type_id"			=> $this->input->post('izin_type_id')
									);
		$checkDataSK			= ManyFilter('m_sk',$params);
		if(!$checkDataSK){
			$permohonan_id			= $this->input->post('permohonan_id');
			$perusahaan_id			= $this->input->post('perusahaan_id');
			$codeSK					= $this->M_surat->getIdSK($this->input->post('izin_category_id'));
			$nameBarcode			= random_string(10);
			$qr_code				= $this->QrCode($nameBarcode,$codeSK);
			$data	 				= array(
											'status_sk' 				=> 1
											);
			$update					= $this->M_surat->edit('m_permohonan',$data,'permohonan_id',$permohonan_id);
			$sk_id					= Id('m_sk','sk_id');
			$dataSK	 				= array(
											'sk_id' 				=> $sk_id,
											'user_management_id' 	=> $this->input->post('user_management_id'),
											'izin_category_id' 		=> $this->input->post('izin_category_id'),
											'izin_id' 				=> $this->input->post('izin_id'),
											'izin_type_id' 			=> $this->input->post('izin_type_id'),
											'permohonan_id' 		=> $permohonan_id,
											'alamat_perusahaan_id' 	=> $this->input->post('alamat_perusahaan_id'),
											'no_sk' 				=> $codeSK,
											'status' 				=> 0,
											'path_location_barcode' => path_view_qrcode.$nameBarcode.'.png',
											'created_date' 			=> date('Y-m-d H:i:s'),
											'created_user' 			=> $UserId_Session
											);
			$addSK					= Add('m_sk',$dataSK);
			
			$idSKDetail				= Id('m_sk_detail','sk_detail_id');
			
			$original_tgl_pengesahan	=  $this->input->post('tgl_pengesahan');
			$tgl_pengesahan 			= date("Y-m-d", strtotime(str_replace('/', '-',$original_tgl_pengesahan)));
			
			$original_start_date =  $this->input->post('start_date');
			$start_date = date("Y-m-d", strtotime(str_replace('/', '-',$original_start_date)));
			
			$original_end_date =  $this->input->post('end_date');
			$end_date = date("Y-m-d", strtotime(str_replace('/', '-', $original_end_date)));
			$dataToJs	 			= array(
											'is_permanen' 				=> $this->input->post('permanen'),
											'awal_masa_berlaku' 		=> $start_date,
											'akhir_masa_berlaku' 		=> $end_date,
											'no_tdp' 					=> $this->input->post('no_tdp'),
											'no_pengesahan_menkop' 		=> $this->input->post('no_pengesahan_menkop'),
											'tgl_pengesahan' 			=> $tgl_pengesahan,
											'status' 					=> $this->input->post('status'),
											'd1' 						=> $this->input->post('d1'),
											'd2' 						=> $this->input->post('d2'),
											'klui' 						=> $this->input->post('klui'),
											
											);
			$dataSKDetail			= array(
											"sk_detail_id"	=> Id('m_sk_detail','sk_detail_id'),
											'sk_id'			=> $sk_id,
											'data_detail'	=> json_encode($dataToJs)
											);
			$addSKDetail			= Add('m_sk_detail',$dataSKDetail);
			
			$search					= array(
											"a.permohonan_id"			=> $permohonan_id
											);
			$getHistoryPermohonan	= $this->M_approval->getDataHistoryPermohonan($search);
			$getIdHistor			= '';
			if($getHistoryPermohonan){
				$getIdHistory		= $getHistoryPermohonan[0]['id'];
			}
			$dataIzin				= SingleFilter('m_izin','izin_id',$this->input->post('izin_id'));
			$izin_name				= '';
			if($dataIzin){
				$izin_name			= $dataIzin[0]['izin_name'];
			}
			$dataHistory	 				= array(
													'tgl_selesai'		=> date('Y-m-d H:i:s'),
													'catatan_approval'	=> 'SK '.$izin_name.' Sudah Siap dicetak',
													'status_approval'	=> 1,
													'modified_date' 	=> date('Y-m-d H:i:s'),
													'modified_user' 	=> $UserId_Session,
													);
			$update					= $this->M_approval->edit('history_permohonan',$dataHistory,'id',$getIdHistory);
			if($update){
					$this->email($permohonan_id);
					echo "sukses";
				} else {
					echo "gagal";
				}
		} else {
			echo "duplikat";
		}
	}
}
