<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Izin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_izin');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Izin Name',
										'Keterangan'
										);
		$search							= array(
											"a.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Izin';
		$data['title2']			= 'Search Data Izin';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_izin->getIzin($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Izin/autocomplete');
		$data['print'] 			= site_url('Izin/cetak/print');
		$data['excell'] 		= site_url('Izin/cetak/excell');
		$data['add'] 			= site_url('Izin/add/');
		$data['edit'] 			= site_url('Izin/edit/');
		$data['detail'] 		= site_url('Izin/detail/');
		$data['page_action']	= site_url('Izin/');
		$data['back_action']	= site_url('Izin/');
		$data['delete']			= site_url('Izin/delete/');
		$this->load->view('master/izin/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Izin';
		$data['title2']			= 'Create New Izin';
		$data['getIzinCategory']= SingleFilter('m_izin_category');
		$data['action']			= site_url('Izin/saveForm/add');
		$data['back']			= site_url('Izin/');
		$this->load->view('master/izin/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.izin_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Izin';
		$data['title2']			= 'Edit Izin';
		$data['default']		= $this->M_izin->getIzin($search);
		$data['getIzinCategory']= SingleFilter('m_izin_category');
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['action']			= site_url('Izin/saveForm/edit');
		$data['back']			= site_url('Izin/');
		
		$this->load->view('master/izin/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Izin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.izin_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Izin';
		$data['title2']			= 'Data Detail Izin';
		$data['default']		= $this->M_izin->getIzin($search);
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Izin/');
		$this->load->view('master/izin/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$directto				= site_url('Izin/');
		$filter					= array(
										"izin_category_id" 	=> $this->input->post('izin_category'),
										"izin_name"			=> $this->input->post('izin_name')
										);
		$dataPost				= $this->input->post();
		$checkData				= ManyFilter('m_izin',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_izin','izin_id');
			$data	 				= array(
										'izin_id'			=> $id,
										'izin_category_id' 	=> $this->input->post('izin_category'),
										'izin_name' 		=> $this->input->post('izin_name'),
										'durasi_pekerjaan' 	=> $this->input->post('durasi'),
										'keterangan' 		=> $this->input->post('keterangan'),
										'created_date' 		=> date('Y-m-d H:i:s'),
										'created_user' 		=> $UserId_Session,
										'modified_date' 	=> date('Y-m-d H:i:s'),
										'modified_user' 	=> $UserId_Session,
										'activation' 		=> 'Y'
										);
			$save					= Add('m_izin',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Izin/');
		$id						= $this->input->post('izin_id');
		$data	 				= array(
										'izin_category_id' 	=> $this->input->post('izin_category'),
										'izin_name' 		=> $this->input->post('izin_name'),
										'durasi_pekerjaan' 	=> $this->input->post('durasi'),
										'keterangan' 		=> $this->input->post('keterangan'),
										'modified_date' 	=> date('Y-m-d H:i:s'),
										'modified_user' 	=> $UserId_Session,
										'activation' 		=> 'Y'
										);
		$update					= Edit('m_izin',$data,'izin_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_izin->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
