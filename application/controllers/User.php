<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_user');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$JabatanId_Session				= $dataSession['jabatan_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'User';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'NIK',
										'First Name',
										'Last Name',
										'Email',
										'No Tlp'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data User';
		$data['title2']			= 'Search Data User';
		if(($LevelId_Session	 != 1) && ($JabatanId_Session	 != 1)){
		$search							= array(
											"a.activation"			=> 'Y',
											"b.level_id"			=> '8',
											"a.user_management_id"	=> $UserId_Session
										);
		} else {
		$search							= array(
											"b.level_id"			=> '8',
											"a.activation"			=> 'Y'
										);	
		}
		$data['default']		= $this->M_user->getUser($search,'');
		$data['getData']		= $data['default'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('User/autocomplete');
		$data['print'] 			= site_url('User/cetak/print');
		$data['excell'] 		= site_url('User/cetak/excell');
		$data['add'] 			= site_url('User/add/');
		$data['edit'] 			= site_url('User/edit/');
		$data['detail'] 		= site_url('User/detail/');
		$data['page_action']	= site_url('User/');
		$data['back_action']	= site_url('User/');
		$data['delete']			= site_url('User/delete/');
		$this->load->view('setting/user/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		
		// $config['center'] = '37.4419, -122.1419';
		
		$data['path_info']				= 'User';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data User';
		$data['title2']			= 'Create New User';
		$data['getLevel']		= $this->M_user->getDataCombo('m_level','','','level_name','asc');
		$data['getProvinsi']	= $this->M_user->getDataCombo('provinsi','','','nama','asc');
		$data['getData']		= site_url('User/saveForm/getData');
		$data['cekEmail']		= site_url('User/saveForm/cekEmail');
		$data['action']			= site_url('User/saveForm/add');
		$data['uploadImage']	= site_url('User/uploadImage');
		$data['back']			= site_url('User/');
		$this->load->view('setting/user/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'User';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.user_management_id'	=>	$id
										);
		
		$data['title1']			= 'Data User';
		$data['title2']			= 'Edit User';
		$data['default']		= $this->M_user->getUser($search);
		$data['getProvinsi']	= singleFilter('provinsi','','','nama','Asc');
		$provinsi_id = '';
		$kabupaten_id = '';
		$kecamatan_id = '';
		$kelurahan_id = '';
		if($data['default']){
			foreach($data['default'] as $row){
				$provinsi_id = $row['provinsi_id'];
				$kabupaten_id = $row['kabupaten_id'];
				$kecamatan_id = $row['kecamatan_id'];
				$kelurahan_id = $row['kelurahan_id'];
			}
		}
		$data['getKabupaten']	= singleFilter('kabupaten','id_prov',$provinsi_id,'nama','asc');
		$data['getKecamatan']	= singleFilter('kecamatan','id_kabupaten',$kabupaten_id,'nama','asc');
		$data['getKelurahan']	= singleFilter('kelurahan','id_kecamatan',$kecamatan_id,'nama','asc');
		$data['getDataResult']	= $data['default'];
		$data['getLevel']		= singleFilter('m_level','','','level_name','asc');
		$data['getProvinsi']	= singleFilter('provinsi','','','nama','asc');
		// echo '<pre>';print_r($data);exit;
		$data['getData']		= site_url('User/saveForm/getData');
		$data['cekEmail']		= site_url('User/saveForm/cekEmail');
		$data['action']			= site_url('User/saveForm/edit');
		$data['uploadImage']	= site_url('User/uploadImage');
		$data['back']			= site_url('User/');
		
		$this->load->view('setting/user/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'User';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.user_management_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data User';
		$data['title2']			= 'Data Detail User';
		$data['default']		= $this->M_user->getUser($search);
		$data['getProvinsi']	= singleFilter('provinsi','','','nama','Asc');
		$provinsi_id = '';
		$kabupaten_id = '';
		$kecamatan_id = '';
		$kelurahan_id = '';
		if($data['default']){
			foreach($data['default'] as $row){
				$provinsi_id = $row['provinsi_id'];
				$kabupaten_id = $row['kabupaten_id'];
				$kecamatan_id = $row['kecamatan_id'];
				$kelurahan_id = $row['kelurahan_id'];
			}
		}
		$data['getKabupaten']	= singleFilter('kabupaten','id_prov',$provinsi_id);
		$data['getKecamatan']	= singleFilter('kecamatan','id_kabupaten',$kabupaten_id);
		$data['getKelurahan']	= singleFilter('kelurahan','id_kecamatan',$kecamatan_id);
		$data['getDataResult']	= $data['default'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('User/');
		$this->load->view('setting/user/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'getData'){$directto	= $this->getData();} 
		if($action == 'cekEmail'){$directto	= $this->validasiEmail();} 
	}
	private function validasiEmail(){
		$id				= $this->input->post('id');
		$email			= $this->input->post('email');
		$result			= 'Not Ok';
		$filter			= array(
								"email" 		=> $email
								);
		$checkData		= $this->M_user->checkData("m_user_management",$filter);
		if(!$checkData){
				$result = 'Ok';
			} else {
				foreach($checkData as $row){
						if($id == $row['user_management_id']){
							$result = 'Ok';
						}
				}
			}
		echo $result;
	}
	private function getData(){
		$find		= $this->input->post('find');
		$id			= $this->input->post('id');
		$getData	= '';
		if($id){
		$getData	= $this->M_user->getData($find,$id);
		} 
		echo json_encode($getData);
	}
	function uploadImage(){
		$dataSession 			= $this->session->userdata('temp');
		if(!$dataSession){
			$dataSession 		= $this->session->userdata('user_data');
		}
		$getUserId_Session		= $dataSession['user_id'];
		$fullname				= $dataSession['first_name'].' '.$dataSession['last_name'];
		$LevelId_Session				= $dataSession['level_id'];
		
		if(!file_exists(path_user_upload.'user_'.$getUserId_Session)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$getUserId_Session, 0777);
		}
		if(!file_exists(path_user_upload.'user_'.$getUserId_Session.'/image')) {
		$create_folder			= mkdir(path_user_upload.'user_'.$getUserId_Session.'/image', 0777);
		}
		if(!file_exists(path_user_upload.'user_'.$getUserId_Session.'/image/thumbnail')) {
		$create_folder			= mkdir(path_user_upload.'user_'.$getUserId_Session.'/image/thumbnail/', 0777);
		}
		
		$nmfile 					= 'user_'.$getUserId_Session; //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] 		= path_user_upload.'user_'.$getUserId_Session.'/image/'; //path folder
        $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] 		= '5048'; //maksimum besar file 2M
        $config['file_name'] 		= $nmfile; //nama yang terupload nantinya
		$config['overwrite'] 		= TRUE;
		$this->load->library('upload',$config);
        $this->upload->initialize($config);
		
		
			if ($this->upload->do_upload('file_upload')){	
				$gbr		= $this->upload->data();
				$image_name	= $nmfile.$gbr['file_ext'];
				$data 		= array(
									'avatar'	=> $image_name
								   );
								   
				$update		= $this->M_user->edit('m_user_management',$data,'user_management_id',$getUserId_Session);
				
				$create_thumbnail	= $this->create_thumbnail($image_name,200,150);
				if(!$create_thumbnail){
				$error				= 13;
				$this->session->set_flashdata('error', $error);	
				}
				$directto			= site_url('User/');
				redirect($directto);
				exit;
			} else {
				$directto			= site_url('User/');
				redirect($directto);
				exit;
			}
		
	}
	function create_thumbnail($fileName,$width,$height) 
    {
		$dataSession 			= $this->session->userdata('temp');
		if(!$dataSession){
			$dataSession 		= $this->session->userdata('user_data');
		}
		$getUserId_Session		= $dataSession['user_id'];
		$fullname				= $dataSession['first_name'].' '.$dataSession['last_name'];
		$LevelId_Session				= $dataSession['level_id'];
		
		$source_image	= path_user_upload.'user_'.$getUserId_Session.'/image/'.$fileName;
		$new_image		= path_user_upload.'user_'.$getUserId_Session.'/image/thumbnail/'.$fileName;
		$this->load->library('image_lib');
        $config['image_library'] 	= 'gd2';
        $config['source_image'] 	= $source_image;       
        $config['create_thumb'] 	= TRUE;
        $config['maintain_ratio'] 	= TRUE;
        $config['width'] 			= $width;
        $config['height'] 			= $height;
        $config['new_image'] 		= $new_image;               
        $this->image_lib->initialize($config);
        if($this->image_lib->resize())
        { return true; } else { return false; }        
    }
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('User/');
		$filter					= array(
										"ktp" => $this->input->post('ktp'),
										"email" => $this->input->post('email')
										);
		$dataPost				= $this->input->post();
		$checkData				= $this->M_user->checkData('m_user_management',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= $this->M_user->getId();
			$data	 				= array(
										'user_management_id'	=> $id,
										'level_id' 				=> $this->input->post('level_id'),
										'ktp' 					=> $this->input->post('ktp'),
										'npwp' 					=> $this->input->post('npwp'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'username' 				=> $this->input->post('username'),
										'password' 				=> md5(md5($this->input->post('password'))),
										'no_tlp' 				=> $this->input->post('tlp'),
										'address' 				=> $this->input->post('alamat'),
										'rt' 					=> $this->input->post('rt'),
										'rw' 					=> $this->input->post('rw'),
										'provinsi_id' 			=> $this->input->post('provinsi'),
										'kabupaten_id' 			=> $this->input->post('kabupaten'),
										'kecamatan_id' 			=> $this->input->post('kecamatan'),
										'kelurahan_id' 			=> $this->input->post('kelurahan'),
										'created_date' 			=> date('Y-m-d H:i:s'),
										'activation' 			=> 'Y'
										);
			$save					= $this->M_user->add('m_user_management',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session				= $dataSession['user_id'];
		$JabatanId_Session			= $dataSession['jabatan_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('User/');
		$id						= $this->input->post('user_id');
		$password				= $this->input->post('password');
		$data	 				= array(
										'user_management_id'	=> $id,
										'level_id' 				=> $this->input->post('level_id'),
										'ktp' 					=> $this->input->post('ktp'),
										'npwp' 					=> $this->input->post('npwp'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'username' 				=> $this->input->post('username'),
										'no_tlp' 				=> $this->input->post('tlp'),
										'address' 				=> $this->input->post('alamat'),
										'rt' 					=> $this->input->post('rt'),
										'rw' 					=> $this->input->post('rw'),
										'provinsi_id' 			=> $this->input->post('provinsi'),
										'kabupaten_id' 			=> $this->input->post('kabupaten'),
										'kecamatan_id' 			=> $this->input->post('kecamatan'),
										'kelurahan_id' 			=> $this->input->post('kelurahan'),
										'last_modif_date' 		=> date('Y-m-d H:i:s'),
										'activation' 			=> 'Y'
										);
		$dataPassword			= array(
									'password' 				=> md5(md5($password))
									);
		if($password){
			if($JabatanId_Session != 1){
				$array_merger		= array_merge($data,$dataPassword);
			} else {
				$array_merger		= $dataPassword;
			}
		} else {
			if($JabatanId_Session != 1){
				$array_merger		= $data;
			}
		}
		$update					= $this->M_user->edit('m_user_management',$array_merger,'user_management_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_user->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
}
