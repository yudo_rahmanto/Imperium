<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_jabatan');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Jabatan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Jabatan Name'
										);
		$search							= array(
											"activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Jabatan';
		$data['title2']			= 'Search Data Jabatan';
		$data['data_search']	= $search;	
		$data['getData']		= ManyFilter('m_jabatan',$search);
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Jabatan/autocomplete');
		$data['print'] 			= site_url('Jabatan/cetak/print');
		$data['excell'] 		= site_url('Jabatan/cetak/excell');
		$data['add'] 			= site_url('Jabatan/add/');
		$data['edit'] 			= site_url('Jabatan/edit');
		$data['detail'] 		= site_url('Jabatan/detail');
		$data['page_action']	= site_url('Jabatan/');
		$data['back_action']	= site_url('Jabatan/');
		$data['delete']			= site_url('Jabatan/delete/');
		$this->load->view('setting/jabatan/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Jabatan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Jabatan';
		$data['title2']			= 'Create New Jabatan';
		$data['action']			= site_url('Jabatan/saveForm/add');
		$data['back']			= site_url('Jabatan/');
		$this->load->view('setting/jabatan/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Jabatan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'jabatan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Jabatan';
		$data['title2']			= 'Edit Jabatan';
		$data['getData']		= ManyFilter('m_jabatan',$search);
		//debug($data['getData']);exit;
		$data['action']			= site_url('Jabatan/saveForm/edit');
		$data['back']			= site_url('Jabatan/');
		
		$this->load->view('setting/jabatan/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Jabatan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'jabatan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Jabatan';
		$data['title2']			= 'Data Detail Jabatan';
		$data['getData']		= ManyFilter('m_jabatan',$search);
		//debug($data['getData']);exit;
		$data['back']			= site_url('Jabatan/');
		$this->load->view('setting/jabatan/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$directto				= site_url('Jabatan/');
		$filter					= array(
										"jabatan_name" => $this->input->post('jabatan_name')
										);
		$dataPost				= $this->input->post();
		$checkData				= ManyFilter('m_jabatan',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_jabatan','jabatan_id');
			$data	 				= array(
										'jabatan_id'			=> $id,
										'jabatan_name' 		=> $this->input->post('jabatan_name'),
										'activation' 		=> 'Y'
										);
			$save					= Add('m_jabatan',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Jabatan/');
		$id						= $this->input->post('jabatan_id');
		$data	 				= array(
										'jabatan_name' 		=> $this->input->post('jabatan_name'),
										);
		$update					= Edit('m_jabatan',$data,'jabatan_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_jabatan->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
