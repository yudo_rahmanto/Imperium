<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_pengaduan');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Pengaduan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Code Pengaduan',
										'Nama Pengirim',
										'Email',
										'Subject'
										);
		$search							= array(
											"a.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Pengaduan';
		$data['title2']			= 'Search Data Pengaduan';
		$data['data_search']	= $search;	
		$data['getData']		= $this->M_pengaduan->getPengaduan($search,'');
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Pengaduan/autocomplete');
		$data['print'] 			= site_url('Pengaduan/cetak/print');
		$data['excell'] 		= site_url('Pengaduan/cetak/excell');
		$data['add'] 			= site_url('Pengaduan/add/');
		$data['edit'] 			= site_url('Pengaduan/edit');
		$data['detail'] 		= site_url('Pengaduan/detail');
		$data['page_action']	= site_url('Pengaduan/');
		$data['back_action']	= site_url('Pengaduan/');
		$data['delete']			= site_url('Pengaduan/delete/');
		$this->load->view('pengaduan/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function process()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$JabatanId_Session				= $dataSession['jabatan_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Pengaduan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Tgl Pengaduan',
										'Code Pengaduan',
										'Nama Pengirim',
										'Email',
										'Subject'
										);
		$search							= array(
											"a.activation"					=> 'Y',
											// "a.jabatan_id_penerima"			=> $JabatanId_Session
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Pengaduan';
		$data['title2']			= 'Search Data Pengaduan';
		$data['data_search']	= $search;	
		$data['inboxPengaduan']		= $this->M_pengaduan->inboxPengaduan($search,'');
		$data['processPengaduan']	= $this->M_pengaduan->processPengaduan($search,'');
		$data['finishPengaduan']	= $this->M_pengaduan->finishPengaduan($search,'');
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Pengaduan/autocomplete');
		$data['add'] 			= site_url('Pengaduan/add/');
		$data['edit'] 			= site_url('Pengaduan/replay');
		$data['detail'] 		= site_url('Pengaduan/detail');
		$data['terimapengaduan']	= site_url('Pengaduan/saveForm/terimapengaduan');
		$data['selesaipengaduan']	= site_url('Pengaduan/saveForm/selesaipengaduan');
		$data['page_action']	= site_url('Pengaduan/');
		$data['back_action']	= site_url('Pengaduan/');
		$data['delete']			= site_url('Pengaduan/delete/');
		$this->load->view('pengaduan/inbox_view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Pengaduan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Pengaduan';
		$data['title2']			= 'Create New Pengaduan';
		$data['getJabatan']		= SingleFilter('m_jabatan','','','jabatan_name','asc');
		$data['action']			= site_url('Pengaduan/saveForm/add');
		$data['back']			= site_url('Pengaduan/');
		$this->load->view('pengaduan/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Pengaduan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'pengaduan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Pengaduan';
		$data['title2']			= 'Edit Pengaduan';
		$data['getData']		= ManyFilter('m_pengaduan',$search);
		$data['getJabatan']		= SingleFilter('m_jabatan','','','jabatan_name','asc');
		
		
		$data['action']			= site_url('Pengaduan/saveForm/edit');
		$data['back']			= site_url('Pengaduan/');
		
		$this->load->view('pengaduan/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function replay()
	{
		##Default##
		$data['path_info']				= 'Pengaduan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'pengaduan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Pengaduan';
		$data['title2']			= 'Repay Pengaduan';
		$data['getData']		= ManyFilter('m_pengaduan',$search);
		$data['getJabatan']		= SingleFilter('m_jabatan','','','jabatan_name','asc');
		
		
		$data['action']			= site_url('Pengaduan/saveForm/sendReplay');
		$data['back']			= site_url('Pengaduan/process');
		
		$this->load->view('pengaduan/replay',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Pengaduan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'pengaduan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Pengaduan';
		$data['title2']			= 'Edit Pengaduan';
		$data['getData']		= $this->M_pengaduan->getPengaduan($search);
		$data['back']			= site_url('Pengaduan/');
		$this->load->view('pengaduan/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'sendReplay'){$directto	= $this->sendReplay();}
		if($action == 'terimapengaduan'){$directto	= $this->terimaPengaduan();}
		if($action == 'selesaipengaduan'){$directto	= $this->selesaiPengaduan();}
	}
	private function terimaPengaduan(){
		$dataSession			= $this->session->userdata('user_data');
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$pengaduan_id			= $this->input->post('pengaduan_id');
		$return					= "Gagal";
		if($pengaduan_id){
				$updateData		= array(
										"tgl_diterima"	=> date('Y-m-d H:i:s')
										);
				$edit			= Edit('m_pengaduan',$updateData,'pengaduan_id',$pengaduan_id);
				$return			= "Ok";
			
		} 
		echo $return;
	}
	private function selesaiPengaduan(){
		$dataSession			= $this->session->userdata('user_data');
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$pengaduan_id			= $this->input->post('pengaduan_id');
		$return					= "Gagal";
		if($pengaduan_id){
				$updateData		= array(
										"tgl_selesai"		=> date('Y-m-d H:i:s'),
										"status_selesai"	=> 1
										);
				$edit			= Edit('m_pengaduan',$updateData,'pengaduan_id',$pengaduan_id);
				$return			= "Ok";
			
		} 
		echo $return;
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Pengaduan/');
		$filter					= array(
										"fullname" 	=> $this->input->post('nama'),
										"email"	 	=> $this->input->post('email'),
										"subject" 	=> $this->input->post('subject')
										);
		$dataPost				= $this->input->post();
		$checkData				= ManyFilter('m_pengaduan',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_pengaduan','pengaduan_id');
			$data	 				= array(
										'pengaduan_id'			=> $id,
										'code' 					=> random_string(10),
										'fullname' 					=> $this->input->post('nama'),
										'email' 					=> $this->input->post('email'),
										'subject' 					=> $this->input->post('subject'),
										'content' 					=> $this->input->post('content'),
										'jabatan_id_pengirim' 		=> $JabatanId_Session,
										'jabatan_id_penerima' 		=> $this->input->post('send_to'),
										'tgl_posting' 				=> date('Y-m-d H:i:s'),
										'activation' 				=> 'Y'
										);
			$save					= Add('m_pengaduan',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Pengaduan/');
		$id						= $this->input->post('pengaduan_id');
		$data	 				= array(
									'fullname' 					=> $this->input->post('nama'),
									'email' 					=> $this->input->post('email'),
									'subject' 					=> $this->input->post('subject'),
									'content' 					=> $this->input->post('content')
									);
		$update					= Edit('m_pengaduan',$data,'pengaduan_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		
	}
	private function sendReplay(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Pengaduan/');
		$dataPost				= $this->input->post();
		if($dataPost){
			$id						= Id('m_pengaduan_replay','pengaduan_replay_id');
			$data	 				= array(
										'pengaduan_replay_id'		=> $id,
										'pengaduan_id' 				=> $this->input->post('pengaduan_id'),
										'jabatan_id' 				=> $JabatanId_Session,
										'admin_id' 					=> $UserId_Session,
										'content' 					=> $this->input->post('replay'),
										'tgl_replay' 				=> date('Y-m-d H:i:s')
										);
			$save					= Add('m_pengaduan_replay',$data);
			$email					= sendEmail($this->input->post('email'),"Balasan Pengaduan dengan code pengaduan : ".$this->input->post('code'),$this->input->post('replay'));
			if(($save) && ($email)) {
				echo "sukses";
			} else {
				echo "gagal";
			}
		} 
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_pengaduan->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
