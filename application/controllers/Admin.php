<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_admin');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Admin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'NIP',
										'First Name',
										'Last Name',
										'Email',
										'No Tlp'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Admin';
		$data['title2']			= 'Search Data Admin';
		$search							= array(
											"a.activation"	=> 'Y',
											"b.level_id"	=> $LevelId_Session
										);
		
		$data['default']		= $this->M_admin->getAdmin($search,'');
		$data['getData']		= $data['default'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Admin/autocomplete');
		$data['print'] 			= site_url('Admin/cetak/print');
		$data['excell'] 		= site_url('Admin/cetak/excell');
		$data['add'] 			= site_url('Admin/add/');
		$data['edit'] 			= site_url('Admin/edit/');
		$data['detail'] 		= site_url('Admin/detail/');
		$data['page_action']	= site_url('Admin/');
		$data['back_action']	= site_url('Admin/');
		$data['delete']			= site_url('Admin/delete/');
		$this->load->view('setting/admin/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		
		// $config['center'] = '37.4419, -122.1419';
		
		$data['path_info']				= 'Admin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Admin';
		$data['title2']			= 'Create New Admin';
		$data['getJabatan']		= singleFilter('m_jabatan','','','jabatan_name','asc');
		$data['getLevel']		= singleFilter('m_level','','','level_name','asc');
		$data['getData']		= site_url('Admin/saveForm/getData');
		$data['cekNip']			= site_url('Admin/saveForm/cekNip');
		$data['cekEmail']		= site_url('Admin/saveForm/cekEmail');
		$data['action']			= site_url('Admin/saveForm/add');
		$data['uploadImage']	= site_url('Admin/uploadImage');
		$data['back']			= site_url('Admin/');
		$this->load->view('setting/admin/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Admin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id						= $this->uri->segment(3);
		$search					= array(
										'a.admin_id'	=>	$id
										);
		
		$data['title1']			= 'Data Admin';
		$data['title2']			= 'Edit Admin';
		$data['default']		= $this->M_admin->getAdmin($search);
		$data['getDataResult']	= $data['default'];
		$data['getJabatan']		= singleFilter('m_jabatan','','','jabatan_name','asc');
		$data['getLevel']		= singleFilter('m_level','','','level_name','asc');
		// echo '<pre>';print_r($data['getDataResult']);exit;
		$data['getData']		= site_url('Admin/saveForm/getData');
		$data['cekNip']			= site_url('Admin/saveForm/cekNip');
		$data['cekEmail']		= site_url('Admin/saveForm/cekEmail');
		$data['action']			= site_url('Admin/saveForm/edit');
		$data['uploadImage']	= site_url('Admin/uploadImage');
		$data['back']			= site_url('Admin/');
		
		$this->load->view('setting/admin/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Admin';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.admin_id'	=>	$id
										);
		
		$data['title1']			= 'Data Admin';
		$data['title2']			= 'Data Detail User';
		$data['default']		= $this->M_admin->getAdmin($search);
		$data['getProvinsi']	= singleFilter('provinsi','','','nama','asc');
		$data['getDataResult']	= $data['default'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Admin/');
		$this->load->view('setting/admin/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'getData'){$directto	= $this->getData();} 
		if($action == 'cekNip'){$directto	= $this->validasiNip();} 
		if($action == 'cekEmail'){$directto	= $this->validasiEmail();} 
	}
	private function validasiEmail(){
		$id				= $this->input->post('id');
		$email			= $this->input->post('email');
		$result			= 'Not Ok';
		$filter			= array(
								"email" 		=> $email
								);
		$checkDataAdmin		= manyFilter("m_admin",$filter);
		$checkDataUser		= manyFilter("m_user_management",$filter);
		if((!$checkDataAdmin) && (!$checkDataUser)){
				$result = 'Ok';
		} else {
			if($checkDataAdmin){
				foreach($checkDataAdmin as $row){
						if($id == $row['admin_id']){
							$result = 'Ok';
						}
				}
			}
		}
		echo $result;
	}
	private function validasiNip(){
		$nip			= $this->input->post('nip');
		$result			= 'Not Ok';
		$filter			= array(
								"nip" 		=> $nip
								);
		$checkData		= $this->M_admin->checkData("m_admin",$filter);
		if(!$checkData){
				$result = 'Ok';
			} 
		echo $result;
	}
	private function getData(){
		$find		= $this->input->post('find');
		$id			= $this->input->post('id');
		$getData	= '';
		if($id){
		$getData	= $this->M_staff->getData($find,$id);
		} 
		echo json_encode($getData);
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Admin/');
		$filter					= array(
										"nip" => $this->input->post('nip'),
										"email" => $this->input->post('email')
										);
		$dataPost				= $this->input->post();
		
		$checkData				= manyFilter('m_admin',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_admin','admin_id');
			$data	 				= array(
										'admin_id'				=> $id,
										'jabatan_id' 			=> 0,
										'level_id' 				=> $this->input->post('level_id'),
										'nip' 					=> $this->input->post('nip'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'username' 				=> str_replace(' ','_',$this->input->post('first_name')).date('His'),
										'password' 				=> md5(md5($this->input->post('password'))),
										'no_tlp' 				=> $this->input->post('tlp'),
										'created_user' 			=> $UserId_Session,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'activation' 			=> 'Y'
										);
			$save					= $this->M_admin->add('m_admin',$data);
			if($save){
				$email				= $this->input->post('email');
				$fullname			= $this->input->post('first_name').' '.$this->input->post('last_name');
				$username			= $data['username'];
				$password			= $this->input->post('password');
				$this->email($email,$fullname,$username,$password);
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('User/');
		$id						= $this->input->post('admin_id');
		$password				= $this->input->post('password');
		$data	 				= array(
										'level_id' 				=> $this->input->post('level_id'),
										'nip' 					=> $this->input->post('nip'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'no_tlp' 				=> $this->input->post('tlp'),
										'modified_user' 		=> $UserId_Session,
										'modified_date' 		=> date('Y-m-d H:i:s'),
										'activation' 			=> 'Y'
										);
		$dataPassword			= array(
									'password' 				=> md5(md5($password))
									);
		if($password){
			$array_merger		= array_merge($data,$dataPassword);
		} else {
			$array_merger		= $data;
		}
		$update					= Edit('m_admin',$array_merger,'admin_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		
	}
	function email($email = null,$fullname = null,$username = null,$password = null){
		
		$subject	= "REGISTRATION STAFF";
		$body	= "
					<p>Terimakasih Telah Melakukan Pendaftaran </p>
								<table>
									<tr>
										<td colspan='3'>Berikut Adalah Info Akun Anda </td>
									</tr>
									<tr>
										<td>Full Name</td><td>:</td><td>".$fullname."</td>
									</tr>
									<tr>
										<td>Email</td><td>:</td><td>".$email."</td>
									</tr>
									<tr>
										<td>User Name</td><td>:</td><td>".$username."</td>
									</tr>
									<tr>
										<td>Password</td><td>:</td><td>".$password."</td>
									</tr>
								</table>
					<p>IMPERIUM © ".date('Y').". All Rights Reserved. Privacy | Terms</p>
								
							";
		$sendEmail		= sendEmail($email,$subject,$body,'');
		if($sendEmail){
			return "sukses";
		} else {
			return "gagal";
		}
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= DeleteTable('m_admin','admin_id',$id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
}