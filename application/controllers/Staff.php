<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_staff');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$data['path_info']				= 'Staff';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'NIP',
										'First Name',
										'Last Name',
										'Email',
										'Level Name'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Staff';
		$data['title2']			= 'Search Data Staff';
		if($LevelId_Session	 != 1){
		$search							= array(
											"a.activation"	=> 'Y',
											"a.admin_id"	=> $UserId_Session
										);
		} else {
		$search							= array(
											"a.activation"			=> 'Y'
										);	
		}
		$data['default']		= $this->M_staff->getStaff($search,'');
		$data['getData']		= $data['default'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Staff/autocomplete');
		$data['print'] 			= site_url('Staff/cetak/print');
		$data['excell'] 		= site_url('Staff/cetak/excell');
		$data['add'] 			= site_url('Staff/add/');
		$data['edit'] 			= site_url('Staff/edit/');
		$data['detail'] 		= site_url('Staff/detail/');
		$data['page_action']	= site_url('Staff/');
		$data['back_action']	= site_url('Staff/');
		$data['delete']			= site_url('Staff/delete/');
		$this->load->view('setting/staff/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		
		// $config['center'] = '37.4419, -122.1419';
		
		$data['path_info']				= 'Staff';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Staff';
		$data['title2']			= 'Create New Staff';
		$data['getJabatan']		= singleFilter('m_jabatan','','','jabatan_name','asc');
		$data['getLevel']		= singleFilter('m_level','','','level_name','asc');
		$data['getData']		= site_url('Staff/saveForm/getData');
		$data['cekNip']			= site_url('Staff/saveForm/cekNip');
		$data['cekEmail']		= site_url('Staff/saveForm/cekEmail');
		$data['action']			= site_url('Staff/saveForm/add');
		$data['uploadImage']	= site_url('Staff/uploadImage');
		$data['back']			= site_url('Staff/');
		$this->load->view('setting/staff/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Staff';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id						= $this->uri->segment(3);
		$search					= array(
										'a.admin_id'	=>	$id
										);
		
		$data['title1']			= 'Data Staff';
		$data['title2']			= 'Edit User';
		$data['default']		= $this->M_staff->getStaff($search);
		$data['getDataResult']	= $data['default'];
		$data['getJabatan']		= singleFilter('m_jabatan','','','jabatan_name','asc');
		$data['getLevel']		= singleFilter('m_level','','','level_name','asc');
		// echo '<pre>';print_r($data['getDataResult']);exit;
		$data['getData']		= site_url('Staff/saveForm/getData');
		$data['cekNip']			= site_url('Staff/saveForm/cekNip');
		$data['cekEmail']		= site_url('Staff/saveForm/cekEmail');
		$data['action']			= site_url('Staff/saveForm/edit');
		$data['uploadImage']	= site_url('Staff/uploadImage');
		$data['back']			= site_url('Staff/');
		
		$this->load->view('setting/staff/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Staff';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.admin_id'	=>	$id
										);
		
		$data['title1']			= 'Data Staff';
		$data['title2']			= 'Data Detail User';
		$data['default']		= $this->M_staff->getStaff($search);
		$data['getProvinsi']	= singleFilter('provinsi','','','nama','asc');
		$data['getDataResult']	= $data['default'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Staff/');
		$this->load->view('setting/staff/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'getData'){$directto	= $this->getData();} 
		if($action == 'cekNip'){$directto	= $this->validasiNip();} 
		if($action == 'cekEmail'){$directto	= $this->validasiEmail();} 
	}
	private function validasiEmail(){
		$id				= $this->input->post('id');
		$email			= $this->input->post('email');
		$result			= 'Not Ok';
		$filter			= array(
								"email" 		=> $email
								);
		$checkDataAdmin		= manyFilter("m_admin",$filter);
		$checkDataUser		= manyFilter("m_user_management",$filter);
		if((!$checkDataAdmin) && (!$checkDataUser)){
				$result = 'Ok';
		} else {
			if($checkDataAdmin){
				foreach($checkDataAdmin as $row){
						if($id == $row['admin_id']){
							$result = 'Ok';
						}
				}
			}
		}
		echo $result;
	}
	private function validasiNip(){
		$nip			= $this->input->post('nip');
		$result			= 'Not Ok';
		$filter			= array(
								"nip" 		=> $nip
								);
		$checkData		= $this->M_staff->checkData("m_admin",$filter);
		if(!$checkData){
				$result = 'Ok';
			} 
		echo $result;
	}
	private function getData(){
		$find		= $this->input->post('find');
		$id			= $this->input->post('id');
		$getData	= '';
		if($id){
		$getData	= $this->M_staff->getData($find,$id);
		} 
		echo json_encode($getData);
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Staff/');
		$filter					= array(
										"nip" => $this->input->post('nip'),
										"email" => $this->input->post('email')
										);
		$dataPost				= $this->input->post();
		
		$checkData				= manyFilter('m_admin',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_admin','admin_id');
			$data	 				= array(
										'admin_id'				=> $id,
										'jabatan_id' 			=> $this->input->post('jabatan_id'),
										'level_id' 				=> $this->input->post('level_id'),
										'nip' 					=> $this->input->post('nip'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'fullname_with_gelar' 	=> $this->input->post('first_name').' '.$this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'username' 				=> str_replace(' ','_',$this->input->post('first_name')).date('His'),
										'password' 				=> md5(md5($this->input->post('password'))),
										'no_tlp' 				=> $this->input->post('tlp'),
										'created_user' 			=> $UserId_Session,
										'created_date' 			=> date('Y-m-d H:i:s'),
										'activation' 			=> 'Y'
										);
			$save					= $this->M_staff->add('m_admin',$data);
			if($save){
				$email				= $this->input->post('email');
				$fullname			= $this->input->post('first_name').' '.$this->input->post('last_name');
				$username			= $data['username'];
				$password			= $this->input->post('password');
				$this->email($email,$fullname,$username,$password);
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('User/');
		$id						= $this->input->post('admin_id');
		$password				= $this->input->post('password');
		$data	 				= array(
										'jabatan_id' 			=> $this->input->post('jabatan_id'),
										'level_id' 				=> $this->input->post('level_id'),
										'nip' 					=> $this->input->post('nip'),
										'first_name' 			=> $this->input->post('first_name'),
										'last_name' 			=> $this->input->post('last_name'),
										'fullname_with_gelar' 	=> $this->input->post('first_name').' '.$this->input->post('last_name'),
										'email' 				=> $this->input->post('email'),
										'no_tlp' 				=> $this->input->post('tlp'),
										'modified_user' 		=> $UserId_Session,
										'modified_date' 		=> date('Y-m-d H:i:s'),
										'activation' 			=> 'Y'
										);
		$dataPassword			= array(
									'password' 				=> md5(md5($password))
									);
		if($password){
			$array_merger		= array_merge($data,$dataPassword);
		} else {
			$array_merger		= $data;
		}
		$update					= Edit('m_admin',$array_merger,'admin_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		
	}
	function email($email = null,$fullname = null,$username = null,$password = null){
		
		$subject	= "REGISTRATION STAFF";
		$body	= "
					<p>Terimakasih Telah Melakukan Pendaftaran </p>
								<table>
									<tr>
										<td colspan='3'>Berikut Adalah Info Akun Anda </td>
									</tr>
									<tr>
										<td>Full Name</td><td>:</td><td>".$fullname."</td>
									</tr>
									<tr>
										<td>Email</td><td>:</td><td>".$email."</td>
									</tr>
									<tr>
										<td>User Name</td><td>:</td><td>".$username."</td>
									</tr>
									<tr>
										<td>Password</td><td>:</td><td>".$password."</td>
									</tr>
								</table>
					<p>IMPERIUM © ".date('Y').". All Rights Reserved. Privacy | Terms</p>
								
							";
		$sendEmail		= sendEmail($email,$subject,$body,'');
		if($sendEmail){
			return "sukses";
		} else {
			return "gagal";
		}
	}
	function uploadImage(){
		$dataSession 			= $this->session->userdata('temp');
		$getUserId_Session		= $dataSession['admin_id'];
		$fullname				= $dataSession['first_name'].' '.$dataSession['last_name'];
		$LevelId_Session		= $dataSession['level_id'];
		
		if(!file_exists(path_user_upload.'default_signature')) {
		$create_folder			= mkdir(path_user_upload.'default_signature', 0777);
		}
		
		$nmfile 					= $getUserId_Session; //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] 		= path_user_upload.'default_signature/'; //path folder
        $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] 		= '5048'; //maksimum besar file 2M
        $config['file_name'] 		= $nmfile; //nama yang terupload nantinya
		$config['overwrite'] 		= TRUE;
		$this->load->library('upload',$config);
        $this->upload->initialize($config);
		
		
			if ($this->upload->do_upload('file_upload')){	
				$gbr		= $this->upload->data();
				$image_name	= $nmfile.$gbr['file_ext'];
				$data 		= array(
									'ttd'	=> path_view_user_upload.'default_signature/'.$image_name
								   );
								   
				$update		= Edit('m_admin',$data,'admin_id',$getUserId_Session);
				
				$directto			= site_url('Staff/');
				redirect($directto);
				exit;
			} else {
				$directto			= site_url('Staff/');
				redirect($directto);
				exit;
			}
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= DeleteTable('m_admin','admin_id',$id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
}