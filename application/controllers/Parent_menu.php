<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_menu extends CI_Controller {

	function __construct(){
		parent::__construct();	
		$this->cekSession 	= $this->authlogin->check_admin_session();
		$this->load->model('M_parent_menu');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Parent_menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Menu Name',
										'Parent Menu Name'
										);
		$search							= array(
											"a.menu_name"			=> $this->input->post('name'),
											"b.parent_menu_name"	=> $this->input->post('parent_name')
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Parent_menu';
		$data['title2']			= 'Search Data Parent_menu';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_parent_menu->getParentMenu($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Parent_menu/autocomplete');
		$data['print'] 			= site_url('Parent_menu/cetak/print');
		$data['excell'] 		= site_url('Parent_menu/cetak/excell');
		$data['add'] 			= site_url('Parent_menu/add/');
		$data['edit'] 			= site_url('Parent_menu/edit/');
		$data['detail'] 		= site_url('Parent_menu/detail/');
		$data['page_action']	= site_url('Parent_menu/');
		$data['back_action']	= site_url('Parent_menu/');
		$data['delete']			= site_url('Parent_menu/delete/');
		$this->load->view('setting/parent_menu/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Parent_menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['default']		= $this->M_parent_menu->getParentMenu('','');
		$data['getData']		= $data['default']['data'];
		$data['getMenu']		= $this->M_parent_menu->getDataCombo('m_menu');
		$data['title1']			= 'Data Parent_menu';
		$data['title2']			= 'Create New Parent_menu';
		$data['action']			= site_url('Parent_menu/saveForm/add');
		$data['back']			= site_url('Parent_menu/');
		$this->load->view('setting/parent_menu/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Parent_menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'b.sub_menu_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Parent_menu';
		$data['title2']			= 'Edit Parent_menu';
		$data['default']		= $this->M_parent_menu->getParentMenu($search);
		$data['getData']		= $data['default']['data'];
		$data['getMenu']		= $this->M_parent_menu->getDataCombo('m_menu');
		//debug($data['getData']);exit;
		$data['action']			= site_url('Parent_menu/saveForm/edit');
		$data['back']			= site_url('Parent_menu/');
		
		$this->load->view('setting/parent_menu/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Parent_menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'b.sub_menu_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Parent_menu';
		$data['title2']			= 'Data Detail Parent_menu';
		$data['default']		= $this->M_parent_menu->getParentMenu($search);
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Parent_menu/');
		$this->load->view('setting/parent_menu/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$directto				= site_url('Parent_menu/');
		$filter					= array(
										"menu_id" 			=> $this->input->post('nama_menu'),
										"parent_menu_name" 	=> $this->input->post('nama_parent_menu')
										);
		$dataPost				= $this->input->post();
		$checkData				= $this->M_parent_menu->checkData('m_sub_menu',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= $this->M_parent_menu->getIdParentMenu();
			$no_urut				= $this->M_parent_menu->getNoUrut($this->input->post('nama_menu'));
			$data	 				= array(
										'sub_menu_id'	=> $id,
										'menu_id'			=> $this->input->post('nama_menu'),
										'parent_menu_name' 	=> $this->input->post('nama_parent_menu'),
										'link' 				=> $this->input->post('link'),
										'no_urut' 			=> $no_urut,
										'activation' 		=> 'Y'
										);
			$save					= $this->M_parent_menu->add('m_sub_menu',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$directto				= site_url('Parent_menu/');
		$id						= $this->input->post('parent_menu_id');
		$data	 				= array(
										'menu_id'			=> $this->input->post('nama_menu'),
										'parent_menu_name' 	=> $this->input->post('nama_parent_menu'),
										'link' 				=> $this->input->post('link'),
										);
		$update					= $this->M_parent_menu->edit('m_sub_menu',$data,'sub_menu_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_parent_menu->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
