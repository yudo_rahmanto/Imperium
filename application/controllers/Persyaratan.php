<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persyaratan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_persyaratan');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Persyaratan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Persyaratan Name'
										);
		$search							= array(
											"activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Persyaratan';
		$data['title2']			= 'Search Data Persyaratan';
		$data['data_search']	= $search;	
		$data['getData']		= ManyFilter('m_persyaratan',$search);
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Persyaratan/autocomplete');
		$data['print'] 			= site_url('Persyaratan/cetak/print');
		$data['excell'] 		= site_url('Persyaratan/cetak/excell');
		$data['add'] 			= site_url('Persyaratan/add/');
		$data['edit'] 			= site_url('Persyaratan/edit/');
		$data['detail'] 		= site_url('Persyaratan/detail/');
		$data['page_action']	= site_url('Persyaratan/');
		$data['back_action']	= site_url('Persyaratan/');
		$data['delete']			= site_url('Persyaratan/delete/');
		$this->load->view('master/persyaratan/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Persyaratan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Persyaratan';
		$data['title2']			= 'Create New Persyaratan';
		$data['getIzin']		= SingleFilter('m_izin');
		$data['getTypeIzin']	= SingleFilter('m_izin_type');
		$data['getPersyaratan']	= SingleFilter('m_persyaratan');
		$data['action']			= site_url('Persyaratan/saveForm/add');
		$data['back']			= site_url('Persyaratan/');
		$this->load->view('master/persyaratan/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Persyaratan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'persyaratan_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Persyaratan';
		$data['title2']			= 'Edit Persyaratan';
		$data['getData']		= ManyFilter('m_persyaratan',$search);
		$data['getIzin']		= SingleFilter('m_izin');
		$data['getTypeIzin']	= SingleFilter('m_izin_type');
		$data['getPersyaratan']	= SingleFilter('m_persyaratan');
		//debug($data['getData']);exit;
		$data['action']			= site_url('Persyaratan/saveForm/edit');
		$data['back']			= site_url('Persyaratan/');
		
		$this->load->view('master/persyaratan/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Persyaratan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'persyaratan_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Persyaratan';
		$data['title2']			= 'Data Detail Persyaratan';
		$data['getData']		= ManyFilter('m_persyaratan',$search);
		//debug($data['getData']);exit;
		$data['back']			= site_url('Persyaratan/');
		$this->load->view('master/persyaratan/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$directto				= site_url('Izin/');
		$filter					= array(
										"name" 			=> $this->input->post('persyaratan_name')
										);
		$dataPost				= $this->input->post();
		$checkData				= ManyFilter('m_persyaratan',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= Id('m_persyaratan','persyaratan_id');
			$data	 				= array(
										'persyaratan_id'	=> $id,
										'name' 				=> $this->input->post('persyaratan_name'),
										'activation' 		=> 'Y'
										);
			$save					= Add('m_persyaratan',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Persyaratan/');
		$id						= $this->input->post('persyaratan_id');
		$data	 				= array(
										'name' 				=> $this->input->post('persyaratan_name'),
										'activation' 		=> 'Y'
										);
		$update					= Edit('m_persyaratan',$data,'persyaratan_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_persyaratan->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
