<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Syarat extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_syarat');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Syarat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Izin Name',
										'Izin Type Name',
										'Syarat Name',
										'Max Size'
										);
		$search							= array(
											"a.activation"			=> 'Y'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Syarat';
		$data['title2']			= 'Search Data Syarat';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_syarat->getSyarat($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Syarat/autocomplete');
		$data['print'] 			= site_url('Syarat/cetak/print');
		$data['excell'] 		= site_url('Syarat/cetak/excell');
		$data['add'] 			= site_url('Syarat/add/');
		$data['edit'] 			= site_url('Syarat/edit/');
		$data['detail'] 		= site_url('Syarat/detail/');
		$data['page_action']	= site_url('Syarat/');
		$data['back_action']	= site_url('Syarat/');
		$data['delete']			= site_url('Syarat/delete/');
		$this->load->view('master/syarat/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Syarat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Syarat';
		$data['title2']			= 'Create New Syarat';
		$data['getIzin']		= $this->M_syarat->getDataCombo('m_izin');
		$data['getTypeIzin']	= $this->M_syarat->getDataCombo('m_izin_type');
		$data['getPersyaratan']	= $this->M_syarat->getDataCombo('m_persyaratan');
		$data['action']			= site_url('Syarat/saveForm/add');
		$data['back']			= site_url('Syarat/');
		$this->load->view('master/syarat/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Syarat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.syarat_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Syarat';
		$data['title2']			= 'Edit Syarat';
		$data['default']		= $this->M_syarat->getSyarat($search);
		$data['getData']		= $data['default']['data'];
		$data['getIzin']		= $this->M_syarat->getDataCombo('m_izin');
		$data['getTypeIzin']	= $this->M_syarat->getDataCombo('m_izin_type');
		$data['getPersyaratan']	= $this->M_syarat->getDataCombo('m_persyaratan');
		//debug($data['getData']);exit;
		$data['action']			= site_url('Syarat/saveForm/edit');
		$data['back']			= site_url('Syarat/');
		
		$this->load->view('master/syarat/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Syarat';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.syarat_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Syarat';
		$data['title2']			= 'Data Detail Syarat';
		$data['default']		= $this->M_syarat->getSyarat($search);
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Syarat/');
		$this->load->view('master/syarat/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function getPersyaratan(){
		$param		= array(
							'a.izin_id'		=> $this->input->post('izin_id'),
							'a.izin_type_id'  => $this->input->post('izin_type_id')
							);
		$getData	= $this->M_syarat->getSyarat($param);
		if($getData){
			echo json_encode($getData);
		}
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session						= $dataSession['user_id'];
		$LevelId_Session							= $dataSession['level_id'];
		$directto				= site_url('Izin/');
		$filter					= array(
										"izin_id" 			=> $this->input->post('izin'),
										"izin_type_id" 		=> $this->input->post('izin_type'),
										"persyaratan_id"	=> $this->input->post('syarat_name')
										);
		$dataPost				= $this->input->post();
		$checkData				= $this->M_syarat->checkData('m_syarat',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= $this->M_syarat->getIdSyarat();
			$data	 				= array(
										'syarat_id'			=> $id,
										'izin_id' 			=> $this->input->post('izin'),
										'izin_type_id' 		=> $this->input->post('izin_type'),
										'persyaratan_id' 	=> $this->input->post('syarat_name'),
										'max_size' 			=> $this->input->post('max_size'),
										'format_file' 		=> $this->input->post('format_file'),
										'mandatory' 		=> $this->input->post('mandatory'),
										'activation' 		=> 'Y'
										);
			$save					= $this->M_syarat->add('m_syarat',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Syarat/');
		$id						= $this->input->post('syarat_id');
		$data	 				= array(
										'izin_id' 			=> $this->input->post('izin'),
										'izin_type_id' 		=> $this->input->post('izin_type'),
										'persyaratan_id' 	=> $this->input->post('syarat_name'),
										'max_size' 			=> $this->input->post('max_size'),
										'format_file' 		=> $this->input->post('format_file'),
										'mandatory' 		=> $this->input->post('mandatory'),
										'activation' 		=> 'Y'
										);
		$update					= $this->M_syarat->edit('m_syarat',$data,'syarat_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_syarat->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
