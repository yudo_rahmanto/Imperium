<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_menu');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		
		$data['path_info']				= 'Menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Menu Name',
										'Menu Link'
										);
		$search							= array(
											"a.menu_name"			=> $this->input->post('name')
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Menu';
		$data['title2']			= 'Search Data Menu';
		$data['data_search']	= $search;	
		$data['default']		= $this->M_menu->getMenu($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Menu/autocomplete');
		$data['print'] 			= site_url('Menu/cetak/print');
		$data['excell'] 		= site_url('Menu/cetak/excell');
		$data['add'] 			= site_url('Menu/add/');
		$data['edit'] 			= site_url('Menu/edit/');
		$data['detail'] 		= site_url('Menu/detail/');
		$data['page_action']	= site_url('Menu/');
		$data['back_action']	= site_url('Menu/');
		$data['delete']			= site_url('Menu/delete/');
		$this->load->view('setting/menu/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		$data['path_info']				= 'Menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['validation']		= 'level_id,name,username';
		$data['default']		= $this->M_menu->getMenu('','');
		$data['getData']		= $data['default']['data'];
		$data['title1']			= 'Data Menu';
		$data['title2']			= 'Create New Menu';
		$data['action']			= site_url('Menu/saveForm/add');
		$data['back']			= site_url('Menu/');
		$this->load->view('setting/menu/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$data['path_info']				= 'Menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.menu_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Menu';
		$data['title2']			= 'Edit Menu';
		$data['default']		= $this->M_menu->getMenu($search);
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['action']			= site_url('Menu/saveForm/edit');
		$data['back']			= site_url('Menu/');
		
		$this->load->view('setting/menu/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Menu';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.menu_id'	=>	"".$id.""
										);
		
		$data['title1']			= 'Data Menu';
		$data['title2']			= 'Data Detail Menu';
		$data['default']		= $this->M_menu->getMenu($search);
		$data['getData']		= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['back']			= site_url('Menu/');
		$this->load->view('setting/menu/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
	}
	private function saveAdd(){
		$directto				= site_url('Menu/');
		$filter					= array(
										"menu_name" => $this->input->post('nama')
										);
		$dataPost				= $this->input->post();
		$checkData				= $this->M_menu->checkData('m_menu',$filter);
		if((!$checkData) && ($dataPost)){
			$id						= $this->M_menu->getIdMenu();
			$no_urut				= $this->M_menu->getNoUrut($id);
			$data	 				= array(
										'menu_id'		=> $id,
										'menu_name' 	=> $this->input->post('nama'),
										'link' 			=> $this->input->post('link'),
										'no_urut' 		=> $no_urut,
										'activation' 	=> 'Y'
										);
			$save					= $this->M_menu->add('m_menu',$data);
			if($save){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			echo "duplikat";
		}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);		
		// redirect($directto);
		// }
		// exit;
	}
	private function saveEdit(){
		$directto				= site_url('Menu/');
		$id						= $this->input->post('menu_id');
		$data	 				= array(
										'menu_name' 	=> $this->input->post('nama'),
										'link' 			=> $this->input->post('link'),
										);
		$update					= $this->M_menu->edit('m_menu',$data,'menu_id',$id);
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		// if($save){
		// $error				= 1;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// } else {
		// $error				= 2;
		// $this->session->set_flashdata('error', $error);	
		// redirect($directto);
		// exit;
		// }
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_menu->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
