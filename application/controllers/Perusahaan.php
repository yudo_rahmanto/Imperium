<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->cekSession 	= $this->authlogin->check_admin_session();		
		$this->load->model('M_perusahaan');
		$this->load->library('googlemaps');
		$this->dataSession 	= $this->session->userdata('user_data');
		
	}
	public function index()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Jenis Perusahaan',
										'Perusahaan Name',
										'Status Perusahaan',
										'Kecamatan',
										'Kelurahan',
										'No Tlp',
										'Alamat Perusahaan'
										);
		
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Perusahaan';
		$data['title2']			= 'Search Data Perusahaan';
		$search							= array(
											"b.user_management_id"	=> $UserId_Session
										);
		$data['default']		= $this->M_perusahaan->getPerusahaan($search,'');
		$data['getData']		= $data['default']['data'];
		//debug($search);exit;
		$data['autocomplete'] 	= site_url('Perusahaan/autocomplete');
		$data['print'] 			= site_url('Perusahaan/cetak/print');
		$data['excell'] 		= site_url('Perusahaan/cetak/excell');
		$data['add'] 			= site_url('Perusahaan/add/');
		$data['edit'] 			= site_url('Perusahaan/edit');
		$data['detail'] 		= site_url('Perusahaan/detail');
		$data['page_action']	= site_url('Perusahaan/');
		$data['back_action']	= site_url('Perusahaan/');
		$data['delete']			= site_url('Perusahaan/delete');
		$this->load->view('master/perusahaan/view',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
		public function add()
	{
		##Default##
		
		// $config['center'] = '37.4419, -122.1419';
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$data['title1']			= 'Data Perusahaan';
		$data['title2']			= 'Create New Perusahaan';
		$data['type']					= $this->uri->segment(3);
		$data['getPerusahaan']			= singleFilter('m_perusahaan','user_management_id',$UserId_Session,'nama_perusahaan','asc');
		$data['getJenisPerusahaan']		= singleFilter('m_perusahaan_type','','','perusahaan_type','asc');
		$data['getProvinsi']			= singleFilter('provinsi','','','nama','asc');
		$data['getKabupaten']			= singleFilter('kabupaten','','','nama','asc');
		$data['getJenisUsaha']			= singleFilter('m_jenis_usaha','','','name','asc');
		$data['getLingkungan']			= singleFilter('m_lingkungan_perusahaan','','','name','asc');
		$data['getLokasi']				= singleFilter('m_lokasi_perusahaan','','','name','asc');
		$data['getIndexGangguan']		= singleFilter('m_index_gangguan','','','name','asc');
		$data['getAlatKerja']			= singleFilter('m_alat_kerja');
		$data['getData']				= site_url('Perusahaan/saveForm/getData');
		$data['getDataSelect']			= site_url('Perusahaan/saveForm/getDataSelect');
		$data['action']					= site_url('Perusahaan/saveForm/add');
		if(isset($_SERVER['HTTP_REFERER'])){
			if(strpos($_SERVER['HTTP_REFERER'], 'Perusahaan') !== false) {
			$data['back']			= site_url('Perusahaan/');
			} else {
			$data['back']			= $_SERVER['HTTP_REFERER'];	
			}
		} else {
			$data['back']			= site_url('Perusahaan/');
		}
		$this->load->view('master/perusahaan/add',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function edit()
	{
		##Default##
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['level_id'];
		$data['path_info']				= 'Perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.alamat_perusahaan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Perusahaan';
		$data['title2']			= 'Edit Perusahaan';
		$data['default']		= $this->M_perusahaan->getPerusahaan($search);
		$data['getPerusahaan']	= singleFilter('m_perusahaan','user_management_id',$UserId_Session,'nama_perusahaan','asc');
		$data['getProvinsi']	= singleFilter('provinsi','','','nama','asc');
		$data['getKabupaten']	= singleFilter('kabupaten','','','nama','asc');
		$data['getKecamatan']	= singleFilter('kecamatan','id_kabupaten',1671,'nama','asc');
		$kecamatan_id = '';
		if($data['getKecamatan']){
			foreach($data['getKecamatan'] as $row){
				$kecamatan_id = $row['id'];
			}
		}
		$data['getKelurahan']	= singleFilter('kelurahan','id_kecamatan',$kecamatan_id,'nama','asc');
		$data['getDataResult']	= $data['default']['data'];
		$data['getJenisPerusahaan']		= singleFilter('m_perusahaan_type','','');
		$data['getJenisUsaha']	= singleFilter('m_jenis_usaha','','','name','asc');
		$data['getLingkungan']	= singleFilter('m_lingkungan_perusahaan','','','name','asc');
		$data['getLokasi']		= singleFilter('m_lokasi_perusahaan','','','name','asc');
		$data['getIndexGangguan']	= singleFilter('m_index_gangguan','','','name','asc');
		$data['getAlatKerja']	= singleFilter('m_alat_kerja','','','name','asc');
		//debug($data['getData']);exit;
		$data['getDataSelect']	= site_url('Perusahaan/saveForm/getDataSelect');
		$data['getData']		= site_url('Perusahaan/saveForm/getData');
		$data['action']			= site_url('Perusahaan/saveForm/edit');
		$data['back']			= site_url('Perusahaan/');
		
		$this->load->view('master/perusahaan/edit',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function detail()
	{
		##Default##
		$data['path_info']				= 'Perusahaan';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		$this->load->view('common/head',$data);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		##Default##
		
		##Costumize##
		$id				= $this->uri->segment(3);
		$search			= array(
										'a.alamat_perusahaan_id'	=>	$id
										);
		
		$data['title1']			= 'Data Perusahaan';
		$data['title2']			= 'Data Detail Perusahaan';
		$data['default']		= $this->M_perusahaan->getPerusahaan($search);
		$data['getDataResult']	= $data['default']['data'];
		//debug($data['getData']);exit;
		$data['getData']		= site_url('Perusahaan/saveForm/getData');
		$data['back']			= site_url('Perusahaan/');
		$this->load->view('master/perusahaan/detail',$data);
		##Costumize##
		
		##Default##
		$this->load->view('common/footer');
		##Default##
	}
	public function saveForm(){
		$action		= $this->uri->segment(3);
		if($action == 'add'){$directto	= $this->saveAdd();} 
		if($action == 'edit'){$directto	= $this->saveEdit();} 
		if($action == 'getData'){$directto	= $this->getData();} 
		if($action == 'getDataSelect'){$directto	= $this->getDataSelect();} 
	}

	private function getData(){
		$find		= $this->input->post('find');
		$id			= $this->input->post('id');
		$getData	= '';
		if($id){
		$getData	= $this->M_perusahaan->getData($find,$id);
		} 
		echo json_encode($getData);
	}
	private function getDataSelect(){
		$find		= $this->input->post('find');
		$field		= $this->input->post('field');
		$id			= $this->input->post('id');
		$getData	= '';
		if($id){
		$getData	= singleFilter($find,$field,$id);
		} 
		echo json_encode($getData);
	}
	private function saveAdd(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session		= $dataSession['level_id'];
		$directto				= site_url('Menu/');
		$type					= $this->input->post('type');
		$dataPost				= $this->input->post();
		if($type == 'pusat'){
			$filter					= array(
											"nama_perusahaan" => $this->input->post('nama_perusahaan')
											);
			$checkData				= ManyFilter('m_perusahaan',$filter);
			if((!$checkData) && ($dataPost)){
				$jenis_usaha			= $this->input->post('jenis_usaha');
				if($jenis_usaha){
					$jenis_usaha_lainnya	= $this->input->post('jenis_usaha_lainnya');
					if($jenis_usaha_lainnya){
					$jenis_usaha_id			= $this->M_perusahaan->getIdJenisUsaha();
					$jenis_usaha			= $jenis_usaha_id;
					
					$data_jenis_usaha	= array(
													'jenis_usaha_id'	=> $jenis_usaha_id,
													'name'				=> strtoupper($jenis_usaha_lainnya),
													'activation'		=> 'Y'
												);
					$save				= $this->M_perusahaan->add('m_jenis_usaha',$data_jenis_usaha);
					}
				
				}
				$idPerusahaan			= Id('m_perusahaan','perusahaan_id');
				$data	 				= array(
											'perusahaan_id'				=> $idPerusahaan,
											'user_management_id'		=> $UserId_Session,
											'perusahaan_type_id' 		=> $this->input->post('jenis_perusahaan'),
											'nama_perusahaan' 			=> $this->input->post('nama_perusahaan'),
											'activation' 				=> 'Y'
											);
				$save					= Add('m_perusahaan',$data);
				$idAlamatPerusahaan		= Id('m_alamat_perusahaan','alamat_perusahaan_id');
				$data	 				= array(
											'alamat_perusahaan_id'		=> $idAlamatPerusahaan,
											'user_management_id'		=> $UserId_Session,
											'perusahaan_id'				=> $idPerusahaan,
											'status' 					=> 1,
											'nama_cabang' 				=> 'Kantor Pusat',
											'jenis_usaha_id' 			=> $jenis_usaha,
											'lingkungan_perusahaan_id' 	=> $this->input->post('lingkungan'),
											'lokasi_perusahaan_id' 		=> $this->input->post('lokasi_perusahaan'),
											'luas' 						=> $this->input->post('luas'),
											'alamat' 					=> $this->input->post('alamat'),
											'rt' 						=> $this->input->post('rt'),
											'rw' 						=> $this->input->post('rw'),
											'provinsi_id' 				=> $this->input->post('provinsi'),
											'kabupaten_id' 				=> $this->input->post('kabupaten'),
											'kecamatan_id' 				=> $this->input->post('kecamatan'),
											'kelurahan_id' 				=> $this->input->post('kelurahan'),
											'tlp' 						=> $this->input->post('tlp'),
											'lat' 						=> $this->input->post('lat'),
											'lng' 						=> $this->input->post('lng'),
											'created_date' 				=> date('Y-m-d H:i:s'),
											'created_user' 				=> $UserId_Session,
											'modified_date' 			=> date('Y-m-d H:i:s'),
											'modified_user' 			=> $UserId_Session,
											'activation' 				=> 'Y'
											);
				$save					= Add('m_alamat_perusahaan',$data);
			
				if($save){
					echo "sukses";
				} else {
					echo "gagal";
				}
			} else {
				echo "duplikat";
			}
		} else {
			if($dataPost){
				$jenis_usaha			= $this->input->post('jenis_usaha');
				if($jenis_usaha){
					$jenis_usaha_lainnya	= $this->input->post('jenis_usaha_lainnya');
					if($jenis_usaha_lainnya){
					$jenis_usaha_id			= $this->M_perusahaan->getIdJenisUsaha();
					$jenis_usaha			= $jenis_usaha_id;
					
					$data_jenis_usaha	= array(
													'jenis_usaha_id'	=> $jenis_usaha_id,
													'name'				=> strtoupper($jenis_usaha_lainnya),
													'activation'		=> 'Y'
												);
					$save				= $this->M_perusahaan->add('m_jenis_usaha',$data_jenis_usaha);
					}
				
				}
				$idAlamatPerusahaan		= Id('m_alamat_perusahaan','alamat_perusahaan_id');
				$idPerusahaan			= $this->input->post('nama_perusahaan');
				$data	 				= array(
											'alamat_perusahaan_id'		=> $idAlamatPerusahaan,
											'user_management_id'		=> $UserId_Session,
											'perusahaan_id'				=> $idPerusahaan,
											'status' 					=> 2,
											'nama_cabang' 				=> $this->input->post('nama_cabang'),
											'jenis_usaha_id' 			=> $jenis_usaha,
											'lingkungan_perusahaan_id' 	=> $this->input->post('lingkungan'),
											'lokasi_perusahaan_id' 		=> $this->input->post('lokasi_perusahaan'),
											'luas' 						=> $this->input->post('luas'),
											'alamat' 					=> $this->input->post('alamat'),
											'rt' 						=> $this->input->post('rt'),
											'rw' 						=> $this->input->post('rw'),
											'provinsi_id' 				=> $this->input->post('provinsi'),
											'kabupaten_id' 				=> $this->input->post('kabupaten'),
											'kecamatan_id' 				=> $this->input->post('kecamatan'),
											'kelurahan_id' 				=> $this->input->post('kelurahan'),
											'tlp' 						=> $this->input->post('tlp'),
											'lat' 						=> $this->input->post('lat'),
											'lng' 						=> $this->input->post('lng'),
											'created_date' 				=> date('Y-m-d H:i:s'),
											'created_user' 				=> $UserId_Session,
											'modified_date' 			=> date('Y-m-d H:i:s'),
											'modified_user' 			=> $UserId_Session,
											'activation' 				=> 'Y'
											);
				$save					= Add('m_alamat_perusahaan',$data);
			
				if($save){
					echo "sukses";
				} else {
					echo "gagal";
				}
			} else {
				echo "duplikat";
			}
		}
	}
	private function saveEdit(){
		$dataSession			= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session			= $dataSession['level_id'];
		$directto				= site_url('Menu/');
		$type					= $this->input->post('type');
		$jenis_usaha			= $this->input->post('jenis_usaha');
			if($jenis_usaha){
				$jenis_usaha_lainnya	= $this->input->post('jenis_usaha_lainnya');
				if($jenis_usaha_lainnya){
				$jenis_usaha_id			= $this->M_perusahaan->getIdJenisUsaha();
				$jenis_usaha			= $jenis_usaha_id;
				
				$data_jenis_usaha	= array(
												'jenis_usaha_id'	=> $jenis_usaha_id,
												'name'				=> strtoupper($jenis_usaha_lainnya),
												'activation'		=> 'Y'
											);
				$save				= $this->M_perusahaan->add('m_jenis_usaha',$data_jenis_usaha);
				}
			
			}
		if($type == 1){
		$idPerusahaan						= $this->input->post('perusahaan_id');
		$idAlamatPerusahaan					= $this->input->post('alamat_perusahaan_id');
		$dataPerusahaan	 				= array(
												'perusahaan_type_id' 		=> $this->input->post('jenis_perusahaan'),
												'nama_perusahaan' 			=> $this->input->post('nama_perusahaan'),
												'activation' 				=> 'Y'
												);
		$update							= Edit('m_perusahaan',$dataPerusahaan,'perusahaan_id',$idPerusahaan);
		$dataAlamatPerusahaan			= array(
											'jenis_usaha_id' 			=> $jenis_usaha,
											'lingkungan_perusahaan_id' 	=> $this->input->post('lingkungan'),
											'lokasi_perusahaan_id' 		=> $this->input->post('lokasi_perusahaan'),
											'luas' 						=> $this->input->post('luas'),
											'alamat' 					=> $this->input->post('alamat'),
											'rt' 						=> $this->input->post('rt'),
											'rw' 						=> $this->input->post('rw'),
											'provinsi_id' 				=> $this->input->post('provinsi'),
											'kabupaten_id' 				=> $this->input->post('kabupaten'),
											'kecamatan_id' 				=> $this->input->post('kecamatan'),
											'kelurahan_id' 				=> $this->input->post('kelurahan'),
											'tlp' 						=> $this->input->post('tlp'),
											'lat' 						=> $this->input->post('lat'),
											'lng' 						=> $this->input->post('lng'),
											'modified_date' 			=> date('Y-m-d H:i:s'),
											'modified_user' 			=> $UserId_Session,
											'activation' 				=> 'Y'
											);
		$update							= Edit('m_alamat_perusahaan',$dataAlamatPerusahaan,'alamat_perusahaan_id',$idAlamatPerusahaan);
		} else  {
		$idAlamatPerusahaan				= $this->input->post('alamat_perusahaan_id');	
		$dataAlamatPerusahaan			= array(
											'nama_cabang' 				=> $this->input->post('nama_cabang'),
											'jenis_usaha_id' 			=> $jenis_usaha,
											'lingkungan_perusahaan_id' 	=> $this->input->post('lingkungan'),
											'lokasi_perusahaan_id' 		=> $this->input->post('lokasi_perusahaan'),
											'luas' 						=> $this->input->post('luas'),
											'alamat' 					=> $this->input->post('alamat'),
											'rt' 						=> $this->input->post('rt'),
											'rw' 						=> $this->input->post('rw'),
											'provinsi_id' 				=> $this->input->post('provinsi'),
											'kabupaten_id' 				=> $this->input->post('kabupaten'),
											'kecamatan_id' 				=> $this->input->post('kecamatan'),
											'kelurahan_id' 				=> $this->input->post('kelurahan'),
											'tlp' 						=> $this->input->post('tlp'),
											'lat' 						=> $this->input->post('lat'),
											'lng' 						=> $this->input->post('lng'),
											'modified_date' 			=> date('Y-m-d H:i:s'),
											'modified_user' 			=> $UserId_Session,
											'activation' 				=> 'Y'
											);
		$update							= Edit('m_alamat_perusahaan',$dataAlamatPerusahaan,'alamat_perusahaan_id',$idAlamatPerusahaan);
		}
		if($update){
				echo "sukses";
			} else {
				echo "gagal";
			}
		
	}
	public function delete(){
		$id			= $this->input->post('id');
		$result		= "Failed";
		if($id){
		$delete					= $this->M_perusahaan->delete($id);
			if($delete){
				$result		= "Success";
			}
		} else {
			$result		= "Failed";
		}
		echo $result;		
		
	}
	
}
