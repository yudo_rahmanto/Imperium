<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();	
		$this->cekSession 	= $this->authlogin->check_admin_session();
		$this->dataSession 	= $this->session->userdata('user_data');
	}
	
	public function index()
	{
		
		$dataSession	= $this->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$JabatanId_Session		= $dataSession['jabatan_id'];
		$fullname_Session		= $dataSession['first_name'].' '.$dataSession['last_name'];
		$LevelId_Session		= $dataSession['level_id'];
		
		$data['path_info']				= 'Home';
		$data['default_menu']			= $this->M_default_access->menu();
		$data['menu']					= $data['default_menu']['data'];
		$data['deafult_parentMenu']		= $this->M_default_access->parents();
		$data['parentMenu']				= $data['deafult_parentMenu']['data'];
		$asset['assets_css']			= assets_css($data['path_info']);
		$asset['assets_js']				= assets_js($data['path_info']);
		
		
		
		// debug($this->session->userdata('user_data'));exit;
		$this->load->view('common/head',$asset);
		$this->load->view('common/menu',$data);
		$this->load->view('common/top_navigation');
		
		$this->load->view('home/index',$data);
		$this->load->view('common/footer',$asset);
	}
	public function dataInbox()
	{
		##Default##
		$this->load->model('M_approval');
		$dataSession					= $this->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$JabatanId_Session				= $dataSession['jabatan_id'];
		$LevelId_Session				= $dataSession['level_id'];
		// debug($dataSession);exit;
		$data['path_info']				= 'Approval';
		$data['assets_css']				= assets_css($data['path_info']);
		$data['assets_js']				= assets_js($data['path_info']);
		
		##Default##
		
		##Costumize##
		$data['field_table']	= array(
										'Tgl Pendaftaran',
										'Nama Perusahaan',
										'Nama Izin',
										'Jenis Permohonan'
										);
		$search					= array(
											"a.activation"		=> 'Y',
											"a.status"			=> 'On Progress',
											"e.jabatan_id"		=> $JabatanId_Session
										);
		$searchOnProgress			= array(
											"a.activation"				=> 'Y',
											"a.jabatan_id_penerima"		=> $JabatanId_Session
										);
		$searchFinish			= array(
											"a.activation"				=> 'Y',
											"a.jabatan_id_penerima"		=> $JabatanId_Session
										);
			//echo '<pre>';print_r($search);exit;
		$data['title1']			= 'Data Approval';
		$data['title2']			= 'Search Data Approval';
		$data['data_search']	= $search;	
		$data['getDataInbox']		= $this->M_approval->getInbox($search,'');
		// $data['getDataOnProgress']	= $this->M_approval->getOnProgress($searchOnProgress,'');
		$data['getDataFinish']		= $this->M_approval->getFinish($searchFinish,'');
		// debug($data);exit;
		$data['autocomplete'] 	= site_url('Approval/autocomplete');
		$data['print'] 			= site_url('Approval/cetak/print');
		$data['excell'] 		= site_url('Approval/cetak/excell');
		$data['add'] 			= site_url('Approval/add/');
		$data['edit'] 			= site_url('Approval/edit');
		$data['detail'] 		= site_url('Approval/detail');
		$data['cetak_resi'] 	= site_url('Approval/cetak_resi');
		$data['terim_berkas'] 	= site_url('Approval/saveForm/terimaberkas');
		$data['page_action']	= site_url('Approval/');
		$data['back_action']	= site_url('Approval/');
		$data['delete']			= site_url('Approval/delete');
		$this->load->view('home/inbox',$data);
		##Costumize##
		
		
	}
}
