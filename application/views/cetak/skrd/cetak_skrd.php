<?php

if($getData){
	$user_management_id	= $getData[0]['user_management_id'];
	$permohonan_id	= $getData[0]['permohonan_id'];
	$izin_id		= $getData[0]['izin_id'];
	$izin_type_id	= $getData[0]['izin_type_id'];
	$nama_pemohon	= $getData[0]['first_name'].' '.$getData[0]['last_name'];
	$alamat_pemohon	= $getData[0]['alamat'];
	$rt_user		= $getData[0]['rt_user'];
	$rw_user		= $getData[0]['rw_user'];
	$kel_user		= $getData[0]['kel_user'];
	$kec_user		= $getData[0]['kec_user'];
	$kab_user		= $getData[0]['kab_user'];
	$prov_user		= $getData[0]['prov_user'];
	$jenis_perusahaan	= $getData[0]['jenis_perusahaan'];
	$nama_perusahaan	= $getData[0]['nama_perusahaan'];
	$qr_code			= $getData[0]['qr_code'];
	if($jenis_perusahaan){
		$nama_perusahaan	= $jenis_perusahaan.'. '.$nama_perusahaan;
	}
	if(!file_exists(path_user_upload.'user_'.$user_management_id)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id, 0777);
		}
	if(!file_exists(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id, 0777);
		}
	if(!file_exists(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id.'/skrd')) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id.'/skrd', 0777);
	}
} else {
	$user_management_id	='';
	$permohonan_id	= '';
	$izin_id		= '';
	$izin_type_id	= '';
	$nama_pemohon	= '';
	$alamat_pemohon	= '';
	$nama_perusahaan	= '';
	$qr_code		= '';
}
if($getDataSignature){
	$signature	= base_url($getDataSignature[0]['path_location']);
} else {
	$signature	= '';
}
if($getKepalaDinas){
	$level_id		= $getKepalaDinas[0]['level_id'];
	$level_name 	= '';
	$nip_kepala		= $getKepalaDinas[0]['nip'];
	$nama_kepala	= $getKepalaDinas[0]['fullname_with_gelar'];
	$getLevel		= SingleFilter('m_level','level_id',$level_id);
	if($getLevel){
		$level_name = $getLevel[0]['level_name'];
	}
} else {
	$level_id		= '';
	$level_name 	= '';
	$nip_kepala		= '';
	$nama_kepala	= '';
}
// echo '<pre>';print_r($getDataRetribusi);exit;
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 5, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
				
					<table align="center" style="width:100%;text-align:center;border-left: 1px solid black;border-right: 1px solid black;border-top: 1px solid black;border-bottom: 1px solid black;">
						<tr>
							<td width="60%" style="height:60px;">
								<table align="center" style="width:100%;text-align:center;border-right: 1px solid black;">
									<tr>
										<td rowspan="5" width="20%"><img src="'.base_url('assets/images/logo/logo.png').'" width="auto" height="60px"></td>
										<td width="80%" style="font-size: 65%;height:15px;"><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td style="font-size: 65%;height:15px;"><b>DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</b></td>
									</tr>
									<tr>
										<td style="font-size: 60%;height:15px;">Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
									</tr>
									<tr>
										<td style="font-size: 60%;height:15px;">Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td style="font-size: 60%;">PALEMBANG</td>
									</tr>
								</table>
							</td>
							<td width="40%" style="height:60px;">
								<table align="center" style="width:100%;text-align:left;">
								<tr>
										<td colspan="3" style="font-size: 80%;">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3" style="font-size: 80%;"><div align="center"><b>S K R D</b></div></td>
									</tr>
									<tr>
										<td colspan="3" style="font-size: 60%;"><div align="center">(Surat Ketetapan Retribusi Daerah)</div></td>
									</tr>
									<tr>
										<td width="20%" style="font-size: 60%;">Nomor </td><td width="5%" style="font-size: 60%;">:</td><td width="75%" style="font-size: 60%;"><div align="left">'.$permohonan_id.'.'.$izin_id.'.'.$izin_type_id.'/IG-SKRD/KPPT/'.date('Y').'</div></td>
									</tr>
									<tr>
										<td width="20%" style="font-size: 60%;">Tahun </td><td width="5%" style="font-size: 60%;">:</td><td width="75%" style="font-size: 60%;"><div align="left">'.date('Y').' Bulan : '.date('F').'</div></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" style="width:100%;text-align:left;font-size: 80%;">
						<tr>
							<td width="20%">Nama</td>
							<td width="5%"> : </td>
							<td width="75%"><b>'.$nama_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td> : </td>
							<td><b>'.strtoupper($alamat_pemohon).' RT. '.$rt_user.' RW. '.$rw_user.' KEL. '.$kel_user.' KEC. '.$kec_user.' '.$kab_user.' - '.$prov_user.'</b></td>
						</tr>
						<tr>
							<td>Nama Usaha</td>
							<td> : </td>
							<td><b>'.$nama_perusahaan.'</b></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" border="1" style="width:100%;text-align:left;font-size: 80%;">
						<tr>
							<td align="center" width="10%"><b>No</b></td>
							<td align="center" width="20%"><b>Nomor Pendaftaran</b></td>
							<td align="center" width="30%"><b>Jenis Permohonan Ijin</b></td>
							<td align="center" width="20%"><b>Kode Rekening</b></td>
							<td align="center" width="20%"><b>Jumlah Retribusi</b></td>
						</tr>';
						$total = 0;
						if($getDataRetribusi){
								$i = 1;
								foreach($getDataRetribusi as $row){
									$total	= $total + $row['total_akhir'];
						
$html	.='				<tr>
							<td valign="top" width="10%" align="center">'.$i.'</td>
							<td align="center" width="20%"><p>'.$row['qr_code'].'</p></td>
							<td align="left" width="30%"><p>'.$row['keterangan_izin'].'</p></td>
							<td align="center" width="20%"><p>1.16.1.16.01.4.1.2.03.03</p></td>
							<td align="right" width="20%"><p>Rp '.number_format($row['total_akhir']).'</p></td>
						</tr>';
						
								$i++;
								}
							}
						
$html	.='				
						<tr>
							<td valign="top" width="10%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
							<td align="left" width="30%"><p>Denda</p></td>
							<td align="center" width="20%"><p>1.20.1.20.03.4.1.4.14.03</p></td>
							<td align="right" width="20%"><p>Rp 0</p></td>
						</tr>
						<tr>
							<td colspan="4">JUMLAH TOTAL</td>
							<td align="right">Rp '.number_format($total).'</td>
						</tr>
						<tr>
							<td colspan="5">TOTAL : '.ucwords(strtolower(terbilang($total))).' Rupiah</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="center" style="width:100%;font-size: 80%;">
						<tr>
							<td colspan="2"><div align="left"><u><b>PERHATIAN :</b></u></div></td>
						</tr>
						<tr>
							<td width="5%"><div align="left">1</div></td>
							<td width="95%"><div align="left">Pembayaran dilakukan di loket pembayaran (Bank SUMSEL) Badan Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kota
						Palembang Jl. Merdeka No.1 Palembang pada jam kerja, selambat-lambatnya tanggal 3 (tiga) bulan sejak diterbitkannya Surat Pemberitahuan Retribusi.</div></td>
						</tr>
						<tr>
							<td width="5%"><div align="left">2</div></td>
							<td width="95%"><div align="left">SKRD ini berfungsi juga sebagai Surat Pemberitahuan RetribusiDaerah (SPTRD) dan Surat Setoran Retribusi Daerah (SSRD)</div></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="center" style="width:100%;font-size: 80%;">
						<tr>
							<td width="70%">&nbsp;</td>
							<td width="30%"><div align="center">Palembang, '.str_replace(' 00:00:00','',indonesia_date(date('d M Y'))).'</div></td>
						</tr>
						<tr>
							<td width="70%">&nbsp;</td>
							<td><div align="center">KEPALA BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</div></td>
						</tr>
						<tr>
							<td width="70%">&nbsp;</td>
							<td style="height:100px;"><img src="'.$signature.'" width="150px" height="150px"></td>
						</tr>
						<tr>
							<td width="70%">&nbsp;</td>
							<td><div align="center"><b>'.$nama_kepala.'</b></div></td>
						</tr>
						<tr>
							<td width="70%">&nbsp;</td>
							<td><div align="center">'.$level_name.'</div></td>
						</tr>
						<tr>
							<td width="70%">&nbsp;</td>
							<td><div align="center">NIP. '.$nip_kepala.'</div></td>
						</tr>
					</table>
					';
 // echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('SKRD.pdf', 'I');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+