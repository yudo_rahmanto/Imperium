<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$nama_perusahaan	= $getData[0]['nama_perusahaan'];
	$izin_name			= $getData[0]['izin_name'];
	$izin_type_name		= $getData[0]['izin_type_name'];
	$originalDate 		= $getData[0]['created_date'];
	$created_date 		= date("d M Y H:i:s", strtotime($originalDate));
	$posisi_alur		= $getData[0]['posisi_alur'];
} else {
	$nama_perusahaan	= '';
	$izin_name			= '';
	$izin_type_name		= '';
	$created_date		= '';
	$posisi_alur		= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<section class="panel">
						  <header class="panel-heading">
							  Permohonan Detail
						  </header>
						<div class="panel-body">
							<div class="col-sm-12">
								<ul class="list-group list-group-unbordered">
									<li class="list-group-item"><b>Tanggal Permohonan</b><a class="pull-right"><?php echo $created_date; ?></a></li>
									<li class="list-group-item"><b>Nama Perusahaan</b><a class="pull-right"><?php echo $nama_perusahaan; ?></a></li>
									<li class="list-group-item"><b>Nama Ijin</b><a class="pull-right"><?php echo $izin_name; ?></a></li>
									<li class="list-group-item"><b>Jenis Permohonan</b><a class="pull-right"><?php echo $izin_type_name; ?></a></li>
									<li class="list-group-item"><b>Status Izin</b><a class="pull-right"><?php echo $posisi_alur; ?></a></li>
									<li class="list-group-item"><b>Progress</b>
										<a class="pull-right">
										<div class="progress progress_sm">
										  <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="10"></div>
										</div>
										<small>10% Complete</small>
										</a>
									</li>
									
								</ul>
							</div>
						</div>
						
					  </section>
					</div>
					<div class="x_content">
					<div class="x_title">
					<h3>Data Persyaratan</h3>
					</div>
						<?php 
							if($getDataSyarat){
								foreach($getDataSyarat as $row){
									if (file_exists($row['path_location'])) {
									$path_image		= $row['path_location'];
									} else {
									$path_image		= 'assets/images/no_file.png';	
									}
						?>
						<div class="col-sm-3">
								<div class="form-group">
									<?php
										$nama_file	= $row['nama_file'];
										if (strpos($nama_file, 'pdf') !== false) {
									?>
									<div class="thumbnail">
									  <div class="image view view-first">
										<img style="width: 100%; display: block;" src="<?php echo base_url('assets/images/pdf.png'); ?>" alt="image" height="100px;" width="70px;"/>
										<div class="mask">
										  <p><?php echo $row['syarat_name']; ?></p>
										  
										</div>
									  </div>
									  <div class="caption">
										<p><?php echo $row['syarat_name']; ?></p>
									  </div>
									</div>
									<?php
										} else {
										?>
									<div class="thumbnail">
									  <div class="image view view-first">
										<img style="width: 100%; display: block;" src="<?php echo base_url($path_image); ?>" alt="image" />
										<div class="mask">
										  <p><?php echo $row['syarat_name']; ?></p>
										  
										</div>
									  </div>
									  <div class="caption">
										<p><?php echo $row['syarat_name']; ?></p>
									  </div>
									</div>
									<?php
											}
										?>
								</div>
						</div>
						<?php
								}
							}
						?>
					</div>
					  <div class="box-footer">
							<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						 </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>

<script>

function back(){
	window.location = "<?php echo $back; ?>";
}

</script>