<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<link rel="stylesheet" href="<?php echo base_url().'assets/css/canvas/signature-pad.css'; ?>">
<div id="signature-pad" class="m-signature-pad" >
	<div class="m-signature-pad--body" align="center">
	  <canvas id="thecanvas"></canvas>
	</div>
	<div class="m-signature-pad--footer">
	  <div align="center">
		<input type="hidden" id="action" name="action" value="<?php if($action){echo $action; } ?>">
		<input type="hidden" id="back" name="back" value="<?php if($back){echo $back; } ?>">
		<input type="hidden" id="permohonan_id" name="permohonan_id" value="<?php if($permohonan_id){echo $permohonan_id; } ?>">
		<input type="hidden" id="izin_category_id" name="izin_category_id" value="<?php if($izin_category_id){echo $izin_category_id; } ?>">
		<input type="hidden" id="signature" name="signature">
		<button type="button" class="button clear" data-action="clear">Clear</button>
		<button type="button" class="button save" data-action="save-png">Save</button>
	  </div>
	</div>
  </div>
</form>
<script src="<?php echo base_url().'assets/vendors/jquery/dist/jquery.min.js'; ?>"></script>
<script src="<?php echo base_url().'assets/js/canvas/signature_pad.js'; ?>"></script>
<script src="<?php echo base_url().'assets/js/canvas/app.js'; ?>"></script>
