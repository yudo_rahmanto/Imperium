<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Nama Template<font color="red"> * </font> <i id="ket_title" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="title" name="title" placeholder="Nama Template" required="required">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Upload File Doc</label>
							  <input type="file" class="form-control" id="file_upload" name="file_upload" placeholder="File Doc" required="required">
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		
   
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}

function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						uploadFile()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function uploadFile(){
		var doc = document.form;
			doc.action= "<?php echo $uploadFile; ?>";
			doc.submit();
	}		

</script>