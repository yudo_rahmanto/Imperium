<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($dataTemplate){
	$isiData		= "";
	foreach($dataTemplate as $row_left=>$row_right){
		$isiData[$row_left]	= $row_right;
	}
} 
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Nama Template<font color="red"> * </font> <i id="ket_title" style="color:silver;"></i></label>
							  <input type="hidden" class="form-control" id="template_rtf_id" name="template_rtf_id" placeholder="Nama Template" value="<?php if($isiData){echo $isiData['template_rtf_id'];} ?>">
							  <input type="text" class="form-control" id="title" name="title" placeholder="Nama Template" required="required" value="<?php if($isiData){echo $isiData['title'];} ?>">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Upload File Doc</label>
							  <input type="file" class="form-control" id="file" name="file" placeholder="File Doc" >
								<input type="hidden" id="apikey" name="apikey" value="uTPwBVvq_XQErSsOH_e0uGF7ho20Lbm0yC-lvp78uA8n5DpmIdhljY2LL3Gqr9pGVt4roWubTSXApzeAL_k44A">
								<input type="hidden" id="inputformat" name="inputformat" value="docx">
								<input type="hidden" id="outputformat" name="outputformat" value="html">
								<input type="hidden" id="input" name="input" value="upload">
								<input type="hidden" id="filename" name="filename" value="SURAT+IJIN+WALIKOTA+PALEMBANG.docx">
								<input type="hidden" name="output[ftp][host]" value="ftp.yudodata.com">
								<input type="hidden" name="output[ftp][port]" value="21">
								<input type="hidden" name="output[ftp][user]" value="yudo@yudodata.com">
								<input type="hidden" name="output[ftp][password]" value="delta456">
								<input type="hidden" name="output[ftp][path]" value="">
								<input type="hidden" id="callback" name="callback" value="https://apps.semutdata.co.id/imperium2/">
								<input type="hidden" name="wait" value="false">
								<input type="hidden" name="download" value="false">
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
$( "#file" ).change(function(event){
	var file_data 	= $('#file').prop('files')[0];  
	var nama_file	= file_data['name'];
	var extensi		= nama_file.substr(nama_file.indexOf(".") + 1)
	var title		= document.getElementById('title').value.replace(" ", "_");
	var encrypt_title 	= str_base64_encrypt(title);
	var code			= '';
		if(encrypt_title){code	+= 'title='+encrypt_title;}
	var callback	= "https://apps.semutdata.co.id/imperium2";
	document.getElementById('filename').value = title+'.'+extensi;
	document.getElementById('callback').value = callback;
	
});
function uploadFileToConvert(){
		var doc = document.form;
			doc.target = "formresult";
			doc.action= "https://api.cloudconvert.com/convert";
			doc.submit();
			formresult.close();
	}	
function uploadFile(){
	
	var file_data = $('#file').prop('files')[0];   
    var form_data = new FormData(); 
	var title 	= str_base64_encrypt(document.getElementById('title').value);
	var code			= '';
		if(title){code	+= 'title='+title;}
	form_data.append('file', file_data);
    $.ajax({
                url: "<?php echo $uploadFile; ?>?"+code, // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(respon){
                    if(respon == 'Ok'){
						uploadFileToConvert();
					}
                }
     });
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
	
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						uploadFile()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
</script>