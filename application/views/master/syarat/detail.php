<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$syarat_id		= $getData[0]['syarat_id'];
	$izin_id		= $getData[0]['izin_id'];
	$izin_name		= $getData[0]['izin_name'];
	$izin_type_name	= $getData[0]['izin_type_name'];
	$persyaratan_name	= $getData[0]['persyaratan_name'];
	$max_size		= $getData[0]['max_size'];
	$format_file	= $getData[0]['format_file'];
	$mandatory		= $getData[0]['mandatory'];
	if($mandatory == 1){$mandatory = "Wajib";} else{$mandatory = "Optional";}
} else {
	$syarat_id		= '';
	$izin_id		= '';
	$izin_name		= '';
	$izin_type_name	= '';
	$persyaratan_name	= '';
	$max_size		= '';
	$format_file	= '';
	$mandatory		= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<section class="panel">
						  <header class="panel-heading">
							  Syarat Detail
						  </header>
						<div class="panel-body">
							<div class="col-sm-12">
								<ul class="list-group list-group-unbordered">
									<li class="list-group-item"><b>Izin</b><a class="pull-right"><?php echo $izin_name; ?></a></li>
									<li class="list-group-item"><b>Izin Type</b><a class="pull-right"><?php echo $izin_type_name; ?></a></li>
									<li class="list-group-item"><b>Syarat Name</b><a class="pull-right"><?php echo $persyaratan_name; ?></a></li>
									<li class="list-group-item"><b>Max Size</b><a class="pull-right"><?php echo $max_size; ?></a></li>
									<li class="list-group-item"><b>Format File</b><a class="pull-right"><?php echo $format_file; ?></a></li>
									<li class="list-group-item"><b>Attribute File</b><a class="pull-right"><?php echo $mandatory; ?></a></li>
								</ul>
							</div>
						</div>
						  <div class="box-footer">
							<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						 </div>
					  </section>
					</div>
					
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>

<script>

function back(){
	window.location = "<?php echo $back; ?>";
}

</script>