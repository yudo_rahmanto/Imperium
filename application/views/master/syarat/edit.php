<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($getData){
	$syarat_id		= $getData[0]['syarat_id'];
	$izin_id		= $getData[0]['izin_id'];
	$izin_type_id	= $getData[0]['izin_type_id'];
	$persyaratan_id	= $getData[0]['persyaratan_id'];
	$max_size		= $getData[0]['max_size'];
	$format_file	= $getData[0]['format_file'];
	$mandatory		= $getData[0]['mandatory'];
} else {
	$syarat_id		= '';
	$izin_id		= '';
	$izin_type_id	= '';
	$persyaratan_id	= '';
	$max_size		= '';
	$format_file	= '';
	$mandatory		= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-6">
								<div class="form-group">
								  <label for="exampleInputEmail1">Izin<font color="red"> * </font> <i id="ket_izin" style="color:silver;"></i></label>
								  <input type="hidden" class="form-control" id="syarat_id" name="syarat_id" placeholder="Syarat Id" required="required" value="<?php echo $syarat_id; ?>">
								  <select class="form-control select2" id="izin" name="izin" style="width: 100%;" required="required">
									  <option value="">Select</option>
									<?php
										if($getIzin){
											foreach($getIzin as $row){
												if($izin_id == $row['izin_id']){$select	= "Selected = 'selected'";} else {$select	= '';}
									?>
									  <option value="<?php echo $row['izin_id']; ?>" <?php echo $select; ?>><?php echo $row['izin_name']; ?></option>
									<?php
											}
										}
									?>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
								  <label for="exampleInputEmail1">Type Izin<font color="red"> * </font> <i id="ket_izin_type" style="color:silver;"></i></label>
								  <select class="form-control select2" id="izin_type" name="izin_type" style="width: 100%;" required="required">
									  <option value="">Select</option>
									<?php
										if($getTypeIzin){
											foreach($getTypeIzin as $row){
												if($izin_type_id == $row['izin_type_id']){$select	= "Selected = 'selected'";} else {$select	= '';}
									?>
									  <option value="<?php echo $row['izin_type_id']; ?>" <?php echo $select; ?>><?php echo $row['type_name']; ?></option>
									<?php
											}
										}
									?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
								  <label for="exampleInputEmail1">Syarat Name<font color="red"> * </font> <i id="ket_syarat_name" style="color:silver;"></i></label>
								   <select class="form-control select2" id="syarat_name" name="syarat_name" style="width: 100%;" required="required">
									  <option value="">Select</option>
									<?php
										if($getPersyaratan){
											foreach($getPersyaratan as $row){
												if($persyaratan_id == $row['persyaratan_id']){$select	= "Selected = 'selected'";} else {$select	= '';}
									?>
									  <option value="<?php echo $row['persyaratan_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
									<?php
											}
										}
									?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
								  <label for="exampleInputEmail1">Max Size <i id="ket_max_size" style="color:silver;"></i></label>
								  <input type="text" class="form-control" id="max_size" name="max_size" placeholder="Max Size (Mb)" required="required"  value="<?php echo $max_size; ?>">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
								  <label for="exampleInputEmail1">Format File<i id="ket_max_size" style="color:silver;"></i></label>
								 <input type="text" class="form-control" id="format_file" name="format_file" placeholder="Contoh pengisisn : JPG|PNG|PDF|ZIP" required="required" value="<?php echo strtoupper($format_file); ?>">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
								<label for="exampleInputEmail1">Attribute File<i id="ket_mandatory" style="color:silver;"></i></label>
								<select class="form-control select2" id="mandatory" name="mandatory" style="width: 100%;" required="required">
									<option value="1" <?php if($mandatory == 1){echo "Checked='checked'";} ?>>Wajib</option>
									<option value="0" <?php if($mandatory == 0){echo "Checked='checked'";} ?>>Optional</option>
								</select>
								</div>
							</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>

<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
</script>