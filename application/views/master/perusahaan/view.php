 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession			= $this->session->userdata('user_data');
$UserId_Session			= $dataSession['user_id'];
$JabatanId_Session		= $dataSession['jabatan_id'];
$LevelId_Session		= $dataSession['level_id'];


?>	
<form class="form-signin" name="search" id="search" method="post" enctype="multipart/form-data">
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  <div class="clearfix">
						 <div class="btn-group">
							<?php
								if($JabatanId_Session	== 7){
							?>
								 <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm" type="button" aria-expanded="false"><i class="glyphicon glyphicon-plus"></i> Add Perusahaan <span class="caret"></span>
								</button>
								<ul role="menu" class="dropdown-menu">
								  <li><a onclick="create_new('pusat')">Kantor Pusat</a>
								  </li>
								  <li><a onclick="create_new('cabang')">Kantor Cabang</a>
								  </li>
								  
								</ul>
								<!--button type="button" class="btn btn-primary" ><i class="glyphicon glyphicon-plus"></i> Add Perusahaan</button-->
							<?php
								}
							?>
					  </div>
					</div>
					<div class="space15">&nbsp;</div>
                  <div class="x_content">
                   <table class="table table-striped table-bordered" id="datatable">
                              <thead>
                              <tr>
										<?php	
												if($field_table){
													foreach($field_table as $left=>$right){
														
										?>
										<th align="center"><?php echo ucwords(strtolower($right)); ?> </th>
										<?php
													}
												}
										?>
										<th align="center">Action</th>
                              </tr>
                              </thead>
                              <tbody>
							  <?php
										if($getData){
											$i	= 1;
											foreach($getData as $row2){
											if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}	
								?>
                              <tr class="">
								    <td>
										<input type="checkbox" class="tableflat" name="id<?php echo $i; ?>" id="id<?php echo $i; ?>" value="<?php echo $row2['alamat_perusahaan_id']; ?>" style="display:none;">
										<a href="<?php echo $edit.'/'.$row2['alamat_perusahaan_id']; ?>"><?php echo $row2['jenis_perusahaan']; ?></a>
									</td>
									<td><?php echo $row2['nama_perusahaan']; ?></td>
									<td><?php echo $row2['nama_cabang']; ?></td>
									<td><?php echo $row2['kecamatan_name']; ?></td>
									<td><?php echo $row2['kelurahan_name']; ?></td>
									<td><?php echo $row2['tlp']; ?></td>
									<td><?php echo $row2['alamat']; ?></td>
									<td>
										<button class="btn btn-round btn-info" type="button" onclick="location.href='<?php echo $detail.'/'.$row2['alamat_perusahaan_id']; ?>'">View</button>
										<button class="btn btn-round btn-warning" type="button" onclick="location.href='<?php echo $edit.'/'.$row2['alamat_perusahaan_id']; ?>'">Edit</button>
										<button class="btn btn-round btn-danger" type="button"  onclick="parseDataDelete('<?php echo $row2['alamat_perusahaan_id']; ?>')">Delete</button>
									</td>
								</tr>
								<?php
											$i++;
											}
										} else {
								?>
								<tr class="">
										<td colspan="9" align="center">No data result(s)</td>
									</tr>
								<?php
										}
								?>
							  
                              </tbody>
                          </table>
                  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		

<script>
<?php if($getData){ ?>
$(document).ready(function() {
		$('#datatable').DataTable();
		});
<?php	}	?>
function find(){
		var doc = document.search;
		doc.action= "<?php echo $page_action; ?>";
		doc.submit();
}
function parseDataDelete(id){
	var doc = document.search;
	var x;
	var r = confirm("Anda yakin ingin menghapus data ini");
	if (r==true){
		deleteData(id);
	 } else {
		return false;
	}
}
function deleteData(id){
	if(id){
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $delete; ?>',
			   data: {'id':id},
			   success: function(response) {
				   if(response == 'Success'){
					    document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete Data Success';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000); 
				   }
				    else if(response == 'Failed'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete File Failed';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						}, 2000);
				   }
			   }
		});
	}
}
function create_new(type){
	
	window.location = "<?php echo $add; ?>"+type;
	
}

</script>