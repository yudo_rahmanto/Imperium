<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getDataResult){
	$dataPerusahaan = '';
	foreach($getDataResult[0] as $row_left=>$row_right){
		$dataPerusahaan[$row_left] = $row_right;
	}
} 
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWtenJEq5zNO23Y8HOI_gMlhAZ3-7FxC0"></script>
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<ul class="list-group list-group-unbordered">
								<?php
									if(($dataPerusahaan['status']) && ($dataPerusahaan['status'] == 1)){
								?>
								<li class="list-group-item"><b>Jenis Perusahaan</b><a class="pull-right"><?php if($dataPerusahaan['jenis_perusahaan']){echo $dataPerusahaan['jenis_perusahaan'];} ?></a></li>
								<li class="list-group-item"><b>Nama Perusahaan</b><a class="pull-right"><?php if($dataPerusahaan['nama_perusahaan']){echo $dataPerusahaan['nama_perusahaan'];} ?></a></li>
								<?php
									} else {
								?>
								<li class="list-group-item"><b>Nama Perusahaan</b><a class="pull-right"><?php if($dataPerusahaan['nama_perusahaan']){echo $dataPerusahaan['nama_perusahaan'];} ?></a></li>
								<li class="list-group-item"><b>Nama Cabang</b><a class="pull-right"><?php if($dataPerusahaan['nama_cabang']){echo $dataPerusahaan['nama_cabang'];} ?></a></li>
								<?php
									}
								?>
								<li class="list-group-item"><b>No Tlp</b><a class="pull-right"><?php if($dataPerusahaan['tlp']){echo $dataPerusahaan['tlp'];} ?></a></li>
								<li class="list-group-item"><b>Alamat</b><a class="pull-right"><?php if($dataPerusahaan['alamat']){echo $dataPerusahaan['alamat'];} ?></a></li>
								<li class="list-group-item"><b>RT/RW</b><a class="pull-right"><?php if($dataPerusahaan['rt']){echo $dataPerusahaan['rt'];} ?> / <?php if($dataPerusahaan['rw']){echo $dataPerusahaan['rw'];} ?></a></li>
								<li class="list-group-item"><b>Provinsi</b><a class="pull-right"><?php if($dataPerusahaan['provinsi_name']){echo $dataPerusahaan['provinsi_name'];} ?></a></li>
								<li class="list-group-item"><b>Kabupaten</b><a class="pull-right"><?php if($dataPerusahaan['kabupaten_name']){echo $dataPerusahaan['kabupaten_name'];} ?></a></li>
								<li class="list-group-item"><b>Kecamatan</b><a class="pull-right"><?php if($dataPerusahaan['kecamatan_name']){echo $dataPerusahaan['kecamatan_name'];} ?></a></li>
								<li class="list-group-item"><b>Kelurahan</b><a class="pull-right"><?php if($dataPerusahaan['kelurahan_name']){echo $dataPerusahaan['kelurahan_name'];} ?></a></li>
								<li class="list-group-item"><b>Google Maps</b></li>
								<li class="list-group-item">
									<div id="map" style="height:400px;"></div>
									<input type="hidden" class="form-control" name="lat" id="lat" value="<?php if($dataPerusahaan['lat']){echo $dataPerusahaan['lat'];} ?>"> 
									<input type="hidden" class="form-control" name="lng" id="lng" value="<?php if($dataPerusahaan['lng']){echo $dataPerusahaan['lng'];} ?>"> 
								</li>
							</ul>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>

 
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
initMap(<?php if($dataPerusahaan['lat']){echo $dataPerusahaan['lat'];} ?>,<?php if($dataPerusahaan['lng']){echo $dataPerusahaan['lng'];} ?>);
function initMap(lat,lng) {
		var marker;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: lat, lng: lng}
        });

        marker = new google.maps.Marker({
          map: map,
          draggable: false,
          animation: google.maps.Animation.DROP,
          position: {lat: lat, lng: lng}
        });
        marker.addListener('click');
		google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng; 
        currentLatitude = latLng.lat();
        currentLongitude = latLng.lng();
		document.getElementById('lat').value = currentLatitude;
		document.getElementById('lng').value = currentLongitude;
	});
		
	}
</script>