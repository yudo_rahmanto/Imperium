<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getDataResult){
	$dataPerusahaan = '';
	foreach($getDataResult[0] as $row_left=>$row_right){
		$dataPerusahaan[$row_left] = $row_right;
	}
} 
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWtenJEq5zNO23Y8HOI_gMlhAZ3-7FxC0"></script>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
					<input type="hidden" class="form-control" id="perusahaan_id" name="perusahaan_id" placeholder="Id Perusahaan" required="required" value="<?php if($dataPerusahaan['perusahaan_id']){echo $dataPerusahaan['perusahaan_id'];} ?>">
					<input type="hidden" class="form-control" id="alamat_perusahaan_id" name="alamat_perusahaan_id" placeholder="Id Alamat Perusahaan" required="required" value="<?php if($dataPerusahaan['alamat_perusahaan_id']){echo $dataPerusahaan['alamat_perusahaan_id'];} ?>">
					<input type="hidden" class="form-control" id="type" name="type" placeholder="Type" required="required" value="<?php if($dataPerusahaan['status']){echo $dataPerusahaan['status'];} ?>">
						<?php
							if(($dataPerusahaan['status']) && ($dataPerusahaan['status'] == 1)){
						?>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Jenis Perusahan<font color="red"> * </font> <i id="ket_jenis_perusahaan" style="color:silver;"></i></label>
								<select class="form-control select2" id="jenis_perusahaan" name="jenis_perusahaan" style="width: 100%;" required="required">
									<option value="">Select</option>
									<?php
										if($getJenisPerusahaan){
											foreach($getJenisPerusahaan as $row){
												if($dataPerusahaan['perusahaan_type_id'] == $row['perusahaan_type_id']){$select	= "selected='selected'";} else {$select = "";}
									?>
									  <option value="<?php echo $row['perusahaan_type_id']; ?>" <?php echo $select; ?>><?php echo $row['perusahaan_type']; ?></option>
									<?php
											}
										}
									?>
								</select>	
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Nama Perusahaan <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" required="required" value="<?php if($dataPerusahaan['nama_perusahaan']){echo $dataPerusahaan['nama_perusahaan'];} ?>">
							</div>
						</div>
						<?php
							} else {
						?>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Nama Perusahaan<font color="red"> * </font> <i id="ket_jenis_perusahaan" style="color:silver;"></i></label>
							  <select class="form-control select2" id="nama_perusahaan" name="nama_perusahaan" style="width: 100%;" required="required">
									<?php
										if($getPerusahaan){
											foreach($getPerusahaan as $row){
												if($dataPerusahaan['perusahaan_id'] == $row['perusahaan_id']){$select	= "selected='selected'";} else {$select	= "";}
									?>
									  <option value="<?php echo $row['perusahaan_id']; ?>" <?php echo $select; ?>><?php echo $row['nama_perusahaan']; ?></option>
									<?php
											}
										}
									?>
								</select>	
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Nama Cabang <font color="red"> * </font> <i id="ket_nama_cabang" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="nama_cabang" name="nama_cabang" placeholder="Nama Cabang" required="required" value="<?php if($dataPerusahaan['nama_cabang']){echo $dataPerusahaan['nama_cabang'];} ?>">
							</div>
						</div>
						<?php
							}
						?>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">Jenis Usaha<font color="red"> * </font> <i id="ket_jenis_usaha" style="color:silver;"></i></label>
								<select class="form-control select2" id="jenis_usaha" name="jenis_usaha" style="width: 100%;" required="required" onchange="cek_jenis_usaha()">
								  <option value="">Select</option>
								<?php
									if($getJenisUsaha){
										foreach($getJenisUsaha as $row){
											if($dataPerusahaan['jenis_usaha_id'] == $row['jenis_usaha_id']){$select	= "selected='selected'";} else {$select	= "";}
								?>
								  <option value="<?php echo $row['jenis_usaha_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
								<?php
										}
									}
								?>
									<option value="Lainnya">Lainnya</option>
								</select>
								<input type="text" class="form-control" id="jenis_usaha_lainnya" name="jenis_usaha_lainnya" placeholder="Jenis Usaha Lainnya" <?php if(!$row['jenis_usaha_id']){echo "required='required'"; } ?> disabled="disabled" style="display:none;">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">Lingkungan Perusahaan <font color="red"> * </font> <i id="ket_lingkungan" style="color:silver;"></i></label>
								<select class="form-control select2" id="lingkungan" name="lingkungan" style="width: 100%;" required="required">
								  <option value="">Select</option>
								<?php
									if($getLingkungan){
										foreach($getLingkungan as $row){
											if($dataPerusahaan['lingkungan_perusahaan_id'] == $row['lingkungan_perusahaan_id']){$select	= "selected='selected'";} else {$select	= "";}
								?>
								  <option value="<?php echo $row['lingkungan_perusahaan_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
								<?php
										}
									}
								?>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
							  <label for="exampleInputEmail1">Indeks Lokasi Perusahaan <font color="red"> * </font> <i id="ket_lokasi_perusahaann" style="color:silver;"></i></label>
								<select class="form-control select2" id="lokasi_perusahaan" name="lokasi_perusahaan" style="width: 100%;" required="required">
								  <option value="">Select</option>
								<?php
									if($getLokasi){
										foreach($getLokasi as $row){
											if($dataPerusahaan['lokasi_perusahaan_id'] == $row['lokasi_perusahaan_id']){$select	= "selected='selected'";} else {$select	= "";}
								?>
								  <option value="<?php echo $row['lokasi_perusahaan_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
								<?php
										}
									}
								?>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
							  <label for="exampleInputEmail1">Luas Ruangan Tempat Usaha<font color="red"> * </font> <i id="ket_luas" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="luas" name="luas" placeholder="Luas Ruangan Tempat Usaha M2" required="required" value="<?php if($dataPerusahaan['luas']){echo $dataPerusahaan['luas'];} ?>" onkeyup="cekTyping('luas')">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
							  <label for="exampleInputEmail1">No Telp Perusahaan <font color="red"> * </font> <i id="ket_tlp" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="tlp" name="tlp" placeholder="No Telp Perusahaan" required="required" value="<?php if($dataPerusahaan['tlp']){echo $dataPerusahaan['tlp'];} ?>" onkeyup="cekTyping('tlp')">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Alamat Perusahaan <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
							</div>
							<div class="form-group">
							<input type="hidden" class="form-control" id="address" name="address" placeholder="address">
							<textarea class="resizable_textarea form-control" name="alamat" id="alamat" placeholder="Alamat Perusahaan" required="required"><?php if($dataPerusahaan['alamat']){echo $dataPerusahaan['alamat'];} ?></textarea>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group">
							  <label for="exampleInputEmail1">Rt<font color="red"> * </font> <i id="ket_kelurahan" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="rt" name="rt" placeholder="RT" required="required" value="<?php if($dataPerusahaan['rt']){echo $dataPerusahaan['rt'];} ?>">
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group">
							  <label for="exampleInputEmail1">Rw<font color="red"> * </font> <i id="ket_kelurahan" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="rw" name="rw" placeholder="RW" required="required" value="<?php if($dataPerusahaan['rw']){echo $dataPerusahaan['rw'];} ?>">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">Provinsi<font color="red"> * </font> <i id="ket_provinsi" style="color:silver;"></i></label>
								<select class="form-control select2" id="provinsi" name="provinsi" style="width: 100%;" onchange="getData(this.value,'kabupaten')" required="required">
									<option value="">Select</option>
									<?php
										if($getProvinsi){
											foreach($getProvinsi as $row){
												if($dataPerusahaan['status'] != 1){
													if($row['id'] == 16){
													echo "<option value='".$row['id']."'>".$row['nama']."</option>";
													}
												} else {
													if($dataPerusahaan['provinsi_id'] == $row['id']){$select	= "Selected='selected'";} else {$select	= "";}
													echo "<option value='".$row['id']."' ".$select.">".$row['nama']."</option>";
												}
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">Kabupaten<font color="red"> * </font> <i id="ket_kabupaten" style="color:silver;"></i></label>
								<select class="form-control select2" id="kabupaten" name="kabupaten" style="width: 100%;" onchange="getData(this.value,'kecamatan')" required="required">
									<?php
										if($getKabupaten){
											foreach($getKabupaten as $row){
												if($dataPerusahaan['status'] != 1){
													if($row['id'] == 1671){
													echo "<option value='".$row['id']."'>".$row['nama']."</option>";
													}
												} else {
													if($dataPerusahaan['kabupaten_id'] == $row['id']){$select	= "Selected='selected'";} else {$select	= "";}
													echo "<option value='".$row['id']."' ".$select.">".$row['nama']."</option>";
												}
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1">Kecamatan<font color="red"> * </font> <i id="ket_kecamatan" style="color:silver;"></i></label>
								<select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" onchange="getData(this.value,'kelurahan')" required="required">
									<?php
										if($getKecamatan){
											foreach($getKecamatan as $row){
												if($dataPerusahaan['kecamatan_id'] == $row['id']){$select	= "Selected='selected'";} else {$select	= "";}
										?>
										<option value="<?php echo $row['id']; ?>" <?php echo $select;?>><?php echo $row['nama']; ?></option>
										<?php
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1">Kelurahan<font color="red"> * </font> <i id="ket_kecamatan" style="color:silver;"></i></label>
							  <select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required="required" onchange="getCordinatSelect()">
									  <?php
										if($getKelurahan){
											foreach($getKelurahan as $row){
												if($dataPerusahaan['kelurahan_id'] == $row['id']){$select	= "Selected='selected'";} else {$select	= "";}
										?>
										<option value="<?php echo $row['id']; ?>" <?php echo $select;?>><?php echo $row['nama']; ?></option>
										<?php
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Google Maps <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
							</div>
							<div class="form-group">
								<div id="map" style="height:400px;">
								</div>
								<input type="hidden" class="form-control" name="lat" id="lat" value="<?php if($dataPerusahaan['lat']){echo $dataPerusahaan['lat'];} ?>"> 
								<input type="hidden" class="form-control" name="lng" id="lng" value="<?php if($dataPerusahaan['lng']){echo $dataPerusahaan['lng'];} ?>"> 
							</div>
							
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>
initMap(<?php if($dataPerusahaan['lat']){echo $dataPerusahaan['lat'];} ?>,<?php if($dataPerusahaan['lng']){echo $dataPerusahaan['lng'];} ?>);
function getCordinatSelect(){
	var kecamatan_id 	= document.getElementById('kecamatan').value;
	var kelurahan_id 	= document.getElementById('kelurahan').value;
	var address 		= '';
	var kabupaten		= '';
	if(kabupaten_id){
		$.each(<?php echo json_encode(SingleFilter('kabupaten')); ?>, function (index, data) {
			var id		= data['id'];
			var nama	= data['nama'];
			if(kabupaten_id == id){
				kabupaten	= nama;
			}
		});
	}
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getDataSelect; ?>',
			   data: {'find':'kecamatan','field':'id','id':kecamatan_id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						$.each(response, function (index, data) {
							var kecamatan	= data['nama'];
							document.getElementById('address').value = kecamatan;
						});
					}
			   }
	});
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getDataSelect; ?>',
			   data: {'find':'kelurahan','field':'id','id':kelurahan_id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						address = document.getElementById('address').value;
						$.each(response, function (index, data) {
							var kelurahan	= data['nama'];
							document.getElementById('address').value = address+', '+kelurahan+', '+kabupaten;
						});
					}
			   }
	});
	setTimeout(function(){
	findLocation();
	 }, 2000);
}
function findLocation(){
	var address = document.getElementById('address').value;
	if(!address){
			$.ajax({
			  url:"https://maps.googleapis.com/maps/api/geocode/json?address=palembang&sensor=false",
			  type: "POST",
			  success:function(res){
				var lat	= res.results[0].geometry.location.lat;
				var lng	= res.results[0].geometry.location.lng;
				initMap(lat,lng);
				document.getElementById('lat').value = lat;
				document.getElementById('lng').value = lng;
			  }
			});
		} else {
			$.ajax({
			  url:"https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
			  type: "POST",
			  success:function(res){
				if(res){
				var lat	= res.results[0].geometry.location.lat;
				var lng	= res.results[0].geometry.location.lng;
				initMap(lat,lng);
				document.getElementById('lat').value = lat;
				document.getElementById('lng').value = lng;
				}
			  }
			});
		}
}


function initMap(lat,lng) {
		if(!lat){lat = <?php if($dataPerusahaan['lat']){echo $dataPerusahaan['lat'];} ?>;}
		if(!lng){lng = <?php if($dataPerusahaan['lng']){echo $dataPerusahaan['lng'];} ?>;}
		var marker;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: lat, lng: lng}
        });

        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: {lat: lat, lng: lng}
        });
        marker.addListener('click');
		google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng; 
        currentLatitude = latLng.lat();
        currentLongitude = latLng.lng();
		document.getElementById('lat').value = currentLatitude;
		document.getElementById('lng').value = currentLongitude;
	});
		
	}
function back(){
	window.location = "<?php echo $back; ?>";
}
function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = '';
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function cek_jenis_usaha(){
	var value	= document.getElementById('jenis_usaha').value;
	if(value == 'Lainnya'){
		document.getElementById('jenis_usaha_lainnya').disabled = false;
		document.getElementById('jenis_usaha_lainnya').style.display = "Block";
		document.getElementById("jenis_usaha_lainnya").required = true;
	} else {
		document.getElementById('jenis_usaha_lainnya').disabled = true;
		document.getElementById('jenis_usaha_lainnya').style.display = "none";
		document.getElementById("jenis_usaha_lainnya").required = false;
	}
}
function cekTyping(field){
	var value	= document.getElementById(field).value;
	 if(field == 'tlp'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'luas'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
}
function simpan(){
		var validasi	= $('#form').parsley().validate();
		var lat 	= document.getElementById('lat').value;
		var lng 	= document.getElementById('lng').value;
		if((validasi) && (!lat) && (!lng)){
			document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
			document.getElementById('message-text').innerHTML = 'Maaf, Anda belum belum menetukan alamat anda pada google maps';
			$('#box-information').modal("show");
			setTimeout(function(){
			 $('#box-information').modal("hide");
			 }, 2000);
		}
		else if((validasi) && (lat) && (lng)){
			$.ajax({
					   type: 'post',
					   url: '<?php echo $action; ?>',
					   data: $('form').serialize(),
					   success: function(response) {
						  if(response == 'sukses'){
								document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								back()
								}, 2000);
							}
							else if(response == 'duplikat'){
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								}, 2000);
							  
							} else {
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								}, 2000);
								
						  }
					   }
					});
		}
}
</script>