<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($dataTemplate){
	$isiData		= "";
	foreach($dataTemplate as $row_left=>$row_right){
		$isiData[$row_left]	= $row_right;
	}
} 
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Nama Template<font color="red"> * </font> <i id="ket_title" style="color:silver;"></i></label>
							  <input type="hidden" class="form-control" id="template_surat_id" name="template_surat_id" placeholder="template_surat_id" required="required" value="<?php echo $isiData['template_surat_id']; ?>">
							  <input type="text" class="form-control" id="title" name="title" placeholder="Nama Template" required="required" value="<?php echo $isiData['title']; ?>">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Jumlah Halaman</label>
							  <select class="form-control select2" id="jml_halaman" name="jml_halaman" style="width: 10%;" onchange="getEditor()">
								<?php
									for($i=1;$i<=10;$i++){
										if(count($isiData['content']) == $i){$select = "Selected='selected'";} else {$select = "";}
										echo "<option value='".$i."' ".$select.">".$i."</option>";
									}
								?>
							  </select>
							</div>
						</div>
						<div id="extend" class="x_content"></div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>
getEditor();
function getEditor(){
	CKEDITOR.config.fullPage = true;
	CKEDITOR.config.allowedContent = true;
	var jml_page	= document.getElementById('jml_halaman').value;
	var editor		= "";
	for(var i=1;i<=jml_page;i++){
		editor	+= "<div class='col-sm-12'>";
		editor	+= "	<div class='form-group'>";
		editor	+= "	<label>Halaman "+i+"</label>";
		editor	+= "		<textarea class='form-control ckeditor' id='editor"+i+"' name='editor"+i+"'></textarea>";
		editor	+= "	</div>";
		editor	+= "</div>";
	}
	document.getElementById('extend').innerHTML = editor;
	for(var i=1;i<=jml_page;i++){
		CKEDITOR.replace('editor'+i);
	}
	var i = 1;
	$.each(<?php echo json_encode($isiData['content']); ?>, function (index, data) {
			 var content	= data['editor'];  
			 CKEDITOR.instances['editor'+i].setData(content);
			i++;
		});
		
}
function back(){
	window.location = "<?php echo $back; ?>";
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		for (instance in CKEDITOR.instances) {
				CKEDITOR.instances[instance].updateElement();
			}
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
</script>