<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$alur_izin_id	= $getData[0]['alur_izin_id'];
	$izin_id		= $getData[0]['izin_id'];
	$izin_type_id	= $getData[0]['izin_type_id'];
	
} else {
	$alur_izin_id	= '';
	$izin_id		= '';
	$izin_type_id	= '';
	
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
							<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Izin<font color="red"> * </font> <i id="ket_izin" style="color:silver;"></i></label>
								  <select class="form-control select2" id="izin" name="izin" style="width: 100%;" required="required">
									  <option value="">Select</option>
									<?php
										if($getIzin){
											foreach($getIzin as $row){
												if($izin_id == $row['izin_id']){$select	= "selected='selected'";} else {$select	= "";}
									?>
									  <option value="<?php echo $row['izin_id']; ?>" <?php echo $select; ?>><?php echo $row['izin_name']; ?></option>
									<?php
											}
										}
									?>
									</select>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Type Izin<font color="red"> * </font> <i id="ket_izin_type" style="color:silver;"></i></label>
								  <select class="form-control select2" id="izin_type" name="izin_type" style="width: 100%;" required="required">
									  <option value="">Select</option>
									<?php
										if($getIzinType){
											foreach($getIzinType as $row){
													if($izin_type_id == $row['izin_type_id']){$select	= "selected='selected'";} else {$select	= "";}
									?>
									  <option value="<?php echo $row['izin_type_id']; ?>" <?php echo $select; ?>><?php echo $row['type_name']; ?></option>
									<?php
											}
										}
									?>
									</select>
								</div>
							</div>
					</div>
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Struktur Approval Izin</label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<table class="table table-striped table-hover table-bordered" id="table_struktur">
									<thead>
									<tr id="struktur_0">
										<th width="10%" align="center">No</th>
										<th width="60%" align="center">Jabatan</th>
										<th width="30%" align="center">Action</th>
									</tr>
									</thead>
									<?php
										if($getData){
											$i = 1;
											foreach($getData as $row){
												$jabatan_id = $row['jabatan_id'];
									?>
									<tbody id="struktur_<?php echo $i; ?>">
									<tr>
										<td align="center"><?php echo $i; ?></td>
										<td align="center">
											<input type="hidden" class="form-control" name="alur_izin_id_<?php echo $i; ?>" id="alur_izin_id_<?php echo $i; ?>" value="<?php echo $row['alur_izin_id'] ?>">
											<select class="form-control select2" id="jabatan_<?php echo $i; ?>" name="jabatan_<?php echo $i; ?>" style="width: 100%;" required="required">
											<option value="">Select</option>
											<?php
												if($getJabatan){
													foreach($getJabatan as $row){
														if($jabatan_id == $row['jabatan_id']){$select	= "selected='selected'";} else {$select	= "";}
											?>
											  <option value="<?php echo $row['jabatan_id']; ?>" <?php echo $select; ?>><?php echo $row['jabatan_name']; ?></option>
											<?php
													}
												}
											?>
											</select>
										</td>
										<td align="center">
											<button type="button" class="btn btn-primary" onclick="tambah()">+</button>
											<button type="button" class="btn btn-primary" onclick="kurang('<?php echo $i; ?>')">-</button>
										</td>
									</tr>
									</tbody>
									<?php
											$i++;
											}
										}
									?>
								</table>	
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<input type="hidden" class="form-control" name="jml_row" id="jml_row" value="<?php if($getData){echo count($getData);} ?>">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>

<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function tambah(){
	var jml_row		 		= document.getElementById('jml_row').value;
	if(!jml_row){jml_row	= 1;}
	var i					= parseInt(jml_row) + 1;
	var jabatan	= "<select class='form-control select2' id='jabatan_"+i+"' name='jabatan_"+i+"' style='width: 100%;' required='required'>";
		jabatan	+= "<option value=''> Select </option>";
		$.each(<?php echo json_encode($getJabatan); ?>, function (index, data) {
			var jabatan_id			= data['jabatan_id'];	
			var jabatan_name			= data['jabatan_name'];						
			jabatan	+= "<option value='"+jabatan_id+"'>"+jabatan_name+"</option>";
		});
		jabatan	+= "</select>";
	var contentTable 	= document.getElementById('table_struktur');
		var newTbody 		= document.createElement('tbody');
		newTbody.setAttribute('id','struktur_'+i);
		var newTr 			= newTbody.appendChild(document.createElement('tr'));
		var newTd1 			= newTr.appendChild(document.createElement('td'));		
			newTd1.innerHTML 	= "<div align='center'>"+i+"</div>";		
		var newTd2 			= newTr.appendChild(document.createElement('td'));
			newTd2.innerHTML 	= "<div align='center'>"+jabatan+"</div>";
		var newTd3 			= newTr.appendChild(document.createElement('td'));
			newTd3.innerHTML 	= "<div align='center'><button type='button' class='btn btn-primary' onclick='tambah()'>+</button><button type='button' class='btn btn-primary' onclick='kurang("+i+")'>-</button></div>";
		
		
		contentTable.appendChild(newTbody);
	
	document.getElementById('jml_row').value = i;
}
function kurang(no){
	var jml_row 	= parseInt($('#jml_row').val());
		var i = 0;
		if(jml_row > 1){			
			
			var contentTable 	= document.getElementById('table_struktur');		
			contentTable.removeChild(document.getElementById('struktur_'+no));
			
			for(z=1;z<=jml_row;z++){			
	   			if(z!=no){   		
					i = eval(i) + 1;
					var getJabatanId	= document.getElementById('jabatan_'+z).value;
					var jabatan	= "<select class='form-control select2' id='jabatan_"+i+"' name='jabatan_"+i+"' style='width: 100%;' required='required'>";
						jabatan	+= "<option value=''> Select </option>";
					$.each(<?php echo json_encode($getJabatan); ?>, function (index, data) {
						var jabatan_id			= data['jabatan_id'];	
						var jabatan_name		= data['jabatan_name'];	
						if(getJabatanId == jabatan_id){var selected = "Selected='selected'";} else {var selected = "";}
												
						jabatan	+= "<option value='"+jabatan_id+"' "+selected+">"+jabatan_name+"</option>";
					});
						jabatan	+= "</select>";
					var newTbody 		= document.createElement('tbody');
						newTbody.setAttribute('id','struktur_'+i);
					var newTr 			= newTbody.appendChild(document.createElement('tr'));
					var newTd1 			= newTr.appendChild(document.createElement('td'));		
						newTd1.innerHTML 	= "<div align='center'>"+i+"</div>";		
					var newTd2 			= newTr.appendChild(document.createElement('td'));
						newTd2.innerHTML 	= "<div align='center'>"+jabatan+"</div>";
					var newTd9 			= newTr.appendChild(document.createElement('td'));
						newTd9.innerHTML 	= "<div align='center'><button type='button' class='btn btn-primary' onclick='tambah()'>+</button><button type='button' class='btn btn-primary' onclick='kurang("+i+")'>-</button></div>";
					contentTable.removeChild(document.getElementById('struktur_'+z));		
					contentTable.appendChild(newTbody);
					
				}
				
	   		}	   		
			no = no - 1; 
			$('#jml_row').val(no);
			
		}
	
}
</script>