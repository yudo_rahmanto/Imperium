<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$alur_izin_id	= $getData[0]['alur_izin_id'];
	$izin_id		= $getData[0]['izin_id'];
	$izin_name		= $getData[0]['izin_name'];
	$izin_type_id	= $getData[0]['izin_type_id'];
	$izin_type_name	= $getData[0]['izin_type_name'];
} else {
	$alur_izin_id	= '';
	$izin_id		= '';
	$izin_name		= '';
	$izin_type_id	= '';
	$izin_type_name	= '';
	
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
							<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Izin<font color="red"> * </font> <i id="ket_izin" style="color:silver;"></i></label>
								</div>
								<div class="form-group">
									<?php echo $izin_name; ?>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Type Izin<font color="red"> * </font> <i id="ket_izin_type" style="color:silver;"></i></label>
								</div>
								<div class="form-group">
									<?php echo $izin_type_name; ?>
								</div>
							</div>
					</div>
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Struktur Approval Izin</label>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<table class="table table-striped table-hover table-bordered" id="table_struktur">
									<thead>
									<tr id="struktur_0">
										<th width="10%"><div align="center">No</div></th>
										<th width="90%"><div align="center">Jabatan</div></th>
									</tr>
									</thead>
									<?php
										if($getData){
											$i = 1;
											foreach($getData as $row){
												$jabatan_name = $row['jabatan_name'];
									?>
									<tbody id="struktur_<?php echo $i; ?>">
									<tr>
										<td align="center"><?php echo $i; ?></td>
										<td align="center"><?php echo $jabatan_name; ?></td>
									</tr>
									</tbody>
									<?php
											$i++;
											}
										}
									?>
								</table>	
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
					 </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>

<script>

function back(){
	window.location = "<?php echo $back; ?>";
}

</script>