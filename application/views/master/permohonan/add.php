<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$UserId_Session					= $dataSession['user_id'];
$JabatanId_Session				= $dataSession['jabatan_id'];
$LevelId_Session				= $dataSession['level_id'];
?>

<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div id="wizard" class="form_wizard wizard_horizontal">
						  <ul class="wizard_steps">
							<li>
							  <a href="#step-1">
								<span class="step_no">1</span>
								<span class="step_descr">
												  Step 1<br />
												  <small>Pilih Izin</small>
											  </span>
							  </a>
							</li>
							<li>
							  <a href="#step-2">
								<span class="step_no">2</span>
								<span class="step_descr">
												  Step 2<br />
												  <small>Display Data Perusahaan</small>
											  </span>
							  </a>
							</li>
							<li>
							  <a href="#step-3">
								<span class="step_no">3</span>
								<span class="step_descr">
												  Step 3<br />
												  <small>Upload Data Persyaratan</small>
											  </span>
							  </a>
							</li>
							
						  </ul>
						  <div id="step-1">
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Pilih Perusahaan<font color="red"> * </font> <i id="ket_perusahaan" style="color:silver;"></i></label>
									  <select class="form-control select2" id="perusahaan" name="perusahaan" style="width: 100%;" required="required" onchange="getAlamatPerusahaan()">
											<option value="">Select</option>
											<option value="new">Baru</option>
											<?php
												if($getPerusahaan){
													foreach($getPerusahaan as $row){
											?>
											<option value="<?php echo $row['perusahaan_id']; ?>"><?php echo $row['nama_perusahaan']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Pilih Alamat Perusahaan<font color="red"> * </font> <i id="ket_alamat_perusahaan" style="color:silver;"></i></label>
										<select class="form-control select2" id="pilih_alamat_perusahaan" name="pilih_alamat_perusahaan" style="width: 100%;" required="required" onchange="getSyarat()">
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Nama Ijin<font color="red"> * </font> <i id="ket_izin" style="color:silver;"></i></label>
										<input type="hidden" class="form-control" id="izin_category_id" name="izin_category_id" placeholder="izin_category_id">
									  	<select class="form-control select2" id="izin" name="izin" style="width: 100%;" required="required" onchange="getSyarat()">
											<option value="">Select</option>
											<?php
												if($getIzin){
													foreach($getIzin as $row){
											?>
											<option value="<?php echo $row['izin_id']; ?>"><?php echo $row['izin_name']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Jenis Permohonan<font color="red"> * </font> <i id="ket_izin_type" style="color:silver;"></i></label>
										<select class="form-control select2" id="izin_type" name="izin_type" style="width: 100%;" required="required" onchange="getSyarat()">
											<option value="">Select</option>
											<?php
												if($getTypeIzin){
													foreach($getTypeIzin as $row){
											?>
											<option value="<?php echo $row['izin_type_id']; ?>"><?php echo $row['type_name']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12" style="height:150px;">
									<div class="form-group">
									  <label for="exampleInputEmail1">Persyaratan<font color="red"> * </font> <i id="ket_persyaratan" style="color:silver;"></i></label>
									</div>
									<div class="form-group">
										<table class="table table-striped table-hover table-bordered" id="table_persyaratan"></table>	
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">&nbsp;</label>
									</div>
								</div>
							
						  </div>
							<div id="step-2">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-12 col-sm-12 col-xs-12" for="exampleInput"><h2 class="StepTitle">Data Izin Permohonan</h2></label>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Nama Izin</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="nama_izin" style="margin-left:2%;">Nama Izin</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Jenis Permohonan</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="jenis_permohonan" style="margin-left:2%;">Jenis Permohonan</label>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-12 col-sm-12 col-xs-12" for="exampleInput"><h2 class="StepTitle">Data Pemohon</h2></label>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>No KTP</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="ktp" style="margin-left:2%;">No KTP</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Full Name</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="full_name" style="margin-left:2%;">Full Name</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Email</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="email" style="margin-left:2%;">Email</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>No Telp</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="no_tlp_user" style="margin-left:2%;">No Telp</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Provinsi</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="provinsi_user" style="margin-left:2%;">Provinsi</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Kota / Kabupaten</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="kabupaten_user" style="margin-left:2%;">Kabupaten</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Kecamatan</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="kecamatan_user" style="margin-left:2%;">Kecamatan</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Kelurahan</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="kelurahan_user" style="margin-left:2%;">Kelurahan</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>RT / RW</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="rt/rw_user" style="margin-left:2%;">RT / RW</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Alamat</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="alamat_user" style="margin-left:2%;">Alamat</label>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-12 col-sm-12 col-xs-12" for="exampleInput"><h2 class="StepTitle">Data Perusahaan</h2></label>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Nama Perusahaan</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="nama_perusahaan" style="margin-left:2%;">Nama Perusahaan</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>No Tlp</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="no_tlp_perusahaan" style="margin-left:2%;">No Tlp</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Provinsi</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="provinsi_perusahaan" style="margin-left:2%;">Provinsi</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Kota / Kabupaten</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="kabupaten_perusahaan" style="margin-left:2%;">Kabupaten</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Kecamatan</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="kecamatan_perusahaan" style="margin-left:2%;">Kecamatan</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Kelurahan</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="kelurahan_perusahaan" style="margin-left:2%;">Kelurahan</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>RT/RW</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="rt/rw_perusahaan" style="margin-left:2%;">RT/RW</label>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Alamat</b></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<label for="exampleInput" id="alamat_perusahaan" style="margin-left:2%;">Alamat</label>
										</div>
									</div>
								</div>
								<div id="extend" class="x_content"></div>
							</div>
							<div id="step-3">
								<h2 class="StepTitle">Step 3 Upload Data Persyaratan</h2>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-md-12 col-sm-12 col-xs-12" for="exampleInput">Upload Persyaratan</label>
									</div>
									<div class="form-group">
										<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan"></table>
									</div>									
								</div>
							</div>
						</div>
						<input type="hidden" name="jml_row_syarat" id="jml_row_syarat">
					</div>
					
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		
  
<script>

$('#wizard').smartWizard();
$('.buttonNext').hide();
$('.buttonPrevious').hide();
$('.buttonFinish').hide();
function detection_perusahaan(){
	var perusahaan = document.getElementById('perusahaan').value;
	if(perusahaan == 'new'){
		window.location.href = "<?php echo $addPerusahaan; ?>";
	}
}
function getAlamatPerusahaan(){
	var perusahaan = document.getElementById('perusahaan').value;
	if(perusahaan == 'new'){
		window.location.href = "<?php echo $addPerusahaan.'/pusat'; ?>";
	} else {
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $linkAlamatPerusahaan; ?>',
			   data: {'perusahaan_id':perusahaan},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = "<option value=''>Select</option>";
						$.each(response, function (index, data) {
							var id		= data['alamat_perusahaan_id'];
							var nama	= data['nama_cabang'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById('pilih_alamat_perusahaan').innerHTML = html;
					}
			   }
	});
	}
}

function checkFile(id){
	var file		= document.getElementById('upload_file'+id).files;
	var name_file 	= file[0]['name'].toLowerCase();
	var size_file	= file[0]['size'];
	var format_default 	= document.getElementById('format_file'+id).value.toLowerCase();
	var size_default 	= document.getElementById('max_size'+id).value;
	var explode_format_file	= format_default.split("|");
	var hasil		= false;
	if(name_file){
		var explode = name_file.split('.');
		var format	= explode[explode.length-1];
		for(var i = 0; i < explode_format_file.length; i++){
			if(explode_format_file[i] == format){
				hasil = true;
			}
			else if(size_file < size_default){
				hasil = true;
			}
		}
		
		if(!hasil){
			alert('Maaf Format file / Size tidak sesuai');
			document.getElementById('upload_file'+id).value = '';
		}
	}
}
function getSyarat(){
	var perusahaan			= document.getElementById('perusahaan').value;
	var alamat_perusahaan	= document.getElementById('pilih_alamat_perusahaan').value;
	var izin				= document.getElementById('izin').value;
	var izin_type			= document.getElementById('izin_type').value;
	var modalUsaha			= "";
	var kegiatanUsaha		= "";
	var kelembagaan			= "";
	var product				= "";
	if((alamat_perusahaan) && (izin) && (izin_type)){
			
		$.each(<?php echo json_encode($getIzin); ?>, function (index, data) {
			 var izin_category_id	= data['izin_category_id'];  
			 var izin_id			= data['izin_id'];  
			 var izin_name			= data['izin_name']; 
			 $.each(<?php echo json_encode($getTypeIzin); ?>, function (index, data) {
				var izin_type_id		= data['izin_type_id']; 
				var izin_type_name		= data['type_name']; 
				if(izin_type_id == izin_type){
					document.getElementById('jenis_permohonan').innerHTML = izin_type_name;
				}
			 });
			if(izin == izin_id){
				document.getElementById('izin_category_id').value = izin_category_id; 
				document.getElementById('nama_izin').innerHTML = izin_name; 
				 if(izin_category_id == 1){
					modalUsaha += " <div class='col-sm-12'>";
					modalUsaha += "		<div class='form-group'>";
					modalUsaha += "			<label class='control-label col-md-3 col-sm-3 col-xs-12' for='exampleInputEmail1'>Modal Usaha<font color='red'> * </font> <i id='ket_modal_usaha' style='color:silver;'></i></label>";
					modalUsaha += "			<div class='col-md-6 col-sm-6 col-xs-12'>";
					modalUsaha += "				<input type='hidden' class='form-control' id='modal_usaha_min' name='modal_usaha_min' placeholder='Modal Usaha Min'>";
					modalUsaha += "				<input type='hidden' class='form-control' id='modal_usaha_max' name='modal_usaha_max' placeholder='Modal Usaha Max'>";
					modalUsaha += "				<input type='text' class='form-control' id='modal_usaha' name='modal_usaha' placeholder='Modal Usaha' required='required' onblur='cekInt(modal_usaha)'>";
					modalUsaha += "			</div>";
					modalUsaha += "		</div>";
					modalUsaha += "	</div>";
					
					kegiatanUsaha += " <div class='col-sm-12'>";
					kegiatanUsaha += "	  <div class='form-group'>";
					kegiatanUsaha += "		  <label class='control-label col-md-3 col-sm-3 col-xs-12' for='exampleInput'>Kegiatan Usaha<font color='red'> * </font> <i id='ket_kegiatan_usaha' style='color:silver;'></i></label>";
					kegiatanUsaha += "		<div class='col-md-6 col-sm-6 col-xs-12'>";
					kegiatanUsaha += "		  <input type='text' class='form-control' id='kegiatan_usaha' name='kegiatan_usaha' placeholder='Kegiatan Usaha' required='required'>";
					kegiatanUsaha += "			</div>";
					kegiatanUsaha += "	  </div>";
					kegiatanUsaha += " </div>";
					
					kelembagaan += " <div class='col-sm-12'>";
					kelembagaan += "	  <div class='form-group'>";
					kelembagaan += "		  <label class='control-label col-md-3 col-sm-3 col-xs-12' for='exampleInputEmail1'>Kelembagaan<font color='red'> * </font> <i id='ket_kelembagaan' style='color:silver;'></i></label>";
					kelembagaan += "		<div class='col-md-6 col-sm-6 col-xs-12'>";
					kelembagaan += "		  <input type='text' class='form-control' id='kelembagaan' name='kelembagaan' placeholder='Kelembagaan' required='required'>";
					kelembagaan += "		</div>";
					kelembagaan += "	  </div>";
					kelembagaan += " </div>";
					
					product += " <div class='col-sm-12'>";
					product += "	  <div class='form-group'>";
					product += "		  <label class='control-label col-md-3 col-sm-3 col-xs-12' for='exampleInputEmail1'>Product<font color='red'> * </font> <i id='ket_product' style='color:silver;'></i></label>";
					product += "		<div class='col-md-6 col-sm-6 col-xs-12'>";
					product += "		  <input type='text' class='form-control' id='product' name='product' placeholder='Product' required='required'>";
					product += "		</div>";
					product += "	  </div>";
					product += " </div>";
				 }
				 
			}
				document.getElementById('extend').innerHTML = modalUsaha+" "+kegiatanUsaha+" "+kelembagaan+" "+product;
				
		 });
		 var user_id	= "<?php echo $UserId_Session; ?>";
		 getPemohon(user_id);
		 jQuery.ajax({
							   type: 'post',
							   url: '<?php echo $linkPerusahaan; ?>',
							   data: {'alamat_perusahaan':alamat_perusahaan},
							   dataType: 'json',
							   success: function(response) {
									if(response){
										$.each(response, function (index, data) {
											var perusahaan_id	= data['perusahaan_id'];
											var nama_perusahaan	= data['nama_perusahaan'];
											var tlp				= data['tlp'];
											var kecamatan_name	= data['kecamatan_name'];
											var kelurahan_name	= data['kelurahan_name'];
											var kabupaten_name	= data['kabupaten_name'];
											var provinsi_name	= data['provinsi_name'];
											var rt	= data['rt'];
											var rw	= data['rw'];
											var alamat	= data['alamat'];
											document.getElementById('nama_perusahaan').innerHTML = nama_perusahaan;
											document.getElementById('no_tlp_perusahaan').innerHTML = tlp;
											document.getElementById('provinsi_perusahaan').innerHTML = provinsi_name;
											document.getElementById('kabupaten_perusahaan').innerHTML = kabupaten_name;
											document.getElementById('kecamatan_perusahaan').innerHTML = kecamatan_name;
											document.getElementById('kelurahan_perusahaan').innerHTML = kelurahan_name;
											document.getElementById('rt/rw_perusahaan').innerHTML = rt+' / '+rw;
											document.getElementById('alamat_perusahaan').innerHTML = alamat;
										});
										
									}
							   }
					});
		 jQuery.ajax({
							   type: 'post',
							   url: '<?php echo $getIzinValidasi; ?>',
							   data: {'izin_id':izin},
							   dataType: 'json',
							   success: function(response) {
								   console.log(response);
								   $.each(response, function (index, data) {
										var min		= data['min'];
										var max		= data['max'];
										document.getElementById('modal_usaha_min').value = min;
										document.getElementById('modal_usaha_max').value = max;
									});
							   }
					});
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $linkSyarat; ?>',
			   data: {'izin_id':izin,'izin_type_id':izin_type},
			   dataType: 'json',
			   success: function(response) {
					
				   if(response){
						$('.buttonNext').show();
						$('.buttonPrevious').show();
						$('.buttonFinish').show();
						$('.buttonNext').addClass('btn btn-success');
						$('.buttonPrevious').addClass('btn btn-primary');
						$('.buttonFinish').addClass('btn btn-default');
						$('.buttonFinish').removeAttr("href");
						$('.buttonFinish').attr("id","finish_button");
						document.getElementById('finish_button').setAttribute("onclick","simpan()");
						var html	= "";
							html	+= "	<thead>";
							html	+= "		<tr>";
							html	+= "			<th>No</th>";
							html	+= "			<th> Persyaratan </th>";
							html	+= "			<th> Attribute File </th>";
							html	+= "		</tr>";
							html	+= "	</thead>";
						
						var html1	= "";
							html1	+= "	<thead>";
							html1	+= "		<tr>";
							html1	+= "			<th width='5%'>No</th>";
							html1	+= "			<th width='30%'> Persyaratan </th>";
							html1	+= "			<th width='25%'> Format & Max Size </th>";
							html1	+= "			<th width='10%'> Attribute File </th>";
							html1	+= "			<th width='30%'> Attach </th>";
							html1	+= "		</tr>";
							html1	+= "	</thead>";
						var i = 1;
					   $.each(response, function (index, data) {
						var syarat_id		= data['syarat_id'];  
						var syarat_name		= data['syarat_name'];  
						var max_size		= data['max_size'];  
						var format_file		= data['format_file'];  
						var mandatory		= data['mandatory'];  
						if(mandatory == 1){
							var ket_mandatory 	= "Wajib";
							var requirment		= "required='required'";
							} else {
								var ket_mandatory 	= "Optional";
								var requirment		= "";
							}
							html	+= "	<tbody id='syarat"+i+"'>";
							html	+= "		<tr>";
							html	+= "			<td>"+i+"</td>";
							html	+= "			<td> "+syarat_name+"</td>";
							html	+= "			<td> "+ket_mandatory+"</td>";
							html	+= "		</tr>";
							html	+= "	</tbody>";
							
							html1	+= "	<tbody id='syarat"+i+"'>";
							html1	+= "		<tr>";
							html1	+= "			<td>"+i+"</td>";
							html1	+= "			<td> "+syarat_name+"</td>";
							html1	+= "			<td> "+format_file+" ( Max Size : "+max_size+" MB)</td>";
							html1	+= "			<td> "+ket_mandatory+" </td>";
							html1	+= "			<td> <input type='hidden' class='form-control' name='syarat_id"+i+"' id='syarat_id"+i+"' value='"+syarat_id+"'><input type='hidden' class='form-control' name='max_size"+i+"' id='max_size"+i+"' value='"+max_size+"'><input type='hidden' class='form-control' name='format_file"+i+"' id='format_file"+i+"' value='"+format_file+"'><input type='file' class='form-control' name='upload_file"+i+"' id='upload_file"+i+"' "+requirment+" onchange='checkFile("+i+")'></td>";
							html1	+= "		</tr>";
							html1	+= "	</tbody>";
						i++;
					   });
							html1	+= "	<tbody id='syarat_spasi'>";
							html1	+= "		<tr>";
							html1	+= "			<td colspan='5'>&nbsp;</td>";
							html1	+= "		</tr>";
							html1	+= "	</tbody>";
							html1	+= "	<tbody id='syarat_konfirm'>";
							html1	+= "		<tr>";
							html1	+= "			<td colspan='5'><input type='checkbox' name='konfirmasi' id='konfirmasi' value='1'> &nbsp;Dengan ini saya menyatakan bahwa data yang disampaikan adalah benar dan sesuai dengan aslinya.</td>";
							html1	+= "		</tr>";
							html1	+= "	</tbody>";
							document.getElementById('table_persyaratan').innerHTML = html;
							document.getElementById('table_upload_persyaratan').innerHTML = html1;
							document.getElementById('jml_row_syarat').value = (i-1);
				   } 
				   
			   },
			   error: function(error){
					$('#wizard').smartWizard();
					$('.buttonNext').hide();
					$('.buttonPrevious').hide();
					$('.buttonFinish').hide();
					document.getElementById('table_persyaratan').innerHTML = "";
					document.getElementById('table_upload_persyaratan').innerHTML = "";
					document.getElementById('jml_row_syarat').value = 0;
			   }
		})
	}
}
function getPemohon(id){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $linkPemohon; ?>',
			   data: {'id':id},
			   dataType: 'json',
			   success: function(response) {
				   if(response){
						
					   $.each(response, function (index, data) {
						var ktp					= data['ktp'];  
						var first_name			= data['first_name'];  
						var last_name			= data['last_name'];  
						var email				= data['email'];  
						var no_tlp				= data['no_tlp'];  
						var address				= data['address'];  
						var rt					= data['rt'];  
						var rw					= data['rw'];  
						var provinsi_name		= data['provinsi_name'];  
						var kabupaten_name		= data['kabupaten_name'];  
						var kecamatan_name		= data['kecamatan_name'];  
						var kelurahan_name		= data['kelurahan_name'];  
						document.getElementById('ktp').innerHTML = ktp;
						document.getElementById('full_name').innerHTML = first_name+' '+last_name;
						document.getElementById('email').innerHTML = email;
						document.getElementById('no_tlp_user').innerHTML = no_tlp;
						document.getElementById('provinsi_user').innerHTML = provinsi_name;
						document.getElementById('kabupaten_user').innerHTML = kabupaten_name;
						document.getElementById('kecamatan_user').innerHTML = kecamatan_name;
						document.getElementById('kelurahan_user').innerHTML = kelurahan_name;
						document.getElementById('rt/rw_user').innerHTML = rt+' / '+rw;
						document.getElementById('alamat_user').innerHTML = address;
						});
							
				   }
			   }
		});
		
}
function back(){
	window.location = "<?php echo $back; ?>";
}
function cekInt(){
	
	var value	= document.getElementById('modal_usaha').value;
	var izin	= document.getElementById('izin').value;
	var min		= document.getElementById('modal_usaha_min').value;
	var max		= document.getElementById('modal_usaha_max').value;
		if(value.match(/[^0-9]/)){
				document.getElementById('modal_usaha').value = '';
			}
		if(value < min){
			document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
			document.getElementById('message-text').innerHTML = 'Maaf, nominal min RP '+min.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			$('#box-information').modal("show");
			setTimeout(function(){
				$('#box-information').modal("hide");
				document.getElementById('modal_usaha').value = 0;
			}, 2000);
		}
		else if((max) && (value > max)){
			document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
			document.getElementById('message-text').innerHTML = 'Maaf, nominal max RP '+max.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			$('#box-information').modal("show");
			setTimeout(function(){
				$('#box-information').modal("hide");
				document.getElementById('modal_usaha').value = 0;
			}, 2000);
		}
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	var konfirmasi	= document.getElementById('konfirmasi').checked;
	if(konfirmasi== false){
		document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
		document.getElementById('message-text').innerHTML = 'Maaf, anda belum melakukan konfirmasi tentang keaslian dokumen yang telah anda upload';
		$('#box-information').modal("show");
		setTimeout(function(){
		$('#box-information').modal("hide");
		}, 2000);
	}
	if((validasi) && (konfirmasi == true)){
	
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						uploadImage()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function uploadImage(){
		var doc = document.form;
			doc.action= "<?php echo $uploadImage; ?>";
			doc.submit();
	}
</script>