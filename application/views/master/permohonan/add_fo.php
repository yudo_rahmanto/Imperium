<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div id="wizard" class="form_wizard wizard_horizontal">
						  <ul class="wizard_steps">
							<li>
							  <a href="#step-1">
								<span class="step_no">1</span>
								<span class="step_descr">
												  Step 1<br />
												  <small>Pilih Izin</small>
											  </span>
							  </a>
							</li>
							<li>
							  <a href="#step-2">
								<span class="step_no">2</span>
								<span class="step_descr">
												  Step 2<br />
												  <small>Create User</small>
											  </span>
							  </a>
							</li>
							
							<li>
							  <a href="#step-3">
								<span class="step_no">3</span>
								<span class="step_descr">
												  Step 3<br />
												  <small>Create Perusahaan</small>
											  </span>
							  </a>
							</li>
							<li>
							  <a href="#step-4">
								<span class="step_no">4</span>
								<span class="step_descr">
												  Step 4<br />
												  <small>Upload Data Persyaratan</small>
											  </span>
							  </a>
							</li>
							
						  </ul>
							<div id="step-1">
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Nama Ijin<font color="red"> * </font> <i id="ket_izin" style="color:silver;"></i></label>
									  <input type="hidden" class="form-control" id="izin_category_id" name="izin_category_id" placeholder="izin_category_id">
									  	<select class="form-control select2" id="izin" name="izin" style="width: 100%;" required="required" onchange="getSyarat()">
											<option value="">Select</option>
											<?php
												if($getIzin){
													foreach($getIzin as $row){
											?>
											<option value="<?php echo $row['izin_id']; ?>"><?php echo $row['izin_name']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Jenis Permohonan<font color="red"> * </font> <i id="ket_izin_type" style="color:silver;"></i></label>
										<select class="form-control select2" id="izin_type" name="izin_type" style="width: 100%;" required="required" onchange="getSyarat()">
											<option value="">Select</option>
											<?php
												if($getTypeIzin){
													foreach($getTypeIzin as $row){
											?>
											<option value="<?php echo $row['izin_type_id']; ?>"><?php echo $row['type_name']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12" style="height:150px;">
									<div class="form-group">
									  <label for="exampleInputEmail1">Persyaratan<font color="red"> * </font> <i id="ket_persyaratan" style="color:silver;"></i></label>
									</div>
									<div class="form-group">
										<table class="table table-striped table-hover table-bordered" id="table_persyaratan"></table>	
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">&nbsp;</label>
									</div>
								</div>
							</div>
							<div id="step-2">
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">KTP <font color="red"> * </font> <i id="ket_ktp" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="ktp" name="ktp" placeholder="KTP" required="required">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">NPWP <font color="red"> * </font> <i id="ket_npwp" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="npwp" name="npwp" placeholder="NPWP" required="required">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
									  <label for="exampleInputEmail1">First Name<font color="red"> * </font> <i id="ket_first_name" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required="required">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
									  <label for="exampleInputEmail1">Last Name<font color="red"> * </font> <i id="ket_last_name" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required="required">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
									  <label for="exampleInputEmail1">Email<font color="red"> * </font> <i id="ket_email" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="email" name="email" placeholder="Email" required="required" onblur="cekEmail()">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
									  <label for="exampleInputEmail1">No Telp <font color="red"> * </font> <i id="ket_tlp" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="tlp" name="tlp" placeholder="No Telp" required="required" onkeyup="cekTyping('tlp')">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Level<font color="red"> * </font> <i id="ket_level_id" style="color:silver;"></i></label>
										<select class="form-control select2" id="level_id" name="level_id" style="width: 100%;" required="required">
											<?php
												if($getLevel){
													foreach($getLevel as $row){
														if($LevelId_Session		== 1){
												?>
												<option value="<?php echo $row['level_id']; ?>"><?php echo $row['level_name']; ?></option>
												<?php
														} else {
															if($row['level_id']	== 8){
												?>
												<option value="<?php echo $row['level_id']; ?>"><?php echo $row['level_name']; ?></option>
												<?php
															}
														}
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">User Name<font color="red"> * </font> <i id="ket_username" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="username" name="username" placeholder="User Name" required="required">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Password<font color="red"> * </font> <i id="ket_password" style="color:silver;"></i></label>
									  <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Konfirmasi Password<font color="red"> * </font> <i id="ket_password1" style="color:silver;"></i></label>
									  <input type="password" class="form-control" id="password1" name="password1" placeholder="Konfirmasi Password" required="required">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Provinsi<font color="red"> * </font> <i id="ket_provinsi" style="color:silver;"></i></label>
										<select class="form-control select2" id="provinsi" name="provinsi" style="width: 100%;" onchange="getData(this.value,'kabupaten')" required="required">
											<option value="">Select</option>
											<?php
												if($getProvinsi){
													foreach($getProvinsi as $row){
												?>
												<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
												<?php
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Kota / Kabupaten<font color="red"> * </font> <i id="ket_kabupaten" style="color:silver;"></i></label>
										<select class="form-control select2" id="kabupaten" name="kabupaten" style="width: 100%;" onchange="getData(this.value,'kecamatan')" required="required">
											<?php
												if($getKabupaten){
													foreach($getKabupaten as $row){
												?>
												<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
												<?php
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
									  <label for="exampleInputEmail1">Kecamatan<font color="red"> * </font> <i id="ket_kecamatan" style="color:silver;"></i></label>
										<select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" onchange="getData(this.value,'kelurahan')" required="required">
											<?php
												if($getKecamatan){
													foreach($getKecamatan as $row){
												?>
												<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
												<?php
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
									  <label for="exampleInputEmail1">Kelurahan<font color="red"> * </font> <i id="ket_kecamatan" style="color:silver;"></i></label>
									  <select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required="required">
											  <?php
												if($getKelurahan){
													foreach($getKelurahan as $row){
												?>
												<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
												<?php
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
									  <label for="exampleInputEmail1">Rt<font color="red"> * </font> <i id="ket_kelurahan" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="rt" name="rt" placeholder="RT" required="required">
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
									  <label for="exampleInputEmail1">Rw<font color="red"> * </font> <i id="ket_kelurahan" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="rw" name="rw" placeholder="RW" required="required">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Alamat <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
									</div>
									<div class="form-group">
									 <textarea class="resizable_textarea form-control" name="alamat" id="alamat" placeholder="Alamat" required="required"></textarea>
									</div>
								</div>
							</div>
							<div id="step-3">
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Apakah ini kantor pusat / kantor cabang<font color="red"> * </font> <i id="ket_jenis_perusahaan" style="color:silver;"></i></label>
									  <select class="form-control select2" id="type_perusahaan" name="type_perusahaan" style="width: 100%;" required="required" onchange="cekTypePerusahaan()">
											<option value="1">Kantor Pusat</option>
											<option value="2">Kantor Cabang</option>
										</select>	
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Jenis Perusahan<font color="red"> * </font> <i id="ket_jenis_perusahaan" style="color:silver;"></i></label>
									  <select class="form-control select2" id="jenis_perusahaan" name="jenis_perusahaan" style="width: 100%;" required="required">
											<option value="">Select</option>
											<?php
												if($getJenisPerusahaan){
													foreach($getJenisPerusahaan as $row){
											?>
											  <option value="<?php echo $row['perusahaan_type_id']; ?>"><?php echo $row['perusahaan_type']; ?></option>
											<?php
													}
												}
											?>
										</select>	
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInput">Nama Perusahaan <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" required="required">
									</div>
								</div>
								<div class="col-sm-12" id="div_nama_cabang" style="display:none;">
									<div class="form-group">
									  <label for="exampleInputEmail1">Nama Cabang <font color="red"> * </font> <i id="ket_nama_cabang" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="nama_cabang" name="nama_cabang" placeholder="Nama Cabang">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
									  <label for="exampleInputEmail1">Jenis Usaha<font color="red"> * </font> <i id="ket_jenis_usaha" style="color:silver;"></i></label>
										<select class="form-control select2" id="jenis_usaha" name="jenis_usaha" style="width: 100%;" required="required" onchange="cek_jenis_usaha()">
										  <option value="">Select</option>
										<?php
											if($getJenisUsaha){
												foreach($getJenisUsaha as $row){
										?>
										  <option value="<?php echo $row['jenis_usaha_id']; ?>"><?php echo $row['name']; ?></option>
										<?php
												}
											}
										?>
										<option value="Lainnya">Lainnya</option>
										</select>
										<input type="text" class="form-control" id="jenis_usaha_lainnya" name="jenis_usaha_lainnya" placeholder="Jenis Usaha Lainnya" required="required" disabled="disabled" style="display:none;">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
									  <label for="exampleInputEmail1">Lingkungan Perusahaan <font color="red"> * </font> <i id="ket_lingkungan_perusahaan" style="color:silver;"></i></label>
										<select class="form-control select2" id="lingkungan_perusahaan" name="lingkungan_perusahaan" style="width: 100%;" required="required">
										  <option value="">Select</option>
										<?php
											if($getLingkungan){
												foreach($getLingkungan as $row){
										?>
										  <option value="<?php echo $row['lingkungan_perusahaan_id']; ?>"><?php echo $row['name']; ?></option>
										<?php
												}
											}
										?>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Indeks Lokasi Perusahaan <font color="red"> * </font> <i id="ket_lokasi_perusahaann" style="color:silver;"></i></label>
										<select class="form-control select2" id="lokasi_perusahaan" name="lokasi_perusahaan" style="width: 100%;" required="required">
										  <option value="">Select</option>
										<?php
											if($getLokasi){
												foreach($getLokasi as $row){
										?>
										  <option value="<?php echo $row['lokasi_perusahaan_id']; ?>"><?php echo $row['name']; ?></option>
										<?php
												}
											}
										?>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
									  <label for="exampleInputEmail1">Panjang Ruangan Tempat Usaha<font color="red"> * </font> <i id="ket_luas" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="panjang_perusahaan" name="panjang_perusahaan" placeholder="Panjang Ruangan Tempat Usaha M2" required="required" onkeyup="cekTyping('panjang_perusahaan')">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
									  <label for="exampleInputEmail1">Lebar Ruangan Tempat Usaha<font color="red"> * </font> <i id="ket_luas" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="lebar_perusahaan" name="lebar_perusahaan" placeholder="Lebar Ruangan Tempat Usaha M2" required="required" onkeyup="cekTyping('lebar_perusahaan')">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
									  <label for="exampleInputEmail1">Luas Ruangan Tempat Usaha<font color="red"> * </font> <i id="ket_luas" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="luas_perusahaan" name="luas_perusahaan" placeholder="Luas Ruangan Tempat Usaha M2" disabled="disabled">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">No Telp Perusahaan <font color="red"> * </font> <i id="ket_tlp" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="tlp_perusahaan" name="tlp_perusahaan" placeholder="No Telp Perusahaan" required="required" onkeyup="cekTyping('tlp_perusahaan')">
									</div>
								</div>
								<div id="extend" class="x_content">
								
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Alamat Perusahaan <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
									</div>
									<div class="form-group">
									<input type="hidden" class="form-control" id="address_perusahaan" name="address_perusahaan" placeholder="address" value="">
									 <textarea class="resizable_textarea form-control" name="alamat_perusahaan" id="alamat_perusahaan" placeholder="Alamat Perusahaan" required="required"></textarea>
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
									  <label for="exampleInputEmail1">Rt<font color="red"> * </font> <i id="ket_rt_perusahaan" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="rt_perusahaan" name="rt_perusahaan" placeholder="RT" required="required">
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
									  <label for="exampleInputEmail1">Rw<font color="red"> * </font> <i id="ket_rw_perusahaan" style="color:silver;"></i></label>
									  <input type="text" class="form-control" id="rw_perusahaan" name="rw_perusahaan" placeholder="RW" required="required">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
									  <label for="exampleInputEmail1">Provinsi<font color="red"> * </font> <i id="ket_provinsi" style="color:silver;"></i></label>
										<select class="form-control select2" id="provinsi_perusahaan" name="provinsi_perusahaan" style="width: 100%;" onchange="getData(this.value,'kabupaten_perusahaan')" required="required">
											<option value="">Select</option>
											<?php
												if($getProvinsi){
													foreach($getProvinsi as $row){
												?>
												<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
												<?php
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
									  <label for="exampleInputEmail1">Kota / Kabupaten<font color="red"> * </font> <i id="ket_kabupaten" style="color:silver;"></i></label>
										<select class="form-control select2" id="kabupaten_perusahaan" name="kabupaten_perusahaan" style="width: 100%;" onchange="getData(this.value,'kecamatan_perusahaan')" required="required">
											<?php
												if($getKabupaten){
													foreach($getKabupaten as $row){
												?>
												<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
												<?php
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Kecamatan<font color="red"> * </font> <i id="ket_kecamatan_perusahaan" style="color:silver;"></i></label>
										<select class="form-control select2" id="kecamatan_perusahaan" name="kecamatan_perusahaan" style="width: 100%;" onchange="getData(this.value,'kelurahan_perusahaan')" required="required">
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
									  <label for="exampleInputEmail1">Kelurahan<font color="red"> * </font> <i id="ket_kelurahan_perusahaan" style="color:silver;"></i></label>
									  <select class="form-control select2" id="kelurahan_perusahaan" name="kelurahan_perusahaan" style="width: 100%;" required="required" onchange="getCordinatSelect()">
											  
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Google Maps <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
									</div>
									<div class="form-group">
										<div id="map" style="height:400px;"></div>
										<input type="hidden" class="form-control" name="lat" id="lat"> 
										<input type="hidden" class="form-control" name="lng" id="lng"> 
									</div>
								</div>
							</div>	
							
							<div id="step-4">
								<h2 class="StepTitle">Step 3 Upload Data Persyaratan</h2>
								<div class="col-sm-12" style="height:300px;">
									<div class="form-group">
										<label for="exampleInputEmail1">Upload Persyaratan<font color="red"> * </font></label>
									</div>
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan"></table>	
								</div>
								</div>
							</div>
						 
						</div>
						<input type="hidden" name="jml_row_syarat" id="jml_row_syarat">
					</div>
					
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		
  
<script>
$('#wizard').smartWizard();
$('.buttonNext').hide();
$('.buttonPrevious').hide();
$('.buttonFinish').hide();
/* Javascript User */
function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = "<option value=''>Select</option>";	
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function matchPassword(){
	var password	= document.getElementById('password').value;
	var password1	= document.getElementById('password1').value;
	if(password == password1){
		return true;
	} else {
		return false;
	}
}
function cekEmail(){
	var user_id	= '';
	var email = document.getElementById('email').value;
	var atpos 				= email.indexOf("@");
	var dotpos 				= email.lastIndexOf(".");
	var i = 0;
	if((atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)){
		document.getElementById('ket_email').innerHTML = "<font color='red'>Email address is not valid</font>";
		i++;
		} else {
			$.ajax({
			   type: 'post',
			   url: '<?php echo $cekEmail; ?>',
			   data: {'id':user_id,'email':email},
			   success: function(response) {
				   if(response == 'Not Ok'){
						document.getElementById('ket_email').innerHTML = "<font color='red'>Email address is duplicate</font>";
						document.getElementById('email').focus();
					i++;
					} else {
						document.getElementById('ket_email').innerHTML = "";
					}
			   }
			});
		}
		if(i == 0){
			return true;
		} else {
			return false;
		}
}
function checkFile(id){
	var file		= document.getElementById('upload_file'+id).files;
	var name_file 	= file[0]['name'].toLowerCase();
	var size_file	= file[0]['size'];
	var format_default 	= document.getElementById('format_file'+id).value.toLowerCase();
	var size_default 	= document.getElementById('max_size'+id).value;
	var explode_format_file	= format_default.split("|");
	var hasil		= false;
	if(name_file){
		var explode = name_file.split('.');
		var format	= explode[explode.length-1];
		for(var i = 0; i < explode_format_file.length; i++){
			if(explode_format_file[i] == format){
				hasil = true;
			}
			else if(size_file < size_default){
				hasil = true;
			}
		}
		
		if(!hasil){
			alert('Maaf Format file / Size tidak sesuai');
			document.getElementById('upload_file'+id).value = '';
		}
	}
}


/* Javascript Perusahaan */
// var kabupaten_id = "1671";
// getData(kabupaten_id,'kecamatan_perusahaan');
findLocation();
function getCordinatSelect(){
	var kabupaten_id 	= document.getElementById('kabupaten_perusahaan').value;
	var kecamatan_id 	= document.getElementById('kecamatan_perusahaan').value;
	var kelurahan_id 	= document.getElementById('kelurahan_perusahaan').value;
	document.getElementById('address_perusahaan').value = "";
	var address 		= '';
	var kabupaten		= '';
	if(kabupaten_id){
		$.each(<?php echo json_encode(SingleFilter('kabupaten')); ?>, function (index, data) {
			var id		= data['id'];
			var nama	= data['nama'];
			if(kabupaten_id == id){
				kabupaten	= nama;
			}
		});
	}
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getDataSelect; ?>',
			   data: {'find':'kecamatan','field':'id','id':kecamatan_id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						$.each(response, function (index, data) {
							var kecamatan	= data['nama'];
							document.getElementById('address_perusahaan').value = kecamatan;
						});
					}
			   }
	});
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getDataSelect; ?>',
			   data: {'find':'kelurahan','field':'id','id':kelurahan_id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						address = document.getElementById('address_perusahaan').value;
						$.each(response, function (index, data) {
							var kelurahan	= data['nama'];
							document.getElementById('address_perusahaan').value = address+', '+kelurahan+', '+kabupaten;
						});
					}
			   }
	});
	setTimeout(function(){
	findLocation();
	 }, 2000);
}
function findLocation(){
	var address = document.getElementById('address_perusahaan').value;
	if(!address){
			$.ajax({
			  url:"https://maps.googleapis.com/maps/api/geocode/json?address=palembang&sensor=false",
			  type: "POST",
			  success:function(res){
				if(res){
					var lat	= res.results[0].geometry.location.lat;
					var lng	= res.results[0].geometry.location.lng;
					initMap(lat,lng);
					document.getElementById('lat').value = lat;
					document.getElementById('lng').value = lng;
				}
			  }
			});
		} else {
			$.ajax({
			  url:"https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
			  type: "POST",
			  success:function(res){
				if(res){
				var lat	= res.results[0].geometry.location.lat;
				var lng	= res.results[0].geometry.location.lng;
				initMap(lat,lng);
				document.getElementById('lat').value = lat;
				document.getElementById('lng').value = lng;
				}
			  }
			});
		}
}


function initMap(lat,lng) {
		
		var marker;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: parseFloat(lat), lng: parseFloat(lng)}
        });

        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: {lat: parseFloat(lat), lng: parseFloat(lng)}
        });
        marker.addListener('click');
		google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng; 
        currentLatitude = latLng.lat();
        currentLongitude = latLng.lng();
		document.getElementById('lat').value = currentLatitude;
		document.getElementById('lng').value = currentLongitude;
	});
		
	}
function cek_jenis_usaha(){
	var value	= document.getElementById('jenis_usaha').value;
	if(value == 'Lainnya'){
		document.getElementById('jenis_usaha_lainnya').disabled = false;
		document.getElementById('jenis_usaha_lainnya').style.display = "Block";
		document.getElementById("jenis_usaha_lainnya").required = true;
	} else {
		document.getElementById('jenis_usaha_lainnya').disabled = true;
		document.getElementById('jenis_usaha_lainnya').style.display = "none";
		document.getElementById("jenis_usaha_lainnya").required = false;
	}
}
function cekTyping(field){
	var value	= document.getElementById(field).value;
	 if(field == 'tlp'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'tlp_perusahaan'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'luas'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'panjang_perusahaan'){
		if(value.match(/[^0-9.]/)){
			document.getElementById(field).value = '';
		} else {
			var panjang_perusahaan 	= document.getElementById(field).value;
			var lebar_perusahaan 	= document.getElementById('lebar_perusahaan').value;
			var luas_perusahaan		= 0;
			if(!panjang_perusahaan){panjang_perusahaan = 0;}
			if(!lebar_perusahaan){lebar_perusahaan = 0;}
			luas_perusahaan			= (panjang_perusahaan * lebar_perusahaan).toFixed(1);
			document.getElementById('luas_perusahaan').value = luas_perusahaan;
		}
	}
	else if(field == 'lebar_perusahaan'){
		if(value.match(/[^0-9.]/)){
			document.getElementById(field).value = '';
		} else {
			var panjang_perusahaan 	= document.getElementById(field).value;
			var lebar_perusahaan 	= document.getElementById('lebar_perusahaan').value;
			var luas_perusahaan		= 0;
			if(!panjang_perusahaan){panjang_perusahaan = 0;}
			if(!lebar_perusahaan){lebar_perusahaan = 0;}
			luas_perusahaan			= (panjang_perusahaan * lebar_perusahaan).toFixed(1);
			document.getElementById('luas_perusahaan').value = luas_perusahaan;
		}
	}
}
function cekTypePerusahaan(){
	var type_perusahaan	= document.getElementById('type_perusahaan').value;
	if(type_perusahaan == 1){
		document.getElementById('div_nama_cabang').style.display = "none";
		$('#nama_cabang').removeAttr('required');
	}
	else if(type_perusahaan == 2){
		document.getElementById('div_nama_cabang').style.display = "block";
		$('#nama_cabang').attr('required', 'required');
	}
}
/* Javascript Izin */
function getSyarat(){
	var izin		= document.getElementById('izin').value;
	var izin_type	= document.getElementById('izin_type').value;
	var modalUsaha		= "";
	var kegiatanUsaha	= "";
	var kelembagaan		= "";
	var product			= "";
	if((izin) && (izin_type)){
		
		$.each(<?php echo json_encode($getIzin); ?>, function (index, data) {
			 var izin_category_id	= data['izin_category_id'];  
			 var izin_id			= data['izin_id'];  
			 var izin_name			= data['izin_name']; 
			if(izin == izin_id){
				document.getElementById('izin_category_id').value = izin_category_id; 
				 if(izin_category_id == 1){
					
					modalUsaha += " <div class='col-sm-3'>";
					modalUsaha += "		<div class='form-group'>";
					modalUsaha += "			<label for='exampleInputEmail1'>Modal Usaha<font color='red'> * </font> <i id='ket_modal_usaha' style='color:silver;'></i></label>";
					modalUsaha += "			<input type='hidden' class='form-control' id='modal_usaha_min' name='modal_usaha_min' placeholder='Modal Usaha Min'>";
					modalUsaha += "			<input type='hidden' class='form-control' id='modal_usaha_max' name='modal_usaha_max' placeholder='Modal Usaha Max'>";
					modalUsaha += "			<input type='text' class='form-control' id='modal_usaha' name='modal_usaha' placeholder='Modal Usaha' required='required' onblur='cekInt(modal_usaha)'>";
					modalUsaha += "		</div>";
					modalUsaha += "	</div>";
					
					kegiatanUsaha += " <div class='col-sm-3'>";
					kegiatanUsaha += "	  <div class='form-group'>";
					kegiatanUsaha += "		  <label for='exampleInputEmail1'>Kegiatan Usaha<font color='red'> * </font> <i id='ket_kegiatan_usaha' style='color:silver;'></i></label>";
					kegiatanUsaha += "		  <input type='text' class='form-control' id='kegiatan_usaha' name='kegiatan_usaha' placeholder='Kegiatan Usaha' required='required'>";
					kegiatanUsaha += "	  </div>";
					kegiatanUsaha += " </div>";
					
					kelembagaan += " <div class='col-sm-3'>";
					kelembagaan += "	  <div class='form-group'>";
					kelembagaan += "		  <label for='exampleInputEmail1'>Kelembagaan<font color='red'> * </font> <i id='ket_kelembagaan' style='color:silver;'></i></label>";
					kelembagaan += "		  <input type='text' class='form-control' id='kelembagaan' name='kelembagaan' placeholder='Kelembagaan' required='required'>";
					kelembagaan += "	  </div>";
					kelembagaan += " </div>";
					
					product += " <div class='col-sm-3'>";
					product += "	  <div class='form-group'>";
					product += "		  <label for='exampleInputEmail1'>Product<font color='red'> * </font> <i id='ket_product' style='color:silver;'></i></label>";
					product += "		  <input type='text' class='form-control' id='product' name='product' placeholder='Product' required='required'>";
					product += "	  </div>";
					product += " </div>";
				 }
			}
				document.getElementById('extend').innerHTML = modalUsaha+" "+kegiatanUsaha+" "+kelembagaan+" "+product;
				
		 });
		 jQuery.ajax({
							   type: 'post',
							   url: '<?php echo $getIzinValidasi; ?>',
							   data: {'izin_id':izin},
							   dataType: 'json',
							   success: function(response) {
								   $.each(response, function (index, data) {
										var min		= data['min'];
										var max		= data['max'];
										document.getElementById('modal_usaha_min').value = min;
										document.getElementById('modal_usaha_max').value = max;
									});
							   }
					});
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $linkSyarat; ?>',
			   data: {'izin_id':izin,'izin_type_id':izin_type},
			   dataType: 'json',
			   success: function(response) {
				   if(response){
						$('.buttonNext').show();
						$('.buttonPrevious').show();
						$('.buttonFinish').show();
						$('.buttonNext').addClass('btn btn-success');
						$('.buttonPrevious').addClass('btn btn-primary');
						$('.buttonFinish').addClass('btn btn-default');
						$('.buttonFinish').removeAttr("href");
						$('.buttonFinish').attr("id","finish_button");
						document.getElementById('finish_button').setAttribute("onclick","simpan()");
						var html	= "";
							html	+= "	<thead>";
							html	+= "		<tr>";
							html	+= "			<th>No</th>";
							html	+= "			<th> Persyaratan </th>";
							html	+= "			<th> Attribute File </th>";
							html	+= "		</tr>";
							html	+= "	</thead>";
						
						var html1	= "";
							html1	+= "	<thead>";
							html1	+= "		<tr>";
							html1	+= "			<th width='5%'>No</th>";
							html1	+= "			<th width='30%'> Persyaratan </th>";
							html1	+= "			<th width='25%'> Format & Max Size </th>";
							html1	+= "			<th width='10%'> Attribute File </th>";
							html1	+= "			<th width='30%'> Attach </th>";
							html1	+= "		</tr>";
							html1	+= "	</thead>";
						var i = 1;
					   $.each(response, function (index, data) {
						var syarat_id		= data['syarat_id'];  
						var syarat_name		= data['syarat_name'];  
						var max_size		= data['max_size'];  
						var format_file		= data['format_file'];  
						var mandatory		= data['mandatory'];  
						if(mandatory == 1){
							var ket_mandatory 	= "Wajib";
							var requirment		= "required='required'";
							} else {
								var ket_mandatory 	= "Optional";
								var requirment		= "";
							}
							html	+= "	<tbody id='syarat"+i+"'>";
							html	+= "		<tr>";
							html	+= "			<td>"+i+"</td>";
							html	+= "			<td> "+syarat_name+"</td>";
							html	+= "			<td> "+ket_mandatory+"</td>";
							html	+= "		</tr>";
							html	+= "	</tbody>";
							
							html1	+= "	<tbody id='syarat"+i+"'>";
							html1	+= "		<tr>";
							html1	+= "			<td>"+i+"</td>";
							html1	+= "			<td> "+syarat_name+"</td>";
							html1	+= "			<td> "+format_file+" ( Max Size : "+max_size+" MB)</td>";
							html1	+= "			<td> "+ket_mandatory+" </td>";
							html1	+= "			<td> <input type='hidden' class='form-control' name='syarat_id"+i+"' id='syarat_id"+i+"' value='"+syarat_id+"'><input type='hidden' class='form-control' name='max_size"+i+"' id='max_size"+i+"' value='"+max_size+"'><input type='hidden' class='form-control' name='format_file"+i+"' id='format_file"+i+"' value='"+format_file+"'><input type='file' class='form-control' name='upload_file"+i+"' id='upload_file"+i+"' "+requirment+" onchange='checkFile("+i+")'></td>";
							html1	+= "		</tr>";
							html1	+= "	</tbody>";
						i++;
					   });
							html1	+= "	<tbody id='syarat_spasi'>";
							html1	+= "		<tr>";
							html1	+= "			<td colspan='5'>&nbsp;</td>";
							html1	+= "		</tr>";
							html1	+= "	</tbody>";
							html1	+= "	<tbody id='syarat_konfirm'>";
							html1	+= "		<tr>";
							html1	+= "			<td colspan='5'><input type='checkbox' name='konfirmasi' id='konfirmasi' value='1'> &nbsp;Dengan ini saya menyatakan bahwa data yang disampaikan adalah benar dan sesuai dengan aslinya.</td>";
							html1	+= "		</tr>";
							html1	+= "	</tbody>";
							document.getElementById('table_persyaratan').innerHTML = html;
							document.getElementById('table_upload_persyaratan').innerHTML = html1;
							document.getElementById('jml_row_syarat').value = (i-1);
				   }
			   }
		});
		
	}
}
function cekInt(){
	
	var value	= document.getElementById('modal_usaha').value;
	var izin	= document.getElementById('izin').value;
	var min		= document.getElementById('modal_usaha_min').value;
	var max		= document.getElementById('modal_usaha_max').value;
		if(value.match(/[^0-9]/)){
				document.getElementById('modal_usaha').value = '';
			}
		if(value < min){
			document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
			document.getElementById('message-text').innerHTML = 'Maaf, nominal min RP '+min.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			$('#box-information').modal("show");
			setTimeout(function(){
				$('#box-information').modal("hide");
				document.getElementById('modal_usaha').value = 0;
			}, 2000);
		}
		else if((max) && (value > max)){
			document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
			document.getElementById('message-text').innerHTML = 'Maaf, nominal max RP '+max.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			$('#box-information').modal("show");
			setTimeout(function(){
				$('#box-information').modal("hide");
				document.getElementById('modal_usaha').value = 0;
			}, 2000);
		}
}

function back(){
	window.location = "<?php echo $back; ?>";
}

function simpan(){
	var validasi	= $('#form').parsley().validate();
	var ket_email	= document.getElementById('ket_email').innerHTML;
	var konfirmasi	= document.getElementById('konfirmasi').checked;
	if(konfirmasi== false){
		document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
		document.getElementById('message-text').innerHTML = 'Maaf, anda belum melakukan konfirmasi tentang keaslian dokumen yang telah anda upload';
		$('#box-information').modal("show");
		setTimeout(function(){
		$('#box-information').modal("hide");
		}, 2000);
	}
	if((!ket_email) && (validasi) && (konfirmasi == true)){
	
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						uploadImage()
						}, 2000);
					}
					else if(response == 'duplikat user'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data user sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} 
					else if(response == 'duplikat perusahaan'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data perusahaan sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function uploadImage(){
		var doc = document.form;
			doc.action= "<?php echo $uploadImage; ?>";
			doc.submit();
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWtenJEq5zNO23Y8HOI_gMlhAZ3-7FxC0&callback=initMap"></script>