<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($getData){
	$permohonan_id		= $getData[0]['permohonan_id'];
	$tanda_terima_id	= $getData[0]['tanda_terima_id'];
	$user_management_id	= $getData[0]['user_management_id'];
	$perusahaan_id		= $getData[0]['perusahaan_id'];
	$izin_id			= $getData[0]['izin_id'];
	$izin_type_id		= $getData[0]['izin_type_id'];
	$nama_perusahaan	= $getData[0]['nama_perusahaan'];
	$ktp				= $getData[0]['ktp'];
	$npwp				= $getData[0]['npwp'];
	$first_name			= $getData[0]['first_name'];
	$last_name			= $getData[0]['last_name'];
	$email				= $getData[0]['email'];
	$no_tlp_user		= $getData[0]['no_tlp_user'];
	$alamat_user		= $getData[0]['alamat_user'];
	$izin_name			= $getData[0]['izin_name'];
	$izin_type_name		= $getData[0]['izin_type_name'];
} else {
	$permohonan_id		= '';
	$tanda_terima_id	= '';
	$user_management_id	= '';
	$perusahaan_id		= '';
	$izin_id			= '';
	$izin_type_id		= '';
	$nama_perusahaan	= '';
	$ktp				= '';
	$npwp				= '';
	$first_name			= '';
	$last_name			= '';
	$email				= '';
	$no_tlp_user		= '';
	$alamat_user		= '';
	$izin_name			= '';
	$izin_type_name		= '';
}

if($getPerusahaan){
	$perusahaan_id			= $getPerusahaan[0]['perusahaan_id'];
	$nik					= $getPerusahaan[0]['nik'];
	$nama_perusahaan		= $getPerusahaan[0]['nama_perusahaan'];
	$alamat_perusahaan		= $getPerusahaan[0]['alamat'];
} else {
	$perusahaan_id			= '';
	$nik					= '';
	$nama_perusahaan		= '';
	$alamat_perusahaan		= '';
}
?>
<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<h2 class="StepTitle">Data Izin Permohonan</h2>
								<div class="col-sm-12">
									<ul class="list-group list-group-unbordered">
										<input type="hidden" class="form-control" id="permohonan_id" name="permohonan_id" placeholder="Id Permohonan" required="required" value="<?php echo $permohonan_id; ?>">
										<input type="hidden" class="form-control" id="perusahaan_id" name="perusahaan_id" placeholder="Id Perusahaan" required="required" value="<?php echo $perusahaan_id; ?>">
										<input type="hidden" class="form-control" id="user_management_id" name="user_management_id" placeholder="user management id" required="required" value="<?php echo $user_management_id; ?>">
										<input type="hidden" class="form-control" id="izin_id" name="izin_id" placeholder="Id Izin" required="required" value="<?php echo $izin_id; ?>">
										<input type="hidden" class="form-control" id="izin_type_id" name="izin_type_id" placeholder="izin type id" required="required" value="<?php echo $izin_type_id; ?>">
										<li class="list-group-item"><b>Nama Izin</b><a class="pull-right" id="nama_izin"><?php echo $izin_name; ?></a></li>
										<li class="list-group-item"><b>Jenis Permohonan</b><a class="pull-right" id="jenis_permohonan"><?php echo $izin_type_name; ?></a></li>
									</ul>
								</div>
							
								<h2 class="StepTitle">Data Pemohon</h2>
								<div class="col-sm-12">
									<ul class="list-group list-group-unbordered">
										<li class="list-group-item"><b>NO KTP</b><a class="pull-right" id="nik"><?php echo $ktp; ?></a></li>
										<li class="list-group-item"><b>Full Name</b><a class="pull-right" id="full_name"><?php echo $first_name.' '.$last_name; ?></a></li>
										<li class="list-group-item"><b>Email</b><a class="pull-right" id="email"><?php echo $email; ?></a></li>
										<li class="list-group-item"><b>No Telp</b><a class="pull-right" id="no_tlp_user"><?php echo $no_tlp_user; ?></a></li>
										<li class="list-group-item"><b>Alamat</b><a class="pull-right" id="alamat_user"><?php echo $alamat_user; ?></a></li>
									</ul>
								</div>
							
								<h2 class="StepTitle">Data Perusahaan</h2>
								<div class="col-sm-12">
									<ul class="list-group list-group-unbordered">
										<li class="list-group-item"><b>Nama Perusahaan</b><a class="pull-right" id="nama_perusahaan"><?php echo $nama_perusahaan; ?></a></li>
										<li class="list-group-item"><b>Alamat</b><a class="pull-right" id="alamat_perusahaan"><?php echo $alamat_perusahaan; ?></a></li>
									</ul>
								</div>
					</div>
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
								<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
									<thead>
										<tr>
											<th width='5%'>No</th>
											<th width='30%'> Persyaratan </th>
											<th width='25%'> Format & Max Size </th>
											<th width='40%'> Attach </th>
										</tr>
									</thead>
									<?php 
										$i = 1;
										$x = 1;
										if($getDataSyarat){
											foreach($getDataSyarat as $row){
												if($getDataUploadSyarat){
													foreach($getDataUploadSyarat as $row1){
														if($row['syarat_id'] == $row1['syarat_id']){
															$x = $x -1;$file_input		= " OK ";break;
														} else {
															$file_input		= "<input type='hidden' class='form-control' name='syarat_id".$x."' id='syarat_id".$x."' value='".$row['syarat_id']."'><input type='hidden' class='form-control' name='max_size".$x."' id='max_size".$x."' value='".$row['max_size']."'><input type='hidden' class='form-control' name='format_file".$x."' id='format_file".$x."' value='".$row['format_file']."'><input type='file' class='form-control' name='upload_file".$x."' id='upload_file".$x."' required='required' onchange='checkFile(".$x.")'>";
														
														}
													}
												}
									?>
									<tbody>
										<tr>
											<th width='5%'><?php echo $i; ?></th>
											<th width='30%'> <?php echo $row['syarat_name']; ?> </th>
											<th width='25%'> <?php echo $row['format_file'].' ( '.$row['max_size'].' MB )'; ?> </th>
											<th width='40%'> <?php echo $file_input; ?> </th>
										</tr>
									</tbody>
									<?php
											$i++;
											$x++;
											}
										}
									?>
								</table>
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<input type="hidden" name="jml_row_syarat" id="jml_row_syarat" value="<?php echo ($x-1); ?>">
						<button type="button" class="btn btn-primary" onclick="simpan()">Update</button>
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
 
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function checkFile(id){
	var file		= document.getElementById('upload_file'+id).files;
	var name_file 	= file[0]['name'].toLowerCase();
	var size_file	= file[0]['size'];
	var format_default 	= document.getElementById('format_file'+id).value.toLowerCase();
	var size_default 	= document.getElementById('max_size'+id).value;
	var explode_format_file	= format_default.split("|");
	var hasil		= false;
	if(name_file){
		var explode = name_file.split('.');
		var format	= explode[explode.length-1];
		for(var i = 0; i < explode_format_file.length; i++){
			if(explode_format_file[i] == format){
				hasil = true;
			}
			else if(size_file < size_default){
				hasil = true;
			}
		}
		
		if(!hasil){
			alert('Maaf Format file / Size tidak sesuai');
			document.getElementById('upload_file'+id).value = '';
		}
	}
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		uploadFile();
	}
}
function uploadFile(){
		var doc = document.form;
			doc.action= "<?php echo $uploadFile; ?>";
			doc.submit();
	}
</script>