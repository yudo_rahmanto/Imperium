<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$jenis_perusahaan	= $getData[0]['jenis_perusahaan'];
	$nama_perusahaan	= $getData[0]['nama_perusahaan'];
	$izin_id			= $getData[0]['izin_id'];
	$izin_name			= $getData[0]['izin_name'];
	$izin_type_id		= $getData[0]['izin_type_id'];
	$izin_type_name		= $getData[0]['izin_type_name'];
	$originalDate 		= $getData[0]['created_date'];
	$created_date 		= date("d M Y H:i:s", strtotime($originalDate));
	$status_level		= $getData[0]['status_level'];
	$posisi_alur		= $getData[0]['posisi_alur'];
	
	$alur_izin			= grabDataAlurIzin($izin_id,$izin_type_id);
	$jml_alur_izin		= count($alur_izin);
	$persentase			= ($status_level/$jml_alur_izin) * 100;
} else {
	$nama_perusahaan	= '';
	$izin_name			= '';
	$izin_type_name		= '';
	$created_date		= '';
	$status_level		= '';
	$posisi_alur		= '';
	$persentase			= 0;
}

?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<section class="panel">
						  <header class="panel-heading">
							  Permohonan Detail
						  </header>
						<div class="panel-body">
							<div class="col-sm-12">
								<ul class="list-group list-group-unbordered">
									<li class="list-group-item"><b>Tanggal Permohonan</b><a class="pull-right"><?php echo $created_date; ?></a></li>
									<li class="list-group-item"><b>Nama Perusahaan</b><a class="pull-right"><?php if($jenis_perusahaan){ echo $jenis_perusahaan.'. '.$nama_perusahaan;} else {echo $nama_perusahaan;} ?></a></li>
									<li class="list-group-item"><b>Nama Ijin</b><a class="pull-right"><?php echo $izin_name; ?></a></li>
									<li class="list-group-item"><b>Jenis Permohonan</b><a class="pull-right"><?php echo $izin_type_name; ?></a></li>
									<li class="list-group-item"><b>Status Izin</b><a class="pull-right"><?php if($persentase < 100){echo $posisi_alur;} else {echo "Siap Cetak";} ?></a></li>
									<li class="list-group-item"><b>Progress</b>
										<a class="pull-right">
										<div class="progress progress_sm">
										  <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?php echo $persentase; ?>" style="width: <?php echo $persentase; ?>%;"></div>
										</div>
										<small><?php echo floor($persentase); ?>% Complete</small>
										</a>
									</li>
									
								</ul>
							</div>
						</div>
						
					  </section>
					</div>
					<div class="x_content">
					<div class="x_title">
					<h3>Data Persyaratan</h3>
					</div>
						<?php 
							if($getDataSyarat){
								foreach($getDataSyarat as $row){
									if (file_exists($row['path_location'])) {
									$path_image		= $row['path_location'];
									} else {
									$path_image		= 'assets/images/no_file.png';	
									}
						?>
						<div class="col-sm-3">
								<div class="form-group">
									<?php
										$nama_file	= $row['nama_file'];
										if (strpos($nama_file, 'pdf') !== false) {
											$send_file	= "onclick=openFile('".$row['path_location']."')";	
									?>
									<div class="thumbnail">
									  <div class="image view view-first">
										<img style="width: 100%; display: block;" src="<?php echo base_url('assets/images/pdf.png'); ?>" alt="image" height="100px;" width="70px;"/>
										<div class="mask">
										  <p <?php echo $send_file; ?> style="cursor: pointer;"><?php echo $row['syarat_name']; ?></p>
										  
										</div>
									  </div>
									  <div class="caption">
										<p><?php echo $row['syarat_name']; ?></p>
									  </div>
									</div>
									<?php
										} else {
											$send_file	= "onclick=sendFile('".$row['path_location']."')";
										?>
									<div class="thumbnail">
									  <div class="image view view-first">
										<img style="width: 100%; display: block;" src="<?php echo base_url($path_image); ?>" alt="image" />
										<div class="mask">
										  <p <?php echo $send_file; ?> style="cursor: pointer;"><?php echo $row['syarat_name']; ?></p>
										  
										</div>
									  </div>
									  <div class="caption">
										<p><?php echo $row['syarat_name']; ?></p>
									  </div>
									</div>
									<?php
											}
										?>
								</div>
						</div>
						<?php
								}
							}
						?>
					</div>
					  <div class="box-footer">
							<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						 </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>

<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function sendFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = "<center><img src='"+base_url+'/'+path+"' width='400px' height='auto'></center>";
	$('#box-information').modal("show");
}
function openFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	window.open(base_url+'/'+path);
}
</script>
