<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$izin_id			= $getData[0]['izin_id'];
	$izin_category_id	= $getData[0]['izin_category_id'];
	$izin_name			= $getData[0]['izin_name'];
	$durasi_pekerjaan	= $getData[0]['durasi_pekerjaan'];
	$keterangan			= $getData[0]['keterangan'];
} else {
	$izin_id			= '';
	$izin_category_id	= '';
	$izin_name			= '';
	$durasi_pekerjaan	= '';
	$keterangan			= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Izin Category <font color="red"> * </font> <i id="ket_izin_category" style="color:silver;"></i></label>
							  <select class="form-control select2" id="izin_category" name="izin_category" style="width: 100%;" required="required">
								  <option value="">Select</option>
								<?php
									if($getIzinCategory){
										foreach($getIzinCategory as $row){
											if($izin_category_id == $row['izin_category_id']){$select	= "selected='selected'";} else {$select = "";}
								?>
								  <option value="<?php echo $row['izin_category_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
								<?php
										}
									}
								?>
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Izin Name <font color="red"> * </font> <i id="ket_izin_name" style="color:silver;"></i></label>
							  <input type="hidden" class="form-control" id="izin_id" name="izin_id" placeholder="Izin Id" required="required" value="<?php echo $izin_id; ?>">
							  <input type="text" class="form-control" id="izin_name" name="izin_name" placeholder="Izin Name" required="required" value="<?php echo $izin_name; ?>">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Durasi Pekerjaan<font color="red"> * </font> <i id="ket_izin_name" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="durasi" name="durasi" placeholder="5 Hari" value="<?php echo $durasi_pekerjaan; ?>">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Keterangan </label>
							  <textarea class="resizable_textarea form-control" name="keterangan" id="keterangan" placeholder="Keterangan" required="required"><?php echo $keterangan; ?></textarea>
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
</script>