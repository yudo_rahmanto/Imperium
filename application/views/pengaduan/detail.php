<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$isiData		= "";
	foreach($getData[0] as $row_left=>$row_right){
		$isiData[$row_left]	= $row_right;
	}
} 
?>
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Code Pengaduan</b></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<label for="exampleInput" style="margin-left:2%;"><?php echo $isiData['code']; ?></label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Nama Pengirim</b></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<label for="exampleInput" style="margin-left:2%;"><?php echo $isiData['fullname']; ?></label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Email Pengirim</b></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<label for="exampleInput" style="margin-left:2%;"><?php echo $isiData['email']; ?></label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Subject Email</b></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<label for="exampleInput" style="margin-left:2%;"><?php echo $isiData['subject']; ?></label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Isi Pengaduan</b></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<label for="exampleInput" style="margin-left:2%;"><?php echo $isiData['content']; ?></label>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInput"><b>Pejabat yang dituju</b></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<label for="exampleInput" style="margin-left:2%;"><?php echo $isiData['jabatan_name']; ?></label>
								</div>
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>

  
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}
</script>