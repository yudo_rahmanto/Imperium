 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$UserId_Session			= $dataSession['user_id'];
$LevelId_Session						= $dataSession['level_id'];

	$filter			= '';
	if($data_search){
		foreach ($data_search as $row_left=>$row_right){
			$filter[$row_left]			= '';
			if($row_right){
				$filter[$row_left]		= $row_right;
			}
		}
	}
	

?>
<form class="form-signin" name="search" id="search" method="post" enctype="multipart/form-data">	
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  <div class="x_content">
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						 <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Pengaduan Baru</a></li>
							<li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Pengaduan Proses</a></li>
							<li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Pengaduan Finish</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
								<table class="table table-striped table-bordered" id="datatableInbox">
								  <thead>
								  <tr>
											<?php	
													if($field_table){
														foreach($field_table as $left=>$right){
															
											?>
											<th align="center"><?php echo ucwords(strtolower($right)); ?> </th>
											<?php
														}
													}
											?>
											<th align="center">Action</th>
								  </tr>
								  </thead>
								  <tbody>
								  <?php
											if($inboxPengaduan){
												$i	= 1;
												foreach($inboxPengaduan as $row2){
												if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}
												$tgl_pengaduan 	=  indonesia_date($row2['tgl_posting']);							
									?>
								  <tr class="">
										<td><?php echo $tgl_pengaduan; ?></td>
										<td>
											<input type="checkbox" class="tableflat" name="id<?php echo $i; ?>" id="id<?php echo $i; ?>" value="<?php echo $row2['pengaduan_id']; ?>" style="display:none;">
											<a href="<?php echo $edit.'/'.$row2['pengaduan_id']; ?>"><?php echo $row2['code']; ?></a>
										</td>
										<td><?php echo $row2['fullname']; ?></td>
										<td><?php echo $row2['email']; ?></td>
										<td><?php echo $row2['subject']; ?></td>
										<td>
											<button class="btn btn-round btn-success" type="button" onclick="terimaPengaduan('<?php echo $row2['pengaduan_id']; ?>')">Terima</button>
										</td>
									</tr>
									<?php
												$i++;
												}
											} else {
									?>
									<tr class="">
											<td colspan="6" align="center">No data result(s)</td>
										</tr>
									<?php
											}
									?>
								  </tbody>
							  </table>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="home-tab">
								<table class="table table-striped table-bordered" id="datatableOnProgress">
								  <thead>
								  <tr>
											<?php	
													if($field_table){
														foreach($field_table as $left=>$right){
															
											?>
											<th align="center"><?php echo ucwords(strtolower($right)); ?> </th>
											<?php
														}
													}
											?>
											<th align="center">Action</th>
								  </tr>
								  </thead>
								  <tbody>
								  <?php
											if($processPengaduan){
												$i	= 1;
												foreach($processPengaduan as $row2){
												if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}
												$tgl_pengaduan 	=  indonesia_date($row2['tgl_posting']);				
									?>
								  <tr class="">
										<td><?php echo $tgl_pengaduan; ?></td>
										<td>
											<input type="checkbox" class="tableflat" name="id<?php echo $i; ?>" id="id<?php echo $i; ?>" value="<?php echo $row2['pengaduan_id']; ?>" style="display:none;">
											<a href="<?php echo $edit.'/'.$row2['pengaduan_id']; ?>"><?php echo $row2['code']; ?></a>
										</td>
										<td><?php echo $row2['fullname']; ?></td>
										<td><?php echo $row2['email']; ?></td>
										<td><?php echo $row2['subject']; ?></td>
										<td>
											<button class="btn btn-round btn-warning" type="button" onclick="location.href='<?php echo $edit.'/'.$row2['pengaduan_id']; ?>'">Replay</button>
											<button class="btn btn-round btn-success" type="button" onclick="selesaiPengaduan('<?php echo $row2['pengaduan_id']; ?>')">Finish</button>
										</td>
									</tr>
									<?php
												$i++;
												}
											} else {
									?>
									<tr class="">
											<td colspan="6" align="center">No data result(s)</td>
										</tr>
									<?php
											}
									?>
								  </tbody>
							  </table>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="home-tab">
								<table class="table table-striped table-bordered" id="datatableFinish">
								  <thead>
									<tr>
											<?php	
													if($field_table){
														foreach($field_table as $left=>$right){
															
											?>
											<th align="center"><?php echo ucwords(strtolower($right)); ?> </th>
											<?php
														}
													}
											?>
									</tr>
								  </thead>
								  <tbody>
								  <?php
											if($finishPengaduan){
												$i	= 1;
												foreach($finishPengaduan as $row2){
												if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}
												$tgl_pengaduan 	=  indonesia_date($row2['tgl_posting']);					
									?>
								  <tr class="">
										<td><?php echo $tgl_pengaduan; ?></td>
										<td>
											<input type="checkbox" class="tableflat" name="id<?php echo $i; ?>" id="id<?php echo $i; ?>" value="<?php echo $row2['pengaduan_id']; ?>" style="display:none;">
											<a href="<?php echo $edit.'/'.$row2['pengaduan_id']; ?>"><?php echo $row2['code']; ?></a>
										</td>
										<td><?php echo $row2['fullname']; ?></td>
										<td><?php echo $row2['email']; ?></td>
										<td><?php echo $row2['subject']; ?></td>
									</tr>
									<?php
												$i++;
												}
											} else {
									?>
									<tr class="">
											<td colspan="5" align="center">No data result(s)</td>
										</tr>
									<?php
											}
									?>
								  </tbody>
							  </table>
							</div>
						</div>
					</div>
					
                   
                  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		
 
<script>
<?php 
if($inboxPengaduan){ 
?>
$(document).ready(function() {
			$('#datatableInbox').DataTable();
		});
<?php	
}
if($processPengaduan){
?>
$(document).ready(function() {
			$('#datatableOnProgress').DataTable();
		});
<?php	
}
if($finishPengaduan){
?>
$(document).ready(function() {
			$('#datatableFinish').DataTable();
		});
<?php	}	?>		
function find(){
		var doc = document.search;
		doc.action= "<?php echo $page_action; ?>";
		doc.submit();
}
function back(){
	window.location = "<?php echo $back_action; ?>";
}
function parseDataDelete(id){
	var doc = document.search;
	var x;
	var r = confirm("Anda yakin ingin menghapus data ini");
	if (r==true){
		deleteData(id);
	 } else {
		return false;
	}
}
function terimaPengaduan(id){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $terimapengaduan; ?>',
			   data: {'pengaduan_id':id},
			   success: function(response) {
				   if(response == "Ok"){
					    document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Pengaduan sudah diterima';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000);
				   } else {
					   document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Pengaduan gagal diterima';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000);
				   }
			   }
	});
}
function selesaiPengaduan(id){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $selesaipengaduan; ?>',
			   data: {'pengaduan_id':id},
			   success: function(response) {
				   if(response == "Ok"){
					    document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Pengaduan sudah selesai';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000);
				   } else {
					   document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Pengaduan gagal diproses';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000);
				   }
			   }
	});
}
function deleteData(id){
	if(id){
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $delete; ?>',
			   data: {'id':id},
			   success: function(response) {
				   if(response == 'Success'){
					    document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete Data Success';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000); 
				   }
				    else if(response == 'Failed'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete File Failed';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						}, 2000);
				   }
			   }
		});
	}
}

function create_new(){
	window.location = "<?php echo $add; ?>";
	
}

</script>