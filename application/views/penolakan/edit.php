<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession			= $this->session->userdata('user_data');
$UserId_Session			= $dataSession['user_id'];
$JabatanId_Session		= $dataSession['jabatan_id'];
$LevelId_Session		= $dataSession['level_id'];

$permohonan_id			= '';
$izin_category_id		= '';
$izin_id				= '';
$izin_type_id			= '';
$status_level			= '';
// echo '<pre>';print_r($dataSession);exit;

if($getDataPermohonan){
	$dataPermohonan = '';
	foreach($getDataPermohonan[0] as $row_left=>$row_right){
		$dataPermohonan[$row_left] = $row_right;
	}
	$permohonan_id			= $dataPermohonan['permohonan_id'];
	$izin_category_id		= $dataPermohonan['izin_category_id'];
	$izin_id				= $dataPermohonan['izin_id'];
	$izin_type_id			= $dataPermohonan['izin_type_id'];
	$status_level			= $dataPermohonan['status_level'];
}
if($getDataPerusahaan){
	$dataPerusahaan = '';
	foreach($getDataPerusahaan[0] as $row_left=>$row_right){
		$dataPerusahaan[$row_left] = $row_right;
	}
} 
if($getDataUser){
	$dataUser = '';
	foreach($getDataUser[0] as $row_left=>$row_right){
		$dataUser[$row_left] = $row_right;
	}
}
$getAlurIzin			= grabDataAlurIzin($izin_id,$izin_type_id);
$jabatan_id_penerima	= $getAlurIzin[1]['jabatan_id'];
$jabatan_penerima		= $getAlurIzin[$status_level]['jabatan_name'];

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWtenJEq5zNO23Y8HOI_gMlhAZ3-7FxC0"></script>
<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Validasi Penolakan</h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_panel">
                  <div class="x_content">
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
						<li role="presentation" class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Permohonan</a></li>
                        <li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data User</a></li>
                        <li role="presentation" class="active"><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Data Perusahaan</a></li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data Persyaratan</a></li>
						<?php
							if(($JabatanId_Session	 == 3) && ($izin_category_id == 2)){
						?>
						<li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data Retribusi</a></li>
						<li role="presentation" class=""><a href="#tab_content6" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Surat Tugas</a></li>
						
						<?php
							}
							else if(($getRetribusi) && ($izin_category_id == 2)){
						?>
						<li role="presentation" class=""><a href="#tab_content7" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data Rekomendasi</a></li>
						<?php
							}
						?>
						<li role="presentation" class=""><a href="#tab_content8" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data History</a></li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade" id="tab_content1" aria-labelledby="home-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Izin</td>
												<td width="1%">:</td>
												<td width="79%">
												<input type="hidden" class="form-control" id="permohonan_id" name="permohonan_id" placeholder="Permohonan Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['permohonan_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_category_id" name="izin_category_id" placeholder="Izin Category Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_category_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_id" name="izin_id" placeholder="Izin Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_type_id" name="izin_type_id" placeholder="Izin Type Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_type_id']; } ?>">
												<input type="hidden" class="form-control" id="status_level" name="status_level" placeholder="Status Level" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['status_level']; } ?>">
												<?php if($getDataPermohonan){echo $dataPermohonan['izin_name']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Izin</td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){echo $dataPermohonan['izin_type_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Tanggal Pembuatan </td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){
																																		$originalDate = $dataPermohonan['created_date'];
																																		$date = date("d M Y H:i:s", strtotime($originalDate));
																																		echo $date; 
																																		}
																															?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="page1-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> No KTP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['ktp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No NPWP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['npwp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Full Name</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['first_name'].' '.$dataUser['last_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Email</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['email']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No Tlp</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['no_tlp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['address']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['rt'].' / '.$dataUser['rw']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Provinsi </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['provinsi_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kabupaten </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kabupaten_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kecamatan_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kelurahan_name']; } ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="page2-tab">
                          <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<input type="hidden" class="form-control" id="alamat_perusahaan_id" name="alamat_perusahaan_id" placeholder="Id Perusahaan" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['alamat_perusahaan_id']; } ?>">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['nama_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Usaha </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php
														if($getJenisUsaha){
															foreach($getJenisUsaha as $row){
																if($getDataPerusahaan){
																	if($dataPerusahaan['jenis_usaha_id'] == $row['jenis_usaha_id']){echo $row['name'];}
																}
															}
														}
													?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lingkungan Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php
														if($getLingkungan){
															foreach($getLingkungan as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['lingkungan_perusahaan_id'] == $row['lingkungan_perusahaan_id']){echo $row['name'];}
																	}
															}
														}
													?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lokasi Perusahaan</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php
														if($getLokasi){
															foreach($getLokasi as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['lokasi_perusahaan_id'] == $row['lokasi_perusahaan_id']){echo $row['name'];}
																	}
															}
														}
													?>
												</td>
											</tr>
											<?php
												if($izin_category_id == 2){
											?>
											<tr>
												<td width="20%"> Index Gangguan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php
														if($getIndexGangguan){
															foreach($getIndexGangguan as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['index_gangguan_id'] == $row['index_gangguan_id']){ echo $row['name'];}
																	}
															}
														}
														?>
												</td>
											</tr>
											<tr id="kekuatan_mesin">
												
											</tr>
											<?php
												}
											?>
											<tr>
												<td width="20%"> Luas </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['luas']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['alamat']; } ?>
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<div id="map" style="height:300px;"></div>
													<input type="hidden" class="form-control" name="lat" id="lat" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['lat']; } ?>"> 
													<input type="hidden" class="form-control" name="lng" id="lng" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['lng']; } ?>"> 
												</td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td>
												<td width="1%">:</td>
												<td width="79%">
												<?php if($getDataPerusahaan){echo $dataPerusahaan['rt']; } ?> / 
												<?php if($getDataPerusahaan){echo $dataPerusahaan['rw']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php
														if($getKecamatan){
															foreach($getKecamatan as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['kecamatan_id'] == $row['id']){echo $row['nama'];}
																	}
															}
														}
														?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php
														if($getKelurahan){
															foreach($getKelurahan as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['kelurahan_id'] == $row['id']){echo $row['nama'];}
																	}
															}
														}
														
														?>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="page3-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="30%"> Persyaratan</td><td width="80%">File</td>
											</tr>
											<?php
												if($getDataPersyaratan){
													foreach($getDataPersyaratan as $row){
														$file_parts = pathinfo($row['path_location']);
														$extension	= $file_parts['extension'];
														if(($extension == 'jpg') | ($extension == 'jpeg') || ($extension == 'png')){
															$send_file	= "onclick=sendFile('".$row['path_location']."')";
															} else {
															$send_file	= "onclick=openFile('".$row['path_location']."')";	
															}
														// echo '<pre>';print_r($file_parts);
											?>
											<tr>
												<td width="80%"> <?php echo $row['syarat_name']; ?></td><td width="20%"><img class="avatar" src="<?php echo base_url('assets/images/file.jpg'); ?>" alt="Avatar" <?php echo $send_file; ?> style="cursor: pointer;"></td>
											</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
						<?php
							if(($JabatanId_Session	 == 3) && ($izin_category_id == 2)){
						?>
						<div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="page3-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_retribusi"></table>
								</div>
							</div>
                        </div>
						<div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="page3-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<a onclick="window.location.href = '<?php echo $cetak_surat_kerja.'/'.$permohonan_id; ?>'" style="cursor: pointer;">Cetak Surat Tugas</a>
								</div>
							</div>
                        </div>
						<?php
							}
							else if(($getRetribusi) && ($izin_category_id == 2)){
						?>
						<div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="page3-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered">
										<?php	
											if($getRetribusi){
												$dataRetribusi = '';
												foreach($getRetribusi[0] as $row_left=>$row_right){
													$dataRetribusi[$row_left] = $row_right;
												}
											}
										?>
										<tr>
											<td width='20%'> Index Gangguan  </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo $dataRetribusi['index_gangguan']; } ?></td>
										</tr>	
										<tr>
											<td width='20%'> Lingkungan Perusahaan  </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo $dataRetribusi['lingkungan_perusahaan']; } ?></td>
										</tr>
										<tr>
											<td width='20%'> Lokasi Perusahaan  </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo $dataRetribusi['lokasi_perusahaan']; } ?></td>
										</tr>
										<tr>
											<td width='20%'> Luas  </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo $dataRetribusi['luas']; } ?></td>
										</tr>
										<?php if($getRetribusi){ if($dataRetribusi['alat_kerja']){?>
										<tr>
											<td width='20%'> HARGA RIGB  </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo 'Rp '.number_format($dataRetribusi['total_rigb']); } ?></td>
										</tr>
										<tr>
											<td width='20%'> Kekuatan Mesin  </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo $dataRetribusi['alat_kerja']; } ?></td>
										</tr>
										<tr>
											<td width='20%'> Harga Fasilitas  </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo 'Rp '.number_format($dataRetribusi['bobot_alat_kerja']); } ?></td>
										</tr>
										<?php	}}	?>
										<tr>
											<td width='20%'> Harga </td>
											<td width='1%'>:</td>
											<td width='79%'><?php if($getRetribusi){echo 'Rp '.number_format($dataRetribusi['total_akhir']); } ?></td>
										</tr>
									</table>
								</div>
							</div>
                        </div>
						<?php
							}
						?>
						<div role="tabpanel" class="tab-pane fade" id="tab_content8" aria-labelledby="page4-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td> Tgl Mulai</td>
												<td>Nama Pengirim</td>
												<td>Keterangan Pengiriman</td>
												<td> Tgl Berkas Terkirim </td>
												<td> Tgl Berkas Diterima </td>
												<td> Nama Penerima </td>
												<td> Status Permohonan </td>
												<td> Tgl Selesai </td>
												<td> Catatan </td>
											</tr>
											<?php
											
												if($getDataHistory){
													foreach($getDataHistory as $row){
													$tgl_mulai = '';
													$tgl_berkas_dikirim = '';
													$tgl_diterima = '';
													$tgl_selesai = '';
													$tgl_tolak = '';
													if($row['tgl_mulai'] != '0000-00-00 00:00:00'){
													$original_tgl_mulai =  $row['tgl_mulai'];
													$tgl_mulai = date("d M Y H:i:s", strtotime($original_tgl_mulai));
													}
													if($row['tgl_berkas_dikirim'] != '0000-00-00 00:00:00'){
													$original_tgl_berkas_dikirim =  $row['tgl_berkas_dikirim'];
													$tgl_berkas_dikirim = date("d M Y H:i:s", strtotime($original_tgl_berkas_dikirim));
													}
													if($row['tgl_diterima'] != '0000-00-00 00:00:00'){
													$original_tgl_diterima =  $row['tgl_diterima'];
													$tgl_diterima = date("d M Y H:i:s", strtotime($original_tgl_diterima));
													}
													if($row['tgl_selesai'] != '0000-00-00 00:00:00'){
													$original_tgl_selesai =  $row['tgl_selesai'];
													$tgl_selesai = date("d M Y H:i:s", strtotime($original_tgl_selesai));
													}
													if($row['tgl_tolak'] != '0000-00-00 00:00:00'){
													$original_tgl_tolak =  $row['tgl_tolak'];
													$tgl_tolak = date("d M Y H:i:s", strtotime($original_tgl_tolak));
													}
													$status	= $row['status_approval'];
													if($status == 0){$status_permohonan	= 'On Progress';$finish_date = '';$catatan	= '';}
													else if($status == 1){$status_permohonan	= 'Approval';$finish_date = $tgl_selesai;$catatan	= $row['catatan_approval'];}
													else if($status == 2){$status_permohonan	= 'Reject';$finish_date = $tgl_tolak;$catatan	= $row['catatan_penolakan'];}
											?>
											<tr>
												<td> <?php echo $tgl_mulai; ?> </td>
												<td><?php echo $row['nama_pengirim']; ?></td>
												<td><?php echo $row['keterangan_pengirim']; ?></td>
												<td> <?php echo $tgl_berkas_dikirim; ?> </td>
												<td> <?php echo $tgl_diterima; ?> </td>
												<td> <?php echo $row['nama_penerima']; ?> </td>
												<td> <?php echo $status_permohonan; ?> </td>
												<td> <?php echo $finish_date; ?> </td>
												<td> <?php echo $catatan; ?> </td>
											</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
					 <div class="box-footer">
						<textarea class="resizable_textarea form-control" name="catatan" id="catatan" placeholder="Catatan" style="display:none;"></textarea>
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-success" onclick="noticeSetuju()">Setujui</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>

var jabatan_penerima = "<?php echo $jabatan_penerima; ?>";
if(jabatan_penerima == "Korlap"){
	checkGangguan();
}
function back(){
	window.location = "<?php echo $back; ?>";
}
initMap(<?php if($getDataPerusahaan){echo $dataPerusahaan['lat']; } ?>,<?php if($getDataPerusahaan){echo $dataPerusahaan['lng']; } ?>);
function getCordinatSelect(){
	var kecamatan_id 	= document.getElementById('kecamatan').value;
	var kelurahan_id 	= document.getElementById('kelurahan').value;
	var address 		= '';
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getDataSelect; ?>',
			   data: {'find':'kecamatan','field':'id','id':kecamatan_id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						$.each(response, function (index, data) {
							var kecamatan	= data['nama'];
							document.getElementById('address').value = kecamatan;
						});
					}
			   }
	});
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getDataSelect; ?>',
			   data: {'find':'kelurahan','field':'id','id':kelurahan_id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						address = document.getElementById('address').value;
						$.each(response, function (index, data) {
							var kelurahan	= data['nama'];
							document.getElementById('address').value = address+', '+kelurahan+', Palembang';
						});
					}
			   }
	});
	setTimeout(function(){
	findLocation();
	 }, 2000);
}
function findLocation(){
	var address = document.getElementById('address').value;
	if(!address){
			$.ajax({
			  url:"https://maps.googleapis.com/maps/api/geocode/json?address=palembang&sensor=false",
			  type: "POST",
			  success:function(res){
				var lat	= res.results[0].geometry.location.lat;
				var lng	= res.results[0].geometry.location.lng;
				initMap(lat,lng);
				document.getElementById('lat').value = lat;
				document.getElementById('lng').value = lng;
			  }
			});
		} else {
			$.ajax({
			  url:"https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
			  type: "POST",
			  success:function(res){
				if(res){
				var lat	= res.results[0].geometry.location.lat;
				var lng	= res.results[0].geometry.location.lng;
				initMap(lat,lng);
				document.getElementById('lat').value = lat;
				document.getElementById('lng').value = lng;
				}
			  }
			});
		}
}


function initMap(lat,lng) {
		if(!lat){lat = <?php if($getDataPerusahaan){echo $dataPerusahaan['lat']; } ?>;}
		if(!lng){lng = <?php if($getDataPerusahaan){echo $dataPerusahaan['lng']; } ?>;}
		var marker;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: parseFloat(lat), lng: parseFloat(lng)}
        });

        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: {lat: parseFloat(lat), lng: parseFloat(lng)}
        });
        marker.addListener('click');
		google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng; 
        currentLatitude = latLng.lat();
        currentLongitude = latLng.lng();
		document.getElementById('lat').value = currentLatitude;
		document.getElementById('lng').value = currentLongitude;
	});
		
	}
function checkGangguan(){
	var getJabatanId		= "<?php echo $JabatanId_Session	; ?>";
	var html_mesin		= "";
	var html			= "";
	var option			= "";
	var ket_golongan	= "";
	var ket_total		= "";
	var name 		  	= "";
	var izin_category_id	= document.getElementById('izin_category_id').value;
	if((getJabatanId == 3) && (izin_category_id == 2)){
		var index_gangguan	= document.getElementById('index_gangguan').value;
		var luas			= parseInt(document.getElementById('luas').value);
		
		var nama_gangguan	= '';
		$.each(<?php echo json_encode($getIndexGangguan); ?>, function (index, data) {
				var index_gangguan_id 	= data['index_gangguan_id'];
				var name 		  		= data['name'];
				if(index_gangguan_id == index_gangguan){nama_gangguan	= name;}
			});
		if(index_gangguan == 1){
			if(luas <= 29){ket_golongan	= "Golongan I";ket_total = 200000;}
			else if((luas >= 30) && (luas <= 59)){ket_golongan	= "Golongan II";ket_total = 275000;}
			else if((luas >= 60) && (luas <= 100)){ket_golongan	= "Golongan III";ket_total = 375000;}
			else if(luas > 100){ket_golongan	= "Golongan IV";ket_total = 475000;}
			document.getElementById('kekuatan_mesin').innerHTML = '';
			html	+= "<tr>";
			html	+= "	<td width='20%'> Index Gangguan  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_index_gangguan'>"+nama_gangguan+"</div></td>";
			html	+= "</tr>";	
			html	+= "<tr>";
			html	+= "	<td width='20%'> Luas </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_luas'>"+luas+"</div></td>";
			html	+= "</tr>";	
			html	+= "<tr>";
			html	+= "	<td width='20%'> Golongan  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_golongan'>"+ket_golongan+"</div><input type='hidden' name='golongan' id='golongan' value='"+ket_golongan+"'></td>";
			html	+= "</tr>";	
			html	+= "	<td width='20%'> Harga  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_total'>"+ket_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+"</div><input type='hidden' name='total' id='total' value='"+ket_total+"'> </td>";
			html	+= "</tr>";	
		} else {
			option			+= "<option value=''>Select</option>";
			$.each(<?php echo json_encode($getAlatKerja); ?>, function (index, data) {
				var alat_kerja_id = data['alat_kerja_id'];
				var name 		  = data['name'];
			option			+= "<option value='"+alat_kerja_id+"'>"+name+"</option>";	
			});
			html_mesin	+= "	<td width='20%'> Index Gangguan  </td>";
			html_mesin	+= "	<td width='1%'>:</td>";
			html_mesin	+= "	<td width='79%'>";
			html_mesin	+= "		<select class='form-control select2' id='alat_kerja' name='alat_kerja' style='width: 100%;' required='required' onchange='hitungRetribusi()'>"+option+"</select>";
			html_mesin	+= "	</td>";	
			document.getElementById('kekuatan_mesin').innerHTML = html_mesin;
			
			html	+= "<tr>";
			html	+= "	<td width='20%'> Index Gangguan  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_index_gangguan'></div></td>";
			html	+= "</tr>";	
			html	+= "<tr>";
			html	+= "	<td width='20%'> Lingkungan Perusahaan  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_lingkungan'></div></td>";
			html	+= "</tr>";	
			html	+= "<tr>";
			html	+= "	<td width='20%'> Lokasi Perusahaan  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_lokasi_perusahaan'></div></td>";
			html	+= "</tr>";	
			html	+= "<tr>";
			html	+= "	<td width='20%'> Luas  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_luas'></div></td>";
			html	+= "</tr>";	
			html	+= "<tr>";
			html	+= "	<td width='20%'> HARGA RIGB  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_total_rigb'></div><input type='hidden' name='total_rigb' id='total_rigb' value=''></td>";
			html	+= "</tr>";
			html	+= "<tr>";
			html	+= "	<td width='20%'> Kekuatan Mesin  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_alat_kerja'></div></td>";
			html	+= "</tr>";
			html	+= "<tr>";
			html	+= "	<td width='20%'> Harga Fasilitas  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_harga_fasilitas'></div></td>";
			html	+= "</tr>";
			html	+= "	<td width='20%'> Harga  </td>";
			html	+= "	<td width='1%'>:</td>";
			html	+= "	<td width='79%'><div id='ket_total'></div><input type='hidden' name='total' id='total' value=''> </td>";
			html	+= "</tr>";	
		}
		document.getElementById('table_retribusi').innerHTML = html;
		hitungRetribusi();
	}
}
function hitungRetribusi(){
	var index_gangguan		= document.getElementById('index_gangguan').value;
	if(index_gangguan == 1){
		var luas			= document.getElementById('luas').value;
		var ket_golongan	= '';
		var ket_total	= '';
		if(luas <= 29){ket_golongan	= "Golongan I";ket_total = 200000;}
		else if((luas >= 30) || (luas <= 59)){ket_golongan	= "Golongan II";ket_total = 275000;}
		else if((luas >= 60) || (luas <= 100)){ket_golongan	= "Golongan III";ket_total = 375000;}
		else if(luas > 100){ket_golongan	= "Golongan IV";ket_total = 475000;}
		
		document.getElementById('ket_luas').innerHTML		= luas.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
		document.getElementById('ket_golongan').innerHTML	= ket_golongan; 
		document.getElementById('golongan').value			= ket_golongan; 
		document.getElementById('ket_total').innerHTML 		= ket_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
		document.getElementById('total').value 				= ket_total; 
	} else {
		var alat_kerja			= document.getElementById('alat_kerja').value;
		var lokasi_perusahaan	= document.getElementById('lokasi_perusahaan').value;
		var lingkungan			= document.getElementById('lingkungan').value;
		var luas				= document.getElementById('luas').value;
		var nama_gangguan		= '';
		var nama_lokasi			= '';
		var nama_lingkungan		= '';
		var nama_alat			= '';
		var bobot_gangguan 		= 0;
		var bobot_lokasi 		= 0;
		var bobot_lingkungan 	= 0;
		var bobot_alat	 		= 0;
		var total_rigb			= 0;
		var total				= 0;
		$.each(<?php echo json_encode($getIndexGangguan); ?>, function (index, data) {
			var index_gangguan_id 	= data['index_gangguan_id'];
			var name 		  		= data['name'];
			var bobot 		  		= data['bobot'];
			if(index_gangguan_id == index_gangguan){nama_gangguan = name;bobot_gangguan	= bobot;}
		});
		$.each(<?php echo json_encode($getLokasi); ?>, function (index, data) {
			var lokasi_perusahaan_id 	= data['lokasi_perusahaan_id'];
			var name 		  			= data['name'];
			var bobot 		  			= data['bobot'];
			if(lokasi_perusahaan_id == lokasi_perusahaan){nama_lokasi = name;bobot_lokasi	= bobot;}
		});
		$.each(<?php echo json_encode($getLingkungan); ?>, function (index, data) {
			var lingkungan_perusahaan_id 	= data['lingkungan_perusahaan_id'];
			var name 		  				= data['name'];
			var bobot 		  				= data['bobot'];
			if(lingkungan_perusahaan_id == lingkungan){nama_lingkungan = name;bobot_lingkungan	= bobot;}
		});
		$.each(<?php echo json_encode($getAlatKerja); ?>, function (index, data) {
			var alat_kerja_id 	= data['alat_kerja_id'];
			var name 		  	= data['name'];
			var bobot 		  		= data['bobot'];
			if(alat_kerja_id == alat_kerja){nama_alat = name;bobot_alat	= bobot;}
		});
		total_rigb	= parseInt(bobot_gangguan) * parseFloat(bobot_lokasi) * parseInt(bobot_lingkungan) * parseInt(luas);
		total		= (parseInt(bobot_gangguan) * parseFloat(bobot_lokasi) * parseInt(bobot_lingkungan) * parseInt(luas)) + parseInt(bobot_alat);
		
		document.getElementById('ket_index_gangguan').innerHTML = nama_gangguan;
		document.getElementById('ket_lingkungan').innerHTML = nama_lingkungan;
		document.getElementById('ket_lokasi_perusahaan').innerHTML = nama_lokasi;
		document.getElementById('ket_luas').innerHTML = luas.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		document.getElementById('ket_total_rigb').innerHTML = total_rigb.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		document.getElementById('total_rigb').value = total_rigb;
		document.getElementById('ket_alat_kerja').innerHTML = nama_alat;
		document.getElementById('ket_harga_fasilitas').innerHTML = bobot_alat.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		document.getElementById('ket_total').innerHTML = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		document.getElementById('total').value = total;
	}
}

function noticeSetuju(){
	var html	 = " <p style='margin-top:20px;text-align:left;'>Catatan Persetujuan</p>";
		html	+= " <textarea class='resizable_textarea form-control' name='catatan_temp' id='catatan_temp' placeholder='Catatan'></textarea>";
		html	+= " <div class='modal-footer'>";
		html	+= "	<button data-dismiss='modal' class='btn btn-default' type='button'>Cancel</button>";
		html	+= "	<button class='btn btn-success' type='button' onclick='setuju()'>Submit</button>";
		html	+= " </div>";
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = html;
	$('#box-information').modal("show");
	document.getElementById('status_level').value = "<?php echo $status_level; ?>";
}
function sendLevel(level_id){
	var level_id	= document.getElementById('kembali_berkas').value;
	document.getElementById('status_level').value = level_id;
}
function sendFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = "<center><img src='"+base_url+'/'+path+"' width='400px' height='auto'></center>";
	$('#box-information').modal("show");
}
function openFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	window.open(base_url+'/'+path);
}
function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = '';
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function checkFile(id){
	var file		= document.getElementById('upload_file'+id).files;
	var name_file 	= file[0]['name'].toLowerCase();
	var size_file	= file[0]['size'];
	var format_default 	= document.getElementById('format_file'+id).value.toLowerCase();
	var size_default 	= document.getElementById('max_size'+id).value;
	var explode_format_file	= format_default.split("|");
	var hasil		= false;
	if(name_file){
		var explode = name_file.split('.');
		var format	= explode[explode.length-1];
		for(var i = 0; i < explode_format_file.length; i++){
			if(explode_format_file[i] == format){
				hasil = true;
			}
			else if(size_file < size_default){
				hasil = true;
			}
		}
		
		if(!hasil){
			alert('Maaf Format file / Size tidak sesuai');
			document.getElementById('upload_file'+id).value = '';
		}
	}
}
function setuju(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		var permohonan_id	= document.getElementById('permohonan_id').value;
		var catatan_temp	= document.getElementById('catatan_temp').value;
		var izin_category_id	= document.getElementById('izin_category_id').value;
		document.getElementById('catatan').value = catatan_temp;
		$.ajax({
			   type: 'post',
			   url: '<?php echo $setuju; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						window.location = "<?php echo $signature.'/'; ?>"+permohonan_id+"/"+izin_category_id;
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function uploadFile(){
		var doc = document.form;
			doc.action= "<?php echo $signature; ?>";
			doc.submit();
	}
</script>