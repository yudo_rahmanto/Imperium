<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">Nama Pengirim<font color="red"> * </font> <i id="ket_nama" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Pengirim" required="required">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">Email Pengirim<font color="red"> * </font> <i id="ket_email" style="color:silver;"></i></label>
							  <input type="email" class="form-control" id="email" name="email" placeholder="Email Pengirim" required="required">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Subject Email<font color="red"> * </font> <i id="ket_subject" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject Email" required="required">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Isi Pengaduan</label>
								<textarea class="form-control ckeditor" id="content" name="content"></textarea>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Pejabat yang dituju</label>
							  <select class="form-control select2" id="send_to" name="send_to" style="width: 10%;">
								<?php
									if($getJabatan){
										foreach($getJabatan as $row){
											if($row['jabatan_id'] == 2){
												echo "<option value='".$row['jabatan_id']."'>".$row['jabatan_name']."</option>";
											}
										}
									}
								?>
							  </select>
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		
   
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}

function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		for (instance in CKEDITOR.instances) {
				CKEDITOR.instances[instance].updateElement();
			}
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
		

</script>