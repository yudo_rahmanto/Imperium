 <?php
$dataSession			= $this->session->userdata('user_data');	
if($dataSession){
$fullname_Session		= $dataSession['first_name'].' '.$dataSession['last_name'];
$LevelId_Session			= $dataSession['level_id'];
$getLevelName_Session	= $dataSession['level_name'];
} else {
$fullname_Session				= 'No Name';
$LevelId_Session			= '';
$getLevelName_Session	= '';
}
$icon_menu	= array('1' => 'dashboard', '4' => 'fa-cog','2' => 'fa-edit','3' => 'fa-book','5' => 'file-text-alt','6' => ' fa-ticket','7' => ' fa-archive','8' => ' fa-file-o','9' => ' fa-print','10' => ' fa-dollar','11' => ' fa fa-dropbox');
// echo '<pre>';print_r($menu);exit;
// echo $_SERVER['REQUEST_URI'];exit;
?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
		 <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('Home'); ?>" class="site_title"><i class="fa fa-bank"></i> <span>IMPERIUM v1.0</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('assets/images/user.png'); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $fullname_Session; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3><?php echo $getLevelName_Session; ?></h3>
                <ul class="nav side-menu">
					<?php 
						if($menu){
							foreach($menu as $rowMenu){
								if($rowMenu['parent_menu'] == 'N'){
					?>
					<li>
						<a id="menu<?php echo $rowMenu['menu_id']; ?>" <?php if($path_info == $rowMenu['link']) {echo "class='active'"; } ?> href="<?php echo site_url($rowMenu['link']); ?>" >
							<i class="fa <?php echo $icon_menu[$rowMenu['menu_id']]; ?>"></i>
							<span><?php echo $rowMenu['menu_name']; ?></span>
						</a>
					</li>
					<?php
							} else {
								if($parentMenu){
									// if(($LevelId_Session	 != 1) && ($rowMenu['menu_id'] == 5)){break;}
					?>
						 <li class="sub-menu">
						  <a id="menu<?php echo $rowMenu['menu_id']; ?>" href="#" >
							  <i class="fa <?php echo $icon_menu[$rowMenu['menu_id']]; ?>"></i>
							  <?php echo $rowMenu['menu_name']; ?><span class="fa fa-chevron-down"></span>
						  </a>
						  <ul class="nav child_menu">
					<?php
										foreach($parentMenu as $rowParentMenu){
											if($rowMenu['menu_id'] == $rowParentMenu['menu_id']){
														
					?>	
							  <li <?php if($path_info == $rowParentMenu['link']) {echo "class='active'"; echo "onload='menuActive(".$rowParentMenu['menu_id'].")'"; } ?>><a  href="<?php echo site_url($rowParentMenu['link']); ?>"><?php echo $rowParentMenu['parent_menu_name']; ?></a></li>
					<?php
													
											}
										}
					?>          
						  </ul>
						</li>
					<?php		
										
									}
								}
							}
						}
					?>
                 
                </ul>
              </div>
              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <!--div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div-->
            <!-- /menu footer buttons -->
          </div>
        </div>
<?php 
		# Create Css #
		if($assets_js){
			foreach($assets_js as $row_left=>$row_right){
				echo "<script src='".$row_right."'></script>";
			}
		}
	?>		