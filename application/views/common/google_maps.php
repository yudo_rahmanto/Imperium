

			<p style="text-align:left;">NIK <font color="red">*</font> <i id="ket_ktp"></i></p>
			<input id="ktp" name="ktp" type="text" class="form-control" placeholder="NO KTP" onkeyup="cekTyping(ktp)" />
			<p style="margin-top:20px;text-align:left;">Jenis Perusahan <font color="red">*</font> <i id="ket_jenis_perusahaan"></i></p>
			<select class="form-control select2" id="jenis_perusahaan" name="jenis_perusahaan" style="width: 100%;" required="required">
				<?php
					if($getJenisPerusahaan){
						foreach($getJenisPerusahaan as $row){
				?>
				  <option value="<?php echo $row['perusahaan_type_id']; ?>"><?php echo $row['perusahaan_type']; ?></option>
				<?php
						}
					}
				?>
			</select>		
			<p style="margin-top:20px;text-align:left;">Nama Perusahaan <font color="red">*</font> <i id="ket_nama_perusahaan"></i></p>
			<input id="nama_perusahaan" name="nama_perusahaan" type="text" class="form-control" placeholder="Nama Perusahaan" onkeyup="cekTyping(first_name)" />
			<p style="margin-top:20px;text-align:left;">Jenis Usaha<font color="red">*</font> <i id="ket_jenis_usaha"></i></p>
			<select class="form-control select2" id="jenis_usaha" name="jenis_usaha" style="width: 100%;" required="required">
				<?php
					if($getJenisUsaha){
						foreach($getJenisUsaha as $row){
				?>
				  <option value="<?php echo $row['jenis_usaha_id']; ?>"><?php echo $row['name']; ?></option>
				<?php
						}
					}
				?>
			</select>	
			<p style="margin-top:20px;text-align:left;">Lingkungan Perusahaan <font color="red">*</font> <i id="ket_lingkungan"></i></p>
			<select class="form-control select2" id="lingkungan" name="lingkungan" style="width: 100%;" required="required">
				<?php
					if($getLingkungan){
						foreach($getLingkungan as $row){
				?>
				  <option value="<?php echo $row['lingkungan_perusahaan_id']; ?>"><?php echo $row['name']; ?></option>
				<?php
						}
					}
				?>				
			</select>	
			<p style="margin-top:20px;text-align:left;">Luas Ruangan Tempat Usaha <font color="red">*</font> <i id="ket_luas"></i></p>
			<input id="luas" name="luas" type="text" class="form-control" placeholder="Luas Ruangan Tempat Usaha"/>
			<p style="margin-top:20px;text-align:left;">No Telp Perusahaan<font color="red">*</font> <i id="ket_tlp"></i></p>
			<input id="tlp" name="tlp" type="text" class="form-control" placeholder="No Telp Perusahaan"/>
			<p style="margin-top:20px;text-align:left;">Kecamatan <font color="red">*</font> <i id="ket_kecamatan"></i></p>
			<select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" required="required" onchange="getData(this.value,`kelurahan`)">
				<?php
					if($getKecamatan){
						foreach($getKecamatan as $row){
				?>
				  <option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
				<?php
						}
					}
				?>	
			</select>	
			<p style="margin-top:20px;text-align:left;">Kelurahan <font color="red">*</font> <i id="ket_kelurahan"></i></p>
			<select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required="required"></select>
			<p style="margin-top:20px;text-align:left;">RT<font color="red">*</font> <i id="ket_rt"></i></p>
			<input id="rt" name="rt" type="text" class="form-control" placeholder="RT"/>
			<p style="margin-top:20px;text-align:left;">RW<font color="red">*</font> <i id="ket_luas"></i></p>
			<input id="rw" name="rw" type="text" class="form-control" placeholder="RW"/>
			<p style="margin-top:20px;text-align:left;">Alamat Perusahaan<font color="red">*</font> <i id="ket_luas"></i></p>
			<textarea class="resizable_textarea form-control" name="alamat" id="alamat" placeholder="Alamat Perusahaan" required="required"></textarea>
			<p style="margin-top:20px;text-align:left;">Google Maps<font color="red">*</font> <i id="ket_luas"></i></p>
			<div id="map" style="height: 354px; width:auto;">
				<?php
				// $CI 							= & get_instance();  //get instance, access the CI superobject
				$this->load->library('googlemaps');
				$config['center'] = '-2.9908761792316305,104.75667910185234';
				$config['zoom'] = 'auto';
				$this->googlemaps->initialize($config);
				$marker = array();
				$marker['position'] = '-2.9908761792316305,104.75667910185234';
				$marker['draggable'] = true;
				// $marker['ondragend'] = 'console.log(\'You just dropped me at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
				$marker['ondragend'] = '$("#lat").val(event.latLng.lat());$("#lng").val(event.latLng.lng());';
				$this->googlemaps->add_marker($marker);
				$map = $this->googlemaps->create_map();
				?>
				<?php echo $map['js']; ?>
				<?php echo $map['html']; ?>
				<input type="hidden" name="lat" id="lat"> 
				<input type="hidden" name="lng" id="lng"> 
			</div>
<script>
  // var marker;
// function initMap() {
        // var map = new google.maps.Map(document.getElementById('map'), {
          // zoom: 13,
          // center: {lat: -2.9908761792316305, lng: 104.75667910185234}
        // });
		
        // marker = new google.maps.Marker({
          // map: map,
          // draggable: true,
          // animation: google.maps.Animation.DROP,
          // position: {lat: -2.9908761792316305, lng: 104.75667910185234}
        // });
        // marker.addListener('click', toggleBounce);
		 // }

      // function toggleBounce() {
		// if (marker.getAnimation() !== null) {
          // marker.setAnimation(null);
        // } else {
			// marker.setAnimation(google.maps.Animation.BOUNCE);
        // }
		
      // }
	  
	  
</script>