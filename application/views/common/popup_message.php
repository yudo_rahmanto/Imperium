<?php
		$error	= $this->session->flashdata('error');
		$message_error	= $this->alert->notice($error);
		$alert		= "";
		if($error){
			foreach($message_error as $alert_left=>$alert_right){
				if($alert_left == 1){$alert = 'success';}
				if($alert_left == 2){$alert = 'danger';}
	?>
<div class="modal fade" id="box-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" id="head-box" style="background-color:blue;color:white;text-align:center;">
			<b id="header-text">Information Box</b>
			</div>
			<div class="modal-body">
				<i id="message-text"><strong><?php echo ucwords(strtolower($alert))." : "; ?></strong> <?php echo $alert_right; ?></i>
			</div>
		</div>
	</div>
</div>
	<?php
			}
		}
	?>
<div class="modal fade" id="box-information" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" id="head-box" style="background-color:blue;color:white;text-align:center;">
			<b id="header-text">Information Box</b>
			</div>
			<div class="modal-body">
				<i id="message-text">Insert Data Success</i>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-lg" id="tabProgressBar" tabindex="-1" role="dialog" aria-labelledby="tabProgressBar">
  <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="progress active">
				<div id="barProgress" class="progress-bar progress-bar-success" style="width: 10%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar">
				<label id="labelProgress" class="sr-only">10% Complete</label>
				</div>
			</div>
		</div>
	</div>
</div>