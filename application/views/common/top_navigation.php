<?php
$dataSession	= $this->session->userdata('user_data');
$CI 			= & get_instance();


if($dataSession){
$UserId_Session			= $dataSession['user_id'];
$JabatanId_Session		= $dataSession['jabatan_id'];
$fullname_Session		= $dataSession['first_name'].' '.$dataSession['last_name'];
$LevelId_Session		= $dataSession['level_id'];
} else {
$UserId_Session			= '';
$JabatanId_Session		= '';
$fullname_Session		= 'No Name';
$LevelId_Session		= '';
}
$notification			= "";
$countInbox				= 0;
$countOnProgress		= 0;
$countOnRetribusi		= 0;
$countSuratIzin			= 0;
if(($JabatanId_Session >= 2) && ($JabatanId_Session <= 5)){
	$CI->load->model('M_approval');

	$search					= array(
										"a.activation"		=> 'Y',
										"a.status"			=> 'On Progress',
										"e.jabatan_id"		=> $JabatanId_Session
									);
	$searchOnProgress		= array(
										"a.activation"				=> 'Y',
										"a.jabatan_id_penerima"		=> $JabatanId_Session
									);
	$getDataInbox			= $CI->M_approval->getInbox($search,'');
	$getDataOnProgress		= $CI->M_approval->getOnProgress($searchOnProgress,'');
	if($getDataInbox){
		$countInbox				= count($getDataInbox);
	}
	if($getDataOnProgress){
		$countOnProgress		= count($getDataOnProgress);
	}
	$total						= $countInbox + $countOnProgress;
	$notification				= '
									<li role="presentation" class="dropdown">
										  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-envelope-o"></i>
											<span class="badge bg-green">'.$total.'</span>
										  </a>
										  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
											<li>
											  <a href="'.base_url('Approval').'">
												<span class="image"><i class="fa fa-envelope-o"></i></span>
												<span>
												  <span>Berkas Baru</span>
												  <span class="time">'.$countInbox.'</span>
												</span>
											   </a>
											</li>
											<li>
											  <a>
												<span class="image"><i class="fa fa-envelope-o" style="margin-left:-5px;"></i></span>
												<span>
												  <span>Berkas Proses</span>
												  <span class="time">'.$countOnProgress.'</span>
												</span>
											   </a>
											</li>
										  </ul>
										</li>
									';
}
else if($JabatanId_Session == 6){
	$CI->load->model('M_finance');
	$search					= array(
										"e.izin_category_id"	=> 2
									);
	$getDataRetribusi		= $CI->M_finance->getFinanceRetribusi($search,'');
	if($getDataRetribusi['data']){
		$countOnRetribusi		= count($getDataRetribusi['data']);
	}
	$notification				= '
									<li role="presentation" class="dropdown">
										  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-envelope-o"></i>
											<span class="badge bg-green">'.$countOnRetribusi.'</span>
										  </a>
										  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
											<li>
											  <a href="'.base_url('Finance/Retribusi').'">
												<span class="image"><i class="fa fa-envelope-o"></i></span>
												<span>
												  <span>Berkas Retribusi</span>
												  <span class="time">'.$countOnRetribusi.'</span>
												</span>
											   </a>
											</li>
										  </ul>
										</li>
									';
}
else if($JabatanId_Session == 1){
	$searchNewSk					= array(
											"status_finish"		=> 1,
											"status_sk"			=> 0,
											"activation"		=> 'Y'
											);
	$searchAllSk					= array(
											"status_finish"		=> 1,
											"status_sk"			=> 1,
											"activation"		=> 'Y'
											);
	$searchPenolakanSk					= array(
											"reject"			=> 1,
											"activation"		=> 'Y'
											);
	$getDataNewSk				= ManyFilter('m_permohonan',$searchNewSk);
	$getDataAllSk				= ManyFilter('m_permohonan',$searchAllSk);
	$getDataAllPenolakanSk		= ManyFilter('m_permohonan',$searchPenolakanSk);
	$countNewSk				= 0;
	$countAllSk				= 0;
	$countAllPenolakanSk	= 0;
	$countAll				= 0;
	if($getDataNewSk){
		$countNewSk			= number_format(count($getDataNewSk));
	}
	if($getDataAllSk){
		$countAllSk			= number_format(count($getDataAllSk));
	}
	if($getDataAllPenolakanSk){
		$countAllPenolakanSk = number_format(count($getDataAllPenolakanSk));
	}
	$countAll				= $countNewSk+$countAllSk+$countAllPenolakanSk;
	$notification			= '
									<li role="presentation" class="dropdown">
										  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-envelope-o"></i>
											<span class="badge bg-green">'.$countAll.'</span>
										  </a>
										  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
											<li>
											  <a>
												<span class="image"><i class="fa fa-envelope-o"></i></span>
												<span>
												  <span>Berkas Siap Cetak</span>
												  <span class="time">'.$countSuratIzin.'</span>
												</span>
											   </a>
											</li>
											<li>
											  <a>
												<span class="image"><i class="fa fa-envelope-o" ></i></span>
												<span>
												  <span>Surat / Sk Siap Cetak</span>
												  <span class="time">'.$countAllSk.'</span>
												</span>
											   </a>
											</li>
											<li>
											  <a>
												<span class="image"><i class="fa fa-envelope-o" style="margin-left:-5px;"></i></span>
												<span>
												  <span>Total SK Penolakan</span>
												  <span class="time">'.$countAllPenolakanSk.'</span>
												</span>
											   </a>
											</li>
											</ul>
										</li>
									';
}
?>
<!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('assets/images/user.png'); ?>" alt=""><?php echo $fullname_Session; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php if($JabatanId_Session == 7){echo base_url('User/edit/'.$UserId_Session);} else {echo base_url('Admin/edit/'.$UserId_Session);} ?>"> Profile</a></li>
                    <!--li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li-->
                    <li><a href="<?php echo base_url('Login/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
					<?php echo $notification; ?>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
		<?php $this->load->view('common/popup_message'); ?>