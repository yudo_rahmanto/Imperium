<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$parent_menu_id		= $getData[0]['sub_menu_id'];
	$menu_id			= $getData[0]['menu_id'];
	$name				= $getData[0]['parent_menu_name'];
	$link				= $getData[0]['link'];
} else {
	$parent_menu_id		= '';
	$menu_id			= '';
	$name				= '';
	$link				= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Menu Name <font color="red"> * </font> <i id="ket_nama_menu" style="color:silver;"></i></label>
								  <input type="hidden" class="form-control" id="parent_menu_id" name="parent_menu_id" placeholder="Parent Menu Id" value="<?php echo $parent_menu_id; ?>">
								  <select class="form-control select2" id="nama_menu" name="nama_menu" style="width: 100%;">
									  <option value="">Select</option>
									<?php
										if($getMenu){
											foreach($getMenu as $row){
												if($menu_id == $row['menu_id']){$selected	= "Selected='selected'";} else {$selected	= '';}
									?>
									  <option value="<?php echo $row['menu_id']; ?>" <?php echo $selected; ?>><?php echo $row['menu_name']; ?></option>
									<?php
											}
										}
									?>
									</select>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Parent Menu Name<font color="red"> * </font> <i id="ket_nama_parent_menu" style="color:silver;"></i></label>
								  <input type="text" class="form-control" id="nama_parent_menu" name="nama_parent_menu" placeholder="Parent Menu Name" value="<?php echo $name; ?>">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Link Parent Menu <i id="ket_nama" style="color:silver;"></i></label>
								  <input type="text" class="form-control" id="link" name="link" placeholder="Link Parent Menu" value="<?php echo $link; ?>">
								</div>
							</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>

<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function simpan(){
		var nama_menu				= document.getElementById('nama_menu').value;
		var nama_parent_menu		= document.getElementById('nama_parent_menu').value;
		document.getElementById('ket_nama_menu').innerHTML = '';
		document.getElementById('ket_nama_parent_menu').innerHTML = '';
		if(!nama_menu){
			document.getElementById('ket_nama_menu').innerHTML = '(Sorry, cannot be empty)';
		}
		else if(!nama_parent_menu){
			document.getElementById('ket_nama_parent_menu').innerHTML = '(Sorry, cannot be empty)';
		} else {
			$.ajax({
					   type: 'post',
					   url: '<?php echo $action; ?>',
					   data: $('form').serialize(),
					   success: function(response) {
						  if(response == 'sukses'){
								document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Update Data Success';
								$('#box-information').modal({'show' : true});
								setTimeout(back(), 5000);
							  
							} else {
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Update Data Failed';
								$('#box-information').modal({'show' : true});
								
						  }
					   }
					});
		}
		
	}

</script>