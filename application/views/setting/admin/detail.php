<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$UserId_Session						= $dataSession['user_id'];
$LevelId_Session							= $dataSession['level_id'];
if($getDataResult){
	$dataAdmin = '';
	foreach($getDataResult[0] as $row_left=>$row_right){
		$dataAdmin[$row_left] = $row_right;
	}
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
						<section class="panel">
						  <header class="panel-heading">
							  Profile Detail
						  </header>
						<div class="panel-body">
							<div class="col-sm-12">
								<ul class="list-group list-group-unbordered">
									<li class="list-group-item"><b>NIP</b><a class="pull-right"><?php if($dataAdmin['nip']){echo $dataAdmin['nip'];} ?></a></li>
									<li class="list-group-item"><b>First Name</b><a class="pull-right"><?php if($dataAdmin['first_name']){echo $dataAdmin['first_name'];} ?></a></li>
									<li class="list-group-item"><b>Last Name</b><a class="pull-right"><?php if($dataAdmin['last_name']){echo $dataAdmin['last_name'];} ?></a></li>
									<li class="list-group-item"><b>Email</b><a class="pull-right"><?php if($dataAdmin['email']){echo $dataAdmin['email'];} ?></a></li>
									<li class="list-group-item"><b>No Telp</b><a class="pull-right"><?php if($dataAdmin['no_tlp']){echo $dataAdmin['no_tlp'];} ?></a></li>
									<li class="list-group-item"><b>User Name</b><a class="pull-right"><?php if($dataAdmin['username']){echo $dataAdmin['username'];} ?></a></li>
								</ul>
							</div>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						</div>
					  </section>
					</div>
				
					</div>
					
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}
</script>