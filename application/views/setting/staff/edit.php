<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$UserId_Session						= $dataSession['user_id'];
$LevelId_Session							= $dataSession['level_id'];
$dataAdmin = '';
if($getDataResult){
	
	foreach($getDataResult[0] as $row_left=>$row_right){
		$dataAdmin[$row_left] = $row_right;
	}
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInput">Jabatan<font color="red"> * </font> <i id="ket_jabatan" style="color:silver;"></i></label>
							  <input type="hidden" class="form-control" id="admin_id" name="admin_id" placeholder="admin_id" value="<?php if($dataAdmin['admin_id']){echo $dataAdmin['admin_id'];}; ?>">
								<select class="form-control select2" id="jabatan_id" name="jabatan_id" style="width: 100%;" required="required">
									<?php
										if($getJabatan){
											foreach($getJabatan as $row){
												if($LevelId_Session	 == 1){
												if($row['jabatan_id'] == $dataAdmin['jabatan_id']){$selected = "Selected='selected'";} else {$selected = "";}
													echo "<option value='".$row['jabatan_id']."' ".$selected.">".$row['jabatan_name']."</option>";
												} else {
													if($row['jabatan_id'] == $dataAdmin['jabatan_id']){
														echo "<option value='".$row['jabatan_id']."'>".$row['jabatan_name']."</option>";
													}
												}
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInput">NIP <font color="red"> * </font> <i id="ket_nip" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP" required="required" onblur="cekNip()" value="<?php if($dataAdmin['nip']){echo $dataAdmin['nip'];} ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInput">First Name<font color="red"> * </font> <i id="ket_first_name" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required="required" value="<?php if($dataAdmin['first_name']){echo $dataAdmin['first_name'];} ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInput">Last Name<font color="red"> * </font> <i id="ket_last_name" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required="required" value="<?php if($dataAdmin['last_name']){echo $dataAdmin['last_name'];} ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInput">Email<font color="red"> * </font> <i id="ket_email" style="color:silver;"></i></label>
							  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required" onblur="cekEmail()" value="<?php if($dataAdmin['email']){echo $dataAdmin['email'];} ?>">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInput">No Telp <font color="red"> * </font> <i id="ket_tlp" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="tlp" name="tlp" placeholder="No Telp" required="required" value="<?php if($dataAdmin['no_tlp']){echo $dataAdmin['no_tlp'];} ?>">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInput">Level<font color="red"> * </font> <i id="ket_level_id" style="color:silver;"></i></label>
								<select class="form-control select2" id="level_id" name="level_id" style="width: 100%;" required="required">
									<?php
										if($getLevel){
											foreach($getLevel as $row){
												if($LevelId_Session		== 1){
													if($row['level_id'] == $dataAdmin['level_id']){$selected = "Selected='selected'";} else {$selected = "";}
										?>
										<option value="<?php echo $row['level_id']; ?>" <?php echo $selected; ?>><?php echo $row['level_name']; ?></option>
										<?php
												} else {
													if($row['level_id']	== 8){
										?>
										<option value="<?php echo $row['level_id']; ?>"><?php echo $row['level_name']; ?></option>
										<?php
													}
												}
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInput">Password <i id="ket_password" style="color:silver;"></i></label>
							  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInput">Konfirmasi Password <i id="ket_password1" style="color:silver;"></i></label>
							  <input type="password" class="form-control" id="password1" name="password1" placeholder="Konfirmasi Password">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Preview Default Tanda Tangan</label>
							</div>
							<div class="form-group">
							 <img src="<?php if($getDataResult){if(file_exists($dataAdmin['ttd'])){echo base_url($dataAdmin['ttd']); } else {echo base_url("assets/images/no_file.png");}} ?>" height="50px" width="auto">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Replace Default Tanda Tangan</label>
							  <input type="file" class="form-control" id="file_upload" name="file_upload" placeholder="Avatar">
							</div>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}
function change_file(){
	document.getElementById('file_upload').style.display = 'block';
	document.getElementById('frame_avatar').style.display = 'none';
	document.getElementById('change_avatar').style.display = 'none';
}
function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = '';
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function matchPassword(){
	var password	= document.getElementById('password').value;
	var password1	= document.getElementById('password1').value;
	if(password == password1){
		return true;
	} else {
		return false;
	}
}
function cekNip(){
	var i 		= 0;
	var nip = document.getElementById('nip').value;
	
			$.ajax({
			   type: 'post',
			   url: '<?php echo $cekNip; ?>',
			   data: {'nip':nip},
			   success: function(response) {
				   if(response == 'Not Ok'){
						document.getElementById('ket_nip').innerHTML = "Sorry, NIP is duplicate";
					    document.getElementById('nip').focus();
					    new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, NIP is duplicate",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
						i++;
					} else {
						document.getElementById('ket_nip').innerHTML = "";
					}
			   }
			});
		if(i == 0){
			return true;
		} else {
			return false;
		}
}
function cekEmail(){
	var user_id	= '<?php echo $UserId_Session; ?>';
	var email = document.getElementById('email').value;
	var atpos 				= email.indexOf("@");
	var dotpos 				= email.lastIndexOf(".");
	var i = 0;
	if((atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)){
			document.getElementById('ket_email').innerHTML = "Sorry, Email address is not valid";
			 new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, Email address is not valid",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
			i++;
		} else {
			$.ajax({
			   type: 'post',
			   url: '<?php echo $cekEmail; ?>',
			   data: {'id':user_id,'email':email},
			   success: function(response) {
				   if(response == 'Not Ok'){
					   document.getElementById('email').focus();
					   document.getElementById('ket_email').innerHTML = "Sorry, Email address is duplicate";
					    new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, Email address is duplicate",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
						i++;
					}
			   }
			});
			document.getElementById('ket_email').innerHTML = "";
		}
		
}
function simpan(){
		var i 				= 0;
		var nip				= document.getElementById('ket_nip').innerHTML;
		var email			= document.getElementById('ket_email').innerHTML;
		var password		= document.getElementById('password').value;
		var password1		= document.getElementById('password1').value;
		var validasi		= $('#form').parsley().validate();
		if(nip){
			document.getElementById('nip').focus();i++;
		}
		if(email){
			document.getElementById('email').focus();i++;
		}
		if((password) && (password1)){
			var cekPassword	= matchPassword();
			if(!cekPassword){
				document.getElementById('password1').focus();
				new PNotify({
								title: "Failure notice",
								type: "info",
								text: "Sorry, your password not match",
								nonblock: {
									nonblock: true
								},
								addclass: 'dark',
								styling: 'bootstrap3',
								delay:500
							});
				i++;
				}
		}
		if((validasi) && (i == 0)){
			$.ajax({
					   type: 'post',
					   url: '<?php echo $action; ?>',
					   data: $('form').serialize(),
					   success: function(response) {
						  if(response == 'sukses'){
								document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								uploadImage();
								}, 2000);
							}
							else if(response == 'duplikat'){
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								}, 2000);
							  
							} else {
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								}, 2000);
								
						  }
					   }
					});
		}
}
function uploadImage(){
		var doc = document.form;
			doc.action= "<?php echo $uploadImage; ?>";
			doc.submit();
	}
</script>