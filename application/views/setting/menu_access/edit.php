<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getLevel){
	$level_id		= $getLevel[0]['level_id'];
	$level_name		= $getLevel[0]['level_name'];
} else {
	$level_id		= '';
	$level_name		= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
								<div class="form-group">
								  <label for="exampleInputEmail1">Level Name </label>
								  <input type="hidden" class="form-control" id="level_id" name="level_id" placeholder="Level Id" value="<?php echo $level_id; ?>">
									<input type="text" class="form-control" placeholder="Level Name" readonly="readonly" value="<?php echo $level_name; ?>">
								</div>
							</div>
							<?php
								if($getMenu){
									$i = 1;
									foreach($getMenu as $row_menu){
										$check_add		= "";
										$check_edit		= "";
										$check_delete	= "";
										if($getData){
												foreach($getData as $row_isiData){
													if(($row_isiData['menu_id'] == $row_menu['menu_id'])){
														if($row_isiData['level_id']){$check_view	= "checked='checked'";} else {$check_view	= "";}
													} 
												}
											}
							?>
							<div id="body_<?php echo $i ; ?>"  class="col-sm-6">
								<div class="form-group">
									 <label><?php echo $row_menu['menu_name']; ?></label>
									<input type="hidden" class="form-control" id="menu_id<?php echo $i; ?>" name="menu_id<?php echo $i; ?>" value="<?php echo $row_menu['menu_id']; ?>">
								</div>
							</div>
							<div id="body_<?php echo $i ; ?>" class="col-sm-6">
								<div class="form-group">
									<label>&nbsp;</label>
								</div>
							</div>
							<?php
								if($getParentMenu){
									$ii = 1;
									foreach($getParentMenu as $row_parent_menu){
										if($row_parent_menu['menu_id'] == $row_menu['menu_id']){
											$checked_view	= "";
												if($getData){
													foreach($getData as $row_isiData){
														if(($row_isiData['menu_id'] == $row_menu['menu_id']) && ($row_isiData['sub_menu_id'] == $row_parent_menu['sub_menu_id'])){
															$checked_view	= "checked='checked'";
														} 
													}
												}
							?>
						
							<div id="body_<?php echo $i ; ?>"  class="col-sm-1">
								<div class="form-group">
									<label>&nbsp;</label>
								</div>
							</div>
							<div id="body_<?php echo $i ; ?>"  class="col-sm-5">
								<div class="form-group">
									<label><?php echo $row_parent_menu['parent_menu_name']; ?></label>
									<input type="hidden" class="form-control" id="sub_menu_id<?php echo $i.'_'.$ii; ?>" name="sub_menu_id<?php echo $i.'_'.$ii; ?>" value="<?php echo $row_parent_menu['sub_menu_id']; ?>">
								</div>
							</div>
							<div id="body_<?php echo $i ; ?>" class="col-sm-6">
								<div class="form-group">
									<label class="label_check" for="view<?php echo $i.'_'.$ii; ?>">
										<input id="view<?php echo $i.'_'.$ii; ?>" name="view<?php echo $i.'_'.$ii; ?>" value="1" type="checkbox" <?php echo $checked_view; ?> onclick="cekChecked('view<?php echo $i.'_'.$ii; ?>')"/>
										View
										<input type="hidden" class="form-control" id="isi_view<?php echo $i.'_'.$ii; ?>" name="isi_view<?php echo $i.'_'.$ii; ?>" value="<?php if($checked_view){echo 1;} else {echo 0;} ?>">
									</label>
								</div>
							</div>
							
							<?php		
										$ii++;
										}
									}
								}
							?>
							<?php		
									$i++;
									}
								}
							?>
					</div>
					<div class="x_content" style="-moz-box-shadow: 0 0 2px black;-webkit-box-shadow: 0 0 2px black;box-shadow: 0 0 2px black;">
						<div class="col-sm-12">
							<label>Daftar Izin</label>
						</div>
						<?php
							if($getIzin){
								$i = 1;
								foreach($getIzin as $row){
									if(($IzinAccess) && (in_array($row['izin_id'],$IzinAccess))){$checked = "Checked='checked'";} else {$checked = "";}
						?>
						<div class="col-sm-3">
							<input name="izin<?php echo $i; ?>" id="izin<?php echo $i; ?>" value="<?php echo $row['izin_id']; ?>" type="checkbox" <?php echo $checked; ?>> <?php echo $row['izin_name']; ?>
						</div>
						<?php
								$i++;
								}
							}
						?>	
						
					</div>
					<div class="clearfix"></div>
					 <div class="box-footer" style="margin-top:20px;">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
 
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function cekChecked(field){
	var checked	= document.getElementById(field).checked;
		if(checked == true){
			document.getElementById('isi_'+field).value = 1;
		} else {
			document.getElementById('isi_'+field).value = 0;
		}
}
function simpan(){
		
			$.ajax({
					   type: 'post',
					   url: '<?php echo $action; ?>',
					   data: $('form').serialize(),
					   success: function(response) {
						  if(response == 'sukses'){
								document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Update Data Success';
								$('#box-information').modal("show");
								setTimeout(function(){
								 $('#box-information').modal("hide");
								 back();
								}, 2000);
							  
							} else {
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Update Data Failed';
								$('#box-information').modal("show");
								setTimeout(function(){
								 $('#box-information').modal("hide");
								}, 2000);
								
						  }
					   }
					});
		
		
	}

</script>