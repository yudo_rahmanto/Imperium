<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$parent_menu_id		= $getData[0]['parent_menu_id'];
	$menu				= $getData[0]['menu_name'];
	$name				= $getData[0]['parent_menu_name'];
	$link				= $getData[0]['link'];
} else {
	$parent_menu_id		= '';
	$menu			= '';
	$name				= '';
	$link				= '';
}
?>
   <section id="main-content">
          <section class="wrapper">
			<form name="search" id="search" method="post" enctype="multipart/form-data">
            <!-- page start-->
				<div class="modal fade" id="box-information" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header" id="head-box" style="background-color:blue;color:white;text-align:center;">
							<b id="header-text">Information Box<b>
							</div>
							<div class="modal-body">
								<i id="message-text">Insert Data Success</i>
							</div>
						</div>
					</div>
				</div>
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Menu Detail
                          </header>
						<div class="panel-body">
							<div class="col-sm-12">
					<ul class="list-group list-group-unbordered">
						<li class="list-group-item"><b>Menu Name</b><a class="pull-right"><?php echo $menu; ?></a></li>
						<li class="list-group-item"><b>Parent Menu Name</b><a class="pull-right"><?php echo $name; ?></a></li>
						<li class="list-group-item"><b>Link</b><a class="pull-right"><?php echo $link; ?></a></li>
					</ul>
				</div>
                        </div>
						  <div class="box-footer">
							<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						 </div>
                      </section>
                  </div>
              </div>
			</form>
            </section>
      </section>
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}
</script>