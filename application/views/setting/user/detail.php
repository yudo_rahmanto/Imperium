<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$getUserId_Session			= $dataSession['user_id'];
$LevelId_Session						= $dataSession['level_id'];
if($getDataResult){
	$user_management_id		= $getDataResult[0]['user_management_id'];
	$level_id				= $getDataResult[0]['level_id'];
	$ktp					= $getDataResult[0]['ktp'];
	$npwp					= $getDataResult[0]['npwp'];
	$first_name				= $getDataResult[0]['first_name'];
	$last_name				= $getDataResult[0]['last_name'];
	$email					= $getDataResult[0]['email'];
	$no_tlp					= $getDataResult[0]['no_tlp'];
	$address				= $getDataResult[0]['address'];
	$rt						= $getDataResult[0]['rt'];
	$rw						= $getDataResult[0]['rw'];
	$provinsi_id			= $getDataResult[0]['provinsi_id'];
	$kabupaten_id			= $getDataResult[0]['kabupaten_id'];
	$kecamatan_id			= $getDataResult[0]['kecamatan_id'];
	$kelurahan_id			= $getDataResult[0]['kelurahan_id'];
	$provinsi_name			= $getDataResult[0]['provinsi_name'];
	$kabupaten_name			= $getDataResult[0]['kabupaten_name'];
	$kecamatan_name			= $getDataResult[0]['kecamatan_name'];
	$kelurahan_name			= $getDataResult[0]['kelurahan_name'];
	$username				= $getDataResult[0]['username'];
	$image					= $getDataResult[0]['avatar'];
	if(($image) && (file_exists(path_user_upload.'user_'.$getUserId_Session.'/image/'.$image))){
		$extract		= explode('.',$image);
		$path_image		= base_url(path_view_user_upload.'user_'.$getUserId_Session.'/image/thumbnail/'.$extract[0].'_thumb.'.$extract[1]);
	} else {
		$path_image		= base_url('assets/images/user.png');
	}
} else {
	$user_management_id		= '';
	$level_id				= '';
	$ktp					= '';
	$npwp					= '';
	$first_name				= '';
	$last_name				= '';
	$email					= '';
	$no_tlp					= '';
	$address				= '';
	$rt						= '';
	$rw						= '';
	$provinsi_id			= '';
	$kabupaten_id			= '';
	$kecamatan_id			= '';
	$kelurahan_id			= '';
	$provinsi_name			= '';
	$kabupaten_name			= '';
	$kecamatan_name			= '';
	$kelurahan_name			= '';
	$username				= '';
	$image					= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
					<div class="x_content">
						<section class="panel">
						  <header class="panel-heading">
							  Profile Detail
						  </header>
						<div class="panel-body">
							<div class="col-sm-5">&nbsp;</div>
							<div class="col-sm-2">
							<img id="image" src="<?php echo $path_image; ?>" alt="" style="width:100px;height:100px;"/>
							</div>
							<div class="col-sm-5">&nbsp;</div>
							<div class="col-sm-12">&nbsp;</div>
							<div class="col-sm-12">
								<ul class="list-group list-group-unbordered">
									<li class="list-group-item"><b>KTP</b><a class="pull-right"><?php echo $ktp; ?></a></li>
									<li class="list-group-item"><b>NPWP</b><a class="pull-right"><?php echo $npwp; ?></a></li>
									<li class="list-group-item"><b>First Name</b><a class="pull-right"><?php echo $first_name; ?></a></li>
									<li class="list-group-item"><b>Last Name</b><a class="pull-right"><?php echo $last_name; ?></a></li>
									<li class="list-group-item"><b>Email</b><a class="pull-right"><?php echo $email; ?></a></li>
									<li class="list-group-item"><b>No Telp</b><a class="pull-right"><?php echo $no_tlp; ?></a></li>
									<li class="list-group-item"><b>User Name</b><a class="pull-right"><?php echo $username; ?></a></li>
									<li class="list-group-item"><b>Provinsi</b><a class="pull-right"><?php echo $provinsi_name; ?></a></li>
									<li class="list-group-item"><b>Kabupaten</b><a class="pull-right"><?php echo $kabupaten_name; ?></a></li>
									<li class="list-group-item"><b>Kecamatan</b><a class="pull-right"><?php echo $kecamatan_name; ?></a></li>
									<li class="list-group-item"><b>Kelurahan</b><a class="pull-right"><?php echo $kelurahan_name; ?></a></li>
									<li class="list-group-item"><b>RT / RW</b><a class="pull-right"><?php echo $rt.' / '.$rw; ?></a></li>
									<li class="list-group-item"><b>Alamat</b><a class="pull-right"><?php echo $address; ?></a></li>
								</ul>
							</div>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						  </div>
					  </section>
					</div>
				
					</div>
					
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}
</script>