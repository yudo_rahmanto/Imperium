<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$UserId_Session						= $dataSession['user_id'];
$LevelId_Session							= $dataSession['level_id'];
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Avatar <font color="red"> * </font> <i id="ket_nik" style="color:silver;"></i></label>
							  <input type="file" class="form-control" id="file_upload" name="file_upload" placeholder="Avatar">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">KTP <font color="red"> * </font> <i id="ket_ktp" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="ktp" name="ktp" placeholder="KTP" required="required">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">NPWP <font color="red"> * </font> <i id="ket_npwp" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="npwp" name="npwp" placeholder="NPWP" required="required">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">First Name<font color="red"> * </font> <i id="ket_first_name" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required="required">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">Last Name<font color="red"> * </font> <i id="ket_last_name" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required="required">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">Email<font color="red"> * </font> <i id="ket_email" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="email" name="email" placeholder="Email" required="required">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">No Telp <font color="red"> * </font> <i id="ket_tlp" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="tlp" name="tlp" placeholder="No Telp" required="required">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">Level<font color="red"> * </font> <i id="ket_level_id" style="color:silver;"></i></label>
								<select class="form-control select2" id="level_id" name="level_id" style="width: 100%;" required="required">
									<?php
										if($getLevel){
											foreach($getLevel as $row){
												if($LevelId_Session		== 1){
										?>
										<option value="<?php echo $row['level_id']; ?>"><?php echo $row['level_name']; ?></option>
										<?php
												} else {
													if($row['level_id']	== 8){
										?>
										<option value="<?php echo $row['level_id']; ?>"><?php echo $row['level_name']; ?></option>
										<?php
													}
												}
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">User Name<font color="red"> * </font> <i id="ket_username" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="username" name="username" placeholder="User Name" required="required">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">Password<font color="red"> * </font> <i id="ket_password" style="color:silver;"></i></label>
							  <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">Konfirmasi Password<font color="red"> * </font> <i id="ket_password1" style="color:silver;"></i></label>
							  <input type="password" class="form-control" id="password1" name="password1" placeholder="Konfirmasi Password" required="required">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">Provinsi<font color="red"> * </font> <i id="ket_provinsi" style="color:silver;"></i></label>
								<select class="form-control select2" id="provinsi" name="provinsi" style="width: 100%;" onchange="getData(this.value,'kabupaten')" required="required">
									<option value="">Select</option>
									<?php
										if($getProvinsi){
											foreach($getProvinsi as $row){
										?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
										<?php
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
							  <label for="exampleInputEmail1">Kabupaten<font color="red"> * </font> <i id="ket_kabupaten" style="color:silver;"></i></label>
								<select class="form-control select2" id="kabupaten" name="kabupaten" style="width: 100%;" onchange="getData(this.value,'kecamatan')" required="required">
									<?php
										if($getKabupaten){
											foreach($getKabupaten as $row){
										?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
										<?php
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1">Kecamatan<font color="red"> * </font> <i id="ket_kecamatan" style="color:silver;"></i></label>
								<select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" onchange="getData(this.value,'kelurahan')" required="required">
									<?php
										if($getKecamatan){
											foreach($getKecamatan as $row){
										?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
										<?php
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1">Kelurahan<font color="red"> * </font> <i id="ket_kecamatan" style="color:silver;"></i></label>
							  <select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required="required">
									  <?php
										if($getKelurahan){
											foreach($getKelurahan as $row){
										?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
										<?php
											}
										}
										?>
								</select>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group">
							  <label for="exampleInputEmail1">Rt<font color="red"> * </font> <i id="ket_kelurahan" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="rt" name="rt" placeholder="RT" required="required">
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group">
							  <label for="exampleInputEmail1">Rw<font color="red"> * </font> <i id="ket_kelurahan" style="color:silver;"></i></label>
							  <input type="text" class="form-control" id="rw" name="rw" placeholder="RW" required="required">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1">Alamat <font color="red"> * </font> <i id="ket_nama_perusahaan" style="color:silver;"></i></label>
							</div>
							<div class="form-group">
							 <textarea class="resizable_textarea form-control" name="alamat" id="alamat" placeholder="Alamat" required="required"></textarea>
							</div>
						</div>
						
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}

function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = "<option value=''>Select</option>";	
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function matchPassword(){
	var password	= document.getElementById('password').value;
	var password1	= document.getElementById('password1').value;
	if(password == password1){
		return true;
	} else {
		return false;
	}
}
function cekEmail(){
	var user_id	= '';
	var email = document.getElementById('email').value;
	var atpos 				= email.indexOf("@");
	var dotpos 				= email.lastIndexOf(".");
	var i = 0;
	if((atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)){
			 new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, Email address is not valid",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
			i++;
		} else {
			$.ajax({
			   type: 'post',
			   url: '<?php echo $cekEmail; ?>',
			   data: {'id':user_id,'email':email},
			   success: function(response) {
				   if(response == 'Not Ok'){
					   document.getElementById('email').focus();
					    new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, Email address is duplicate",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
						i++;
					}
			   }
			});
		}
		if(i == 0){
			return true;
		} else {
			return false;
		}
}
function simpan(){
		var i = 0;
		var email			= document.getElementById('email').value;
		var password		= document.getElementById('password').value;
		var password1		= document.getElementById('password1').value;
		var validasi		= $('#form').parsley().validate();
		if(email){
			var cek_email	= cekEmail();
			if(!cek_email){document.getElementById('email').focus();i++;}
		}
		if((password) && (password1)){
			var cekPassword	= matchPassword();
			if(!cekPassword){
				document.getElementById('password1').focus();
				new PNotify({
								title: "Failure notice",
								type: "info",
								text: "Sorry, your password not match",
								nonblock: {
									nonblock: true
								},
								addclass: 'dark',
								styling: 'bootstrap3',
								delay:500
							});
				i++;
				}
		}
		if((validasi) && (i == 0)){
			$.ajax({
					   type: 'post',
					   url: '<?php echo $action; ?>',
					   data: $('form').serialize(),
					   success: function(response) {
						  if(response == 'sukses'){
								document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								uploadImage();
								}, 2000);
							}
							else if(response == 'duplikat'){
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								}, 2000);
							  
							} else {
								document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
								document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
								$('#box-information').modal("show");
								setTimeout(function(){
								$('#box-information').modal("hide");
								}, 2000);
								
						  }
					   }
					});
		}
}
function uploadImage(){
		var doc = document.form;
			doc.action= "<?php echo $uploadImage; ?>";
			doc.submit();
	}
</script>