<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div id="wizard" class="form_wizard wizard_horizontal">
						  <ul class="wizard_steps">
							<li>
							  <a href="#step-1">
								<span class="step_no">1</span>
								<span class="step_descr">
												  Step 1<br />
												  <small>Pilih Izin</small>
											  </span>
							  </a>
							</li>
							<li>
							  <a href="#step-2">
								<span class="step_no">2</span>
								<span class="step_descr">
												  Step 2<br />
												  <small>Display Data Perusahaan</small>
											  </span>
							  </a>
							</li>
							<li>
							  <a href="#step-3">
								<span class="step_no">3</span>
								<span class="step_descr">
												  Step 3<br />
												  <small>Upload Data Persyaratan</small>
											  </span>
							  </a>
							</li>
							
						  </ul>
						  <div id="step-1">
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Pilih Perusahaan<font color="red"> * </font> <i id="ket_perusahaan" style="color:silver;"></i></label>
									   <select class="form-control select2" id="detect_perusahaan" name="detect_perusahaan" style="width: 100%;" onchange="detection_perusahaan()">
											<option value="">Select</option>
											<option value="0">Baru</option>
											<option value="1">Sudah Tersedia</option>
										</select>
									  <select class="form-control select2" id="perusahaan" name="perusahaan" style="width: 100%;display:none;" required="required" onchange="getSyarat()">
											<?php
												if($getPerusahaan){
													foreach($getPerusahaan as $row){
											?>
											<option value="<?php echo $row['perusahaan_id']; ?>"><?php echo $row['nama_perusahaan']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Nama Ijin<font color="red"> * </font> <i id="ket_izin" style="color:silver;"></i></label>
									  	<select class="form-control select2" id="izin" name="izin" style="width: 100%;" required="required" onchange="getSyarat()">
											<option value="">Select</option>
											<?php
												if($getIzin){
													foreach($getIzin as $row){
											?>
											<option value="<?php echo $row['izin_id']; ?>"><?php echo $row['izin_name']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">Jenis Permohonan<font color="red"> * </font> <i id="ket_izin_type" style="color:silver;"></i></label>
										<select class="form-control select2" id="izin_type" name="izin_type" style="width: 100%;" required="required" onchange="getSyarat()">
											<option value="">Select</option>
											<?php
												if($getTypeIzin){
													foreach($getTypeIzin as $row){
											?>
											<option value="<?php echo $row['izin_type_id']; ?>"><?php echo $row['type_name']; ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-12" style="height:150px;">
									<div class="form-group">
									  <label for="exampleInputEmail1">Persyaratan<font color="red"> * </font> <i id="ket_persyaratan" style="color:silver;"></i></label>
									</div>
									<div class="form-group">
										<table class="table table-striped table-hover table-bordered" id="table_persyaratan"></table>	
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="exampleInputEmail1">&nbsp;</label>
									</div>
								</div>
							
						  </div>
							<div id="step-2">
								<h2 class="StepTitle">Data Izin Permohonan</h2>
								<div class="col-sm-12">
									<ul class="list-group list-group-unbordered">
										<li class="list-group-item"><b>Nama Izin</b><a class="pull-right" id="nama_izin"></a></li>
										<li class="list-group-item"><b>Jenis Permohonan</b><a class="pull-right" id="jenis_permohonan"></a></li>
									</ul>
								</div>
							
								<h2 class="StepTitle">Data Pemohon</h2>
								<div class="col-sm-12">
									<ul class="list-group list-group-unbordered">
										<li class="list-group-item"><b>NIK</b><a class="pull-right" id="nik"></a></li>
										<li class="list-group-item"><b>Full Name</b><a class="pull-right" id="full_name"></a></li>
										<li class="list-group-item"><b>Email</b><a class="pull-right" id="email"></a></li>
										<li class="list-group-item"><b>No Telp</b><a class="pull-right" id="no_tlp_user"></a></li>
										<li class="list-group-item"><b>Provinsi</b><a class="pull-right" id="provinsi_name_user"></a></li>
										<li class="list-group-item"><b>Kabupaten</b><a class="pull-right" id="kabupaten_name_user"></a></li>
										<li class="list-group-item"><b>Kecamatan</b><a class="pull-right" id="kecamatan_name_user"></a></li>
										<li class="list-group-item"><b>Kelurahan</b><a class="pull-right" id="kelurahan_name_user"></a></li>
										<li class="list-group-item"><b>RT / RW</b><a class="pull-right"  id="rt/rw_user"></a></li>
										<li class="list-group-item"><b>Alamat</b><a class="pull-right" id="alamat_user"></a></li>
									</ul>
								</div>
							
								<h2 class="StepTitle">Data Perusahaan</h2>
								<div class="col-sm-12">
									<ul class="list-group list-group-unbordered">
										<li class="list-group-item"><b>Nama Perusahaan</b><a class="pull-right" id="nama_perusahaan"></a></li>
										<li class="list-group-item"><b>No Tlp</b><a class="pull-right" id="no_tlp_perusahaan"></a></li>
										<li class="list-group-item"><b>Kecamatan</b><a class="pull-right" id="kecamatan_name_perusahaan"></a></li>
										<li class="list-group-item"><b>Kelurahan</b><a class="pull-right" id="kelurahan_name_perusahaan"></a></li>
										<li class="list-group-item"><b>RT/RW</b><a class="pull-right" id="rt/rw_perusahaan"></a></li>
										<li class="list-group-item"><b>Alamat</b><a class="pull-right" id="alamat_perusahaan"></a></li>
									</ul>
								</div>
							</div>	
							<div id="step-3">
								<h2 class="StepTitle">Step 3 Upload Data Persyaratan</h2>
								<div class="col-sm-12" style="height:300px;">
									<div class="form-group">
										<label for="exampleInputEmail1">Upload Persyaratan<font color="red"> * </font></label>
									</div>
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan"></table>	
								</div>
								</div>
							</div>
						 
						</div>
						<input type="hidden" name="jml_row_syarat" id="jml_row_syarat">
					</div>
					
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		
  
<script>

$('#wizard').smartWizard();
$('.buttonNext').hide();
$('.buttonPrevious').hide();
$('.buttonFinish').hide();
function detection_perusahaan(){
	var action = document.getElementById('detect_perusahaan').value;
	if(action == 1){
		document.getElementById('detect_perusahaan').style.display = 'none'
		document.getElementById('perusahaan').style.display = 'block';
	} else{
		window.location.href = "<?php echo $addPerusahaan; ?>";
	}
}
function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = "<option value=''>Select</option>";
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function checkFile(id){
	var file		= document.getElementById('upload_file'+id).files;
	var name_file 	= file[0]['name'].toLowerCase();
	var size_file	= file[0]['size'];
	var format_default 	= document.getElementById('format_file'+id).value.toLowerCase();
	var size_default 	= document.getElementById('max_size'+id).value;
	var explode_format_file	= format_default.split("|");
	var hasil		= false;
	if(name_file){
		var explode = name_file.split('.');
		var format	= explode[explode.length-1];
		for(var i = 0; i < explode_format_file.length; i++){
			if(explode_format_file[i] == format){
				hasil = true;
			}
			else if(size_file < size_default){
				hasil = true;
			}
		}
		
		if(!hasil){
			alert('Maaf Format file / Size tidak sesuai');
			document.getElementById('upload_file'+id).value = '';
		}
	}
}
function getSyarat(){
	var perusahaan	= document.getElementById('perusahaan').value;
	var izin		= document.getElementById('izin').value;
	var izin_type	= document.getElementById('izin_type').value;
	
	if((perusahaan) && (izin) && (izin_type)){
		$('.buttonNext').show();
		$('.buttonPrevious').show();
		$('.buttonFinish').show();
		$('.buttonNext').addClass('btn btn-success');
		$('.buttonPrevious').addClass('btn btn-primary');
		$('.buttonFinish').addClass('btn btn-default');
		$('.buttonFinish').removeAttr("href");
		$('.buttonFinish').attr("id","finish_button");
		document.getElementById('finish_button').setAttribute("onclick","simpan()");
		
		 $.each(<?php echo json_encode($getIzin); ?>, function (index, data) {
			 var izin_id		= data['izin_id'];  
			 var izin_name		= data['izin_name']; 
			 if(izin == izin_id){
				 document.getElementById('nama_izin').innerHTML = izin_name;
			 }
		 });
		  $.each(<?php echo json_encode($getTypeIzin); ?>, function (index, data) {
			 var izin_type_id		= data['izin_type_id'];  
			 var type_name			= data['type_name']; 
			 if(izin_type == izin_type_id){
				 document.getElementById('jenis_permohonan').innerHTML = type_name;
			 }
		 });
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $linkSyarat; ?>',
			   data: {'izin_id':izin,'izin_type_id':izin_type},
			   dataType: 'json',
			   success: function(response) {
				   if(response){
						var html	= "";
							html	+= "	<thead>";
							html	+= "		<tr>";
							html	+= "			<th>No</th>";
							html	+= "			<th> Persyaratan </th>";
							html	+= "		</tr>";
							html	+= "	</thead>";
						
						var html1	= "";
							html1	+= "	<thead>";
							html1	+= "		<tr>";
							html1	+= "			<th width='5%'>No</th>";
							html1	+= "			<th width='30%'> Persyaratan </th>";
							html1	+= "			<th width='25%'> Format & Max Size </th>";
							html1	+= "			<th width='40%'> Attach </th>";
							html1	+= "		</tr>";
							html1	+= "	</thead>";
						var i = 1;
					   $.each(response, function (index, data) {
						var syarat_id		= data['syarat_id'];  
						var syarat_name		= data['syarat_name'];  
						var max_size		= data['max_size'];  
						var format_file		= data['format_file'];  
							html	+= "	<tbody id='syarat"+i+"'>";
							html	+= "		<tr>";
							html	+= "			<td>"+i+"</td>";
							html	+= "			<td> "+syarat_name+"</td>";
							html	+= "		</tr>";
							html	+= "	</tbody>";
							
							html1	+= "	<tbody id='syarat"+i+"'>";
							html1	+= "		<tr>";
							html1	+= "			<td>"+i+"</td>";
							html1	+= "			<td> "+syarat_name+"</td>";
							html1	+= "			<td> "+format_file+" ( Max Size : "+max_size+" MB)</td>";
							html1	+= "			<td> <input type='hidden' class='form-control' name='syarat_id"+i+"' id='syarat_id"+i+"' value='"+syarat_id+"'><input type='hidden' class='form-control' name='max_size"+i+"' id='max_size"+i+"' value='"+max_size+"'><input type='hidden' class='form-control' name='format_file"+i+"' id='format_file"+i+"' value='"+format_file+"'><input type='file' class='form-control' name='upload_file"+i+"' id='upload_file"+i+"' required='required' onchange='checkFile("+i+")'></td>";
							html1	+= "		</tr>";
							html1	+= "	</tbody>";
						i++;
					   });
							document.getElementById('table_persyaratan').innerHTML = html;
							document.getElementById('table_upload_persyaratan').innerHTML = html1;
							document.getElementById('jml_row_syarat').value = (i-1);
				   }
			   }
		});
		
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $linkPemohon; ?>',
			   data: {},
			   dataType: 'json',
			   success: function(response) {
				   if(response){
						
					   $.each(response, function (index, data) {
						var nik					= data['nik'];  
						var first_name			= data['first_name'];  
						var last_name			= data['last_name'];  
						var email				= data['email'];  
						var no_tlp				= data['no_tlp'];  
						var address				= data['address'];  
						var rt					= data['rt'];  
						var rw					= data['rw'];  
						var provinsi_name		= data['provinsi_name'];  
						var kabupaten_name		= data['kabupaten_name'];  
						var kecamatan_name		= data['kecamatan_name'];  
						var kelurahan_name		= data['kelurahan_name'];  
						
						document.getElementById('nik').innerHTML = nik;
						document.getElementById('full_name').innerHTML = first_name+' '+last_name;
						document.getElementById('email').innerHTML = email;
						document.getElementById('no_tlp_user').innerHTML = no_tlp;
						document.getElementById('provinsi_name_user').innerHTML = provinsi_name;
						document.getElementById('kabupaten_name_user').innerHTML = kabupaten_name;
						document.getElementById('kecamatan_name_user').innerHTML = kecamatan_name;
						document.getElementById('kelurahan_name_user').innerHTML = kelurahan_name;
						document.getElementById('rt/rw_user').innerHTML = rt+' / '+rw;
						document.getElementById('alamat_user').innerHTML = address;
						});
							
				   }
			   }
		});
		
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $linkPerusahaan; ?>',
			   data: {'perusahaan_id':perusahaan},
			   dataType: 'json',
			   success: function(response) {
				   if(response){
						
					   $.each(response, function (index, data) {
						var nama_perusahaan		= data['nama_perusahaan'];  
						var first_name			= data['first_name'];  
						var no_tlp				= data['tlp'];  
						var alamat				= data['alamat'];  
						var rt					= data['rt'];  
						var rw					= data['rw'];  
						var provinsi_name		= data['provinsi_name'];  
						var kabupaten_name		= data['kabupaten_name'];  
						var kecamatan_name		= data['kecamatan_name'];  
						var kelurahan_name		= data['kelurahan_name'];  
						
						document.getElementById('nama_perusahaan').innerHTML = nama_perusahaan;
						document.getElementById('no_tlp_perusahaan').innerHTML = no_tlp;
						document.getElementById('kecamatan_name_perusahaan').innerHTML = kecamatan_name;
						document.getElementById('kelurahan_name_perusahaan').innerHTML = kelurahan_name;
						document.getElementById('rt/rw_perusahaan').innerHTML = rt+' / '+rw;
						document.getElementById('alamat_perusahaan').innerHTML = alamat;
							
						
					   });
							
				   }
			   }
		});
	}
}
function back(){
	window.location = "<?php echo $back; ?>";
}

function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						uploadImage()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function uploadImage(){
		var doc = document.form;
			doc.action= "<?php echo $uploadImage; ?>";
			doc.submit();
	}
</script>