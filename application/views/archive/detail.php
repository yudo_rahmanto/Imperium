<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession			= $this->session->userdata('user_data');
$UserId_Session					= $dataSession['user_id'];
$LevelId_Session			= $dataSession['level_id'];
$izin_id				= '';
$izin_type_id			= '';
$status_level			= '';



if($getDataPermohonan){
	$dataPermohonan = '';
	foreach($getDataPermohonan[0] as $row_left=>$row_right){
		$dataPermohonan[$row_left] = $row_right;
	}
	$izin_id				= $dataPermohonan['izin_id'];
	$izin_type_id			= $dataPermohonan['izin_type_id'];
	$status_level			= $dataPermohonan['status_level'];
}
if($getDataPerusahaan){
	$dataPerusahaan = '';
	foreach($getDataPerusahaan[0] as $row_left=>$row_right){
		$dataPerusahaan[$row_left] = $row_right;
	}
} 
if($getDataUser){
	$dataUser = '';
	foreach($getDataUser[0] as $row_left=>$row_right){
		$dataUser[$row_left] = $row_right;
	}
}
$getAlurIzin		= grabDataAlurIzin($izin_id,$izin_type_id);
$jabatan_penerima	= $getAlurIzin[$status_level]['jabatan_name'];

?>

<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_panel">
                  <div class="x_content">
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
						<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Permohonan</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data User</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Data Perusahaan</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data Persyaratan</a>
                        </li>
						 <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data History</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Izin</td>
												<td width="1%">:</td>
												<td width="79%">
												<input type="hidden" class="form-control" id="permohonan_id" name="permohonan_id" placeholder="Permohonan Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['permohonan_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_id" name="izin_id" placeholder="Izin Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_type_id" name="izin_type_id" placeholder="Izin Type Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_type_id']; } ?>">
												<input type="hidden" class="form-control" id="status_level" name="status_level" placeholder="Status Level" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['status_level']; } ?>">
												<?php if($getDataPermohonan){echo $dataPermohonan['izin_name']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Izin</td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){echo $dataPermohonan['izin_type_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Tanggal Pembuatan </td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){
																																		$originalDate = $dataPermohonan['created_date'];
																																		$date = date("d M Y H:i:s", strtotime($originalDate));
																																		echo $date; 
																																		}
																															?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="page1-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> No KTP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['ktp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No NPWP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['npwp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Full Name</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['first_name'].' '.$dataUser['last_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Email</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['email']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No Tlp</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['no_tlp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['address']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['rt'].' / '.$dataUser['rw']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Provinsi </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['provinsi_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kabupaten </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kabupaten_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kecamatan_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kelurahan_name']; } ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="page2-tab">
                          <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['initial_jenis_perusahaan'].'. '; } ?> <?php if($getDataPerusahaan){echo $dataPerusahaan['nama_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Usaha </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['jenis_usaha']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lingkungan Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['lingkungan_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lokasi Perusahaan</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['lokasi_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Luas </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['luas']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['alamat']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['rt']; } ?> / <?php if($getDataPerusahaan){echo $dataPerusahaan['rw']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['kecamatan_name']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['kelurahan_name']; } ?>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="page3-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="30%"> Persyaratan</td><td width="80%">File</td>
											</tr>
											<?php
												if($getDataPersyaratan){
													foreach($getDataPersyaratan as $row){
														$file_parts = pathinfo($row['path_location']);
														$extension	= $file_parts['extension'];
														if(($extension == 'jpg') | ($extension == 'jpeg') || ($extension == 'png')){
															$send_file	= "onclick=sendFile('".$row['path_location']."')";
															} else {
															$send_file	= "onclick=openFile('".$row['path_location']."')";	
															}
														// echo '<pre>';print_r($file_parts);
											?>
											<tr>
												<td width="80%"> <?php echo $row['syarat_name']; ?></td><td width="20%"><img class="avatar" src="<?php echo base_url('assets/images/file.jpg'); ?>" alt="Avatar" <?php echo $send_file; ?> style="cursor: pointer;"></td>
											</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
						 <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="page4-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td> Tgl Mulai</td>
												<td>Nama Pengirim</td>
												<td>Keterangan Pengiriman</td>
												<td> Tgl Berkas Terkirim </td>
												<td> Tgl Berkas Diterima </td>
												<td> Nama Penerima </td>
												<td> Status Permohonan </td>
												<td> Tgl Selesai </td>
												<td> Catatan </td>
											</tr>
											<?php
												if($getDataHistory){
													foreach($getDataHistory as $row){
													$tgl_mulai = '';
													$tgl_berkas_dikirim = '';
													$tgl_diterima = '';
													$tgl_selesai = '';
													$tgl_tolak = '';
													if($row['tgl_mulai'] != '0000-00-00 00:00:00'){
													$original_tgl_mulai =  $row['tgl_mulai'];
													$tgl_mulai = date("d M Y H:i:s", strtotime($original_tgl_mulai));
													}
													if($row['tgl_berkas_dikirim'] != '0000-00-00 00:00:00'){
													$original_tgl_berkas_dikirim =  $row['tgl_berkas_dikirim'];
													$tgl_berkas_dikirim = date("d M Y H:i:s", strtotime($original_tgl_berkas_dikirim));
													}
													if($row['tgl_diterima'] != '0000-00-00 00:00:00'){
													$original_tgl_diterima =  $row['tgl_diterima'];
													$tgl_diterima = date("d M Y H:i:s", strtotime($original_tgl_diterima));
													}
													if($row['tgl_selesai'] != '0000-00-00 00:00:00'){
													$original_tgl_selesai =  $row['tgl_selesai'];
													$tgl_selesai = date("d M Y H:i:s", strtotime($original_tgl_selesai));
													}
													if($row['tgl_tolak'] != '0000-00-00 00:00:00'){
													$original_tgl_tolak =  $row['tgl_tolak'];
													$tgl_tolak = date("d M Y H:i:s", strtotime($original_tgl_tolak));
													}
													$status	= $row['status_approval'];
													if($status == 0){$status_permohonan	= 'On Progress';$finish_date = '';$catatan	= '';}
													else if($status == 1){$status_permohonan	= 'Approval';$finish_date = $tgl_selesai;$catatan	= $row['catatan_approval'];}
													else if($status == 2){$status_permohonan	= 'Reject';$finish_date = $tgl_tolak;$catatan	= $row['catatan_penolakan'];}
											?>
											<tr>
												<td> <?php echo $tgl_mulai; ?> </td>
												<td><?php echo $row['nama_pengirim']; ?></td>
												<td><?php echo $row['keterangan_pengirim']; ?></td>
												<td> <?php echo $tgl_berkas_dikirim; ?> </td>
												<td> <?php echo $tgl_diterima; ?> </td>
												<td> <?php echo $row['nama_penerima']; ?> </td>
												<td> <?php echo $status_permohonan; ?> </td>
												<td> <?php echo $finish_date; ?> </td>
												<td> <?php echo $catatan; ?> </td>
											</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
					 <div class="box-footer">
						<textarea class="resizable_textarea form-control" name="catatan" id="catatan" placeholder="Catatan" style="display:none;"></textarea>
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
 
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}
function sendFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = "<center><img src='"+base_url+'/'+path+"' width='400px' height='auto'></center>";
	$('#box-information').modal("show");
}
function openFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	window.open(base_url+'/'+path);
}
</script>