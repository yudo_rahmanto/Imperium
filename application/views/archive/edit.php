<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession			= $this->session->userdata('user_data');
$UserId_Session					= $dataSession['user_id'];
$LevelId_Session			= $dataSession['level_id'];
$izin_id				= '';
$izin_type_id			= '';

$getAlurIzin			= grabDataAlurIzin($izin_id,$izin_type_id);
$level_id_penerima		= $getAlurIzin[1]['level_id'];
if($LevelId_Session	 == $level_id_penerima){$hideTolak = 'none';} else {$hideTolak = 'block';}

if($getDataPermohonan){
	$dataPermohonan = '';
	foreach($getDataPermohonan[0] as $row_left=>$row_right){
		$dataPermohonan[$row_left] = $row_right;
	}
	$izin_id				= $dataPermohonan['izin_id'];
	$izin_type_id			= $dataPermohonan['izin_type_id'];
}
if($getDataPerusahaan){
	$dataPerusahaan = '';
	foreach($getDataPerusahaan[0] as $row_left=>$row_right){
		$dataPerusahaan[$row_left] = $row_right;
	}
} 
if($getDataUser){
	$dataUser = '';
	foreach($getDataUser[0] as $row_left=>$row_right){
		$dataUser[$row_left] = $row_right;
	}
}

?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWtenJEq5zNO23Y8HOI_gMlhAZ3-7FxC0&callback=initMap"></script>
<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_panel">
                  <div class="x_title">
                    <h2> Validasi FO</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
						<li role="presentation" class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Permohonan</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data User</a>
                        </li>
                        <li role="presentation" class="active"><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Data Perusahaan</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Data Persyaratan</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade" id="tab_content1" aria-labelledby="home-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Izin</td>
												<td width="1%">:</td>
												<td width="79%">
												<input type="hidden" class="form-control" id="permohonan_id" name="permohonan_id" placeholder="Permohonan Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['permohonan_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_id" name="izin_id" placeholder="Izin Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_type_id" name="izin_type_id" placeholder="Izin Type Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_type_id']; } ?>">
												<input type="hidden" class="form-control" id="status_level" name="status_level" placeholder="Status Level" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['status_level']; } ?>">
												<?php if($getDataPermohonan){echo $dataPermohonan['izin_name']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Izin</td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){echo $dataPermohonan['izin_type_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Tanggal Pembuatan </td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){
																																		$originalDate = $dataPermohonan['created_date'];
																																		$date = date("d M Y H:i:s", strtotime($originalDate));
																																		echo $date; 
																																		}
																															?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="page1-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> No KTP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['ktp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No NPWP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['npwp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Full Name</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['first_name'].' '.$dataUser['last_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Email</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['email']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No Tlp</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['no_tlp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['address']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['rt'].' / '.$dataUser['rw']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Provinsi </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['provinsi_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kabupaten </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kabupaten_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kecamatan_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kelurahan_name']; } ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="page2-tab">
                          <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<input type="hidden" class="form-control" id="perusahaan_id" name="perusahaan_id" placeholder="Id Perusahaan" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['perusahaan_id']; } ?>">
													<input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['nama_perusahaan']; } ?>">
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Usaha </td>
												<td width="1%">:</td>
												<td width="79%">
													<select class="form-control select2" id="jenis_usaha" name="jenis_usaha" style="width: 100%;" required="required">
														<option value="">Select</option>
													<?php
														if($getJenisUsaha){
															foreach($getJenisUsaha as $row){
																if($getDataPerusahaan){
																	if($dataPerusahaan['jenis_usaha_id'] == $row['jenis_usaha_id']){$select	= "selected='selected'";} else {$select	= "";}
																}
																
													?>
														<option value="<?php echo $row['jenis_usaha_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
													<?php
															}
														}
													?>
													</select>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lingkungan Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
												<select class="form-control select2" id="lingkungan" name="lingkungan" style="width: 100%;" required="required">
													<option value="">Select</option>
													<?php
														if($getLingkungan){
															foreach($getLingkungan as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['lingkungan_perusahaan_id'] == $row['lingkungan_perusahaan_id']){$select	= "selected='selected'";} else {$select	= "";}
																	}
													?>
														<option value="<?php echo $row['lingkungan_perusahaan_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
													<?php
															}
														}
													?>
												</select>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lokasi Perusahaan</td>
												<td width="1%">:</td>
												<td width="79%">
													<select class="form-control select2" id="lokasi_perusahaan" name="lokasi_perusahaan" style="width: 100%;" required="required">
														<option value="">Select</option>
													<?php
														if($getLokasi){
															foreach($getLokasi as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['lokasi_perusahaan_id'] == $row['lokasi_perusahaan_id']){$select	= "selected='selected'";} else {$select	= "";}
																	}
													?>
														<option value="<?php echo $row['lokasi_perusahaan_id']; ?>" <?php echo $select; ?>><?php echo $row['name']; ?></option>
													<?php
															}
														}
													?>
													</select>
												</td>
											</tr>
											<tr>
												<td width="20%"> Luas </td>
												<td width="1%">:</td>
												<td width="79%">
													<input type="text" class="form-control" id="luas" name="luas" placeholder="Luas Perusahaan" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['luas']; } ?>">
												</td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td>
												<td width="1%">:</td>
												<td width="79%">
													<textarea class="resizable_textarea form-control" name="alamat" id="alamat" placeholder="Alamat Perusahaan" required="required" onkeyup="findLocation()"><?php if($getDataPerusahaan){echo $dataPerusahaan['alamat']; } ?></textarea>
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<div id="map" style="height:300px;"></div>
													<input type="hidden" class="form-control" name="lat" id="lat" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['lat']; } ?>"> 
													<input type="hidden" class="form-control" name="lng" id="lng" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['lng']; } ?>"> 
												</td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td>
												<td width="1%">:</td>
												<td width="79%">
												<input type="text" class="form-control" id="rt" name="rt" placeholder="RT" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['rt']; } ?>"> / 
												<input type="text" class="form-control" id="rw" name="rw" placeholder="RW" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['rw']; } ?>">
												</td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td>
												<td width="1%">:</td>
												<td width="79%">
													<select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" onchange="getData(this.value,'kelurahan')" required="required">
													<?php
														if($getKecamatan){
															foreach($getKecamatan as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['kecamatan_id'] == $row['id']){$select	= "selected='selected'";} else {$select	= "";}
																	}
														?>
														<option value="<?php echo $row['id']; ?>" <?php echo $select;?>><?php echo $row['nama']; ?></option>
														<?php
															}
														}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td>
												<td width="1%">:</td>
												<td width="79%">
													<select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required="required">
													<?php
														if($getKelurahan){
															foreach($getKelurahan as $row){
																if($getDataPerusahaan){
																		if($dataPerusahaan['kelurahan_id'] == $row['id']){$select	= "selected='selected'";} else {$select	= "";}
																	}
														?>
														<option value="<?php echo $row['id']; ?>" <?php echo $select;?>><?php echo $row['nama']; ?></option>
														<?php
															}
														}
														?>
													</select>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="page3-tab">
                         <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="30%"> Persyaratan</td><td width="80%">File</td>
											</tr>
											<?php
												if($getDataPersyaratan){
													foreach($getDataPersyaratan as $row){
														$file_parts = pathinfo($row['path_location']);
														$extension	= $file_parts['extension'];
														if(($extension == 'jpg') | ($extension == 'jpeg') || ($extension == 'png')){
															$send_file	= "onclick=sendFile('".$row['path_location']."')";
															} else {
															$send_file	= "onclick=openFile('".$row['path_location']."')";	
															}
														// echo '<pre>';print_r($file_parts);
											?>
											<tr>
												<td width="80%"> <?php echo $row['syarat_name']; ?></td><td width="20%"><img class="avatar" src="<?php echo base_url('assets/images/file.jpg'); ?>" alt="Avatar" <?php echo $send_file; ?> style="cursor: pointer;"></td>
											</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
					 <div class="box-footer">
						<textarea class="resizable_textarea form-control" name="catatan" id="catatan" placeholder="Catatan" style="display:none;"></textarea>
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Update</button>
						<button type="button" class="btn btn-danger" onclick="noticeBatal()">Batal</button>
						<button type="button" class="btn btn-danger" onclick="noticeTolak()" style="display:<?php echo $hideTolak; ?>;">Tolak</button>
						<button type="button" class="btn btn-success" onclick="noticeSetuju()">Setujui</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
 
<script>

function back(){
	window.location = "<?php echo $back; ?>";
}

function findLocation(){
	var address = document.getElementById('alamat').value;
	if(!address){
			$.ajax({
			  url:"http://maps.googleapis.com/maps/api/geocode/json?address=palembang&sensor=false",
			  type: "POST",
			  success:function(res){
				var lat	= res.results[0].geometry.location.lat;
				var lng	= res.results[0].geometry.location.lng;
				initMap(lat,lng);
				document.getElementById('lat').value = lat;
				document.getElementById('lng').value = lng;
			  }
			});
		} else {
			$.ajax({
			  url:"http://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
			  type: "POST",
			  success:function(res){
				if(res){
				var lat	= res.results[0].geometry.location.lat;
				var lng	= res.results[0].geometry.location.lng;
				initMap(lat,lng);
				document.getElementById('lat').value = lat;
				document.getElementById('lng').value = lng;
				}
			  }
			});
		}
}


function initMap(lat,lng) {
		if(!lat){lat = <?php if($getDataPerusahaan){echo $dataPerusahaan['lat']; } ?>;}
		if(!lng){lng = <?php if($getDataPerusahaan){echo $dataPerusahaan['lng']; } ?>;}
		var marker;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: lat, lng: lng}
        });

        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: {lat: lat, lng: lng}
        });
        marker.addListener('click');
		google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng; 
        currentLatitude = latLng.lat();
        currentLongitude = latLng.lng();
		document.getElementById('lat').value = currentLatitude;
		document.getElementById('lng').value = currentLongitude;
	});
		
	}
function noticeBatal(){
	var html	 = " <p style='margin-top:20px;text-align:left;'>Catatan Pembatalan</p>";
		html	+= " <textarea class='resizable_textarea form-control' name='catatan_temp' id='catatan_temp' placeholder='Catatan'></textarea>";
		html	+= " <div class='modal-footer'>";
		html	+= "	<button data-dismiss='modal' class='btn btn-default' type='button'>Cancel</button>";
		html	+= "	<button class='btn btn-success' type='button' onclick='batal()'>Submit</button>";
		html	+= " </div>";
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = html;
	$('#box-information').modal("show");
}
function noticeTolak(){
	var html	 = " <p style='margin-top:20px;text-align:left;'>Catatan Penolakan</p>";
		html	+= " <textarea class='resizable_textarea form-control' name='catatan_temp' id='catatan_temp' placeholder='Catatan'></textarea>";
		html	+= " <div class='modal-footer'>";
		html	+= "	<button data-dismiss='modal' class='btn btn-default' type='button'>Cancel</button>";
		html	+= "	<button class='btn btn-success' type='button' onclick='tolak()'>Submit</button>";
		html	+= " </div>";
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = html;
	$('#box-information').modal("show");
}
function noticeSetuju(){
	var html	 = " <p style='margin-top:20px;text-align:left;'>Catatan Persetujuan</p>";
		html	+= " <textarea class='resizable_textarea form-control' name='catatan_temp' id='catatan_temp' placeholder='Catatan'></textarea>";
		html	+= " <div class='modal-footer'>";
		html	+= "	<button data-dismiss='modal' class='btn btn-default' type='button'>Cancel</button>";
		html	+= "	<button class='btn btn-success' type='button' onclick='setuju()'>Submit</button>";
		html	+= " </div>";
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = html;
	$('#box-information').modal("show");
}
function sendFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
	document.getElementById('message-text').innerHTML = "<center><img src='"+base_url+'/'+path+"' width='400px' height='auto'></center>";
	$('#box-information').modal("show");
}
function openFile(path){
	var base_url = <?php echo json_encode(base_url()); ?>;
	window.open(base_url+'/'+path);
}
function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = '';
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function checkFile(id){
	var file		= document.getElementById('upload_file'+id).files;
	var name_file 	= file[0]['name'].toLowerCase();
	var size_file	= file[0]['size'];
	var format_default 	= document.getElementById('format_file'+id).value.toLowerCase();
	var size_default 	= document.getElementById('max_size'+id).value;
	var explode_format_file	= format_default.split("|");
	var hasil		= false;
	if(name_file){
		var explode = name_file.split('.');
		var format	= explode[explode.length-1];
		for(var i = 0; i < explode_format_file.length; i++){
			if(explode_format_file[i] == format){
				hasil = true;
			}
			else if(size_file < size_default){
				hasil = true;
			}
		}
		
		if(!hasil){
			alert('Maaf Format file / Size tidak sesuai');
			document.getElementById('upload_file'+id).value = '';
		}
	}
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		
		$.ajax({
			   type: 'post',
			   url: '<?php echo $update; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function batal(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		var catatan_temp	= document.getElementById('catatan_temp').value;
		document.getElementById('catatan').value = catatan_temp;
		$.ajax({
			   type: 'post',
			   url: '<?php echo $batal; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function tolak(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		var catatan_temp	= document.getElementById('catatan_temp').value;
		document.getElementById('catatan').value = catatan_temp;
		$.ajax({
			   type: 'post',
			   url: '<?php echo $tolak; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function setuju(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		var catatan_temp	= document.getElementById('catatan_temp').value;
		document.getElementById('catatan').value = catatan_temp;
		$.ajax({
			   type: 'post',
			   url: '<?php echo $setuju; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
function uploadFile(){
		var doc = document.form;
			doc.action= "<?php echo $uploadFile; ?>";
			doc.submit();
	}
</script>