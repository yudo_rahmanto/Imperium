 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$UserId_Session			= $dataSession['user_id'];
$LevelId_Session						= $dataSession['level_id'];

	$filter			= '';
	if($data_search){
		foreach ($data_search as $row_left=>$row_right){
			$filter[$row_left]			= '';
			if($row_right){
				$filter[$row_left]		= $row_right;
			}
		}
	}
	

?>
<form class="form-signin" name="search" id="search" method="post" enctype="multipart/form-data">	

					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						 <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Berkas Baru</a></li>
							<!--li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Berkas Proses</a></li-->
							<li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Berkas Finish</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
								<table class="table table-striped table-bordered" id="datatableInbox">
								  <thead>
								  <tr>
											<?php	
													if($field_table){
														foreach($field_table as $left=>$right){
															
											?>
											<th align="center"><?php echo ucwords(strtolower($right)); ?> </th>
											<?php
														}
													}
											?>
											<th align="center">Action</th>
								  </tr>
								  </thead>
								  <tbody>
								  <?php
									// debug($getDataInbox);exit;
											if($getDataInbox){
												$i	= 1;
												foreach($getDataInbox as $row2){
												$date = new DateTime($row2['created_date']);
												if($row2['durasi_pekerjaan']){
													$date->modify('+'.$row2['durasi_pekerjaan'].' day');
													$limit_date = $date->format('Y-m-d H:i:s');
													if(strtotime(date('Y-m-d H:i:s')) > strtotime($limit_date)){$color = "red";}
													else if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}
												} else {
													if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}
												}
												$tgl_pendaftaran 	=  indonesia_date($row2['created_date']);
												$tgl_diterima 		=  $row2['tgl_diterima'];
												
									?>
								  <tr class="<?php echo $color; ?>">
										<td><?php echo $tgl_pendaftaran; ?></td>
										<td>
											<input type="checkbox" class="tableflat" name="id<?php echo $i; ?>" id="id<?php echo $i; ?>" value="<?php echo $row2['permohonan_id']; ?>" style="display:none;">
											<?php if( $row2['jenis_perusahaan']){ echo $row2['jenis_perusahaan'].'. '.$row2['nama_perusahaan']; } else { echo $row2['nama_perusahaan'];} ?>
										</td>
										<td><?php echo $row2['izin_name']; ?></td>
										<td><?php echo $row2['izin_type_name']; ?></td>
										<td>
											<?php
												if(!$tgl_diterima){
													echo '<button class="btn btn-round btn-success" type="button" onclick="terimaBerkas('.$row2['permohonan_id'].')">Terima</button>';
												} else {
													echo '<button class="btn btn-round btn-warning" type="button"> <a href="'.$edit."/".$row2['permohonan_id'].'" style="color:white;">Validasi </a></button>';
												}
											?>
											
										</td>
									</tr>
									<?php
												$i++;
												}
											} else {
									?>
									<tr class="">
											<td colspan="5" align="center">No data result(s)</td>
										</tr>
									<?php
											}
									?>
								  </tbody>
							  </table>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="home-tab">
								<table class="table table-striped table-bordered" id="datatableFinish">
								  <thead>
									<tr>
											<?php	
													if($field_table){
														foreach($field_table as $left=>$right){
															
											?>
											<th align="center"><?php echo ucwords(strtolower($right)); ?> </th>
											<?php
														}
													}
											?>
									</tr>
								  </thead>
								  <tbody>
								  <?php
											if($getDataFinish){
												$i	= 1;
												foreach($getDataFinish as $row2){
												if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}
												$tgl_pendaftaran 	=  indonesia_date($row2['created_date']);							
									?>
								  <tr class="">
										<td><?php echo $tgl_pendaftaran; ?></td>
										<td>
											<input type="checkbox" class="tableflat" name="id<?php echo $i; ?>" id="id<?php echo $i; ?>" value="<?php echo $row2['permohonan_id']; ?>" style="display:none;">
											<?php if( $row2['jenis_perusahaan']){ echo $row2['jenis_perusahaan'].'. '.$row2['nama_perusahaan']; } else { echo $row2['nama_perusahaan'];} ?>
										</td>
										<td><?php echo $row2['izin_name']; ?></td>
										<td><?php echo $row2['izin_type_name']; ?></td>
									</tr>
									<?php
												$i++;
												}
											} else {
									?>
									<tr class="">
											<td colspan="5" align="center">No data result(s)</td>
										</tr>
									<?php
											}
									?>
								  </tbody>
							  </table>
							</div>
						</div>
					</div>
					
                   
                 
</form>		
 
<script>
<?php 
if($getDataInbox){ 
?>
$(document).ready(function() {
			$('#datatableInbox').DataTable();
		});
<?php	
}
if($getDataFinish){
?>
$(document).ready(function() {
			$('#datatableFinish').DataTable();
		});
<?php	}	?>		
function find(){
		var doc = document.search;
		doc.action= "<?php echo $page_action; ?>";
		doc.submit();
}
function back(){
	window.location = "<?php echo $back_action; ?>";
}
function parseDataDelete(id){
	var doc = document.search;
	var x;
	var r = confirm("Anda yakin ingin menghapus data ini");
	if (r==true){
		deleteData(id);
	 } else {
		return false;
	}
}
function terimaBerkas(id){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $terim_berkas; ?>',
			   data: {'permohonan_id':id},
			   success: function(response) {
				  if(response == "Ok"){
					    document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Berkas sudah diterima';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000);
				   } else {
					   document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Berkas gagal diterima';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000);
				   }
			   }
	});
}
function deleteData(id){
	if(id){
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $delete; ?>',
			   data: {'id':id},
			   success: function(response) {
				   if(response == 'Success'){
					    document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete Data Success';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000); 
				   }
				    else if(response == 'Failed'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete File Failed';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						}, 2000);
				   }
			   }
		});
	}
}

function create_new(){
	window.location = "<?php echo $add; ?>";
	
}

</script>