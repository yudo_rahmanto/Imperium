<?php

$dataSession			= $this->session->userdata('user_data');
$UserId_Session			= $dataSession['user_id'];
$JabatanId_Session		= $dataSession['jabatan_id'];
$fullname_Session		= $dataSession['first_name'].' '.$dataSession['last_name'];
$LevelId_Session		= $dataSession['level_id'];
$info					= "";
$access					= 0;

if(($JabatanId_Session >= 2) && ($JabatanId_Session <= 5)){
	$this->load->model('M_approval');
	if($parentMenu){
		foreach($parentMenu as $row){
			$sub_menu_id = $row['sub_menu_id'];
			if($sub_menu_id == 41){
				$access++;
			}
		}
	}
	if($access > 0){
	$search					= array(
										"a.activation"		=> 'Y',
										"a.status"			=> 'On Progress',
										"e.jabatan_id"		=> $JabatanId_Session
									);
	$searchOnProgress			= array(
										"a.activation"				=> 'Y',
										"a.jabatan_id_penerima"		=> $JabatanId_Session
									);
	$searchFinish			= array(
										"a.activation"				=> 'Y',
										"a.jabatan_id_penerima"		=> $JabatanId_Session
									);
	$getDataInbox		= $this->M_approval->getInbox($search,'');
	$getDataOnProgress	= $this->M_approval->getOnProgress($searchOnProgress,'');
	$getDataFinish		= $this->M_approval->getFinish($searchFinish,'');
	$countNew			= 0;
	$countOnProgress	= 0;
	$countFinish		= 0;
	if($getDataInbox){
		$countNew				= number_format(count($getDataInbox));
	}
	if($getDataOnProgress){
		$countOnProgress		= number_format(count($getDataOnProgress));
	}
	if($getDataFinish){
		$countFinish			= number_format(count($getDataFinish));
	}
	$info				= '
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Berkas Baru</span>
									  <div class="count">'.$countNew.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Berkas On Progress</span>
									  <div class="count">'.$countOnProgress.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Berkas Finish</span>
									  <div class="count">'.$countFinish.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									';
	} else {
		$search			= array(
										"jabatan_id_penerima"	=> $JabatanId_Session,
										"status_selesai"		=> 0
									);
		$getDataPengaduan		= ManyFilter('m_pengaduan',$search);
		$countPengaduan			= 0;
		if($getDataPengaduan){
			$countPengaduan			= number_format(count($getDataPengaduan));
		}
		$info				= '
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Pengaduan Baru</span>
									  <div class="count">'.$countPengaduan.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									
									';
	}
}
else if($JabatanId_Session == 6){
	$this->load->model('M_finance');
	$search					= array(
										"e.izin_category_id"	=> 2
									);
	$getDataRetribusi		= $this->M_finance->getFinanceRetribusi($search,'');
	$countOnRetribusi		= 0;
	if($getDataRetribusi['data']){
		$countOnRetribusi		= number_format(count($getDataRetribusi['data']));
	}
	$info				= '
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Berkas Retribusi Baru</span>
									  <div class="count">'.$countOnRetribusi.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									';
}
else if($JabatanId_Session == 7){
	$CI 			= & get_instance();
	$CI->load->model('M_permohonan');
	$searchOnProgress				= array(
											"a.activation"			=> 'Y',
											"a.status"				=> 'On Progress',
											"a.user_management_id"	=> $UserId_Session
										);
	$searchFinish				= array(
											"a.activation"			=> 'Y',
											"a.status_sk"			=> 1,
											"a.status"				=> 'Finish',
											"a.user_management_id"	=> $UserId_Session
										);
	$searchReject				= array(
											"a.activation"			=> 'Y',
											"a.reject"				=> 1,
											"a.user_management_id"	=> $UserId_Session
										);
	$getDataOnProgress	= $CI->M_permohonan->getPermohonan($searchOnProgress,'');
	$getDataFinish		= $CI->M_permohonan->getPermohonan($searchFinish,'');
	$getDataReject		= $CI->M_permohonan->getPermohonan($searchReject,'');
	$countOnProgress	= 0;
	$countFinish		= 0;
	$countReject		= 0;
	if($getDataOnProgress['data']){
		$countOnProgress	= number_format(count($getDataOnProgress['data']));
	}
	if($getDataFinish['data']){
		$countFinish		= number_format(count($getDataFinish['data']));
	}
	if($getDataReject['data']){
		$countReject		= number_format(count($getDataReject['data']));
	}
	$info				= '
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Permohonan On Progress</span>
									  <div class="count">'.$countOnProgress.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Permohonan Finish</span>
									  <div class="count">'.$countFinish.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Permohonan Reject</span>
									  <div class="count">'.$countReject.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									';
}
else if($JabatanId_Session == 1){
	if($parentMenu){
		foreach($parentMenu as $row){
			$sub_menu_id = $row['sub_menu_id'];
			if($sub_menu_id != 44){
				$access++;
			}
		}
	}
	// debug($sub_menu_id);exit;
	if($access > 0){
	$searchNewSk					= array(
											"status_finish"		=> 1,
											"status_sk"			=> 0,
											"activation"		=> 'Y'
											);
	$searchAllSk					= array(
											"status_finish"		=> 1,
											"status_sk"			=> 1,
											"activation"		=> 'Y'
											);
	$searchPenolakanSk					= array(
											"reject"			=> 1,
											"activation"		=> 'Y'
											);
	$getDataNewSk				= ManyFilter('m_permohonan',$searchNewSk);
	$getDataAllSk				= ManyFilter('m_permohonan',$searchAllSk);
	$getDataAllPenolakanSk		= ManyFilter('m_permohonan',$searchPenolakanSk);
	$countNewSk				= 0;
	$countAllSk				= 0;
	$countAllPenolakanSk	= 0;
	if($getDataNewSk){
		$countNewSk				= number_format(count($getDataNewSk));
	}
	if($getDataAllSk){
		$countAllSk				= number_format(count($getDataAllSk));
	}
	if($getDataAllPenolakanSk){
		$countAllPenolakanSk	= number_format(count($getDataAllPenolakanSk));
	}
	$info				= '
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Berkas Siap Cetak</span>
									  <div class="count">'.$countNewSk.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Surat / Sk Siap Cetak</span>
									  <div class="count">'.$countAllSk.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Total SK Penolakan</span>
									  <div class="count">'.$countAllPenolakanSk.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									';
	} else {
		$search			= array(
										"jabatan_id_pengirim"	=> $JabatanId_Session,
										"status_selesai"		=> 0
									);
		$getDataPengaduan		= ManyFilter('m_pengaduan',$search);
		$countPengaduan			= 0;
		if($getDataPengaduan){
			$countPengaduan			= number_format(count($getDataPengaduan));
		}
		$info				= '
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Pengaduan Baru</span>
									  <div class="count">'.$countPengaduan.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									
									';
	}
}
else if(($JabatanId_Session == 0) && ($LevelId_Session == 1)){
	
	$getDataPermohonan		= SingleFilter('m_permohonan','reject',0);
	$getDataPerusahaan		= SingleFilter('m_perusahaan','activation','Y');
	$getDataUser			= SingleFilter('m_perusahaan','activation','Y');
	$countPermohonan		= 0;
	$countPerusahaan		= 0;
	$countUser				= 0;
	if($getDataPermohonan){
		$countPermohonan		= number_format(count($getDataPermohonan));
	}
	if($getDataPerusahaan){
		$countPerusahaan		= number_format(count($getDataPerusahaan));
	}
	if($getDataUser){
		$countUser				= number_format(count($getDataUser));
	}
	$info				= '
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Total User</span>
									  <div class="count">'.$countUser.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Total Perusahaan</span>
									  <div class="count">'.$countPerusahaan.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
									  <span class="count_top"><i class="fa  fa-file-o"></i> Total Permohonan</span>
									  <div class="count">'.$countPermohonan.'</div>
									  <span class="count_bottom"><i class="green"></i> </span>
									</div>
									';
}
?>
<!-- page content -->
        <div class="right_col" role="main" style="height:500px;">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4>Dashboard <small></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Dashboard <small></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="row tile_count">
						<?php echo $info; ?>
					  </div>
                  </div>
                </div>
              </div>
				
            </div>
			
			<div class="clearfix"></div>
			<?php
				if(in_array($JabatanId_Session,array(2,3,4,5))){
			?>
			<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div id="getDataInbox" class="x_content" >
                    	
                  </div>
                </div>
              </div>
				
            </div>
			<?php
				}
			?>
          </div>
        </div>
		
        <!-- /page content -->

        <!-- footer content -->
 <script>
$(document).ready(function() {
		$('#datatable').DataTable();
		});
getDataInbox();
function getDataInbox(){
	
	$('#getDataInbox').load('<?php echo base_url('Home/dataInbox'); ?>');
	
}
</script>		