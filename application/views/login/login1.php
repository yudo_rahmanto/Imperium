<body class="login">
	<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
	<?php $this->load->view('common/popup_message'); ?>
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
	<div class="login_wrapper">
        <div class="animate form login_form">
		<div align="center"><img src="<?php echo base_url('assets/images/logo/logo.png'); ?>"></div>
          <section class="login_content">
            <form>
				<h1>Login Form</h1>
              <div>
				<input type="hidden" class="form-control" id="validation_text" value="<?php if($validation){ echo $validation; } ?>">
                <input id="username" name="username" type="text" class="form-control" placeholder="Email / Username / NIK" required=""/>
              </div>
              <div>
                <input id="password" name="password" type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" onclick="login()">Log in</a>
                <a class="reset_pass" href="#">Lupa Password</a>
              </div>

              <div class="clearfix"></div>

				<div class="separator">
					<p class="change_link">
					  <a href="#signup" class="to_register"> Register </a>
					</p>

					<div class="clearfix"></div>
					<br />
				</div>
            </form>
          </section>
        </div>
		
        <div id="register" class="animate form registration_form">
			<div align="center"><img src="<?php echo base_url('assets/images/logo/logo.png'); ?>"></div>
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
				<label style="float:left;">First Name <span class="required">*</span></label>
                <input id="first_name" name="first_name" type="text" class="form-control" placeholder="First Name" required="required" />
              </div>
              <div>
				<label style="float:left;">Last Name <span class="required">*</span></label>
                <input id="last_name" name="last_name" type="text" class="form-control" placeholder="Last Name" required="required" />
              </div>
              <div>
				<label style="float:left;">Email <span class="required">*</span></label>
                <input id="email" name="email" type="text" class="form-control" placeholder="Email" required="required" />
              </div>
			  <div>
				<label style="float:left;">Sandi <span class="required">*</span></label>
                <input id="new_password" name="new_password" type="password" class="form-control" placeholder="Sandi" required="required" />
              </div>
			   <div>
				<label style="float:left;">Konfirmasi Sandi <span class="required">*</span></label>
                <input id="new_password1" name="new_password1" type="password" class="form-control" placeholder="Konfirmasi Sandi" required="required" />
              </div>
              <div>
                <a class="btn btn-default submit" onclick="daftar()" >Daftar</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

               
              </div>
            </form>
          </section>
        </div>
	</div>
    </div>
	</form>
  </body>
   <?php 
		# Create Css #
		if($assets_js){
			foreach($assets_js as $row_left=>$row_right){
				echo "<script src='".$row_right."'></script>";
			}
		}
	?>
</html>

<script>
var alert	= "<?php echo $this->session->flashdata('error'); ?>";
if(alert){
$('#box-alert').modal("show");
	setTimeout(function(){
							 $('#box-alert').modal("hide");
							}, 2000);
}							
$("#username").keyup(function(event){
		if(event.keyCode == 13){
			login();
		}
	});
$("#password").keyup(function(event){
		if(event.keyCode == 13){
			login();
		}
	});
function back(){
	window.location.href = "<?php echo base_url('Login'); ?>";
}
function login(){
	$('#form').parsley().validate();
	var username	= document.getElementById('username').value;
	var password	= document.getElementById('password').value;
	var checkValidasi = validation();
	if(checkValidasi){
		$.ajax({
		   type: 'post',
		   url: "<?php echo $action; ?>",
		   data: {"username":username,"password":password},
		   success: function(response) {
				if(response == 'disallow character'){
				   new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, character is not allowed.",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
				}
			    else if(response == 'Not Success'){
				   new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, login Failed",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
				}
				 else if(response == 'Success'){
					window.location.href = "<?php echo base_url('Home'); ?>";
				}
		   }
		});
	}
}
function daftar(){
	$('#form').parsley().validate();
	var i = 0;
	var first_name		= document.getElementById('first_name').value;
	var last_name		= document.getElementById('last_name').value;
	var email			= document.getElementById('email').value;
	var new_password	= document.getElementById('new_password').value;
	var new_password1	= document.getElementById('new_password1').value;
	
	if(!first_name){i++;}
	if(!last_name){i++;}
	if(!new_password){i++;}
	if(!new_password1){i++;}
	if(!email){i++;}
	if(email){
		var cek_email	= cekEmail();
		if(!cek_email){i++;}
	}
	if((new_password) && (new_password1)){
		var cekPassword	= matchPassword();
		if(!cekPassword){
			new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, your password not match",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
			i++;
			}
	}
	if(i == 0){
			$.ajax({
					   type: 'post',
					   url: "<?php echo $actionRegister; ?>",
					   data: $('form').serialize(),
					   success: function(response) {
							if(response == 'Success'){
								new PNotify({
										title: "Success notice",
										type: "info",
										text: "Register Data Success, please check your email for activation",
										nonblock: {
											nonblock: true
										},
										addclass: 'dark',
										styling: 'bootstrap3',
										delay:1500
									});
									
							} else {
								new PNotify({
										title: "Failure notice",
										type: "info",
										text: "Register Data Failure",
										nonblock: {
											nonblock: true
										},
										addclass: 'dark',
										styling: 'bootstrap3',
										delay:500
									});
							}
					   }
			})
	}
	return false;
	
		
}
function matchPassword(){
	var password	= document.getElementById('new_password').value;
	var password1	= document.getElementById('new_password1').value;
	if(password == password1){
		return true;
	} else {
		return false;
	}
}
function cekEmail(){
	var email = document.getElementById('email').value;
	var atpos 				= email.indexOf("@");
	var dotpos 				= email.lastIndexOf(".");
	var i = 0;
	if((atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)){
			 new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, Email address is not valid",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
			i++;
		} else {
			$.ajax({
			   type: 'post',
			   url: '<?php echo $cekEmail; ?>',
			   data: {'email':email},
			   success: function(response) {
				   if(response == 'Not Ok'){
					    new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, Email address is duplicate",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
						i++;
					}
			   }
			});
		}
		if(i == 0){
			return true;
		} else {
			return false;
		}
}
function validation(){
	var a = 0;
	var validation_text		= $('#validation_text').val();
	var validation_array 	= validation_text.split(',');
	var validasi			= /[^a-zA-Z0-9_@~!$*.#&]/;
	for (i in validation_array ) {
		validation_array[i] = validation_array[i]; // Explicitly include base as per Álvaro's comment
		var validate_value	= $('#'+validation_array[i]).val();
		if(validate_value.match(validasi)){
			// alert("Sorry, your "+validation_array[i]+" character is not allowed. Examples of permitted characters [1-9A-Z!_*,.@#]");
			new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, your "+validation_array[i]+" character is not allowed.",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:500
						});
			a = a + 1;
		}
		else if(!validate_value){
			$('#ket_'+validation_array[i]).html("<font color='red'>This field is required.</font>");
			new PNotify({
							title: "Failure notice",
							type: "info",
							text: "Sorry, "+validation_array[i]+" This field is required",
							nonblock: {
								nonblock: true
							},
							addclass: 'dark',
							styling: 'bootstrap3',
							delay:800
						});
			a = a + 1;
		} 
		
	}
	if(a == 0){return true;} else {return false;}
}
</script>