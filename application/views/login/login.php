<?php
if($mycaptcha){
	$text_captcha	= $mycaptcha['text'];
	$image_captcha	= $mycaptcha['filename']; 	
} else {
	$text_captcha	= '';
	$image_captcha	= ''; 
}
?>

<body class="login-body">

    <div class="container">
		<div align="center" style="margin-top:50px;"><img src="<?php //echo base_url('assets/images/logo/logo.png'); ?>"></div>
      
		<form class="form-signin" name="form_login" id="form_login" method="post" enctype="multipart/form-data">
			<h2 class="form-signin-heading">IMPERIUM v1.0</h2>
			<div class="login-wrap">
			<?php
					$error	= $this->session->flashdata('error');
					$message_error	= $this->alert->notice($error);
					$alert		= "";
					if($error){
						foreach($message_error as $alert_left=>$alert_right){
							if($alert_left == 1){$alert = 'success';}
							if($alert_left == 2){$alert = 'danger';}
				?>
			<div class="box" id="alert">
				<div class="alert alert-<?php echo $alert; ?>">
				  <strong><?php echo ucwords(strtolower($alert))." : "; ?></strong> <?php echo $alert_right; ?>
				</div>
			</div>
				<?php
						}
					}
				?>
				<input type="hidden" class="form-control" id="validation_text" value="<?php if($validation){ echo $validation; } ?>">
				<i id="ket_username"></i>
				<input type="text" name="username" id="username" class="form-control" onkeyup="cekValue('username')" placeholder="User Name / Email / NIK" style="color:#000000;">
				<i id="ket_password"></i>
				<input type="password" name="password" id="password" class="form-control" onkeyup="cekValue('password')" placeholder="Password" style="color:#000000;">
				<i id="ket_captcha_login"></i>
				<img id="img_captcha_login" src="<?php echo base_url('assets/upload/image/captcha/'.$image_captcha); ?>" width="85%"> 
				<img src="<?php echo base_url('assets/images/refresh.png'); ?>" width="30px" height="auto" onclick="refresh_captcha('login')" style="cursor: pointer;">
				<input type="hidden" name="mycaptcha_login" id="mycaptcha_login" class="form-control" placeholder="Captcha" value="<?php echo $text_captcha; ?>">
				<input type="text" name="captcha" id="captcha" class="form-control" placeholder="Captcha" style="color:#000000;">
				
				<label class="checkbox">
                                       					<span class="pull-right">
					  <a data-toggle="modal" href="#myModal"> Forgot Password?</a>
					</span>
				</label>
				<button class="btn btn-lg btn-login btn-block" type="button" id="signin" onclick="login();">Login</button>
				
				<div class="registration">
					Belum terdaftar ? <a data-toggle="modal" href="#myModalRegister"> Silahkan Klik Disini </a>
				</div>

			</div>
		</form>
          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
			<form name="form_reset" id="form_reset" method="post" enctype="multipart/form-data">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="reset_email" id="reset_email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix" style="color:#000000;">
							<center><i id="ket_reset_email"></i></center>
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" type="button" onclick="resetPassword()">Reset</button>
                      </div>
                  </div>
              </div>
			</form>
          </div>
		  
		  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalRegister" class="modal fade">
			<form name="form_register" id="form_register" method="post" enctype="multipart/form-data">
              <div class="modal-dialog">
                  <div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Register</h4>
						</div>
						<div class="modal-body">
							<p style="text-align:left;">No KTP <font color="red">*</font> <i id="ket_ktp"></i></p>
							<input id="ktp" name="ktp" type="text" class="form-control" placeholder="NO KTP" onkeyup="cekTyping('ktp')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">No NPWP</p>
							<input id="npwp" name="npwp" type="text" class="form-control" placeholder="NO NPWP" onkeyup="cekTyping('npwp')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">First Name <font color="red">*</font> <i id="ket_first_name"></i></p>
							<input id="first_name" name="first_name" type="text" class="form-control" placeholder="First Name" onkeyup="cekTyping('first_name')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">Last Name <font color="red">*</font> <i id="ket_last_name"></i></p>
							<input id="last_name" name="last_name" type="text" class="form-control" placeholder="Last Name" onkeyup="cekTyping('last_name')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">Email <font color="red">*</font> <i id="ket_email"></i></p>
							<input id="email" name="email" type="text" class="form-control" placeholder="Email" onkeyup="cekTyping('email')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">No Telepon </p>
							<input id="no_tlp" name="no_tlp" type="text" class="form-control" placeholder="No Telepon" onkeyup="cekTyping('no_tlp')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">Password <font color="red">*</font> <i id="ket_new_password"></i></p>
							<input id="new_password" name="new_password" type="password" class="form-control" placeholder="Sandi" onkeyup="cekTyping('new_password')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">Konfirmasi Password <font color="red">*</font> <i id="ket_new_password1"></i></p>
							<input id="new_password1" name="new_password1" type="password" class="form-control" placeholder="Konfirmasi Sandi" onkeyup="cekTyping('new_password1')" style="color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">Alamat<font color="red">*</font> <i id="ket_alamat"></i></p>
							<textarea class="resizable_textarea form-control" name="alamat" id="alamat" placeholder="Alamat" onkeyup="cekTyping('alamat')" style="color:#000000;"></textarea>
							<p style="margin-top:20px;text-align:left;">RT<font color="red">*</font> <i id="ket_rt"></i></p>
							<input id="rt" name="rt" type="text" class="form-control" placeholder="RT" onkeyup="cekTyping('rt')" style="width:50px;color:#000000;"/>
							<p style="margin-top:20px;text-align:left;">RW<font color="red">*</font> <i id="ket_rw"></i></p>
							<input id="rw" name="rw" type="text" class="form-control" placeholder="RW" onkeyup="cekTyping('rw')" style="width:50px;color:#000000;"/>
							
							<p style="margin-top:20px;text-align:left;">Provinsi <font color="red">*</font> <i id="ket_provinsi"></i></p>
							<select class="form-control select2" id="provinsi" name="provinsi" style="width: 100%;color:black" onchange="getData(this.value,'kabupaten')" >
									<option value="">Select</option>
									<?php
									if($getProvinsi){
										foreach($getProvinsi as $row){
									?>
									<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
									<?php
										}
									}
									?>
							</select>
							<p style="margin-top:20px;text-align:left;">Kabupaten <font color="red">*</font> <i id="ket_kabupaten"></i></p>
							<select class="form-control select2" id="kabupaten" name="kabupaten" style="width: 100%;color:black" onchange="getData(this.value,'kecamatan')" >
								<?php
									if($getKabupaten){
										foreach($getKabupaten as $row){
									?>
									<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
									<?php
										}
									}
									?>
							</select>
							<p style="margin-top:20px;text-align:left;">Kecamatan <font color="red">*</font> <i id="ket_kecamatan"></i></p>
							<select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;color:black" onchange="getData(this.value,'kelurahan')" >
								<?php
									if($getKecamatan){
										foreach($getKecamatan as $row){
									?>
									<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
									<?php
										}
									}
									?>
							</select>
							<p style="margin-top:20px;text-align:left;">Kelurahan <font color="red">*</font> <i id="ket_kelurahan"></i></p>
							<select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;color:black" onchange="cekTyping('kelurahan')">
								<?php
									if($getKelurahan){
										foreach($getKelurahan as $row){
									?>
									<option value="<?php echo $row['id']; ?>"><?php echo $row['nama']; ?></option>
									<?php
										}
									}
									?>
							</select>
							<p style="margin-top:20px;text-align:left;">Captcha<font color="red">*</font> <i id="ket_captcha_register"></i></p>
							<img id="img_captcha_register" src="<?php echo base_url('assets/upload/image/captcha/'.$image_captcha); ?>" width="50%"> <img src="<?php echo base_url('assets/images/refresh.png'); ?>" width="30px" height="auto" onclick="refresh_captcha('register')" style="cursor: pointer;">
							<input type="hidden" name="mycaptcha_register" id="mycaptcha_register" class="form-control" placeholder="Captcha" value="<?php echo $text_captcha; ?>">
							<input type="text" name="captcha_register" id="captcha_register" class="form-control" placeholder="Captcha" style="color:#000000;">
							
						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
							<button class="btn btn-success" type="button" onclick="daftar()">Simpan</button>
						</div>
                  </div>
              </div>
			</form>
          </div>
          <!-- modal -->

    

    </div>

  </body>
   <?php 
		# Create Css #
		if($assets_js){
			foreach($assets_js as $row_left=>$row_right){
				echo "<script src='".$row_right."'></script>";
			}
		}
	?>
</html>

<script>
setTimeout(function(){
	 $("#alert").toggle();
	}, 5000);							
$("#username").keyup(function(event){
		if(event.keyCode == 13){
			login();
		}
	});
$("#password").keyup(function(event){
		if(event.keyCode == 13){
			login();
		}
	});
$("#captcha").keyup(function(event){
		if(event.keyCode == 13){
			login();
		}
	});
function getData(id,find){
	jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $getData; ?>',
			   data: {'find':find,'id':id},
			   dataType: 'json',
			   success: function(response) {
					if(response){
						var html = "<option value=''>Select</option>";
						$.each(response, function (index, data) {
							var id		= data['id'];
							var nama	= data['nama'];
								html	+= "<option value='"+id+"'>"+nama+"</option>";	
						});
						document.getElementById(find).innerHTML = html;
					}
			   }
	});
}
function refresh_captcha(field){
	
	jQuery.ajax({
			   type: 'post',
			   url: "<?php echo $resetCaptcha; ?>",
			   data: {},
			   dataType: 'json',
			   success: function(response) {
				   console.log(response);
					if(response){
						$.each(response, function (index, data) {
							var text_captcha	= data['text'];
							var image_captcha	= data['filename'];
							
							document.getElementById('mycaptcha_'+field).value = text_captcha;
							document.getElementById("img_captcha_"+field).src = "<?php echo base_url(); ?>assets/upload/image/captcha/"+image_captcha;	
							
						});
						
					}
			   }
	});
	
}
function back(){
	window.location.href = "<?php echo base_url('Login'); ?>";
}
function resetPassword(){
		var email	= document.getElementById('reset_email').value;
		var atpos 	= email.indexOf("@");
	    var dotpos 	= email.lastIndexOf(".");
		if(!email){
			document.getElementById('ket_reset_email').innerHTML = '(Sorry, cannot be empty)';
		}
		else if((atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)){
			document.getElementById('ket_reset_email').innerHTML = '(Sorry, Email address is not valid)';
		} else {
			$.ajax({
					type: 'post',
					url: '<?php echo $resetPassword; ?>',
					data:  {'email':email} ,
					success: function(response) {
					  if(response == 'sukses'){
						  document.getElementById('ket_reset_email').innerHTML = "<font color='Greend'>We've send you an email with a link to reset your password</font>";
							setTimeout(function(){
								$('#myModal').modal('hide');
								}, 2000);
						}
						else if(response == 'gagal'){
							document.getElementById('ket_reset_email').innerHTML = "<font color='Red'>Sorry, send you an email failed</font>";
							
						} else {
							document.getElementById('ket_reset_email').innerHTML = "<font color='Red'>Sorry, your email not found</font>";
							
					  }
				   }
				});
		}
	}
function login(){
	var i = 0;
	var username	= document.getElementById('username').value;
	var password	= document.getElementById('password').value;
	var captcha		= document.getElementById('captcha').value;
	var mycaptcha	= document.getElementById('mycaptcha_login').value;
	if(!username){
		i++;
		document.getElementById('ket_username').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		}
	if(!password){
		i++;
		document.getElementById('ket_password').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		}
	if(!captcha){
		i++;
		document.getElementById('ket_captcha_login').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
	}
	if(captcha){
		if(captcha != mycaptcha){
			i++;
			document.getElementById('ket_captcha_login').innerHTML = "<font color='red'>Maaf, Captcha anda salah</red>";
			refresh_captcha('login');
		}
	}
	if(i == 0){
			var doc_login = document.form_login;
			doc_login.action= "<?php echo $action; ?>";
			doc_login.submit();
	}
}
function daftar(){
	var i = 0;
	var ktp				= document.getElementById('ktp').value;
	var npwp			= document.getElementById('npwp').value;
	var first_name		= document.getElementById('first_name').value;
	var last_name		= document.getElementById('last_name').value;
	var email			= document.getElementById('email').value;
	var new_password	= document.getElementById('new_password').value;
	var new_password1	= document.getElementById('new_password1').value;
	var provinsi		= document.getElementById('provinsi').value;
	var kabupaten		= document.getElementById('kabupaten').value;
	var kecamatan		= document.getElementById('kecamatan').value;
	var kelurahan		= document.getElementById('kelurahan').value;
	var rt				= document.getElementById('rt').value;
	var rw				= document.getElementById('rw').value;
	var alamat			= document.getElementById('alamat').value;
	var captcha		= document.getElementById('captcha_register').value;
	var mycaptcha	= document.getElementById('mycaptcha_register').value;
	
	if(!ktp){
		document.getElementById('ket_ktp').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!npwp){
		document.getElementById('ket_npwp').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!first_name){
		document.getElementById('ket_first_name').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!last_name){
		document.getElementById('ket_last_name').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!email){
		document.getElementById('ket_email').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!new_password){
		document.getElementById('ket_new_password').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!new_password1){
		document.getElementById('ket_new_password1').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!provinsi){
		document.getElementById('ket_provinsi').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!kabupaten){
		document.getElementById('ket_kabupaten').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!kecamatan){
		document.getElementById('ket_kecamatan').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!kelurahan){
		document.getElementById('ket_kelurahan').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!rt){
		document.getElementById('ket_rt').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!rw){
		document.getElementById('ket_rw').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!alamat){
		document.getElementById('ket_alamat').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
		i++;
	}
	if(!captcha){
		i++;
		document.getElementById('ket_captcha_login').innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
	}
	if(captcha){
		if(captcha != mycaptcha){
			i++;
			document.getElementById('ket_captcha_register').innerHTML = "<font color='red'>Maaf, Captcha anda salah</red>";
			refresh_captcha('register');
		}
	}
	if(email){
		var cek_email	= cekEmail();
		if(!cek_email){i++;}
	}
	if((new_password) && (new_password1)){
		var cekPassword	= matchPassword();
		if(!cekPassword){
			document.getElementById('ket_password1').innerHTML = "<font color='red'>Maaf, password anda tidak cocok</red>";
			}
	}
	if(i == 0){
			var doc_login = document.form_register;
			doc_login.action= "<?php echo $actionRegister; ?>";
			doc_login.submit();
	}
	return false;
}
function matchPassword(){
	var password	= document.getElementById('new_password').value;
	var password1	= document.getElementById('new_password1').value;
	if(password == password1){
		return true;
	} else {
		return false;
	}
}
function cekEmail(){
	var email = document.getElementById('email').value;
	var atpos 				= email.indexOf("@");
	var dotpos 				= email.lastIndexOf(".");
	var i = 0;
	if((atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)){
			document.getElementById('ket_email').innerHTML = "<font color='red'>Sorry, Email address is not valid</red>";
			i++;
		} else {
			$.ajax({
			   type: 'post',
			   url: '<?php echo $cekEmail; ?>',
			   data: {'email':email},
			   success: function(response) {
				   if(response == 'Not Ok'){
						document.getElementById('ket_email').innerHTML = "<font color='red'>Sorry, Email address is duplicate</red>";
					    i++;
					} else {
						document.getElementById('ket_email').innerHTML = "<font color='silver'>Email already</red>";
					}
			   }
			});
		}
		if(i == 0){
			return true;
		} else {
			return false;
		}
}
function cekTyping(field){
	var value	= document.getElementById(field).value;
	if(field == 'email'){
		cekEmail();
	}
	else if(field == 'ktp'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'npwp'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'rt'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'rw'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(field == 'no_tlp'){
		if(value.match(/[^0-9]/)){
			document.getElementById(field).value = '';
		}
	}
	else if(value){
		document.getElementById('ket_'+field).innerHTML = "";
	} else {
		document.getElementById('ket_'+field).innerHTML = "<font color='red'>Maaf, kolom ini tidak boleh kosong</red>";
	}
}
function cekValue(id){
		var value		= document.getElementById(id).value;
		var validasi	= /[^a-zA-Z0-9_@~!$*.#&]/;
		if(value.match(validasi)){
			alert("Sorry, your "+id+" character is not allowed. Examples of permitted characters [1-9A-Z!_*,.@#]");
			return false;
		}
	}

</script>