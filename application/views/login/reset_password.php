<?php

if($getData){
	$token		= $getData[0]['token'];
	$jabatan_id	= $getData[0]['jabatan_id'];
	$user_id	= $getData[0]['user_id'];
	$name		= $getData[0]['first_name'].' '.$getData[0]['last_name'];
	$email		= $getData[0]['email'];
} else {
	$token		= '';
	$jabatan_id	= '';
	$user_id	= '';
	$name		= '';
	$email		= '';
}
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="Qnock, CMS">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Qnock CMS</title>
	

    
</head>

  <body class="login-body">

    <div class="container">
		<div align="center" style="margin-top:50px;"><img src="<?php echo base_url('assets/images/logo/logo.png'); ?>"></div>
      <form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
        <h2 class="form-signin-heading">Reset Password</h2>
        <div class="login-wrap">
		<?php
				$error	= $this->session->flashdata('error');
				$message_error	= $this->alert->notice($error);
				$alert		= "";
				if($error){
					foreach($message_error as $alert_left=>$alert_right){
						if($alert_left == 1){$alert = 'success';}
						if($alert_left == 2){$alert = 'danger';}
			?>
		<div class="box">
			<div class="alert alert-<?php echo $alert; ?>">
			  <strong><?php echo ucwords(strtolower($alert))." : "; ?></strong> <?php echo $alert_right; ?>
			</div>
		</div>
			<?php
					}
				}
			?>
			<center>Welcome <p><?php echo $name; ?></p> </center>
			<input type="hidden" class="form-control" id="action" value="<?php if($action){echo $action;} ?>">
			<input type="hidden" class="form-control" id="validation_text" value="<?php if($validation){ echo $validation; } ?>">
			<i id="ket_new_password"></i>
			<input type="hidden" class="form-control" name="token" id="token" value="<?php echo $token; ?>">
			<input type="hidden" class="form-control" name="jabatan_id" id="jabatan_id" value="<?php echo $jabatan_id; ?>">
			<input type="hidden" class="form-control" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
            <input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password" onkeyup="cekValue(this.id)" autofocus>
			<i id="ket_new_password1"></i>
            <input type="password" name="new_password1" id="new_password1" class="form-control" placeholder="Rewrite Password" onkeyup="matchPassword()">
           <button class="btn btn-lg btn-login btn-block" type="button" onclick="simpan();">Process</button>
            <!--div class="registration">
                Don't have an account yet?
                <a class="" href="registration.html">
                    Create an account
                </a>
            </div-->

        </div>

         
      </form>

    </div>

  </body>
</html>
<script>
function matchPassword(){
	var password	= document.getElementById('new_password').value;
	var password1	= document.getElementById('new_password1').value;
	if(password == password1){
		document.getElementById('ket_new_password1').innerHTML = '(Your password match)';
	} else {
		document.getElementById('ket_new_password1').innerHTML = '(Sorry, your password not match)';
	}
}
function cekValue(id){
	var value		= document.getElementById(id).value;
	var validasi	= /[^a-zA-Z0-9_@~!$*.#&]/;
	if(value.match(validasi)){
		if(id == 'password'){id = 'Password';}
		if(id == 'password1'){id = 'Re-type Password';}
		alert("Sorry, your "+id+" character is not allowed. Examples of permitted characters [1-9A-Z!_*,.@#]");
		return false;
	}
	
}	
function simpan(){
		var password			= document.getElementById('new_password').value;
		var password1			= document.getElementById('new_password1').value;
		var jml_alert			= 0;
		var validasi			= /[^a-zA-Z0-9_@~!$*.#&]/;
		document.getElementById('ket_new_password').innerHTML = '';
		document.getElementById('ket_new_password1').innerHTML = '';
		if(!password){
			document.getElementById('ket_new_password').innerHTML = "<font color='red'>(Sorry, cannot be empty)</font>";
			jml_alert++;
		}
		else if(!password1){
			document.getElementById('ket_new_password1').innerHTML = "<font color='red'>(Sorry, cannot be empty)</font>";
			jml_alert++;
		}
		else if(password != password1){
			document.getElementById('ket_new_password1').innerHTML = "<font color='red'>(Sorry, your password not match)</font>";
		} 
		else if((password.match(validasi)) || (password1.match(validasi))){
			alert("Sorry, your Password character is not allowed. Examples of permitted characters [1-9A-Z!_*,.@#]");
		} else {
			var doc = document.form;
			var path		= "<?php echo $action; ?>";
			doc.action= path;
			doc.submit();
		}
		if(jml_alert != 0){
		alert("Sorry, all field cannot be empty");return false;
		}
		
	}
</script>