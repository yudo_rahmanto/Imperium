<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$retribusi_id			= $getData[0]['retribusi_id'];
	$permohonan_id			= $getData[0]['permohonan_id'];
	$nama_perusahaan		= $getData[0]['nama_perusahaan'];
	$nama_pemohon			= $getData[0]['first_name'].' '.$getData[0]['last_name'];
	$no_permohonan			= $getData[0]['code'];
	$total_akhir			= $getData[0]['total_akhir'];
} else {
	$retribusi_id			= '';
	$permohonan_id			= '';
	$nama_perusahaan		= '';
	$nama_pemohon			= '';
	$no_permohonan			= '';
	$total_akhir			= '';
}
?>
<form class="form-signin" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1"><b>Code Permohonan</b></label>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="form-group">
							  <label for="exampleInputEmail1">: <?php echo $no_permohonan; ?></label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1"><b>Nama Perusahaan</b></label>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="form-group">
							  <label for="exampleInputEmail1">: <?php echo $nama_perusahaan; ?></label>
							  <input type="hidden" class="form-control" id="retribusi_id" name="retribusi_id" placeholder="retribusi id" required="required" value="<?php echo $retribusi_id; ?>">
							  <input type="hidden" class="form-control" id="permohonan_id" name="permohonan_id" placeholder="retribusi id" required="required" value="<?php echo $permohonan_id; ?>">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1"><b>Nama Pemohon</b></label>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="form-group">
							  <label for="exampleInputEmail1">: <?php echo $nama_pemohon; ?></label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
							  <label for="exampleInputEmail1"><b>Total Retribusi</b></label>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="form-group">
							 <label for="exampleInputEmail1">: Rp <?php echo number_format($total_akhir); ?></label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1"><b>No Referensi</b></label>
							</div>
							<div class="form-group">
							 <label for="exampleInputEmail1"><input type="text" class="form-control" id="no_ref" name="no_ref" placeholder="No Referensi" required="required"></label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail1"><b>Tgl Bayar</b></label>
							</div>
							<div class="form-group">
							 <label for="exampleInputEmail1"><input type="text" class="form-control" id="tgl_bayar" name="tgl_bayar" placeholder="Tanggal Bayar" required="required" style="width:300px;"></label>
							</div>
						</div>
						
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Submit</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
<script>
$('#tgl_bayar').daterangepicker({
  singleDatePicker: true,
  calender_style: "picker_4"
});
function back(){
	window.location = "<?php echo $back; ?>";
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
	$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
</script>