<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$jenis_usaha_id			= $getData[0]['jenis_usaha_id'];
	$jenis_usaha			= $getData[0]['name'];
} else {
	$jenis_usaha_id			= '';
	$jenis_usaha			= '';
}
?>
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_content">
						<div class="col-sm-12">
							<ul class="list-group list-group-unbordered">
								<li class="list-group-item"><b>Jenis Usaha</b><a class="pull-right"><?php echo $jenis_usaha; ?></a></li>
							</ul>
						</div>
					</div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>

  
<script>
function back(){
	window.location = "<?php echo $back; ?>";
}
</script>