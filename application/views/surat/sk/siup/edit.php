<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession			= $this->session->userdata('user_data');
$UserId_Session					= $dataSession['user_id'];
$LevelId_Session			= $dataSession['level_id'];
$permohonan_id			= '';
$izin_id				= '';
$izin_type_id			= '';
$status_level			= '';



if($getDataPermohonan){
	$dataPermohonan = '';
	foreach($getDataPermohonan[0] as $row_left=>$row_right){
		$dataPermohonan[$row_left] = $row_right;
	}
	$permohonan_id			= $dataPermohonan['permohonan_id'];
	$izin_id				= $dataPermohonan['izin_id'];
	$izin_type_id			= $dataPermohonan['izin_type_id'];
	$status_level			= $dataPermohonan['status_level'];
}
if($getDataPerusahaan){
	$dataPerusahaan = '';
	foreach($getDataPerusahaan[0] as $row_left=>$row_right){
		$dataPerusahaan[$row_left] = $row_right;
	}
} 
if($getDataUser){
	$dataUser = '';
	foreach($getDataUser[0] as $row_left=>$row_right){
		$dataUser[$row_left] = $row_right;
	}
}
$getAlurIzin		= grabDataAlurIzin($izin_id,$izin_type_id);
$level_penerima		= $getAlurIzin[$status_level]['jabatan_name'];

?>

<form class="form-permohonan" name="form" id="form" method="post" enctype="multipart/form-data">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  
					<div class="x_panel">
                  <div class="x_content">
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
						<li role="presentation" class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Permohonan</a></li>
                        <li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data User</a></li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Data Perusahaan</a></li>
                        <li role="presentation" class="active"><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Form SK</a></li>
					 </ul>
                      <div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade" id="tab_content1" aria-labelledby="home-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Izin</td>
												<td width="1%">:</td>
												<td width="79%">
												<input type="hidden" class="form-control" id="permohonan_id" name="permohonan_id" placeholder="Permohonan Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['permohonan_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_category_id" name="izin_category_id" placeholder="Izin Category Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_category_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_id" name="izin_id" placeholder="Izin Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_id']; } ?>">
												<input type="hidden" class="form-control" id="izin_type_id" name="izin_type_id" placeholder="Izin Type Id" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['izin_type_id']; } ?>">
												<input type="hidden" class="form-control" id="status_level" name="status_level" placeholder="Status Level" required="required" value="<?php if($getDataPermohonan){echo $dataPermohonan['status_level']; } ?>">
												<?php if($getDataPermohonan){echo $dataPermohonan['izin_name']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Izin</td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){echo $dataPermohonan['izin_type_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Tanggal Pembuatan </td><td width="1%">:</td><td width="79%"><?php if($getDataPermohonan){
																																		$originalDate = $dataPermohonan['created_date'];
																																		$date = date("d M Y H:i:s", strtotime($originalDate));
																																		echo $date; 
																																		}
																															?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="page1-tab">
							<div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<input type="hidden" class="form-control" id="user_management_id" name="user_management_id" placeholder="User Id" required="required" value="<?php if($getDataUser){echo $dataUser['user_management_id']; } ?>">
												<td width="20%"> No KTP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['ktp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No NPWP</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['npwp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Full Name</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['first_name'].' '.$dataUser['last_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Email</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['email']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> No Tlp</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['no_tlp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['address']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['rt'].' / '.$dataUser['rw']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Provinsi </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['provinsi_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kabupaten </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kabupaten_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kecamatan_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['kelurahan_name']; } ?></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="page2-tab">
                          <div class="col-sm-12">
								<div class="form-group">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> Nama Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<input type="hidden" class="form-control" id="alamat_perusahaan_id" name="alamat_perusahaan_id" placeholder="Perusahaan Id" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['alamat_perusahaan_id']; } ?>">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['nama_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Usaha </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['jenis_usaha']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lingkungan Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['lingkungan_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Lokasi Perusahaan</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['lokasi_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Luas </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['luas']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Alamat</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['alamat']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> RT / RW</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['rt']; } ?> / <?php if($getDataPerusahaan){echo $dataPerusahaan['rw']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kecamatan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['kecamatan_name']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kelurahan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['kelurahan_name']; } ?>
												</td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content4" aria-labelledby="page3-tab">
							<div class="col-sm-12">
								<div class="form-group" align="center">
									<table class="table table-striped table-hover table-bordered" id="table_upload_persyaratan">
										<tbody>
											<tr>
												<td width="20%"> NO SK  </td>
												<td width="1%">:</td>
												<td width="79%">AUTOMATIC BY SYSTEM</div>
											</tr>
											<tr>
												<td width="20%"> Nama Perusahaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<input type="hidden" class="form-control" id="perusahaan_id" name="perusahaan_id" placeholder="Perusahaan Id" required="required" value="<?php if($getDataPerusahaan){echo $dataPerusahaan['perusahaan_id']; } ?>">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['nama_perusahaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> ALAMAT PERUSAHAAN</td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['alamat']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> NAMA PEMILIK / PENANGGUNG JAWAB</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['first_name'].' '.$dataUser['last_name']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> ALAMAT PEMILIK / PENANGGUNG JAWAB</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['address']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> NOMOR TELEPON</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['no_tlp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> NOMOR POKOK WAJIB PAJAK (NPWP)</td><td width="1%">:</td><td width="79%"><?php if($getDataUser){echo $dataUser['npwp']; } ?></td>
											</tr>
											<tr>
												<td width="20%"> KEKAYAAN BERSIH PERUSAHAAN (TIDAK TERMASUK TANAH DAN BANGUNAN) </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo 'Rp '.number_format($dataPerusahaan['modal_usaha']); } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> KEGIATAN USAHA  </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['kegiatan_usaha']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Kelembagaan </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['kelembagaan']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Jenis Usaha </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['jenis_usaha']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> BARANG / JASA DAGANGAN UTAMA </td>
												<td width="1%">:</td>
												<td width="79%">
													<?php if($getDataPerusahaan){echo $dataPerusahaan['product']; } ?>
												</td>
											</tr>
											<tr>
												<td width="20%"> Masa Berlaku </td>
												<td width="1%">:</td>
												<td width="36%">
													<p><input type="checkbox" name="permanen" id="permanen" value="1" onclick="checkPermanen()">Permanen</p>
													<input type="text" id="start_date" name="start_date" placeholder="Mulai" required="required" style="width:100px;"> -
													<input type="text" id="end_date" name="end_date" placeholder="Hingga" required="required" style="width:100px;">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
						
                      </div>
                    </div>

                  </div>
                </div>
					 <div class="box-footer">
						<button type="button" class="btn btn-primary" onclick="back()">Back</button>
						<button type="button" class="btn btn-primary" onclick="simpan()">Create SK</button>
					  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>
 
<script>
$(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
$('#start_date').daterangepicker({
  singleDatePicker: true,
   locale: {
			format: 'DD/MM/YYYY'
			},
  calender_style: "picker_4"
});
 $('#end_date').daterangepicker({
  singleDatePicker: true,
   locale: {
			format: 'DD/MM/YYYY'
			},
  calender_style: "picker_4"
});

function back(){
	window.location = "<?php echo $back; ?>";
}
function checkPermanen(){
	var permanen	= document.getElementById('permanen').checked;
	if(permanen){
		document.getElementById('start_date').disabled = true;
		document.getElementById('end_date').disabled = true;
	} else {
		document.getElementById('start_date').disabled = false;
		document.getElementById('end_date').disabled = false;
	}
}
function simpan(){
	var validasi	= $('#form').parsley().validate();
	if(validasi){
		$.ajax({
			   type: 'post',
			   url: '<?php echo $action; ?>',
			   data: $('form').serialize(),
			   success: function(response) {
				  if(response == 'sukses'){
						document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Data berhasil disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						back()
						}, 2000);
					}
					else if(response == 'duplikat'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda sudah ada';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
					  
					} else {
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Maaf, data anda gagal disimpan';
						$('#box-information').modal("show");
						setTimeout(function(){
						$('#box-information').modal("hide");
						}, 2000);
						
				  }
			   }
			});
	}
}
</script>