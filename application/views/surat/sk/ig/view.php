 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dataSession					= $this->session->userdata('user_data');
$UserId_Session			= $dataSession['user_id'];
$LevelId_Session						= $dataSession['level_id'];

	$filter			= '';
	if($data_search){
		foreach ($data_search as $row_left=>$row_right){
			$filter[$row_left]			= '';
			if($row_right){
				$filter[$row_left]		= $row_right;
			}
		}
	}
	

?>
<form class="form-signin" name="search" id="search" method="post" enctype="multipart/form-data">	
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h4><?php echo $path_info; ?> - <small><?php echo $title1; ?></small></h4>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
				  <div class="x_content">
                   <table class="table table-striped table-bordered" id="datatable">
                              <thead>
                              <tr>
										<?php	
												if($field_table){
													foreach($field_table as $left=>$right){
														
										?>
										<th align="center"><?php echo ucwords(strtolower($right)); ?> </th>
										<?php
													}
												}
										?>
										<th align="center">Action</th>
                              </tr>
                              </thead>
                              <tbody>
							  <?php
										if($getData){
											$i	= 1;
											foreach($getData as $row2){
											if(($i+1)%2 == 0) { $color="even"; }else{$color="odd";}	
								?>
                              <tr class="">
								   <td>
									<input type="checkbox" class="tableflat" name="id<?php echo $i; ?>" id="id<?php echo $i; ?>" value="<?php echo $row2['permohonan_id']; ?>" style="display:none;">
									<?php echo $row2['nama_perusahaan']; ?></td>
									<td><?php echo $row2['code']; ?></td>
									<td><?php echo $row2['izin_name']; ?></td>
									<td><?php echo $row2['izin_type_name']; ?></td>
									<td><?php echo $row2['status_permohonan']; ?></td>
									<td>
										<button class="btn btn-round btn-primary" type="button" onclick="location.href='<?php echo $edit.'/'.$row2['permohonan_id']; ?>'">Add SK</button>
									</td>
								</tr>
								<?php
											$i++;
											}
										} else {
								?>
								<tr class="">
										<td colspan="6" align="center">No data result(s)</td>
									</tr>
								<?php
										}
								?>
							  </tbody>
                          </table>
                  </div>
                </div>
              </div>
				
            </div>
          </div>
        </div>
</form>		
 
<script>
<?php if($getData){ ?>
$(document).ready(function() {
		$('#datatable').DataTable();
		});
<?php	}	?>

function back(){
	window.location = "<?php echo $back; ?>";
}
function parseDataDelete(id){
	var doc = document.search;
	var x;
	var r = confirm("Anda yakin ingin menghapus data ini");
	if (r==true){
		deleteData(id);
	 } else {
		return false;
	}
}
function deleteData(id){
	if(id){
		jQuery.ajax({
			   type: 'post',
			   url: '<?php echo $delete; ?>',
			   data: {'id':id},
			   success: function(response) {
				   if(response == 'Success'){
					    document.getElementById('head-box').setAttribute("style", "background-color:blue;color:white;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete Data Success';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						  location.reload();
						}, 2000); 
				   }
				    else if(response == 'Failed'){
						document.getElementById('head-box').setAttribute("style", "background-color:red;color:black;text-align:center;");
						document.getElementById('message-text').innerHTML = 'Delete File Failed';
						$('#box-information').modal("show");
						setTimeout(function(){
						 $('#box-information').modal("hide");
						}, 2000);
				   }
			   }
		});
	}
}

function create_new(){
	window.location = "<?php echo $add; ?>";
	
}

</script>