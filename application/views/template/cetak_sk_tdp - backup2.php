<?php
// echo '<pre>';print_r($getData);exit;
if($getData){
	$isiData	= "";
	foreach($getData[0] as $row_left=>$row_right){
		$isiData[$row_left]	= $row_right;
		if($isiData[$row_left] == "photo_pemohon"){
			$isiData[$row_left]	= substr($row_right,2);
		}
		if($isiData[$row_left] == "izin_name"){
			if(preg_match('/RINGAN/',$row_right)){$isiData['code_izin'] = 'IG.R';}
			if(preg_match('/BERAT/',$row_right)){$isiData['code_izin'] = 'IG.B';}
		}
	}
	if($isiData['izin_category_id'] == 3){
		$catatan_izin	= " <br> DAN UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 25 TAHUN 1992 TENTANG PERKOPERASIAN";
		$no_pengesahan_menkop	= $getData[0]['no_pengesahan_menkop'];
		$hide_no_menkop	= 'block';
	}
	
} 
if($getDataSignature){
	$signature	= $getDataSignature[0]['path_location'];
} else {
	$signature	= '';
}
if($getKepalaDinas){
	$level_id		= $getKepalaDinas[0]['level_id'];
	$level_name 	= '';
	$nip_kepala		= $getKepalaDinas[0]['nip'];
	$nama_kepala	= $getKepalaDinas[0]['fullname_with_gelar'];
	$getLevel		= SingleFilter('m_level','level_id',$level_id);
	if($getLevel){
		$level_name = $getLevel[0]['level_name'];
	}
} else {
	$level_id		= '';
	$level_name 	= '';
	$nip_kepala		= '';
	$nama_kepala	= '';
}
// echo '<pre>';print_r($photo_pemohon);exit;
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(15, 5, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
					<table align="center" style="width:100%;text-align:center;font-size: 10px;">
						<tr>
							<td style="height:150px;">&nbsp;</td>
						</tr>
						<tr>
							<td style="font-size: 12px;"><b>TANDA DAFTAR PERUSAHAAN</b></td>
						</tr>
						<tr>
							<td style="font-size: 10px;height:30px;"><b>'.str_replace("TANDA DAFTAR PERUSAHAAN","",$isiData['keterangan_izin']).'</b></td>
						</tr>
						<tr>
							<td style="height:30px;">
								BERDASARKAN UNDANG-UNDANG NOMOR 3 TAHUN 1982 TENTANG WAJIB DAFTAR PERUSAHAAN 
								'.$catatan_izin.'
							</td>
						</tr>
					</table>
					<table align="center" style="margin-bottom:20px;width:100%;text-align:center;vertical-align: middle;font-size: 10px;">
						<tr>
							<td style="width:25%;height:30px;border: 1px solid black;"><p><b>NOMOR TDP</b></p><p>'.$isiData['no_sk'].'</p></td>
							<td style="width:25%;height:30px;border: 1px solid black;"><p><b>BERLAKU S/D TANGGAL</b></p>
																						<p>'.$isiData['periode'].'</p>
																						</td>
							<td style="width:25%;height:30px;border: 1px solid black;"><p><br><b>'.$isiData['d1'].'</b></p></td>
							<td style="width:25%;height:30px;border: 1px solid black;"><p><br><b>'.$isiData['d2'].'</b></p></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table id="data_detail" align="center" style="margin-top:20px;width:100%;text-align:left;font-size: 10px;border: 1px solid black;">
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td width="30%" align="left" style="height:30px;">NAMA PERUSAHAAN</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td colspan="5" align="left" width="67%" style="height:30px;"><b>'.$isiData['nama_perusahaan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td width="30%" align="left" style="height:30px;">STATUS</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left" colspan="5" style="height:30px;"><b>'.$isiData['status'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td width="30%" align="left" style="height:30px;">ALAMAT PERUSAHAAN</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left"colspan="5" style="height:30px;"><b>'.$isiData['alamat_perusahaan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td align="left" width="30%" style="height:30px;">NOMOR TELEPON</td>
										<td align="left" width="3%" style="height:30px;"> : </td>
										<td align="left" width="25%" style="height:30px;"><b>'.$isiData['no_tlp_perusahaan'].'</b></td>
										<td align="left" width="5%" style="height:30px;">&nbsp;</td>
										<td align="left" width="10%" style="height:30px;">FAX</td>
										<td align="left" width="3%" style="height:30px;">:</td>
										<td align="left" width="21%" style="height:30px;">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td width="30%" align="left" style="height:30px;">PENANGGUNG JAWAB / PENGURUS</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left" colspan="5" style="height:30px;"><b>'.$isiData['first_name'].' '.$isiData['last_name'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%">
								<table width="100%">
									<tr>
										<td width="30%" align="left" style="height:30px;">KEGIATAN USAHA POKOK</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left" colspan="5" style="height:30px;"><b>'.$isiData['kegiatan_usaha_perusahaan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="height:30px;">
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td width="30%" align="left">[KLUI]</td>
										<td width="3%" align="left"> : </td>
										<td width="67%" align="left" colspan="5"><b>['.$isiData['klui'].']</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="height:30px;display:'.$hide_no_menkop.';	">
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td align="left" colspan="7">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="height:30px;display:'.$hide_no_menkop.';">
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td align="left" colspan="7">PENGESAHAN MENTERI KOPERASI</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="display:'.$hide_no_menkop.';">
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%">
									<tr>
										<td align="left" width="30%" style="height:30px;display:'.$hide_no_menkop.';">NOMOR</td>
										<td align="left" width="3%" style="height:30px;display:'.$hide_no_menkop.';"> : </td>
										<td align="left" width="25%" style="height:30px;display:'.$hide_no_menkop.';"><b>'.$no_pengesahan_menkop.'</b></td>
										<td align="left" width="5%" style="height:30px;display:'.$hide_no_menkop.';">&nbsp;</td>
										<td align="left" width="10%" style="height:30px;display:'.$hide_no_menkop.';">TANGGAL</td>
										<td align="left" width="3%" style="height:30px;display:'.$hide_no_menkop.';">:</td>
										<td align="left" width="21%" style="height:30px;display:'.$hide_no_menkop.';"><b>'.$isiData['tgl_pengesahan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" style="width:100%;text-align:center;font-size: 10px;">
						<tr>
							<td><img src="'.base_url($isiData['qr_code_sk']).'" width="60px" valign="bottom" style="float:right;"> </td>
							<td width="30%">&nbsp;</td>
							<td width="40%">
								<p> Palembang, '.str_replace(' 00:00:00','',indonesia_date(date('d M Y'))).'</p>
								<p> a.n. WALIKOTA PALEMBANG </p>
								<p> KEPALA DINAS PENANAMAN MODAL </p>
								<p> DAN PELAYANAN TERPADU SATU PINTU</p>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><img src="'.base_url($signature).'" width="80px" height="80px"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
								<p><b> '.$nama_kepala.' </b></p>
								<p> '.$level_name.' </p>
								<p> NIP. '.$nip_kepala.' </p>
							</td>
						</tr>
						
					</table>
					
					';
// echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$html	= '';
$html	.= '
			<table align="center" style="width:100%;text-align:center;font-size: 10px;">
				<tr>
					<td>
						<p><b> KETENTUAN IJIN USAHA PERDAGANGAN </b></p>
					</td>
				</tr>
			</table>
			<table align="center" style="width:100%;text-align:left;font-size: 10px;">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td colspan="4" align="left" style="height:30px;">Pemegang Ijin Usaha Perdagangan mempunyai kewajiban mematuhi ketentuan, antara lain sebagai berikut :</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;"></td>
								<td align="left" style="width:3%;height:30px;"><b>A.</b></td>
								<td colspan="2" align="left" style="height:30px;"><b>Ketentuan Tertib Bangunan</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">1.</td>
								<td align="left" style="width:91%;height:30px;">Memiliki Ijin Medirikan Bangunan (IMB)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">2.</td>
								<td align="left" style="height:30px;">Memiliki Ijin Penggunaan Bangunan (IPB) / Sertifikat Laik Fungsi (SLF)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">3.</td>
								<td align="left" style="height:30px;">Memiliki Surat Ijin Gangguan (IG)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;"></td>
								<td align="left" style="width:3%;height:30px;"><b>B.</b></td>
								<td colspan="2" align="left" style="height:30px;"><b>Ketentuan Arsitek Bangunan</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">1.</td>
								<td align="left" style="width:91%;height:30px;">Bangunan atau bagian bangunan yang mengalami perubahan bentuk dan ukuran bangunan yang sudah ada wajib mendapatkan Ijin Walikota  Kota Palembang (Pasal 2 Perda No. 5 Tahun 2010)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">2.</td>
								<td align="left" style="height:30px;">Menjaga keamanan dan Keselamatan bangunan dan lingkungannya serta tidak boleh mengganggu ketentraman dan keselamatan masyarakat (Pasal 8 Ayat (1) Perda No. 5 Tahun 2010)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">3.</td>
								<td align="left" style="height:30px;">Mematuhi ketentuan fungsi bangunan gedung yang dapat dibangun pada lokasi, ketinggian maksimum bangunan gedung, jumlah lantai/lapis bangunan gedung dibawah permukaan tanah, Garis Sempadan dan jarak bebas minimum gedung, KDB, KLB, KDH KTB maksimum yang diijinkan dan jaringan utilitas kota  (Pasal 12 ayat (2) Perda No. 5 Tahun 2010)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;"></td>
								<td align="left" style="width:3%;height:30px;"><b>C.</b></td>
								<td colspan="2" align="left" style="height:30px;"><b>Ketentuan Keselamatan, Kesehatan dan Keserasian Lingkungan</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">1.</td>
								<td align="left" style="width:91%;height:30px;">Mematuhi Ketentuan Undang-undang No. 1 Tahun 1970 Tentang Keselamatan Kerja</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">2.</td>
								<td align="left" style="height:30px;">
									Setiap Bangunan bukan rumah tinggal dengan luas bangunan lebih dari 300 m dan atau dengan ketinggian lebih dari 2 lantai
									harus dilaksanakan oleh konsultan perencana, pelaksana dan pengawas bangunan (Pasal 13 Perda No. 5 Tahun 2010)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">3.</td>
								<td align="left" style="height:30px;">
									Setiap bangunan tidak boleh menggangu kelancaran arus lalu lintas kendaraan, orang dan barang, tidak menggangu dan
									merusak sarana kota maupun prasarana jaringan kota serta tetap memperhatikan keserasian dan arsitektur lingkungan (Pasal 16
									Perda No. 5 Tahun 2010)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">4.</td>
								<td align="left" style="height:30px;">
									Secara periodik setiap bulannya memeriksakan dan menguji alat pemadam kebakaran yang dimilikinya ke Dinas Penanggulangan Bahaya Kebakaran (Pasal 2 Perda No. 31 Tahun 2011)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">5.</td>
								<td align="left" style="height:30px;">
									Menyiapkan kotak sampah dan wajib mengijinkan petugas Pemerintah Daerah untuk memasuki pekarangan,  rumah dan persilnya untuk kepentingan umum (Pasal 3  Perda No. 3 tahun 1981 jo Perda No. 8 Tahun 1987)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">6.</td>
								<td align="left" style="height:30px;">
									Dilarang menebang / merusak pohon-pohon pelindung dan tanaman yang tumbuh disepanjang jalan jalur hijau (Perda No. 44 Tahun 2002)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">7.</td>
								<td align="left" style="height:30px;">
									Memelihara dengan baik dan bersih persilnya dan segala sesuatu dalam persil, memagar, mengecat dan mengapur setiap awal bulan Juni (Pasal 2 Perda No. 3 Tahun 1981 jo Perda No. 8 Tahun 1987)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">8.</td>
								<td align="left" style="height:30px;">
									Menjamin keamanan , ketertiban, kebersihan, kesehatan lingkungan tempat usahanya (Pasal 9 Perda No. 4 Tahun 2002)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">&nbsp;</td>
								<td align="left" style="height:30px;">9.</td>
								<td align="left" style="height:30px;">
									Memenuhi persyaratan untuk mengajukan Ijin Usaha Perdagangan  (Perda No. 19 Tahun 2011)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;"></td>
								<td align="left" style="width:3%;height:30px;"><b>D.</b></td>
								<td colspan="2" align="left" style="height:30px;"><b>Ketentuan Daftar Ulang</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:91%;height:30px;">Ijin Usaha Perdagangan berlaku untuk jangka waktu selama 5 (lima) tahun dengan ketentuan apabila telah habis masa berlakunya, harus melaksanakan daftar ulang (Pasal 15 Perda No. 19 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;"></td>
								<td align="left" style="width:3%;height:30px;"><b>E.</b></td>
								<td colspan="2" align="left" style="height:30px;"><b>Retribusi</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:91%;height:30px;">Perijinan ini tidak dikenakan biaya retribusi (Rp.0,-) berdasarkan UU No.28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah.</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;"></td>
								<td align="left" style="width:3%;height:30px;"><b>F.</b></td>
								<td colspan="2" align="left" style="height:30px;"><b>Ketentuan Larangan</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">1.</td>
								<td align="left" style="width:91%;height:30px;">Tidak memenuhi ketentuan sebagaimana diatur dalam Peraturan Daerah (Perda No. 19 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">2.</td>
								<td align="left" style="width:91%;height:30px;">Tidak memenuhi kewajiban untuk mendaftar ulang  (Perda No. 19 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;"></td>
								<td align="left" style="width:3%;height:30px;"><b>G.</b></td>
								<td colspan="2" align="left" style="height:30px;"><b>Ketentuan Perubahan, Penghapusan dan Penggantian</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">1.</td>
								<td align="left" style="width:91%;height:30px;">Perubahan perusahaan adalah perubahan yang meliputi perubahan nama perusahaan, bentuk perusahaan, alamat kantor perusahaan, nama pemilik  / penanggung jawab perusahaan, alamat pemilik / penanggung jawab perusahaan, NPWP, modal dan kekayaan bersih (Netto), bidang uasaha dan jenis barang / jasa dagangan utama.</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">2.</td>
								<td align="left" style="width:91%;height:30px;">Penghapusan SIUP terjadi apabila, perubahan bentuk perusahaan, pembubaran perusahaan, perusahaan menghentikan segala kegiatan usahanya, perusahaan tersebut terhenti pada waktu pendiriannya, kadaluarsa atau berakhirnya dan tidak diperpanjang, atau</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">3.</td>
								<td align="left" style="width:91%;height:30px;">Perusahaan tersebut dihentikan segala kegiatan usahanya berdasarkan keputusan pengadilan negeri yang mempunyai kekuatan hukum yang tetap.</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">&nbsp;</td>
								<td align="left" style="width:3%;height:30px;">4.</td>
								<td align="left" style="width:91%;height:50px;">Apabila SIUP yang telah dimiliki oleh perusahaan usaha perdagangan hilang atau rusak sehingga tidak terbaca, penanggung jawab atau pengusaha dapat mengajukan permohonan atas penggantian SIUP baru kepada Walikota dengan melampirkan Surat Keterangan hilang dari Kepolisian (Pasal 23 Perda No. 19 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" colspan="4" style="height:50px;">Ijin Usaha Perdagangan pada keputusan ini, akan dicabut dan dinyatakan tidak berlaku apabila pemegang ijin melakukan perubahan kegiatan pokok, perluasan tempat usaha, pindah lokasi dan memindah tangankan ijin usahanya dan tidak melaksanakan kewajiban pada ketentuan keputusan ini.</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			';
$pdf->writeHTML($html, true, false, true, false, '');
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_end_clean();
$pdf->Output('SK TDP.pdf', 'I');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+