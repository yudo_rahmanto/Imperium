<?php
echo '<pre>';print_r($getData);exit;
if($getData){
	$isiData	= "";
	foreach($getData[0] as $row_left=>$row_right){
		$isiData[$row_left]	= $row_right;
		if($isiData[$row_left] == "photo_pemohon"){
			$isiData[$row_left]	= substr($row_right,2);
		}
		if($isiData[$row_left] == "izin_name"){
			if(preg_match('/RINGAN/',$row_right)){$isiData['code_izin'] = 'IG.R';}
			if(preg_match('/BERAT/',$row_right)){$isiData['code_izin'] = 'IG.B';}
		}
	}
} 
if($getDataSignature){
	$signature	= $getDataSignature[0]['path_location'];
} else {
	$signature	= '';
}
if($getKepalaDinas){
	$level_id		= $getKepalaDinas[0]['level_id'];
	$level_name 	= '';
	$nip_kepala		= $getKepalaDinas[0]['nip'];
	$nama_kepala	= $getKepalaDinas[0]['fullname_with_gelar'];
	$getLevel		= SingleFilter('m_level','level_id',$level_id);
	if($getLevel){
		$level_name = $getLevel[0]['level_name'];
	}
} else {
	$level_id		= '';
	$level_name 	= '';
	$nip_kepala		= '';
	$nama_kepala	= '';
}
// echo '<pre>';print_r($photo_pemohon);exit;
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(15, 5, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 10);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
					<table align="center" style="margin-bottom:20px;width:100%;text-align:center;font-size: 15px;">
						<tr>
							<td style="height:150px;">&nbsp;</td>
						</tr>
						<tr>
							<td><b>SURAT IJIN WALIKOTA PALEMBANG</b></td>
						</tr>
						<tr>
							<td style="height:30px;font-size: 10px;">NOMOR 503/'.$isiData['izin_id'].'/'.$isiData['no_sk'].'/BPM-PTSP/'.date('Y').'</td>
						</tr>
						<tr>
							<td style="height:30px;font-size: 10px;">TENTANG <br>IJIN GANGGUAN</td>
						</tr>
					</table>
					<table style="width:100%;font-size: 8px;">
						<tr>
							<td align="center" colspan="3" style="height:30px;font-size: 10px;">DASAR</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">a.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Undang – Undang Gangguan (Hinder Ordonnantie) Stbl Tahun 1926 Nomor 226 diubah dan ditambah dengan Stbl Tahun 1940 Nomor 14 dan Nomor 450
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">b.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Undang-undang Nomor 28 Tahun 1959  tentang Pembentukan Pemerintahan Daerah Tingkat II dan Kotapraja di Sumatra Selatan (Lembaran Negara RI Tahun 1959 Nomor 73, Tambahan lembaran Negara RI Nomor 1821)
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">c.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Undang – Undang Nomor 25 Tahun 2007 tentang Penanaman Modal (Lembaran Negara Republik Indonesia Nomor 1821)
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">d.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Undang-Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah (Lembaran Negara RI Tahun 2009 Nomor130, Tambahan Lembaran Negara RI Nomor 5049)
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">e.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Peraturan Menteri Dalam Negeri Nomor 27 Tahun 2009 tentang Pedoman Penetapan Ijin Gangguan
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">f.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010 tentang Ijin Mendirikan Bangunan (Lembaran Daerah Kota Palembang Tahun 2010 Nomor 5)
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">g.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Peraturan Daerah Kota Palembang Nomor 3 Tahun 2013 tentang izin Lingkungan (Lembaran Daerah Kota Palembang Tahun 2013 Nomor 3)
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">h.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Peraturan Daerah Kota Palembang Nomor 18 Tahun 2011 tentang Pembinaan dan Retribusi Ijin Gangguan (Lembaran Daerah Kota Palembang Tahun 2011 Nomor 18 Seri C)
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;height:10px;">i.</td>
							<td colspan="2" align="left" style="width:97%;height:10px;">
								Peraturan Daerah Kota Palembang Nomor 19 Tahun 2011 tentang Pembinaan di Bidang Industri dan Usaha Perdagangan (Lembaran Daerah Kota Palembang Tahun 2011 Nomor 19 Seri E)
							</td>
						</tr>
						<tr>
							<td align="left" style="width:3%;">j.</td>
							<td colspan="2" align="left" style="width:97%;">
								<table width="100%">
									<tr>
										<td colspan="3">Memperhatikan :</td>
									</tr>
									<tr>
										<td width="3%" style="height:10px;">&nbsp;</td>
										<td width="5%" style="height:10px;">a.</td>
										<td width="92%" style="height:10px;">Surat Permohonan Ijin Gangguan Saudara/i '.$isiData['first_name'].' '.$isiData['last_name'].'</td>
									</tr>
									<tr>
										<td style="height:10px;">&nbsp;</td>
										<td style="height:10px;">b.</td>
										<td style="height:10px;">Hasil penelitian lapangan dan rekomendasi Tim Teknis Badan Penanaman Modal Dan Pelayanan Terpadu Satu Pintu Kota Palembang yang dituangkan dalam Berita Acara Pemeriksaan</td>
									</tr>
									<tr>
										<td style="height:10px;">&nbsp;</td>
										<td style="height:10px;">c.</td>
										<td style="height:30px;">Bahwa berdasarkan pertimbangan sebagaimana tersebut di atas kepada pemohon dapat diberikan Surat Ijin Gangguan yang ditetapkan dengan Surat Ijin Walikota Palembang.</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>	
					<table align="center" style="width:100%;text-align:left;font-size: 8px;">
						<tr>
							<td colspan="3" align="center" style="height:30px;">MENGIJINKAN KEPADA </td>
						</tr>
						<tr>
							<td align="left" style="height:10px;width:30%;">Nama Pemilik / Penanggung Jawab</td>
							<td align="left" style="height:10px;width:3%;"> : </td>
							<td align="left" style="height:10px;width:67%;"><b>'.strtoupper($isiData['first_name'].' '.$isiData['last_name']).'</b></td>
						</tr>
						<tr>
							<td align="left">Alamat Pemilik/ Penanggung Jawab</td>
							<td align="left"> : </td>
							<td align="left"><b>'.strtoupper($isiData['alamat_user']).' RT. '.$isiData['rt_user'].' RW. '.$isiData['rw_user'].' KEL. '.$isiData['kelurahan_user'].' KEC. '.$isiData['kecamatan_user'].' '.$isiData['kabupaten_user'].' - '.$isiData['provinsi_user'].'</b></td>
						</tr>
						<tr>
							<td colspan="3" style="height:20px;">&nbsp; </td>
						</tr>
						<tr>
							<td colspan="3" align="center" style="height:30px;">UNTUK </td>
						</tr>
						<tr>
							<td colspan="3" align="left" style="height:30px;">Mendirikan Tempat Usaha, dengan data sebagai berikut :</td>
						</tr>
						<tr>
							<td align="left" style="height:10px;">1.	Nama Perusahaan</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;"><b>'.strtoupper($isiData['type_perusahaan'].'. '.$isiData['nama_perusahaan']).'</b></td>
						</tr>
						<tr>
							<td align="left" style="height:10px;">2.	Alamat Perusahaan</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;"><b>'.strtoupper($isiData['alamat_perusahaan']).' RT. '.$isiData['rt_perusahaan'].' RW. '.$isiData['rw_perusahaan'].' KEL. '.$isiData['kelurahan_perusahaan'].' KEC. '.$isiData['kecamatan_perusahaan'].' '.$isiData['kabupaten_perusahaan'].' - '.$isiData['provinsi_perusahaan'].'</b></td>
						</tr>
						<tr>
							<td align="left" style="height:10px;">3.	Jenis Usaha</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;"><b>'.strtoupper($isiData['jenis_usaha']).'</b></td>
						</tr>
						<tr>
							<td align="left" style="height:10px;">4.	Luas Tempat Usaha</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;"><b>'.$isiData['luas_perusahaan'].' M</b></td>
						</tr>
						<tr>
							<td align="left" style="height:10px;">5.	Berlaku s.d tanggal</td>
							<td align="left" style="height:10px;"style="height:10px;"> : </td>
							<td align="left" style="height:30px;"><b>'.$isiData['periode'].'</b></td>
						</tr>
					<table>
					<table align="center" style="width:100%;text-align:left;font-size: 8px;">
						<tr>
							<td width="100%">
								<table width="100%">
									<tr>
										<td align="left" colspan="3" style="height:10px;">Surat Ijin Gangguan ini berlaku sejak tanggal ditetapkan, dengan ketentuan sbb:</td>
									</tr>
									<tr>
										<td align="left" width="3%" style="height:10px;">&nbsp;</td>
										<td align="left" width="5%" style="height:10px;">1.</td>
										<td align="left" width="92%" style="height:10px;">Pemegang Ijin wajib mentaati peraturan perundang-undangan yang berlaku</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">2.</td>
										<td align="left" style="height:10px;">Harus memasang Plat Nomor dan Petikan Surat Ijin Gangguan pada dinding depan yang mudah dibaca</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">3.</td>
										<td align="left" style="height:10px;">Dalam pelaksanaan teknis pemasangan, pihak penyelenggara harus selalu menjaga keindahan, kebersihan dan ketertiban umum</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">4.</td>
										<td align="left" style="height:10px;">Surat Ijin Gangguan berlaku selama $lama_berlaku dan diwajibkan mendaftar ulang</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">5.</td>
										<td align="left" style="height:40px;">Surat Ijin Gangguan akan ditinjau kembali dan disempurnakan sebagaimana mestinya, apabila dikemudian hari ternyata terdapat kekeliruan dalam penetapannya.</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table align="left" style="width:100%;text-align:center;font-size: 10px;">
						<tr>
							<td width="30%"><img src="'.base_url($isiData['qr_code_sk']).'" width="60px" valign="bottom" style="float:right;"> </td>
							<td width="20%">&nbsp;</td>
							<td width="50%">
								<table width="100%">
									<tr>
										<td width="10%">&nbsp;</td><td colspan="2">Palembang, '.str_replace(' 00:00:00','',indonesia_date(date('d F Y'))).'</td>
									</tr>
									<tr>
										<td width="10%"> a.n. </td><td colspan="2">WALIKOTA PALEMBANG </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td colspan="2"> KEPALA DINAS PENANAMAN MODAL </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td colspan="2"> DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="30%">&nbsp;</td>
							<td width="30%">&nbsp;</td>
							<td width="40%" align="left"><img src="'.base_url($signature).'" width="80px" height="80px"></td>
						</tr>
						<tr>
							<td width="30%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
							<td width="50%">
								<table width="100%">
									<tr>
										<td width="10%">&nbsp;</td><td><b> '.$nama_kepala.' </b></td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td> '.$level_name.' </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td> NIP. '.$nip_kepala.' </td>
									</tr>
								</table>
							</td>
						</tr>
						
					</table>
					
					';
// echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$html	= '';
$html	.= '
			<table align="center" style="width:100%;text-align:center;font-size: 10px;">
				<tr>
					<td style="height:30px;">
						<p><b> KETENTUAN IJIN GANGGUAN </b></p>
					</td>
				</tr>
			</table>
			<table align="center" style="width:100%;text-align:left;font-size: 10px;">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td colspan="4" align="left" style="height:30px;">Pemegang Ijin Gangguan pada Surat Ijin ini mempunyai kewajiban mematuhi Ketentuan-ketentuan sebagai berikut :</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>A.</b></td>
								<td colspan="2" align="left" style="height:10px;"><b>Ruangan Dan Lingkungan</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;">Memiliki Ijin Medirikan Bangunan (IMB)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;">Memiliki Ijin Penggunaan Bangunan (IPB) / Sertifikat Laik Fungsi (SLF)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:10px;">Setiap memperluas dan merubah bangunan dan ruangan tempat usahanya harus mendapatkan ijin Walikota Palembang (Pasal 4 Perda No. 18 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">4.</td>
								<td align="left" style="height:10px;">Memasang plat nomor dan petikan Ijin Gangguan pada dinding yang mudah dibaca (Pasal 7 Perda No. 18 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">5.</td>
								<td align="left" style="height:10px;">Memasang nama perusahaan / merk usahanya</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">6.</td>
								<td align="left" style="height:10px;">Menjamin tempat usaha dalam keadaan bersih, rapi, dan indah</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">7.</td>
								<td align="left" style="height:10px;">Menjaga kebersihan got dan saluran pembuangan air</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">8.</td>
								<td align="left" style="height:10px;">Memelihara dengan baik dan bersih persilnya serta segala sesuatu dalam persilnya, termasuk memagar, mengecat dan mengapur setiap awal bulan Juni (pasal 2 Perda No. 3 Tahun 1981 jo. Perda No. 8 tahun 1987)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">9.</td>
								<td align="left" style="height:10px;">Menyiapkan kotak sampah dan wajib mengijinkan petugas Pemerintah Daerah untuk memasuki pekarangan rumah dan persilnya untuk kepentingan umum (pasal 3 Perda No. 3 Tahun 1981 jo. Perda No. 8 tahun 1987)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">10.</td>
								<td align="left" style="height:10px;">Dilarang menebang / merusak pohon-pohon pelindung dan tanaman yang tumbuh disepanjang jalan jalur hijau (Perda No. 44 Tahun 2002)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">11.</td>
								<td align="left" style="height:10px;">Bangunan atau bagian bangunan yang mengalami perubahan bentuk dan ukuran bangunan yang sudah ada wajib mendapatkan Ijin Walikota  Kota Palembang (Pasal 2 Perda No. 5 Tahun 2010)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">12.</td>
								<td align="left" style="height:10px;">Bangunan yang ada dalam lingkungan yang mengalami perubahan rencana kota, dapat melakukan perbaikan sesuai dengan peruntukan (Pasal 22 ayat (2) Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">13.</td>
								<td align="left" style="height:10px;">Pada lingkungan bangunan yang tertentu, dapat dilakukan perubahan penggunaan jenis bangunan yang ada, selama masih sesuai dengan golongan peruntukan rencana kota (Pasal 22 ayat (4) Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">14.</td>
								<td align="left" style="height:70px;">Perubahan rencana teknis karena perubahan pada arsitektur, struktur, utilitas (mekanikal dan elektrikal) serta perubahan rencana teknis karena perubahan fungsi harus melalui proses permohonan baru/revisi IMB dengan proses sesuai dengan penggolongan bangunan gedung untuk IMB (Pasal 26 huruf b dan huruf c Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>B.</b></td>
								<td colspan="2" align="left" style="height:10px;"><b>Ketentuan Keselamatan Kerja</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;">Pemegang Ijin Gangguan wajib mematuhi ketentuan undang-undang Nomor 1 Tahun 1970 tentang Keselamatan Kerja</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;">Menjaga ketertiban & keamanan lingkungannya, menjaga kemungkinan timbulnya bahaya kebakaran dan menyediakan racun api yang telah diteliti oleh Dinas Penaggungan Bahaya Kebakaran. (Pasal 7 Keputusan Walikota Palembang No. 45 Tahun 2002)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:10px;">Untuk lingkungan bangunan perumahan dan lingkungan bangunan gedung harus dilengkapi hydrant atau sumur gali atau reservoar kebakaran dan apabila lingkungan bangunan yang berjarak lebih dari 100 (seratus) meter dari jalan lingkungan dilengkapi hydrant tersendiri (Pasal 5 Ayat 2 Perda Nomor 31 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">4.</td>
								<td align="left" style="height:10px;">Untuk bangunan pabrik / mall / toko/ hotel, setiap 800 m2 diwajibkan menyediakan 1 (satu) titik hydrant conex macino 2,5 inchi.</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">5.</td>
								<td align="left" style="height:10px;">Untuk bangunan perumahan dengan tingkat kebakaran tinggi (Pom bensin), setiap 600 m2 diwajibkan menyediakan 1 (satu) titik hydrant conex macino 2,5 inchi.</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">6.</td>
								<td align="left" style="height:10px;">Untuk luas bangunan di bawah 150 m2 diwajibkan memiliki 1 (satu) tabung racun api ukuran 10 (sepuluh) liter dengan isi 3,5 kg dan untuk setiap kelipatan luas bangunan 150 m2 wajib merambah 1 (satu) tabung racun api dengan ukuran yang sama.</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">7.</td>
								<td align="left" style="height:30px;">Memenuhi persyaratan untuk mengajukan Ijin Gangguan  (Pasal 5 Perda No. 18 Tahun 2011)</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>C.</b></td>
								<td colspan="2" align="left" style="height:10px;"><b>Ketentuan Retribusi</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;">Membayar Retribusi Ijin Gangguan berdasarkan Perda No. 18 Tahun 2011</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;">
									Wajib mendaftarkan ulang setiap $lama_berlaku sekali sejak tanggal Ijin Gangguan ditetapkan
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:30px;">
									Dikenakan denda 2% setiap bulan dari retribusi apabila tidak membayar retribusi tepat pada waktunya (Pasal 34 Perda No. 18 Tahun 2011)
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>D.</b></td>
								<td colspan="2" align="left" style="height:10px;"><b>Ketentuan Larangan</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;">Jika Ijin Gangguan diperoleh secara tidak sah</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;">
									Tidak melakukan kegiatan-kegiatan pokok sesuai ijin yang diberikan
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:10px;">
									Tidak memenuhi ketentuan-ketentuan yang ditetapkan dalam Surat Ijin Gangguan
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">4.</td>
								<td align="left" style="height:10px;">
									Mengadakan perluasan (kapasitas, volume dan luas) tempat usahanya tanpa ijin dari Walikota Palembang
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">5.</td>
								<td align="left" style="height:10px;">
									Memindahtangankan ijin tempat usahanya kepada pihak lain
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">6.</td>
								<td align="left" style="height:10px;">
									Tidak melakukan daftar ulang
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">7.</td>
								<td align="left" style="height:30px;">
									Memindahkan tempat usahanya
								</td>
							</tr>
							<tr>
								<td align="left" colspan="4" style="height:50px;">
									Ijin Gangguan pada Surat Ijin ini, akan dicabut dan dinyatakan tidak berlaku apabila pemegang ijin memindah tangankan, merubah kegiatan usahanya, pindah lokasi dan memperluas tempat usaha dan tidak melaksanakan kewajiban pada ketentuan Keputusan ini.
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			';
$pdf->writeHTML($html, true, false, true, false, '');
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_end_clean();
$pdf->Output('SK IG.pdf', 'I');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+