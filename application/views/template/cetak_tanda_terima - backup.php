<?php
if($getData){
	$user_management_id	= $getData[0]['user_management_id'];
	$permohonan_id	= $getData[0]['permohonan_id'];
	$full_name		= $getData[0]['first_name'].' '.$getData[0]['last_name'];
	$alamat			= $getData[0]['alamat_perusahaan'];
	$email			= $getData[0]['email'];
	$originalDate 	= $getData[0]['created_date'];
	$created_date 	= date("d M Y H:i:s", strtotime($originalDate));
	$no_resi		= $getData[0]['code'];
	$qr_code		= $getData[0]['qr_code'];
	$path_location_qr_code		= $getData[0]['path_location_qr_code'];
	
	if(!file_exists(path_user_upload.'user_'.$user_management_id)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id, 0777);
		}
	if(!file_exists(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id, 0777);
		}
	if(!file_exists(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id.'/tanda_terima')) {
	$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id.'/tanda_terima', 0777);
	}
} else {
	$user_management_id	= '';
	$permohonan_id	= '';
	$full_name		= '';
	$alamat			= '';
	$email			= '';
	$created_date 	= '';
	$no_resi		= '';
	$path_location_qr_code		= '';
	$qr_code		= '';
}
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 5, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
				<div align="center"><img src="'.base_url('assets/images/logo/logo.png').'"></div>
					<table align="center" style="width:100%;text-align:center;border-bottom: 1px solid black;">
						<tr>
							<td style="height:30px;"><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
						</tr>
						<tr>
							<td>BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
						</tr>
						<tr>
							<td>Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
						</tr>
						<tr>
							<td>Telp. (0711) 370679, 370681</td>
						</tr>
						<tr>
							<td>PALEMBANG</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" style="width:100%;text-align:left;">
						<tr>
							<td width="30%">Nama</td>
							<td width="5%"> : </td>
							<td width="65%"><b>'.$full_name.'</b></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td> : </td>
							<td><b>'.$alamat.'</b></td>
						</tr>
						<tr>
							<td>Email</td>
							<td> : </td>
							<td><b>'.$email.'</b></td>
						</tr>
						<tr>
							<td>Tanggal Permohonan</td>
							<td> : </td>
							<td><b>'.$created_date.'</b></td>
						</tr>
						<tr>
							<td>No Resi</td>
							<td> : </td>
							<td><b>'.$no_resi.'</b></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" style="width:100%;text-align:left;">
						<tr>
							<td colspan="2"><b>Daftar persyaratan yang sudah dipenuhi :</b></td>
						</tr>';
						if($getDataSyarat){
								$i = 1;
								foreach($getDataSyarat as $row){
						
$html	.='								<tr>
							<td valign="top" width="5%">'.$i.'. </td><td width="95%"><p>'.$row['syarat_name'].'</p></td>
						</tr>';
						
								$i++;
								}
							}
						
$html	.='				
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">Silahkan bawa resi ini sebagai bukti permohonan<br>Terimakasih</td>
						</tr>
					</table>
					<div align="right">
						<img src="'.base_url($path_location_qr_code).'" width="60px" valign="bottom">
					</div>';
// echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'imperium_new/'.path_view_user_upload.'user_'.$user_management_id.'/permohonan_'.$permohonan_id.'/tanda_terima/'.$qr_code.'.pdf', 'F');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+