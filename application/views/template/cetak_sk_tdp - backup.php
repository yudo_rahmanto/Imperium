<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$dataSk	= '';
	$modal_usaha = '';
	foreach($getData[0] as $row_left=>$row_right){
		$dataSk[$row_left] = $row_right;
	}
	$modal_usaha = $dataSk['modal_usaha'];
	
}
if($getDataSignature){
	$signature	= substr($getDataSignature[0]['path_location'],1);
} else {
	$signature	= '';
}
?>		

								<div align="center"><img src="<?php echo 'assets/images/logo/logo.png'; ?>"></div>
								<table align="center" style="width:100%;text-align:center;border-bottom: 1px solid black;font-size: 10px;">
									<tr>
										<td style="height:30px;"><b>PEMERINTAH KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td>BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
									<tr>
										<td>Jalan Merdeka No. 1 Palembang, Provinsi Sumatera Selatan </td>
									</tr>
									<tr>
										<td>Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td>Palembang</td>
									</tr>
								</table>
							
								<table align="center" style="margin-bottom:20px;width:100%;text-align:center;font-size: 10px;">
									<tr>
										<td style="font-size: 12px;"><b>TANDA DAFTAR PERUSAHAAN</b></td>
									</tr>
									<tr>
										<td style="font-size: 10px;"><b><?php if($getData){echo str_replace("TANDA DAFTAR PERUSAHAAN","",$dataSk['keterangan_izin']); } ?></b></td>
									</tr>
									<tr>
										<td>
											BERDASARKAN UNDANG-UNDANG NOMOR 3 TAHUN 1982 TENTANG WAJIB DAFTAR PERUSAHAAN 
											<?php if($getData){if($dataSk['izin_id'] == 12){ ?>
											DAN UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 25 TAHUN 1992 TENTANG PERKOPERASIAN
											<?php	}} ?>
										</td>
									</tr>
								</table>
								<table align="center" style="margin-bottom:20px;width:100%;text-align:center;font-size: 10px;">
									<tr>
										<td style="width:25%;height:30px;border: 1px solid black;"><p><b>NOMOR TDP</b></p><p>No Ijin</p></td>
										<td style="width:25%;height:30px;border: 1px solid black;"><p><b>BERLAKU S/D TANGGAL</b></p><p>
																											<?php 
																											if($getData){
																												$original_start_date =  $dataSk['awal_masa_berlaku'];
																												$start_date = date("d M Y", strtotime($original_start_date));
																												echo $start_date; 
																												} 
																											
																											?> s/d <?php 
																														if($getData){
																															$original_end_date =  $dataSk['akhir_masa_berlaku'];
																															$end_date = date("d M Y", strtotime($original_end_date));
																															echo $end_date; 
																															} 
																													?>
																										</p></td>
										<td style="width:25%;height:30px;border: 1px solid black;"><p><b><?php if($getData){echo $dataSk['d1']; } ?></b></p></td>
										<td style="width:25%;height:30px;border: 1px solid black;"><p><b><?php if($getData){echo $dataSk['d2']; } ?></b></p></td>
									</tr>
								</table>
								<table id="data_detail" align="center" style="width:100%;text-align:left;font-size: 10px;border: 1px solid black;">
									<tr>
										<td width="20%" style="height:30px;">NAMA PERUSAHAAN</td>
										<td width="1%" style="height:30px;"> : </td>
										<td colspan="5" style="height:30px;"><b><?php if($getData){echo $dataSk['nama_perusahaan']; } ?></b></td>
									</tr>
									<tr>
										<td width="20%" style="height:30px;">STATUS</td>
										<td width="1%" style="height:30px;"> : </td>
										<td colspan="5" style="height:30px;"><b><?php if($getData){echo $dataSk['status']; } ?></b></td>
									</tr>
									<tr>
										<td width="20%" style="height:30px;">ALAMAT PERUSAHAAN</td>
										<td width="1%" style="height:30px;"> : </td>
										<td colspan="5" style="height:30px;"><b><?php if($getData){echo $dataSk['alamat_perusahaan']; } ?></b></td>
									</tr>
									<tr>
										<td width="20%" style="height:30px;">NOMOR TELEPON</td>
										<td width="1%" style="height:30px;"> : </td>
										<td width="25%" style="height:30px;"><b><?php if($getData){echo $dataSk['no_tlp_perusahaan']; } ?></b></td>
										<td width="5%" style="height:30px;">&nbsp;</td>
										<td width="20%" style="height:30px;">FAX</td>
										<td width="1%" style="height:30px;">:</td>
										<td width="25%" style="height:30px;">&nbsp;</td>
									</tr>
									
									<tr>
										<td width="20%" style="height:30px;">PENANGGUNG JAWAB / PENGURUS</td>
										<td width="1%" style="height:30px;"> : </td>
										<td colspan="5" style="height:30px;"><b><?php if($getData){echo $dataSk['first_name'].' '.$dataSk['last_name']; } ?></b></td>
									</tr>
									<tr>
										<td width="20%">KEGIATAN USAHA POKOK</td>
										<td width="1%"> : </td>
										<td colspan="5"><b><?php if($getData){echo $dataSk['kegiatan_usaha']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">[KLUI</td>
										<td width="1%"> : </td>
										<td colspan="5"><b><?php if($getData){echo $dataSk['klui']; } ?>]</b></td>
									</tr>
									<tr style="height:30px;">
										<td colspan="7">&nbsp;</td>
									</tr>
									<tr style="height:30px;">
										<td colspan="7">PENGESAHAN MENTERI KOPERASI</td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">NOMOR</td>
										<td width="1%"> : </td>
										<td width="25%"><b><?php if($getData){echo $dataSk['no_pengesahan_menkop']; } ?></b></td>
										<td width="5%">&nbsp;</td>
										<td width="20%">TANGGAL</td>
										<td width="1%">:</td>
										<td width="25%"><b>
										<?php 
											if($getData){
												$original_tgl_pengesahan =  $dataSk['tgl_pengesahan'];
												$tgl_pengesahan = date("d M Y", strtotime($original_tgl_pengesahan));
												echo $tgl_pengesahan; 
												} 
										?>
										</b></td>
									</tr>
								</table>
									
								
								<table align="left" style="width:20%;text-align:center;font-size: 10px;">
									<tr>
										<td> <img src="<?php if($getData){ echo $dataSk['path_location_barcode'];} ?>" width="60px" valign="bottom" style="float:right;"> </td>
									</tr>
								</table>
								<table align="right" style="width:40%;text-align:left;font-size: 10px;">
									<tr>
										<td>
											<p> Ditetapkan di Palembang</p>
											<p> pada tanggal <?php echo date('d M Y'); ?></p>
											<p><b> a.n. WALIKOTA PALEMBANG</b></p>
											<p><b> KEPALA BADAN PENANAMAN MODAL</b></p>
											<p><b> PELAYANAN TERPADU SATU PINTU,</b></p>
										</td>
									</tr>
									<tr>
										<td style="height:70px;"><img src="<?php echo $signature; ?>" width="80px" height="80px"></td>
									</tr>
									<tr>
										<td>
											<p><b> Drs. RATU DEWA, M.Si </b></p>
											<p> Pembina Tingkat I </p>
											<p> NIP. 196907071993031005 </p>
										</td>
									</tr>
									
								</table>
								
								<table align="center" style="margin-top:350px;width:100%;text-align:center;font-size: 10px;">
									<tr>
										<td>
											<p><b> KETENTUAN IJIN USAHA PERDAGANGAN </b></p>
										</td>
									</tr>
								</table>
								<table align="center" style="margin-top:0px;width:100%;text-align:left;font-size: 10px;">
									<tr>
										<td>
											<p>Pemegang Ijin Usaha Perdagangan mempunyai kewajiban mematuhi ketentuan, antara lain sebagai berikut :</p>
											<p>A.	Ketentuan Tertib Bangunan</p>
											<ul>
												<li>1.	Memiliki Ijin Medirikan Bangunan (IMB)</li>
												<li>2.	Memiliki Ijin Penggunaan Bangunan (IPB) / Sertifikat Laik Fungsi (SLF)</li>
												<li>3.	Memiliki Surat Ijin Gangguan (IG)</li>
											</ul>
											<p>&nbsp;</p>
											<p>B.	Ketentuan Arsitek Bangunan </p>
											<ul>
												<li>1.	Bangunan atau bagian bangunan yang mengalami perubahan bentuk dan ukuran bangunan yang sudah ada wajib mendapatkan Ijin Walikota  Kota Palembang (Pasal 2 Perda No. 5 Tahun 2010)</li>
												<li>2.	Menjaga keamanan dan Keselamatan bangunan dan lingkungannya serta tidak boleh mengganggu ketentraman dan keselamatan masyarakat (Pasal 8 Ayat (1) Perda No. 5 Tahun 2010)</li>
												<li>3.	Mematuhi ketentuan fungsi bangunan gedung yang dapat dibangun pada lokasi, ketinggian maksimum bangunan gedung, jumlah lantai/lapis bangunan gedung dibawah permukaan tanah, Garis Sempadan dan jarak bebas minimum gedung, KDB, KLB, KDH KTB maksimum yang diijinkan dan jaringan utilitas kota  (Pasal 12 ayat (2) Perda No. 5 Tahun 2010)</li>
											</ul>
											<p>&nbsp;</p>
											<p>C.	Ketentuan Keselamatan, Kesehatan dan Keserasian Lingkungan </p>
											<ul>
												<li>1.	Mematuhi Ketentuan Undang-undang No. 1 Tahun 1970 Tentang Keselamatan Kerja</li>
												<li>2.	Setiap Bangunan bukan rumah tinggal dengan luas bangunan lebih dari 300 m dan atau dengan ketinggian lebih dari 2 lantai harus dilaksanakan oleh konsultan perencana, pelaksana dan pengawas bangunan (Pasal 13 Perda No. 5 Tahun 2010)</li>
												<li>3.	Setiap bangunan tidak boleh menggangu kelancaran arus lalu lintas kendaraan, orang dan barang, tidak menggangu dan merusak sarana kota maupun prasarana jaringan kota serta tetap memperhatikan keserasian dan arsitektur lingkungan (Pasal 16 Perda No. 5 Tahun 2010)</li>
												<li>4.	Secara periodik setiap bulannya memeriksakan dan menguji alat pemadam kebakaran yang dimilikinya ke Dinas Penanggulangan Bahaya Kebakaran (Pasal 2 Perda No. 31 Tahun 2011)</li>
												<li>5.	Menyiapkan kotak sampah dan wajib mengijinkan petugas Pemerintah Daerah untuk memasuki pekarangan,  rumah dan persilnya untuk kepentingan umum (Pasal 3  Perda No. 3 tahun 1981 jo Perda No. 8 Tahun 1987)</li>
												<li>6.	Dilarang menebang / merusak pohon-pohon pelindung dan tanaman yang tumbuh disepanjang jalan jalur hijau (Perda No. 44 Tahun 2002)</li>
												<li>7.	Memelihara dengan baik dan bersih persilnya dan segala sesuatu dalam persil, memagar, mengecat dan mengapur setiap awal bulan Juni (Pasal 2 Perda No. 3 Tahun 1981 jo Perda No. 8 Tahun 1987)</li>
												<li>8.	Menjamin keamanan , ketertiban, kebersihan, kesehatan lingkungan tempat usahanya (Pasal 9 Perda No. 4 Tahun 2002)</li>
												<li>9.	Memenuhi persyaratan untuk mengajukan Ijin Usaha Perdagangan  (Perda No. 19 Tahun 2011)   </li>
											</ul>
											<p>&nbsp;</p>
											<p>D.	Ketentuan Daftar Ulang </p>
											<ul>
												<li>Ijin Usaha Perdagangan berlaku untuk jangka waktu selama 5 (lima) tahun dengan ketentuan apabila telah habis masa berlakunya, harus melaksanakan daftar ulang (Pasal 15 Perda No. 19 Tahun 2011)</li>
											</ul>
											<p>&nbsp;</p>
											<p>E.	Perijinan ini tidak dikenakan biaya retribusi (Rp.0,-) berdasarkan UU No.28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah.</p>
											<p>&nbsp;</p>
											<p>F.	Ketentuan Larangan</p>
											<ul>
												<li>1.	Tidak memenuhi ketentuan sebagaimana diatur dalam Peraturan Daerah (Perda No. 19 Tahun 2011)</li>
												<li>2.	Tidak memenuhi kewajiban untuk mendaftar ulang  (Perda No. 19 Tahun 2011)</li>
											</ul>
											<p>G.	Ketentuan Perubahan, Penghapusan dan Penggantian</p>
											<ul>
												<li>1.	Perubahan perusahaan adalah perubahan yang meliputi perubahan nama perusahaan, bentuk perusahaan, alamat kantor perusahaan, nama pemilik  / penanggung jawab perusahaan, alamat pemilik / penanggung jawab perusahaan, NPWP, modal dan kekayaan bersih (Netto), bidang uasaha dan jenis barang / jasa dagangan utama.</li>
												<li>2.	Penghapusan SIUP terjadi apabila, perubahan bentuk perusahaan, pembubaran perusahaan, perusahaan menghentikan segala kegiatan usahanya, perusahaan tersebut terhenti pada waktu pendiriannya, kadaluarsa atau berakhirnya dan tidak diperpanjang, atau;</li>
												<li>3.	Perusahaan tersebut dihentikan segala kegiatan usahanya berdasarkan keputusan pengadilan negeri yang mempunyai kekuatan hukum yang tetap.</li>
												<li>4.	Apabila SIUP yang telah dimiliki oleh perusahaan usaha perdagangan hilang atau rusak sehingga tidak terbaca, penanggung jawab atau pengusaha dapat mengajukan permohonan atas penggantian SIUP baru kepada Walikota dengan melampirkan Surat Keterangan hilang dari Kepolisian (Pasal 23 Perda No. 19 Tahun 2011)</li>
											</ul>
											<p>&nbsp;</p>
											<p>Ijin Usaha Perdagangan pada keputusan ini, akan dicabut dan dinyatakan tidak berlaku apabila pemegang ijin melakukan perubahan kegiatan pokok, perluasan tempat usaha, pindah lokasi dan memindah tangankan ijin usahanya dan tidak melaksanakan kewajiban pada ketentuan keputusan ini.</p>
										</td>
									</tr>
								</table>