<?php
// echo '<pre>';print_r($getData);exit;
if($getData){
	$isiData	= "";
	foreach($getData[0] as $row_left=>$row_right){
		$isiData[$row_left]	= $row_right;
		if($row_left == "photo_pemohon"){
			$isiData[$row_left]	= substr($row_right,2);
		}
		if($row_left == "izin_name"){
			
			if(preg_match('/RINGAN/',$row_right)){$isiData['code_izin'] = 'IG.R';}
			if(preg_match('/BERAT/',$row_right)){$isiData['code_izin'] = 'IG.B';}
		}
	}
} 

if($getDataSignature){
	$signature	= $getDataSignature[0]['path_location'];
} else {
	$signature	= '';
}
if($getKepalaDinas){
	$level_id		= $getKepalaDinas[0]['level_id'];
	$level_name 	= '';
	$nip_kepala		= $getKepalaDinas[0]['nip'];
	$nama_kepala	= $getKepalaDinas[0]['fullname_with_gelar'];
	$getLevel		= SingleFilter('m_level','level_id',$level_id);
	if($getLevel){
		$level_name = $getLevel[0]['level_name'];
	}
} else {
	$level_id		= '';
	$level_name 	= '';
	$nip_kepala		= '';
	$nama_kepala	= '';
}
// echo '<pre>';print_r($photo_pemohon);exit;
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document

$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 0, 0);

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

// set image scale factor
$pdf->setImageScale(1.3);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
					<table align="center" style="margin-bottom:20px;width:100%;text-align:center;font-size: 20px;">
						<tr>
							<td style="height:200px;">&nbsp;</td>
						</tr>
						<tr>
							<td><b>SURAT IJIN WALIKOTA PALEMBANG</b></td>
						</tr>
						<tr>
							<td style="height:30px;font-size: 18px;">NOMOR 503/'.$isiData['code_izin'].'/'.$isiData['izin_id'].'/'.$isiData['no_sk'].'/BPM-PTSP/'.date('Y').'</td>
						</tr>
						<tr>
							<td style="height:30px;font-size: 15px;"><b>TENTANG <br>IJIN GANGGUAN</b></td>
						</tr>
						<tr>
							<td style="height:10px;font-size: 15px;">&nbsp;</td>
						</tr>
					</table>
					<table style="width:100%;font-size: 11px;">
						<tr>
							<td align="left" style="width:9%;height:10px;font-size: 14px;"><b>DASAR</b></td>
							<td align="left" style="width:2%;height:10px;">:</td>
							<td align="left" style="width:3%;height:10px;">a.</td>
							<td colspan="2" align="left" style="width:86%;height:10px;">
								<span align="justify">Undang – Undang Gangguan (Hinder Ordonnantie) Stbl Tahun 1926 Nomor 226 diubah dan ditambah dengan Stbl Tahun 1940 Nomor 14 dan Nomor 450</span>
							</td>
						</tr>
						<tr>
							<td align="left" style="font-size: 10px;">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">b.</td>
							<td colspan="2" align="left">
								<span align="justify">Undang-undang Nomor 28 Tahun 1959  tentang Pembentukan Pemerintahan Daerah Tingkat II dan Kotapraja di Sumatra Selatan (Lembaran Negara RI Tahun 1959 Nomor 73, Tambahan lembaran Negara RI Nomor 1821)</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">c.</td>
							<td colspan="2" align="left">
								<span align="justify">Undang – Undang Nomor 25 Tahun 2007 tentang Penanaman Modal (Lembaran Negara Republik Indonesia Nomor 1821)</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">d.</td>
							<td colspan="2" align="left">
								<span align="justify">Undang-Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah (Lembaran Negara RI Tahun 2009 Nomor130, Tambahan Lembaran Negara RI Nomor 5049)</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">e.</td>
							<td colspan="2" align="left">
								<span align="justify">Peraturan Menteri Dalam Negeri Nomor 27 Tahun 2009 tentang Pedoman Penetapan Ijin Gangguan</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">f.</td>
							<td colspan="2" align="left">
								<span align="justify">Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010 tentang Ijin Mendirikan Bangunan (Lembaran Daerah Kota Palembang Tahun 2010 Nomor 5)</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">g.</td>
							<td colspan="2" align="left">
								<span align="justify">Peraturan Daerah Kota Palembang Nomor 3 Tahun 2013 tentang izin Lingkungan (Lembaran Daerah Kota Palembang Tahun 2013 Nomor 3)</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">h.</td>
							<td colspan="2" align="left">
								<span align="justify">Peraturan Daerah Kota Palembang Nomor 18 Tahun 2011 tentang Pembinaan dan Retribusi Ijin Gangguan (Lembaran Daerah Kota Palembang Tahun 2011 Nomor 18 Seri C)</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">i.</td>
							<td colspan="2" align="left">
								<span align="justify">Peraturan Daerah Kota Palembang Nomor 19 Tahun 2011 tentang Pembinaan di Bidang Industri dan Usaha Perdagangan (Lembaran Daerah Kota Palembang Tahun 2011 Nomor 19 Seri E)</span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">j.</td>
							<td colspan="2" align="left">
								<table width="100%">
									<tr>
										<td colspan="3">Memperhatikan :</td>
									</tr>
									<tr>
										<td width="2%" style="height:10px;">1.</td>
										<td width="98%" style="height:10px;"><span align="justify">Surat Permohonan Ijin Gangguan Saudara/i '.$isiData['first_name'].' '.$isiData['last_name'].'</span></td>
									</tr>
									<tr>
										<td style="height:10px;">2.</td>
										<td style="height:10px;"><span align="justify">Hasil penelitian lapangan dan rekomendasi Tim Teknis Badan Penanaman Modal Dan Pelayanan Terpadu Satu Pintu Kota Palembang yang dituangkan dalam Berita Acara Pemeriksaan NO.'.$isiData['retribusi_id'].' tanggal '.date("d F Y", strtotime($isiData['tgl_retribusi'])).'</span></td>
									</tr>
									<tr>
										<td style="height:10px;">3.</td>
										<td style="height:30px;"><span align="justify">Bahwa berdasarkan pertimbangan sebagaimana tersebut di atas kepada pemohon dapat diberikan Surat Ijin Gangguan yang ditetapkan dengan Surat Ijin Walikota Palembang.</span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="height:15px;">&nbsp;</td>
						</tr>
					</table>	
					<table align="center" style="width:100%;text-align:left;font-size: 11px;">
						<tr>
							<td colspan="3" align="center" style="height:30px;font-size: 15px;"><b>MENGIJINKAN :</b> </td>
						</tr>
						<tr>
							<td align="left" style="height:10px;width:10%;font-size: 14px;"><b>KEPADA</b></td>
							<td align="left" style="width:3%;">:</td>
							<td align="left" style="height:10px;width:30%;">Nama Pemilik / Penanggung Jawab</td>
							<td align="left" style="height:10px;width:3%;"> : </td>
							<td align="left" style="height:10px;width:54%;font-size: 10px;"><b>'.strtoupper($isiData['first_name'].' '.$isiData['last_name']).'</b></td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
							<td align="left">Alamat Pemilik/ Penanggung Jawab</td>
							<td align="left"> : </td>
							<td align="left" style="font-size: 10px;"><b>'.strtoupper($isiData['alamat_user']).' RT. '.$isiData['rt_user'].' RW. '.$isiData['rw_user'].' KEL. '.$isiData['kelurahan_user'].' KEC. '.$isiData['kecamatan_user'].' '.$isiData['kabupaten_user'].' - '.$isiData['provinsi_user'].'</b></td>
						</tr>
						<tr>
							<td colspan="5" style="height:10px;">&nbsp; </td>
						</tr>
						<tr>
							<td align="left" style="font-size: 14px;"><b>UNTUK</b></td>
							<td align="left" style="width:3%;">:</td>
							<td colspan="3" align="left" style="height:30px;">Untuk Mendirikan Tempat Usaha, dengan data sebagai berikut :</td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
							<td align="left" style="height:10px;">1.	Nama Perusahaan</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;font-size: 10px;"><b>'.strtoupper($isiData['type_perusahaan'].'. '.$isiData['nama_perusahaan']).'</b></td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
							<td align="left" style="height:10px;">2.	Alamat Perusahaan</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;font-size: 10px;"><b>'.strtoupper($isiData['alamat_perusahaan']).' RT. '.$isiData['rt_perusahaan'].' RW. '.$isiData['rw_perusahaan'].' KEL. '.$isiData['kelurahan_perusahaan'].' KEC. '.$isiData['kecamatan_perusahaan'].' '.$isiData['kabupaten_perusahaan'].' - '.$isiData['provinsi_perusahaan'].'</b></td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
							<td align="left" style="height:10px;">3.	Jenis Usaha</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;font-size: 10px;"><b>'.strtoupper($isiData['jenis_usaha']).'</b></td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
							<td align="left" style="height:10px;">4.	Luas Tempat Usaha</td>
							<td align="left" style="height:10px;"> : </td>
							<td align="left" style="height:10px;font-size: 10px;"><b>'.$isiData['luas_perusahaan'].' M</b></td>
						</tr>
						<tr>
							<td align="left" colspan="2">&nbsp;</td>
							<td align="left" style="height:10px;">5.	Berlaku s.d tanggal</td>
							<td align="left" style="height:10px;"style="height:10px;"> : </td>
							<td align="left" style="height:30px;font-size: 10px;"><b>'.$isiData['periode'].'</b></td>
						</tr>
					<table>
					<table align="center" style="width:100%;text-align:left;font-size: 11px;">
						<tr>
							<td width="100%">
								<table width="100%">
									<tr>
										<td align="left" colspan="3" style="height:10px;">Surat Ijin Gangguan ini berlaku sejak tanggal ditetapkan, dengan ketentuan sbb:</td>
									</tr>
									<tr>
										<td align="left" width="3%" style="height:10px;">&nbsp;</td>
										<td align="left" width="5%" style="height:10px;">1.</td>
										<td align="left" width="92%" style="height:10px;">Pemegang Ijin wajib mentaati peraturan perundang-undangan yang berlaku</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">2.</td>
										<td align="left" style="height:10px;">Harus memasang Plat Nomor dan Petikan Surat Ijin Gangguan pada dinding depan yang mudah dibaca</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">3.</td>
										<td align="left" style="height:10px;">Dalam pelaksanaan teknis pemasangan, pihak penyelenggara harus selalu menjaga keindahan, kebersihan dan ketertiban umum</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">4.</td>
										<td align="left" style="height:10px;">Surat Ijin Gangguan berlaku selama $lama_berlaku dan diwajibkan mendaftar ulang</td>
									</tr>
									<tr>
										<td align="left" style="height:10px;">&nbsp;</td>
										<td align="left" style="height:10px;">5.</td>
										<td align="left" style="height:40px;">Surat Ijin Gangguan akan ditinjau kembali dan disempurnakan sebagaimana mestinya, apabila dikemudian hari ternyata terdapat kekeliruan dalam penetapannya.</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table align="left" style="width:100%;text-align:center;font-size: 12px;">
						<tr>
							<td width="50%" colspan="2" height="80px"><div align="center"><img src="'.base_url($isiData['qr_code_sk']).'" width="60px" style="margin-left: auto;margin-right: auto;	display: block;"></div></td>
							<td width="50%" height="80px">
								<table width="100%">
									<tr>
										<td width="10%">&nbsp;</td><td width="90%" colspan="2">Ditetapkan di Palembang</td>
									</tr>
									<tr>
										<td width="10%" style="height:30px;">&nbsp;</td><td width="90%" colspan="2">pada tanggal, '.str_replace(' 00:00:00','',indonesia_date(date('d F Y'))).'</td>
									</tr>
									<tr>
										<td width="10%"> a.n. </td><td colspan="2">WALIKOTA PALEMBANG </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td colspan="2">KEPALA DINAS PENANAMAN MODAL </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td colspan="2">DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="30%">&nbsp;</td>
							<td width="30%">&nbsp;</td>
							<td width="40%" align="left"><img src="'.base_url($signature).'" width="80px" height="80px"></td>
						</tr>
						<tr>
							<td width="30%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
							<td width="50%">
								<table width="100%">
									<tr>
										<td width="10%">&nbsp;</td><td width="90%"><b>'.$nama_kepala.' </b></td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td>'.$level_name.' </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td>NIP. '.$nip_kepala.' </td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					';
// echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$html	= '';
$html	.= '
			<table align="center" style="width:100%;text-align:center;font-size: 16px;">
				<tr>
					<td style="height:50px;">&nbsp;</td>
				</tr>
				<tr>
					<td style="height:50px;">
						<p><b> KETENTUAN IJIN GANGGUAN </b></p>
					</td>
				</tr>
			</table>
			<table align="center" style="width:100%;text-align:left;font-size: 12px;">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td colspan="4" align="left" style="height:30px;"><span align="justify">Pemegang Ijin Gangguan pada Surat Ijin ini mempunyai kewajiban mematuhi Ketentuan-ketentuan sebagai berikut :</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>A.</b></td>
								<td colspan="2" align="left" style="height:10px;"><span align="justify"><b>Ruangan Dan Lingkungan</b></span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;"><span align="justify">Memiliki Ijin Medirikan Bangunan (IMB)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;"><span align="justify">Memiliki Ijin Penggunaan Bangunan (IPB) / Sertifikat Laik Fungsi (SLF)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:10px;"><span align="justify">Setiap memperluas dan merubah bangunan dan ruangan tempat usahanya harus mendapatkan ijin Walikota Palembang (Pasal 4 Perda No. 18 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">4.</td>
								<td align="left" style="height:10px;"><span align="justify">Memasang plat nomor dan petikan Ijin Gangguan pada dinding yang mudah dibaca (Pasal 7 Perda No. 18 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">5.</td>
								<td align="left" style="height:10px;"><span align="justify">Memasang nama perusahaan / merk usahanya</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">6.</td>
								<td align="left" style="height:10px;"><span align="justify">Menjamin tempat usaha dalam keadaan bersih, rapi, dan indah</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">7.</td>
								<td align="left" style="height:10px;"><span align="justify">Menjaga kebersihan got dan saluran pembuangan air</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">8.</td>
								<td align="left" style="height:10px;"><span align="justify">Memelihara dengan baik dan bersih persilnya serta segala sesuatu dalam persilnya, termasuk memagar, mengecat dan mengapur setiap awal bulan Juni (pasal 2 Perda No. 3 Tahun 1981 jo. Perda No. 8 tahun 1987)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">9.</td>
								<td align="left" style="height:10px;"><span align="justify">Menyiapkan kotak sampah dan wajib mengijinkan petugas Pemerintah Daerah untuk memasuki pekarangan rumah dan persilnya untuk kepentingan umum (pasal 3 Perda No. 3 Tahun 1981 jo. Perda No. 8 tahun 1987)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">10.</td>
								<td align="left" style="height:10px;"><span align="justify">Dilarang menebang / merusak pohon-pohon pelindung dan tanaman yang tumbuh disepanjang jalan jalur hijau (Perda No. 44 Tahun 2002)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">11.</td>
								<td align="left" style="height:10px;"><span align="justify">Bangunan atau bagian bangunan yang mengalami perubahan bentuk dan ukuran bangunan yang sudah ada wajib mendapatkan Ijin Walikota  Kota Palembang (Pasal 2 Perda No. 5 Tahun 2010)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">12.</td>
								<td align="left" style="height:10px;"><span align="justify">Bangunan yang ada dalam lingkungan yang mengalami perubahan rencana kota, dapat melakukan perbaikan sesuai dengan peruntukan (Pasal 22 ayat (2) Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">13.</td>
								<td align="left" style="height:10px;"><span align="justify">Pada lingkungan bangunan yang tertentu, dapat dilakukan perubahan penggunaan jenis bangunan yang ada, selama masih sesuai dengan golongan peruntukan rencana kota (Pasal 22 ayat (4) Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">14.</td>
								<td align="left" style="height:70px;"><span align="justify">Perubahan rencana teknis karena perubahan pada arsitektur, struktur, utilitas (mekanikal dan elektrikal) serta perubahan rencana teknis karena perubahan fungsi harus melalui proses permohonan baru/revisi IMB dengan proses sesuai dengan penggolongan bangunan gedung untuk IMB (Pasal 26 huruf b dan huruf c Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>B.</b></td>
								<td colspan="2" align="left" style="height:10px;"><span align="justify"><b>Ketentuan Keselamatan Kerja</b></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;"><span align="justify">Pemegang Ijin Gangguan wajib mematuhi ketentuan undang-undang Nomor 1 Tahun 1970 tentang Keselamatan Kerja</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;"><span align="justify">Menjaga ketertiban & keamanan lingkungannya, menjaga kemungkinan timbulnya bahaya kebakaran dan menyediakan racun api yang telah diteliti oleh Dinas Penaggungan Bahaya Kebakaran. (Pasal 7 Keputusan Walikota Palembang No. 45 Tahun 2002)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:10px;"><span align="justify">Untuk lingkungan bangunan perumahan dan lingkungan bangunan gedung harus dilengkapi hydrant atau sumur gali atau reservoar kebakaran dan apabila lingkungan bangunan yang berjarak lebih dari 100 (seratus) meter dari jalan lingkungan dilengkapi hydrant tersendiri (Pasal 5 Ayat 2 Perda Nomor 31 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">4.</td>
								<td align="left" style="height:10px;"><span align="justify">Untuk bangunan pabrik / mall / toko/ hotel, setiap 800 m2 diwajibkan menyediakan 1 (satu) titik hydrant conex macino 2,5 inchi.</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">5.</td>
								<td align="left" style="height:10px;"><span align="justify">Untuk bangunan perumahan dengan tingkat kebakaran tinggi (Pom bensin), setiap 600 m2 diwajibkan menyediakan 1 (satu) titik hydrant conex macino 2,5 inchi.</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">6.</td>
								<td align="left" style="height:10px;"><span align="justify">Untuk luas bangunan di bawah 150 m2 diwajibkan memiliki 1 (satu) tabung racun api ukuran 10 (sepuluh) liter dengan isi 3,5 kg dan untuk setiap kelipatan luas bangunan 150 m2 wajib merambah 1 (satu) tabung racun api dengan ukuran yang sama.</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">7.</td>
								<td align="left" style="height:30px;"><span align="justify">Memenuhi persyaratan untuk mengajukan Ijin Gangguan  (Pasal 5 Perda No. 18 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>C.</b></td>
								<td colspan="2" align="left" style="height:10px;"><span align="justify"><b>Ketentuan Retribusi</b></span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;"><span align="justify">Membayar Retribusi Ijin Gangguan berdasarkan Perda No. 18 Tahun 2011</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;">
									<span align="justify">Wajib mendaftarkan ulang setiap $lama_berlaku sekali sejak tanggal Ijin Gangguan ditetapkan</span>
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:30px;">
									<span align="justify">Dikenakan denda 2% setiap bulan dari retribusi apabila tidak membayar retribusi tepat pada waktunya (Pasal 34 Perda No. 18 Tahun 2011)</span>
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;"></td>
								<td align="left" style="width:3%;height:10px;"><b>D.</b></td>
								<td colspan="2" align="left" style="height:10px;"><span align="justify"><b>Ketentuan Larangan</b></span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="width:3%;height:10px;">1.</td>
								<td align="left" style="width:91%;height:10px;"><span align="justify">Jika Ijin Gangguan diperoleh secara tidak sah</span></td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">2.</td>
								<td align="left" style="height:10px;">
									<span align="justify">Tidak melakukan kegiatan-kegiatan pokok sesuai ijin yang diberikan</span>
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">3.</td>
								<td align="left" style="height:10px;">
									<span align="justify">Tidak memenuhi ketentuan-ketentuan yang ditetapkan dalam Surat Ijin Gangguan</span>
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">4.</td>
								<td align="left" style="height:10px;">
									<span align="justify">Mengadakan perluasan (kapasitas, volume dan luas) tempat usahanya tanpa ijin dari Walikota Palembang</span>
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">5.</td>
								<td align="left" style="height:10px;">
									<span align="justify">Memindahtangankan ijin tempat usahanya kepada pihak lain</span>
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">6.</td>
								<td align="left" style="height:10px;">
									<span align="justify">Tidak melakukan daftar ulang</span>
								</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">&nbsp;</td>
								<td align="left" style="height:10px;">7.</td>
								<td align="left" style="height:30px;">
									<span align="justify">Memindahkan tempat usahanya</span>
								</td>
							</tr>
							<tr>
								<td align="left" colspan="4" style="height:50px;">
									<span align="justify">Ijin Gangguan pada Surat Ijin ini, akan dicabut dan dinyatakan tidak berlaku apabila pemegang ijin memindah tangankan, merubah kegiatan usahanya, pindah lokasi dan memperluas tempat usaha dan tidak melaksanakan kewajiban pada ketentuan Keputusan ini.</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			';
$pdf->writeHTML($html, true, false, true, false, '');
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_end_clean();
$pdf->Output('SK IG.pdf', 'I');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+