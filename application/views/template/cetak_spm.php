<?php

if($getData){
	$user_management_id	= $getData[0]['user_management_id'];
	$permohonan_id	= $getData[0]['permohonan_id'];
	$izin_id		= $getData[0]['izin_id'];
	$izin_type_id	= $getData[0]['izin_type_id'];
	$nama_pemohon	= $getData[0]['first_name'].' '.$getData[0]['last_name'];
	$alamat_pemohon	= $getData[0]['alamat'];
	$jenis_perusahaan	= $getData[0]['jenis_perusahaan'];
	$nama_perusahaan	= $getData[0]['nama_perusahaan'];
	$alamat_perusahaan	= $getData[0]['alamat_perusahaan'];
	$provinsi_perusahaan	= $getData[0]['provinsi_perusahaan'];
	$kota_perusahaan		= $getData[0]['kota_perusahaan'];
	$kecamatan_perusahaan	= $getData[0]['kecamatan_perusahaan'];
	$kelurahan_perusahaan	= $getData[0]['kelurahan_perusahaan'];
	$code			= $getData[0]['qr_code'];
	
	if($jenis_perusahaan){
		$nama_perusahaan	= $jenis_perusahaan.'. '.$nama_perusahaan;
	}
	if(!file_exists(path_user_upload.'user_'.$user_management_id)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id, 0777);
		}
	if(!file_exists(path_user_upload.'user_'.$user_management_id.'/document')) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id.'/document', 0777);
		}
	
} else {
	$user_management_id	='';
	$permohonan_id	= '';
	$izin_id		= '';
	$izin_type_id	= '';
	$nama_pemohon	= '';
	$alamat_pemohon	= '';
	$nama_perusahaan	= '';
	$code		= '';
}

if($getDataSignature){
	$signature	= base_url($getDataSignature[0]['ttd']);
} else {
	$signature	= '';
}
if($getKepalaDinas){
	$level_id		= $getKepalaDinas[0]['level_id'];
	$level_name 	= '';
	$nip_kepala		= $getKepalaDinas[0]['nip'];
	$nama_kepala	= $getKepalaDinas[0]['fullname_with_gelar'];
	$getLevel		= SingleFilter('m_level','level_id',$level_id);
	if($getLevel){
		$level_name = $getLevel[0]['level_name'];
	}
} else {
	$level_id		= '';
	$level_name 	= '';
	$nip_kepala		= '';
	$nama_kepala	= '';
}
// echo '<pre>';print_r($signature);exit;
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 5, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
					<table align="center" style="width:100%;text-align:center;border-bottom: 2px solid black;">
						<tr>
							<td rowspan="5" width="15%" style="font-size: 100%;"><img src="'.base_url('assets/images/logo/logo.png').'" width="auto" height="70px"></td>
							<td width="85%" style="font-size: 100%;"><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
						</tr>
						<tr>
							<td style="font-size: 120%;"><b>DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</b></td>
						</tr>
						<tr>
							<td style="font-size: 80%;">Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
						</tr>
						<tr>
							<td style="font-size: 80%;">Telp. (0711) 370679, 370681</td>
						</tr>
						<tr>
							<td style="font-size: 80%;">PALEMBANG</td>
						</tr>
					</table>
					<table align="center" style="width:100%;text-align:center;">
						<tr>
							<td width="10%">&nbsp;</td>
							<td width="5%">&nbsp;</td>
							<td width="39%">&nbsp;</td>
							<td width="15%">&nbsp;</td>
							<td width="30%"><div align="left">Palembang, '.str_replace(' 00:00:00','',indonesia_date(date('d F Y'))).'</div></td>
						</tr>
						<tr>
							<td width="10%"><div align="left">Nomor</div></td>
							<td width="5%"><div align="center">:</div></td>
							<td width="39%"><div align="left">'.$permohonan_id.'/IG-SP/KPPT/'.date('Y').'</div></td>
							<td width="15%">&nbsp;</td>
							<td width="30%"><div align="left">Kepada :</div></td>
						</tr>
						<tr>
							<td width="10%"><div align="left">Perihal</div></td>
							<td width="5%"><div align="center">:</div></td>
							<td width="39%"><div align="left">Pemberitahuan Retribusi Perijinan</div></td>
							<td width="15%">&nbsp;</td>
							<td width="30%"><div align="left">Yth. '.$nama_pemohon.'</div></td>
						</tr>
						<tr>
							<td width="10%"><div align="left">Lampiran</div></td>
							<td width="5%"><div align="center">:</div></td>
							<td width="39%"><div align="left">Slip Setoran Pembayaran Retribusi</div></td>
							<td width="15%">&nbsp;</td>
							<td width="30%"><div align="left">'.$nama_perusahaan.'</div></td>
						</tr>
						<tr>
							<td width="10%">&nbsp;</td>
							<td width="5%">&nbsp;</td>
							<td width="39%">&nbsp;</td>
							<td width="15%">&nbsp;</td>
							<td width="30%"><div align="left">di Palembang</div></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<p>Menunjuk surat permohonan ijin saudara tanggal '.str_replace(' 00:00:00','',indonesia_date(date('d F Y'))).', bersama ini kami beritahukan bahwa retribusi yang harus saudara bayar sebesar :</p>
					<p>&nbsp;</p>
					<table align="left" border="1" style="width:100%;text-align:left;font-size: 80%;">
						<tr>
							<td align="center" width="10%"><b>No</b></td>
							<td align="center" width="20%"><b>Nomor Pendaftaran</b></td>
							<td align="center" width="30%"><b>Jenis Permohonan Ijin</b></td>
							<td align="center" width="20%"><b>Kode Rekening</b></td>
							<td align="center" width="20%"><b>Jumlah Retribusi</b></td>
						</tr>';
						$total = 0;
						if($getDataRetribusi){
								$i = 1;
								foreach($getDataRetribusi as $row){
									$total	= $total + $row['total_akhir'];
						
$html	.='				<tr>
							<td valign="top" width="10%" align="center">'.$i.'</td>
							<td align="center" width="20%"><p>'.$row['code'].'</p></td>
							<td align="left" width="30%"><p>'.$row['keterangan_izin'].'</p></td>
							<td align="center" width="20%"><p>1.16.1.16.01.4.1.2.03.03</p></td>
							<td align="right" width="20%"><p>Rp '.number_format($row['total_akhir']).'</p></td>
						</tr>';
						
								$i++;
								}
							}
						
$html	.='				
						<tr>
							<td valign="top" width="10%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
							<td align="left" width="30%"><p>Denda</p></td>
							<td align="center" width="20%"><p>1.20.1.20.03.4.1.4.14.03</p></td>
							<td align="right" width="20%"><p>Rp 0</p></td>
						</tr>
						<tr>
							<td colspan="4">JUMLAH TOTAL</td>
							<td align="right">Rp '.number_format($total).'</td>
						</tr>
						<tr>
							<td colspan="5">TOTAL : '.ucwords(strtolower(terbilang($total))).' Rupiah</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="center" style="width:100%;font-size: 80%;">
						<tr>
							<td colspan="2"><div align="left"><u><b>Catatan :</b></u></div></td>
						</tr>
						<tr>
							<td width="5%"><div align="left">1</div></td>
							<td width="95%">
								<div align="left">
									Pembayaran dilakukan di loket pembayaran Kantor Kas Bank Sumselbabel Badan Penanaman Modal Dan Pelayanan Terpadu Satu Pintu Kota 
									 Palembang Jl. Merdeka No.1 Palembang pada jam kerja dengan nomor rekening 150.300.0001 a.n PEMKOT PALEMBANG, 
									 selambat-lambatnya 3 (tiga) bulan sejak diterbitkannya Surat Pemberitahuan Retribusi.
								</div>
							</td>
						</tr>
						<tr>
							<td width="5%"><div align="left">2</div></td>
							<td width="95%">
								<div align="left">
									Apabila Melebihi waktu 3 (tiga) bulan sejak diterbitkannya Surat Pemberitahuan Retribusi belum dilakukan pembayaran, 
									maka permohonan perijinan dibatalkan dan dapat melakukan pendaftaran kembali. 
								</div>
							</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="center" style="width:100%;font-size: 80%;">
						<tr>
							<td width="60%">&nbsp;</td>
							<td style="word-wrap: break-word;"><div align="center">KEPALA BADAN PENANAMAN MODAL <br>DAN PELAYANAN TERPADU SATU PINTU</div></td>
						</tr>
						<tr>
							<td width="60%">&nbsp;</td>
							<td style="height:100px;"><div align="center"><img src="'.$signature.'" width="150px" height="150px"></div></td>
						</tr>
						<tr>
							<td width="60%">&nbsp;</td>
							<td><div align="center"><b>'.$nama_kepala.'</b></div></td>
						</tr>
						<tr>
							<td width="60%">&nbsp;</td>
							<td><div align="center">'.$level_name.'</div></td>
						</tr>
						<tr>
							<td width="60%">&nbsp;</td>
							<td><div align="center">NIP. '.$nip_kepala.'</div></td>
						</tr>
					</table>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();	
$pdf->AddPage();
$html				= '				
					<table align="center" style="width:100%;text-align:center;border-left: 1px solid black;border-right: 1px solid black;border-top: 1px solid black;border-bottom: 1px solid black;">
						<tr>
							<td width="60%" style="height:60px;">
								<table align="center" style="width:100%;text-align:center;border-right: 1px solid black;">
									<tr>
										<td style="font-size: 65%;"><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td style="font-size: 80%;"><b>BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</b></td>
									</tr>
									<tr>
										<td style="font-size: 60%;">Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
									</tr>
									<tr>
										<td style="font-size: 60%;">Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td style="font-size: 60%;">PALEMBANG</td>
									</tr>
								</table>
							</td>
							<td width="40%" style="height:60px;">
								<table align="center" style="width:100%;text-align:left;">
									<tr>
										<td colspan="3" style="font-size: 80%;">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3" style="font-size: 80%;"><div align="center"><b>SLIP SETORAN PEMBAYARAN RETRIBUSI </b></div></td>
									</tr>
									<tr>
										<td width="20%" style="font-size: 60%;">Nomor </td>
										<td width="5%" style="font-size: 60%;">:</td>
										<td width="75%" style="font-size: 60%;"><div align="left">'.$permohonan_id.'/IG-SP/KPPT/'.date('Y').'</div></td>
									</tr>
									<tr>
										<td width="20%" style="font-size: 60%;">Tahun </td>
										<td width="5%" style="font-size: 60%;">:</td>
										<td width="75%" style="font-size: 60%;"><div align="left">'.date('Y').' Bulan : '.date('F').'</div></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" style="width:100%;text-align:left;font-size: 80%;">
						<tr>
							<td width="20%">Nama</td>
							<td width="5%"> : </td>
							<td width="75%"><b>'.$nama_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td> : </td>
							<td><b>'.$alamat_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Nama Usaha</td>
							<td> : </td>
							<td><b>'.$nama_perusahaan.'</b></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" border="1" style="width:100%;text-align:left;font-size: 60%;">
						<tr>
							<td align="center" width="10%"><b>No</b></td>
							<td align="center" width="20%"><b>Nomor Pendaftaran</b></td>
							<td align="center" width="30%"><b>Jenis Permohonan Ijin</b></td>
							<td align="center" width="20%"><b>Kode Rekening</b></td>
							<td align="center" width="20%"><b>Jumlah Retribusi</b></td>
						</tr>';
						$total = 0;
						if($getDataRetribusi){
								$i = 1;
								foreach($getDataRetribusi as $row){
									$total	= $total + $row['total_akhir'];
						
$html	.='				<tr>
							<td valign="top" width="10%" align="center">'.$i.'</td>
							<td align="center" width="20%"><p>'.$row['code'].'</p></td>
							<td align="left" width="30%"><p>'.$row['keterangan_izin'].'</p></td>
							<td align="center" width="20%"><p>1.16.1.16.01.4.1.2.03.03</p></td>
							<td align="right" width="20%"><p>Rp '.number_format($row['total_akhir']).'</p></td>
						</tr>';
						
								$i++;
								}
							}
						
$html	.='				
						<tr>
							<td valign="top" width="10%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
							<td align="left" width="30%"><p>Denda</p></td>
							<td align="center" width="20%"><p>1.20.1.20.03.4.1.4.14.03</p></td>
							<td align="right" width="20%"><p>Rp 0</p></td>
						</tr>
						<tr>
							<td colspan="4">JUMLAH TOTAL</td>
							<td align="right">Rp '.number_format($total).'</td>
						</tr>
						<tr>
							<td colspan="5">TOTAL : '.ucwords(strtolower(terbilang($total))).'</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="center" style="text-align:center;width:100%;font-size: 60%;">
						<tr>
							<td width="30%">PETUGAS PENERIMA BANK SUMSEL <br> (TELLER)</td>
							<td width="5%">&nbsp;</td>
							<td width="30%">PEMOHON</td>
							<td width="35%"><div align="center">KEPALA BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU KOTA PALEMBANG </div></td>
						</tr>
						<tr>
							<td colspan="3" style="height:80px;"></td>
							<td colspan="1" style="height:80px;"><img src="'.$signature.'" width="100px" height="100px"></td>
						</tr>
						<tr>
							<td style="border-bottom: 1px solid black;"></td>
							<td width="5%"><div align="center"></div></td>
							<td style="border-bottom: 1px solid black;"></td>
							<td><div align="center"><b>Drs. RATU DEWA, M.Si</b></div></td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td><div align="center">Pembina Tingkat I</div></td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td><div align="center">NIP. 196907071993031005</div></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="center" style="width:100%;text-align:center;border-left: 1px solid black;border-right: 1px solid black;border-top: 1px solid black;border-bottom: 1px solid black;">
						<tr>
							<td width="60%" style="height:60px;">
								<table align="center" style="width:100%;text-align:center;border-right: 1px solid black;">
									<tr>
										<td style="font-size: 65%;"><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td style="font-size: 80%;"><b>BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</b></td>
									</tr>
									<tr>
										<td style="font-size: 60%;">Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
									</tr>
									<tr>
										<td style="font-size: 60%;">Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td style="font-size: 60%;">PALEMBANG</td>
									</tr>
								</table>
							</td>
							<td width="40%" style="height:60px;">
								<table align="center" style="width:100%;text-align:left;">
									<tr>
										<td colspan="3" style="font-size: 80%;">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3" style="font-size: 80%;"><div align="center"><b>SLIP SETORAN PEMBAYARAN RETRIBUSI </b></div></td>
									</tr>
									<tr>
										<td width="20%" style="font-size: 60%;"><div align="center">Nomor </div></td>
										<td width="5%" style="font-size: 60%;">:</td>
										<td width="75%" style="font-size: 60%;"><div align="left">'.$permohonan_id.'/IG-SP/KPPT/'.date('Y').'</div></td>
									</tr>
									<tr>
										<td width="20%" style="font-size: 60%;"><div align="center">Tahun </div></td>
										<td width="5%" style="font-size: 60%;">:</td>
										<td width="75%" style="font-size: 60%;"><div align="left">'.date('Y').' Bulan : '.date('F').'</div></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" style="width:100%;text-align:left;font-size: 80%;">
						<tr>
							<td width="20%">Nama</td>
							<td width="5%"> : </td>
							<td width="75%"><b>'.$nama_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td> : </td>
							<td><b>'.$alamat_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Nama Usaha</td>
							<td> : </td>
							<td><b>'.$nama_perusahaan.'</b></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="left" border="1" style="width:100%;text-align:left;font-size: 60%;">
						<tr>
							<td align="center" width="10%"><b>No</b></td>
							<td align="center" width="20%"><b>Nomor Pendaftaran</b></td>
							<td align="center" width="30%"><b>Jenis Permohonan Ijin</b></td>
							<td align="center" width="20%"><b>Kode Rekening</b></td>
							<td align="center" width="20%"><b>Jumlah Retribusi</b></td>
						</tr>';
						$total = 0;
						if($getDataRetribusi){
								$i = 1;
								foreach($getDataRetribusi as $row){
									$total	= $total + $row['total_akhir'];
						
$html	.='				<tr>
							<td valign="top" width="10%" align="center">'.$i.'</td>
							<td align="center" width="20%"><p>'.$row['code'].'</p></td>
							<td align="left" width="30%"><p>'.$row['keterangan_izin'].'</p></td>
							<td align="center" width="20%"><p>1.16.1.16.01.4.1.2.03.03</p></td>
							<td align="right" width="20%"><p>Rp '.number_format($row['total_akhir']).'</p></td>
						</tr>';
						
								$i++;
								}
							}
						
$html	.='				
						<tr>
							<td valign="top" width="10%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
							<td align="left" width="30%"><p>Denda</p></td>
							<td align="center" width="20%"><p>1.20.1.20.03.4.1.4.14.03</p></td>
							<td align="right" width="20%"><p>Rp 0</p></td>
						</tr>
						<tr>
							<td colspan="4">JUMLAH TOTAL</td>
							<td align="right">Rp '.number_format($total).'</td>
						</tr>
						<tr>
							<td colspan="5">TOTAL : '.ucwords(strtolower(terbilang($total))).'</td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<table align="center" style="text-align:center;width:100%;font-size: 60%;">
						<tr>
							<td width="30%">PETUGAS PENERIMA BANK SUMSEL <br> (TELLER)</td>
							<td width="5%">&nbsp;</td>
							<td width="30%">PEMOHON</td>
							<td width="35%"><div align="center">KEPALA BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU KOTA PALEMBANG </div></td>
						</tr>
						<tr>
							<td colspan="3" style="height:80px;"></td>
							<td colspan="1" style="height:80px;"><img src="'.$signature.'" width="100px" height="100px"></td>
						</tr>
						<tr>
							<td style="border-bottom: 1px solid black;"></td>
							<td width="5%"><div align="center"></div></td>
							<td style="border-bottom: 1px solid black;"></td>
							<td><div align="center"><b>Drs. RATU DEWA, M.Si</b></div></td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td><div align="center">Pembina Tingkat I</div></td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td><div align="center">NIP. 196907071993031005</div></td>
						</tr>
					</table>
					';
 // echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();	

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'].root_folder.'/'.path_view_user_upload.'user_'.$user_management_id.'/document/spm_'.$code.'.pdf', 'F');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+