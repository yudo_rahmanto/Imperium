<?php
if($getData){
	$user_management_id	= $getData[0]['user_management_id'];
	$permohonan_id		= $getData[0]['permohonan_id'];
	$izin_name			= $getData[0]['izin_name'];
	$izin_type_name		= $getData[0]['izin_type_name'];
	$originalDate 		= $getData[0]['created_date'];
	$created_date 		= indonesia_date(date("d F Y H:i:s"));
	$durasi_kerja		= '07';
	$no_resi			= $getData[0]['code'];
	$qr_code			= $getData[0]['qr_code'];
	$path_location_qr_code	= $getData[0]['path_location_qr_code'];
	
	$nama_pemohon		= $getData[0]['first_name'].' '.$getData[0]['last_name'];
	$ktp_pemohon		= $getData[0]['ktp'];
	$npwp_pemohon		= $getData[0]['npwp'];
	$alamat_pemohon		= $getData[0]['alamat_user'];
	$rt_pemohon			= $getData[0]['rt_user'];
	$rw_pemohon			= $getData[0]['rw_user'];
	$provinsi_pemohon	= $getData[0]['provinsi_user'];
	$kota_pemohon		= $getData[0]['kota_user'];
	$kecamatan_pemohon	= $getData[0]['kecamatan_user'];
	$kelurahan_pemohon	= $getData[0]['kelurahan_user'];
	
	$jenis_perusahaan		= $getData[0]['jenis_perusahaan'];
	$nama_perusahaan		= $getData[0]['nama_perusahaan'];
	$alamat_perusahaan		= $getData[0]['alamat_perusahaan'];
	$rt_perusahaan			= $getData[0]['rt_perusahaan'];
	$rw_perusahaan			= $getData[0]['rw_perusahaan'];
	$provinsi_perusahaan	= $getData[0]['provinsi_perusahaan'];
	$kota_perusahaan		= $getData[0]['kota_perusahaan'];
	$kecamatan_perusahaan	= $getData[0]['kecamatan_perusahaan'];
	$kelurahan_perusahaan	= $getData[0]['kelurahan_perusahaan'];
	
	if($jenis_perusahaan){
		$nama_perusahaan	= $jenis_perusahaan.'. '.$nama_perusahaan;
	}
	if(!file_exists(path_user_upload.'user_'.$user_management_id)) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id, 0777);
		}
	if(!file_exists(path_user_upload.'user_'.$user_management_id.'/document')) {
		$create_folder			= mkdir(path_user_upload.'user_'.$user_management_id.'/document', 0777);
		}
	
	
} else {
	$user_management_id	= '';
	$permohonan_id		= '';
	$izin_name			= '';
	$izin_type_name		= '';
	$created_date 		= '';
	$durasi_kerja		= '07';
	$no_resi			= '';
	$qr_code			= '';
	$path_location_qr_code	= '';
	
	$nama_pemohon		= '';
	$ktp_pemohon		= '';
	$npwp_pemohon		= '';
	$alamat_pemohon		= '';
	$rt_pemohon			= '';
	$rw_pemohon			= '';
	$provinsi_pemohon	= '';
	$kota_pemohon		= '';
	$kecamatan_pemohon	= '';
	$kelurahan_pemohon	= '';
	
	$nama_perusahaan		= '';
	$alamat_perusahaan		= '';
	$rt_perusahaan			= '';
	$rw_perusahaan			= '';
	$kota_perusahaan		= '';
	$kecamatan_perusahaan	= '';
	$kelurahan_perusahaan	= '';
}
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 5, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
					<table align="center" style="width:100%;text-align:center;border-bottom: 1px solid black;">
						<tr>
							<td rowspan="5" width="15%"><div align="center"><img src="'.base_url('assets/images/logo/logo.png').'" width="auto" height="70px"></div></td>
							<td width="85%" style="font-size: 100%;font-size:100%;"><b>PEMERINTAHAN KOTA PALEMBANG</b></td>
						</tr>
						<tr>
							<td style="font-size:100%;">DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
						</tr>
						<tr>
							<td  style="font-size:80%;">Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</td>
						</tr>
						<tr>
							<td  style="font-size:80%;">Telp. (0711) 370679, 370681</td>
						</tr>
						<tr>
							<td  style="font-size:80%;">PALEMBANG</td>
						</tr>
					</table>
					<table align="center" style="width:100%;font-size:60%;">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><div align="center"><b><u>TANDA TERIMA</u></b></div></td>
						</tr>
						<tr>
							<td><div align="center">'.$no_resi.'</div></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><div align="left">Telah terima berkas permohonan izin :</div></td>
						</tr>
						
					</table>
					<table align="left" style="border: 1px solid black;font-size:60%;width:100%;text-align:left;">
						<tr>
							<td width="30%">Nama Izin</td>
							<td width="3%"> : </td>
							<td width="67%"><b>'.$izin_name.'</b></td>
						</tr>
						<tr>
							<td>Jenis Permohonan Izin</td>
							<td> : </td>
							<td><b>'.$izin_type_name.'</b></td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td>Nama Pemohon</td>
							<td> : </td>
							<td><b>'.$nama_pemohon.'</b></td>
						</tr>
						<tr>
							<td>No. Identitas</td>
							<td> : </td>
							<td><b>'.$ktp_pemohon.'</b></td>
						</tr>
						<tr>
							<td>No. NPWP</td>
							<td> : </td>
							<td><b>'.$npwp_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Alamat Pemohon</td>
							<td> : </td>
							<td><b>'.strtoupper($alamat_pemohon).' KEL. '.$kelurahan_pemohon.' KEC. '.$kecamatan_pemohon.' '.$kota_pemohon.' - '.$provinsi_pemohon.'</b></td>
						</tr>
						<tr>
							<td>RT</td>
							<td> : </td>
							<td><b>'.$rt_pemohon.'</b></td>
						</tr>
						<tr>
							<td>RW</td>
							<td> : </td>
							<td><b>'.$rw_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Kelurahan</td>
							<td> : </td>
							<td><b>'.$kelurahan_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Kecamatan</td>
							<td> : </td>
							<td><b>'.$kecamatan_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Kota / Kabupaten</td>
							<td> : </td>
							<td><b>'.$kota_pemohon.'</b></td>
						</tr>
						<tr>
							<td>Provinsi</td>
							<td> : </td>
							<td><b>'.$provinsi_pemohon.'</b></td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td>Nama Usaha</td>
							<td> : </td>
							<td><b>'.$nama_perusahaan.'</b></td>
						</tr>
						<tr>
							<td>Alamat Lokasi Usaha / Bangunan</td>
							<td> : </td>
							<td><b>'.strtoupper($alamat_perusahaan).' KEL. '.$kelurahan_perusahaan.' KEC. '.$kecamatan_perusahaan.' '.$kota_perusahaan.' - '.$provinsi_perusahaan.'</b></td>
						</tr>
						<tr>
							<td>RT</td>
							<td> : </td>
							<td><b>'.$rt_perusahaan.'</b></td>
						</tr>
						<tr>
							<td>RW</td>
							<td> : </td>
							<td><b>'.$rw_perusahaan.'</b></td>
						</tr>
						<tr>
							<td>Kelurahan</td>
							<td> : </td>
							<td><b>'.$kelurahan_perusahaan.'</b></td>
						</tr>
						<tr>
							<td>Kecamatan</td>
							<td> : </td>
							<td><b>'.$kecamatan_perusahaan.'</b></td>
						</tr>
						<tr>
							<td>Kota</td>
							<td> : </td>
							<td><b>'.$kota_perusahaan.'</b></td>
						</tr>
					</table>
					<div> &nbsp; </div>
					<table align="left" style="font-size:60%;margin-top:10px;border: 1px solid black;width:100%;text-align:left;">
						<tr>
							<td colspan="2">Persyaratan yang telah dilengkapi : </td>
						</tr>
						';
						if($getDataSyarat){
								$i = 1;
								foreach($getDataSyarat as $row){
						
$html	.='								<tr>
							<td valign="top" width="5%">'.$i.'. </td><td width="95%"><p>'.$row['syarat_name'].'</p></td>
						</tr>';
						
								$i++;
								}
							}
						
$html	.='				
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
					<div> &nbsp; </div>
					<table align="center" style="margin-top:10px;border: 1px solid black;width:100%;font-size:60%;">
						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%"><div align="left">Waktu Penyelesaian Pelayanan</div></td>
							<td width="3%"><div align="center">:</div></td>
							<td width="77%"><div align="left">'.$durasi_kerja.' Hari Kerja</div></td>
						</tr>
						<tr>
							<td colspan="3"><div align="left">(Setelah berkas diterima dengan persyaratan lengkap dan benar)</div></td>
						</tr>
					</table>
					<div> &nbsp; </div>
					<table align="center" style="margin-top:10px;border: 1px solid black;width:100%;font-size:60%;">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><div align="left">Palembang, '.$created_date.' WIB</div></td>
						</tr>
						<tr>
							<td><div align="left"><b>IMPERIUM v1.0</b></div></td>
						</tr>
					</table>
					<div> &nbsp; </div>
					<table align="center" style="margin-top:10px;border: 1px solid black;width:100%;font-size:60%;">
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="50px"><img src="'.base_url($path_location_qr_code).'" height="40px" width="auto"></td>
							<td width="80%" height="50px">
								BUKTI TANDA TERIMA PENDAFTARAN IJIN INI BUKAN MERUPAKAN TANDA BUKTI IJIN
								CEK STATUS BERKAS PERMOHONAN ANDA DI WWW.BPMPTSP.PALEMBANG.GO.ID
								UNTUK PERMOHONAN IJIN YANG MEMPUNYAI RETRIBUSI
								PERMOHONAN IJIN AKAN DIBATALKAN JIKA BELUM MELAKUKAN PEMBAYARAN RETRIBUSI
								LEBIH DARI 3 BULAN SETELAH TANGGAL SPM (SURAT PERINTAH MEMBAYAR) DITERBITKAN
							
							</td>
						</tr>
						</table>
					';
// echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'].root_folder.'/'.path_view_user_upload.'user_'.$user_management_id.'/document/'.$qr_code.'.pdf', 'F');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+