<?php
// echo '<pre>';print_r($getData);exit;
if($getData){
	$isiData	= "";
	foreach($getData[0] as $row_left=>$row_right){
		$isiData[$row_left]	= $row_right;
		
	}
	if($isiData['izin_category_id'] == 3){
		$catatan_izin	= " <br> DAN UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 25 TAHUN 1992 TENTANG PERKOPERASIAN";
		$no_pengesahan_menkop	= $getData[0]['no_pengesahan_menkop'];
		$hide_no_menkop	= 'block';
	}
	
} 
if($getDataSignature){
	$signature	= $getDataSignature[0]['path_location'];
} else {
	$signature	= '';
}
if($getKepalaDinas){
	$level_id		= $getKepalaDinas[0]['level_id'];
	$level_name 	= '';
	$nip_kepala		= $getKepalaDinas[0]['nip'];
	$nama_kepala	= $getKepalaDinas[0]['fullname_with_gelar'];
	$getLevel		= SingleFilter('m_level','level_id',$level_id);
	if($getLevel){
		$level_name = $getLevel[0]['level_name'];
	}
} else {
	$level_id		= '';
	$level_name 	= '';
	$nip_kepala		= '';
	$nama_kepala	= '';
}
// echo '<pre>';print_r($photo_pemohon);exit;
require_once('./assets/tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 0, 0);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(1.3);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 0);

// add a page
$pdf->AddPage();
$html	= '';
$html	.='
					<table align="center" style="width:100%;text-align:center;font-size: 12px;">
						<tr>
							<td style="height:200px;">&nbsp;</td>
						</tr>
						<tr>
							<td style="font-size: 30px;"><b>TANDA DAFTAR PERUSAHAAN</b></td>
						</tr>
						<tr>
							<td style="font-size: 25px;height:50px;"><b>'.strtoupper(str_replace("TANDA DAFTAR PERUSAHAAN","",$isiData['keterangan_izin'])).'</b></td>
						</tr>
						<tr>
							<td style="height:50px;">
								BERDASARKAN UNDANG-UNDANG NOMOR 3 TAHUN 1982 TENTANG WAJIB DAFTAR PERUSAHAAN 
								'.$catatan_izin.'
							</td>
						</tr>
					</table>
					<table align="center" style="margin-bottom:20px;width:98%;text-align:center;vertical-align: middle;font-size: 12px;">
						<tr>
							<td style="width:25%;height:20px;border: 1px solid black;"><p><br><b>NOMOR TDP</b><br><br>'.$isiData['no_sk'].'<br></p></td>
							<td style="width:25%;height:20px;border: 1px solid black;"><p><br><b>BERLAKU S/D TANGGAL</b><br><br>'.$isiData['periode'].'<br></p>
																						</td>
							<td style="width:10%;height:20px;">&nbsp;</td>
							<td style="width:20%;height:20px;border: 1px solid black;font-size: 14px;"><p><br><br><b>'.$isiData['d1'].'</b></p></td>
							<td style="width:20%;height:20px;border: 1px solid black;font-size: 14px;"><p><br><br><b>'.$isiData['d2'].'</b></p></td>
						</tr>
					</table>
					<table align="center" style="width:100%;">
						<tr>
							<td style="height:30px;">&nbsp;</td>
						</tr>	
					</table>
					<table id="data_detail" align="center" style="margin-top:20px;width:98%;text-align:left;font-size: 14px;border: 1px solid black;">
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;height:30px;">
								<table width="100%" style="padding:5px;">
									<tr>
										<td width="30%" align="left" style="height:30px;">NAMA PERUSAHAAN</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td colspan="5" align="left" width="67%" style="height:30px;font-size: 12px;"><b>'.$isiData['nama_perusahaan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%" style="padding:5px;">
									<tr>
										<td width="30%" align="left" style="height:30px;">STATUS</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left" colspan="5" style="height:30px;"><b>'.$isiData['status'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%" style="padding:5px;">
									<tr>
										<td width="30%" align="left" style="height:30px;">ALAMAT PERUSAHAAN</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left"colspan="5" style="height:30px;"><b>'.$isiData['alamat_perusahaan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%" style="padding:5px;">
									<tr>
										<td align="left" width="30%" style="height:30px;">NOMOR TELEPON</td>
										<td align="left" width="3%" style="height:30px;"> : </td>
										<td align="left" width="25%" style="height:30px;"><b>'.$isiData['no_tlp_perusahaan'].'</b></td>
										<td align="left" width="5%" style="height:30px;">&nbsp;</td>
										<td align="left" width="10%" style="height:30px;">FAX</td>
										<td align="left" width="3%" style="height:30px;">:</td>
										<td align="left" width="21%" style="height:30px;">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%" style="padding:5px;">
									<tr>
										<td width="30%" align="left" style="height:30px;">PENANGGUNG JAWAB / PENGURUS</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left" colspan="5" style="height:30px;"><b>'.$isiData['first_name'].' '.$isiData['last_name'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="100%">
								<table width="100%" style="padding:5px;">
									<tr>
										<td width="30%" align="left" style="height:30px;">KEGIATAN USAHA POKOK</td>
										<td width="3%" align="left" style="height:30px;"> : </td>
										<td width="67%" align="left" colspan="5" style="height:30px;"><b>'.$isiData['kegiatan_usaha_perusahaan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="height:30px;">
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%" style="padding:5px;">
									<tr>
										<td width="30%" align="left">[KLUI]</td>
										<td width="3%" align="left"> : </td>
										<td width="67%" align="left" colspan="5"><b>['.$isiData['klui'].']</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="height:30px;display:'.$hide_no_menkop.';">
							<td width="100%" >
								<table width="100%" style="padding:5px;">
									<tr>
										<td align="left" colspan="7">PENGESAHAN MENTERI KOPERASI</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="display:'.$hide_no_menkop.';">
							<td width="100%" style="border-bottom: 1px solid black;">
								<table width="100%" style="padding:5px;">
									<tr>
										<td align="left" width="30%" style="height:30px;display:'.$hide_no_menkop.';">NOMOR</td>
										<td align="left" width="3%" style="height:30px;display:'.$hide_no_menkop.';"> : </td>
										<td align="left" width="25%" style="height:30px;display:'.$hide_no_menkop.';"><b>'.$no_pengesahan_menkop.'</b></td>
										<td align="left" width="3%" style="height:30px;display:'.$hide_no_menkop.';">&nbsp;</td>
										<td align="left" width="12%" style="height:30px;display:'.$hide_no_menkop.';">TANGGAL</td>
										<td align="left" width="3%" style="height:30px;display:'.$hide_no_menkop.';">:</td>
										<td align="left" width="21%" style="height:30px;display:'.$hide_no_menkop.';"><b>'.$isiData['tgl_pengesahan'].'</b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table align="center" style="width:100%;">
						<tr>
							<td style="height:30px;">&nbsp;</td>
						</tr>	
					</table>
					<table align="left" style="width:100%;text-align:center;font-size: 12px;">
						<tr>
							<td width="50%" colspan="2" height="80px"><div align="center"><img src="'.base_url($isiData['qr_code_sk']).'" width="60px" style="margin-left: auto;margin-right: auto;	display: block;"></div></td>
							<td width="50%" height="80px">
								<table width="100%">
									<tr>
										<td width="10%">&nbsp;</td><td width="90%" colspan="2">Ditetapkan di Palembang</td>
									</tr>
									<tr>
										<td width="10%" style="height:30px;">&nbsp;</td><td width="90%" colspan="2">pada tanggal, '.str_replace(' 00:00:00','',indonesia_date(date('d F Y'))).'</td>
									</tr>
									<tr>
										<td width="10%"> a.n. </td><td colspan="2">WALIKOTA PALEMBANG </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td colspan="2">KEPALA DINAS PENANAMAN MODAL </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td colspan="2">DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="30%">&nbsp;</td>
							<td width="30%">&nbsp;</td>
							<td width="40%" align="left"><img src="'.base_url($signature).'" width="80px" height="80px"></td>
						</tr>
						<tr>
							<td width="30%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
							<td width="50%">
								<table width="100%">
									<tr>
										<td width="10%">&nbsp;</td><td width="90%"><b>'.$nama_kepala.' </b></td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td>'.$level_name.' </td>
									</tr>
									<tr>
										<td width="10%">&nbsp;</td><td>NIP. '.$nip_kepala.' </td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					';
// echo $html;exit;
//debug($html);exit;
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$html	= '';
$html	.= '
			<table align="center" style="width:100%;text-align:center;font-size: 16px;">
				<tr>
					<td style="height:50px;">&nbsp;</td>
				</tr>
				<tr>
					<td style="height:30px;">
						<p><b> KETENTUAN IJIN USAHA PERDAGANGAN </b></p>
					</td>
				</tr>
			</table>
			<table align="center" style="width:100%;text-align:left;font-size: 12px;">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td colspan="4" align="left" style="height:30px;">Pemegang Ijin Usaha Perdagangan mempunyai kewajiban mematuhi ketentuan, antara lain sebagai berikut :</td>
							</tr>
							<tr>
								<td align="left" style="width:3%;height:20px;"></td>
								<td align="left" style="width:3%;height:20px;"><b>A.</b></td>
								<td colspan="2" align="left" style="height:20px;"><b>Ketentuan Tertib Bangunan</b></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left" style="width:3%;">1.</td>
								<td align="left" style="width:91%;"><span align="justify">Memiliki Ijin Medirikan Bangunan (IMB)</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">2.</td>
								<td align="left"><span align="justify">Memiliki Ijin Penggunaan Bangunan (IPB) / Sertifikat Laik Fungsi (SLF)</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">3.</td>
								<td align="left" style="height:20px;"><span align="justify">Memiliki Surat Ijin Gangguan (IG)</span></td>
							</tr>
							<tr>
								<td align="left"></td>
								<td align="left"><b>B.</b></td>
								<td colspan="2" align="left" style="height:20px;"><b>Ketentuan Arsitek Bangunan</b></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">1.</td>
								<td align="left"><span align="justify">Bangunan atau bagian bangunan yang mengalami perubahan bentuk dan ukuran bangunan yang sudah ada wajib mendapatkan Ijin Walikota  Kota Palembang (Pasal 2 Perda No. 5 Tahun 2010)</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">2.</td>
								<td align="left"><span align="justify">Menjaga keamanan dan Keselamatan bangunan dan lingkungannya serta tidak boleh mengganggu ketentraman dan keselamatan masyarakat (Pasal 8 Ayat (1) Perda No. 5 Tahun 2010)</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">3.</td>
								<td align="left" style="height:70px;"><span align="justify">Mematuhi ketentuan fungsi bangunan gedung yang dapat dibangun pada lokasi, ketinggian maksimum bangunan gedung, jumlah lantai/lapis bangunan gedung dibawah permukaan tanah, Garis Sempadan dan jarak bebas minimum gedung, KDB, KLB, KDH KTB maksimum yang diijinkan dan jaringan utilitas kota  (Pasal 12 ayat (2) Perda No. 5 Tahun 2010)</span></td>
							</tr>
							<tr>
								<td align="left"></td>
								<td align="left"><b>C.</b></td>
								<td colspan="2" align="left" style="height:20px;"><b>Ketentuan Keselamatan, Kesehatan dan Keserasian Lingkungan</b></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">1.</td>
								<td align="left"><span align="justify">Mematuhi Ketentuan Undang-undang No. 1 Tahun 1970 Tentang Keselamatan Kerja</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">2.</td>
								<td align="left">
									<span align="justify">Setiap Bangunan bukan rumah tinggal dengan luas bangunan lebih dari 300 m dan atau dengan ketinggian lebih dari 2 lantai
									harus dilaksanakan oleh konsultan perencana, pelaksana dan pengawas bangunan (Pasal 13 Perda No. 5 Tahun 2010)</span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">3.</td>
								<td align="left">
									<span align="justify">Setiap bangunan tidak boleh menggangu kelancaran arus lalu lintas kendaraan, orang dan barang, tidak menggangu dan
									merusak sarana kota maupun prasarana jaringan kota serta tetap memperhatikan keserasian dan arsitektur lingkungan (Pasal 16
									Perda No. 5 Tahun 2010)</span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">4.</td>
								<td align="left">
									<span align="justify">Secara periodik setiap bulannya memeriksakan dan menguji alat pemadam kebakaran yang dimilikinya ke Dinas Penanggulangan Bahaya Kebakaran (Pasal 2 Perda No. 31 Tahun 2011)</span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">5.</td>
								<td align="left">
									<span align="justify">Menyiapkan kotak sampah dan wajib mengijinkan petugas Pemerintah Daerah untuk memasuki pekarangan,  rumah dan persilnya untuk kepentingan umum (Pasal 3  Perda No. 3 tahun 1981 jo Perda No. 8 Tahun 1987)</span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">6.</td>
								<td align="left">
									<span align="justify">Dilarang menebang / merusak pohon-pohon pelindung dan tanaman yang tumbuh disepanjang jalan jalur hijau (Perda No. 44 Tahun 2002)</span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">7.</td>
								<td align="left">
									<span align="justify">Memelihara dengan baik dan bersih persilnya dan segala sesuatu dalam persil, memagar, mengecat dan mengapur setiap awal bulan Juni (Pasal 2 Perda No. 3 Tahun 1981 jo Perda No. 8 Tahun 1987)</span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">8.</td>
								<td align="left">
									<span align="justify">Menjamin keamanan , ketertiban, kebersihan, kesehatan lingkungan tempat usahanya (Pasal 9 Perda No. 4 Tahun 2002)</span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">9.</td>
								<td align="left" style="height:20px;">
									<span align="justify">Memenuhi persyaratan untuk mengajukan Ijin Usaha Perdagangan  (Perda No. 19 Tahun 2011)</span>
								</td>
							</tr>
							<tr>
								<td align="left"></td>
								<td align="left"><b>D.</b></td>
								<td colspan="2" align="left" style="height:20px;"><b>Ketentuan Daftar Ulang</b></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left" style="height:20px;"><span align="justify">Ijin Usaha Perdagangan berlaku untuk jangka waktu selama 5 (lima) tahun dengan ketentuan apabila telah habis masa berlakunya, harus melaksanakan daftar ulang (Pasal 15 Perda No. 19 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left"></td>
								<td align="left"><b>E.</b></td>
								<td colspan="2" align="left" style="height:20px;"><b>Retribusi</b></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left" style="height:20px;"><span align="justify">Perijinan ini tidak dikenakan biaya retribusi (Rp.0,-) berdasarkan UU No.28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah.</span></td>
							</tr>
							<tr>
								<td align="left"></td>
								<td align="left"><b>F.</b></td>
								<td colspan="2" align="left" style="height:20px;"><b>Ketentuan Larangan</b></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">1.</td>
								<td align="left"><span align="justify">Tidak memenuhi ketentuan sebagaimana diatur dalam Peraturan Daerah (Perda No. 19 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">2.</td>
								<td align="left" style="height:20px;"><span align="justify">Tidak memenuhi kewajiban untuk mendaftar ulang  (Perda No. 19 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left"></td>
								<td align="left"><b>G.</b></td>
								<td colspan="2" align="left" style="height:20px;"><b>Ketentuan Perubahan, Penghapusan dan Penggantian</b></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">1.</td>
								<td align="left"><span align="justify">Perubahan perusahaan adalah perubahan yang meliputi perubahan nama perusahaan, bentuk perusahaan, alamat kantor perusahaan, nama pemilik  / penanggung jawab perusahaan, alamat pemilik / penanggung jawab perusahaan, NPWP, modal dan kekayaan bersih (Netto), bidang uasaha dan jenis barang / jasa dagangan utama.</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">2.</td>
								<td align="left"><span align="justify">Penghapusan SIUP terjadi apabila, perubahan bentuk perusahaan, pembubaran perusahaan, perusahaan menghentikan segala kegiatan usahanya, perusahaan tersebut terhenti pada waktu pendiriannya, kadaluarsa atau berakhirnya dan tidak diperpanjang, atau</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">3.</td>
								<td align="left"><span align="justify">Perusahaan tersebut dihentikan segala kegiatan usahanya berdasarkan keputusan pengadilan negeri yang mempunyai kekuatan hukum yang tetap.</span></td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
								<td align="left">4.</td>
								<td align="left" style="height:60px;"><span align="justify">Apabila SIUP yang telah dimiliki oleh perusahaan usaha perdagangan hilang atau rusak sehingga tidak terbaca, penanggung jawab atau pengusaha dapat mengajukan permohonan atas penggantian SIUP baru kepada Walikota dengan melampirkan Surat Keterangan hilang dari Kepolisian (Pasal 23 Perda No. 19 Tahun 2011)</span></td>
							</tr>
							<tr>
								<td align="left" colspan="4" style="height:50px;"><span align="justify">Ijin Usaha Perdagangan pada keputusan ini, akan dicabut dan dinyatakan tidak berlaku apabila pemegang ijin melakukan perubahan kegiatan pokok, perluasan tempat usaha, pindah lokasi dan memindah tangankan ijin usahanya dan tidak melaksanakan kewajiban pada ketentuan keputusan ini.</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			';
$pdf->writeHTML($html, true, false, true, false, '');
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_end_clean();
$pdf->Output('SK TDP.pdf', 'I');
// header('Location:'.base_url('assets/export/pdf/'.date('dmY').'.pdf')); 
//============================================================+
// END OF FILE                                                
//============================================================+