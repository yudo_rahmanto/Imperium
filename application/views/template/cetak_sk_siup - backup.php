<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$dataSk	= '';
	$modal_usaha = '';
	foreach($getData[0] as $row_left=>$row_right){
		$dataSk[$row_left] = $row_right;
	}
	$modal_usaha = $dataSk['modal_usaha'];
	
}
if($getDataSignature){
	$signature	= substr($getDataSignature[0]['path_location'],1);
} else {
	$signature	= '';
}
?>			
								<div align="center"><img src="<?php echo 'assets/images/logo/logo.png'; ?>"></div>
								<table align="center" style="width:100%;text-align:center;border-bottom: 1px solid black;font-size: 10px;">
									<tr>
										<td style="height:30px;"><b>PEMERINTAH KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td>BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
									<tr>
										<td>Jalan Merdeka No. 1 Palembang, Provinsi Sumatera Selatan </td>
									</tr>
									<tr>
										<td>Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td>Palembang</td>
									</tr>
								</table>
							
								<table align="center" style="margin-bottom:20px;width:100%;text-align:center;font-size: 10px;">
									<tr>
										<td style="height:30px;"><b>SURAT IJIN USAHA PERDAGANGAN</b></td>
									</tr>
									<tr>
										<td><b>(<?php if($getData){echo $dataSk['izin_name']; } ?>)</b></td>
									</tr>
									<tr>
										<td>NOMOR : 511.3/SIUP/<?php if($getData){echo $dataSk['no_sk']; } ?>/BPM-PTSP/<?php echo date('Y'); ?></td>
									</tr>
									
								</table>
								<table align="center" style="width:100%;text-align:left;font-size: 10px;">
									<tr style="height:30px;">
										<td width="60%">NAMA PERUSAHAAN</td><td> : </td><td width="39%"><b><?php if($getData){echo $dataSk['nama_perusahaan']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>ALAMAT PERUSAHAAN</td><td> : </td><td><b><?php if($getData){echo $dataSk['alamat_perusahaan']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>NAMA PEMILIK / PENANGGUNG JAWAB</td><td> : </td><td><b><?php if($getData){echo $dataSk['first_name'].' '.$dataSk['last_name']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>ALAMAT PEMILIK / PENANGGUNG JAWAB</td><td> : </td><td><b><?php if($getData){echo $dataSk['alamat_user']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>NOMOR TELEPON</td><td> : </td><td><b><?php if($getData){echo $dataSk['no_tlp_user']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>NOMOR POKOK WAJIB PAJAK (NPWP)</td><td> : </td><td><b><?php if($getData){echo $dataSk['npwp']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>KEKAYAAN BERSIH PERUSAHAAN (TIDAK TERMASUK TANAH DAN BANGUNAN)</td><td> : </td><td><b>RP <?php if($getData){echo number_format($dataSk['modal_usaha']); } ?></b></td>
									</tr>
									</tr>
									<tr style="height:30px;">
										<td>KEGIATAN USAHA</td><td> : </td><td><b><?php if($getData){echo $dataSk['kegiatan_usaha']; } ?></b></td>
									</tr>
									</tr>
									<tr style="height:30px;">
										<td>KELEMBAGAAN</td><td width="1%"> : </td><td width="79%"><b><?php if($getData){echo $dataSk['kelembagaan']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>BIDANG USAHA (KBLI)</td><td width="1%"> : </td><td width="79%"><b><?php if($getData){echo $dataSk['bidang_usaha']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>BARANG / JASA DAGANGAN UTAMA</td><td width="1%"> : </td><td width="79%"><b><?php if($getData){echo $dataSk['product']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td colspan="3">&nbsp;</td>
									</tr>
								</table>
									
								
								<table align="center" style="width:100%;text-align:left;font-size: 10px;">
									<tr>
										<td>
											<p> IJIN INI BERLAKU UNTUK MELAKUKAN KEGIATAN USAHA PERDAGANGAN DI SELURUH WILAYAH REPUBLIK INDONESIA, 
											SELAMA PERUSAHAAN MASIH MENJALANKAN USAHANYA, DAN WAJIB DIDAFTAR ULANG SETIAP 5 (LIMA) TAHUN SEKALI</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>MASA BERLAKU : <?php 
																	if($getData){
																		$original_start_date =  $dataSk['awal_masa_berlaku'];
																		$start_date = date("d M Y", strtotime($original_start_date));
																		echo $start_date; 
																		} 
																	
																	?> s/d <?php 
																				if($getData){
																					$original_end_date =  $dataSk['akhir_masa_berlaku'];
																					$end_date = date("d M Y", strtotime($original_end_date));
																					echo $end_date; 
																					} 
																			?>
											</p>
										</td>
									</tr>
								</table>
								<table align="left" style="width:20%;text-align:center;font-size: 10px;">
									<tr>
										<td> <img src="<?php if($getData){ echo $dataSk['path_location_barcode'];} ?>" width="60px" valign="bottom" style="float:right;"> </td>
									</tr>
								</table>
								<table align="right" style="width:40%;text-align:left;font-size: 10px;">
									<tr>
										<td>
											<p> Palembang, <?php echo date('d M Y'); ?></p>
											<p><b> a.n. WALIKOTA PALEMBANG</b></p>
											<p><b> KEPALA BADAN PENANAMAN MODAL</b></p>
											<p><b> PELAYANAN TERPADU SATU PINTU,</b></p>
										</td>
									</tr>
									<tr>
										<td style="height:70px;"><img src="<?php echo $signature; ?>" width="80px" height="80px"></td>
									</tr>
									<tr>
										<td>
											<p><b> Drs. RATU DEWA, M.Si </b></p>
											<p> Pembina Tingkat I </p>
											<p> NIP. 196907071993031005 </p>
										</td>
									</tr>
									
								</table>
								
								<table align="center" style="margin-top:350px;width:100%;text-align:center;font-size: 10px;">
									<tr>
										<td>
											<p><b> KETENTUAN IJIN USAHA PERDAGANGAN </b></p>
										</td>
									</tr>
								</table>
								<table align="center" style="margin-top:-50px;width:100%;text-align:left;font-size: 10px;">
									<tr>
										<td>
											<p>Pemegang Ijin Usaha Perdagangan mempunyai kewajiban mematuhi ketentuan, antara lain sebagai berikut :</p>
											<p>A.	Ketentuan Tertib Bangunan</p>
											<ul>
												<li>1.	Memiliki Ijin Medirikan Bangunan (IMB)</li>
												<li>2.	Memiliki Ijin Penggunaan Bangunan (IPB) / Sertifikat Laik Fungsi (SLF)</li>
												<li>3.	Memiliki Surat Ijin Gangguan (IG)</li>
											</ul>
											<p>&nbsp;</p>
											<p>B.	Ketentuan Arsitek Bangunan </p>
											<ul>
												<li>1.	Bangunan atau bagian bangunan yang mengalami perubahan bentuk dan ukuran bangunan yang sudah ada wajib mendapatkan Ijin Walikota  Kota Palembang (Pasal 2 Perda No. 5 Tahun 2010)</li>
												<li>2.	Menjaga keamanan dan Keselamatan bangunan dan lingkungannya serta tidak boleh mengganggu ketentraman dan keselamatan masyarakat (Pasal 8 Ayat (1) Perda No. 5 Tahun 2010)</li>
												<li>3.	Mematuhi ketentuan fungsi bangunan gedung yang dapat dibangun pada lokasi, ketinggian maksimum bangunan gedung, jumlah lantai/lapis bangunan gedung dibawah permukaan tanah, Garis Sempadan dan jarak bebas minimum gedung, KDB, KLB, KDH KTB maksimum yang diijinkan dan jaringan utilitas kota  (Pasal 12 ayat (2) Perda No. 5 Tahun 2010)</li>
											</ul>
											<p>&nbsp;</p>
											<p>C.	Ketentuan Keselamatan, Kesehatan dan Keserasian Lingkungan </p>
											<ul>
												<li>1.	Mematuhi Ketentuan Undang-undang No. 1 Tahun 1970 Tentang Keselamatan Kerja</li>
												<li>2.	Setiap Bangunan bukan rumah tinggal dengan luas bangunan lebih dari 300 m dan atau dengan ketinggian lebih dari 2 lantai harus dilaksanakan oleh konsultan perencana, pelaksana dan pengawas bangunan (Pasal 13 Perda No. 5 Tahun 2010)</li>
												<li>3.	Setiap bangunan tidak boleh menggangu kelancaran arus lalu lintas kendaraan, orang dan barang, tidak menggangu dan merusak sarana kota maupun prasarana jaringan kota serta tetap memperhatikan keserasian dan arsitektur lingkungan (Pasal 16 Perda No. 5 Tahun 2010)</li>
												<li>4.	Secara periodik setiap bulannya memeriksakan dan menguji alat pemadam kebakaran yang dimilikinya ke Dinas Penanggulangan Bahaya Kebakaran (Pasal 2 Perda No. 31 Tahun 2011)</li>
												<li>5.	Menyiapkan kotak sampah dan wajib mengijinkan petugas Pemerintah Daerah untuk memasuki pekarangan,  rumah dan persilnya untuk kepentingan umum (Pasal 3  Perda No. 3 tahun 1981 jo Perda No. 8 Tahun 1987)</li>
												<li>6.	Dilarang menebang / merusak pohon-pohon pelindung dan tanaman yang tumbuh disepanjang jalan jalur hijau (Perda No. 44 Tahun 2002)</li>
												<li>7.	Memelihara dengan baik dan bersih persilnya dan segala sesuatu dalam persil, memagar, mengecat dan mengapur setiap awal bulan Juni (Pasal 2 Perda No. 3 Tahun 1981 jo Perda No. 8 Tahun 1987)</li>
												<li>8.	Menjamin keamanan , ketertiban, kebersihan, kesehatan lingkungan tempat usahanya (Pasal 9 Perda No. 4 Tahun 2002)</li>
												<li>9.	Memenuhi persyaratan untuk mengajukan Ijin Usaha Perdagangan  (Perda No. 19 Tahun 2011)   </li>
											</ul>
											<p>&nbsp;</p>
											<p>D.	Ketentuan Daftar Ulang </p>
											<ul>
												<li>Ijin Usaha Perdagangan berlaku untuk jangka waktu selama 5 (lima) tahun dengan ketentuan apabila telah habis masa berlakunya, harus melaksanakan daftar ulang (Pasal 15 Perda No. 19 Tahun 2011)</li>
											</ul>
											<p>&nbsp;</p>
											<p>E.	Perijinan ini tidak dikenakan biaya retribusi (Rp.0,-) berdasarkan UU No.28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah.</p>
											<p>&nbsp;</p>
											<p>F.	Ketentuan Larangan</p>
											<ul>
												<li>1.	Tidak memenuhi ketentuan sebagaimana diatur dalam Peraturan Daerah (Perda No. 19 Tahun 2011)</li>
												<li>2.	Tidak memenuhi kewajiban untuk mendaftar ulang  (Perda No. 19 Tahun 2011)</li>
											</ul>
											<p>G.	Ketentuan Perubahan, Penghapusan dan Penggantian</p>
											<ul>
												<li>1.	Perubahan perusahaan adalah perubahan yang meliputi perubahan nama perusahaan, bentuk perusahaan, alamat kantor perusahaan, nama pemilik  / penanggung jawab perusahaan, alamat pemilik / penanggung jawab perusahaan, NPWP, modal dan kekayaan bersih (Netto), bidang uasaha dan jenis barang / jasa dagangan utama.</li>
												<li>2.	Penghapusan SIUP terjadi apabila, perubahan bentuk perusahaan, pembubaran perusahaan, perusahaan menghentikan segala kegiatan usahanya, perusahaan tersebut terhenti pada waktu pendiriannya, kadaluarsa atau berakhirnya dan tidak diperpanjang, atau;</li>
												<li>3.	Perusahaan tersebut dihentikan segala kegiatan usahanya berdasarkan keputusan pengadilan negeri yang mempunyai kekuatan hukum yang tetap.</li>
												<li>4.	Apabila SIUP yang telah dimiliki oleh perusahaan usaha perdagangan hilang atau rusak sehingga tidak terbaca, penanggung jawab atau pengusaha dapat mengajukan permohonan atas penggantian SIUP baru kepada Walikota dengan melampirkan Surat Keterangan hilang dari Kepolisian (Pasal 23 Perda No. 19 Tahun 2011)</li>
											</ul>
											<p>&nbsp;</p>
											<p>Ijin Usaha Perdagangan pada keputusan ini, akan dicabut dan dinyatakan tidak berlaku apabila pemegang ijin melakukan perubahan kegiatan pokok, perluasan tempat usaha, pindah lokasi dan memindah tangankan ijin usahanya dan tidak melaksanakan kewajiban pada ketentuan keputusan ini.</p>
										</td>
									</tr>
								</table>