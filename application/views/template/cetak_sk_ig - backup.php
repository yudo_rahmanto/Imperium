<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getData){
	$dataSk	= '';
	$modal_usaha = '';
	foreach($getData[0] as $row_left=>$row_right){
		$dataSk[$row_left] = $row_right;
	}
	$modal_usaha = $dataSk['modal_usaha'];
	
}
if($getDataSignature){
	$signature	= substr($getDataSignature[0]['path_location'],1);
} else {
	$signature	= '';
}

?>			
								<div align="center"><img src="<?php echo 'assets/images/logo/logo.png'; ?>" height="80px" width="auto"></div>
								<table align="center" style="width:100%;text-align:center;border-bottom: 1px solid black;font-size: 10px;">
									<tr>
										<td style="height:30px;"><b>PEMERINTAH KOTA PALEMBANG</b></td>
									</tr>
									<tr>
										<td>BADAN PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</td>
									</tr>
									<tr>
										<td>Jalan Merdeka No. 1 Palembang, Provinsi Sumatera Selatan </td>
									</tr>
									<tr>
										<td>Telp. (0711) 370679, 370681</td>
									</tr>
									<tr>
										<td>Palembang</td>
									</tr>
								</table>
							
								<table align="center" style="margin-bottom:20px;width:100%;text-align:center;font-size: 10px;">
									<tr>
										<td style="height:30px;"><b>SURAT IJIN USAHA PERDAGANGAN</b></td>
									</tr>
									<tr>
										<td><b>(<?php if($getData){echo $dataSk['izin_name']; } ?>)</b></td>
									</tr>
									<tr>
										<td>NOMOR : 511.3/IG.R/<?php if($getData){echo $dataSk['no_sk']; } ?>/BPM-PTSP/<?php echo date('Y'); ?></td>
									</tr>
									
								</table>
								<table align="center" style="margin-top:5px;width:100%;text-align:left;font-size: 7px;">
									<tr>
										<td width="10%" height="100px" valign="top">DASAR : </td>
										<td width="90%">
											<p>a.	Undang – Undang Gangguan (Hinder Ordonnantie) Stbl Tahun 1926 Nomor 226 diubah dan ditambah dengan Stbl Tahun 1940 Nomor 14 dan Nomor 450</p>
											<p>b.	Undang-undang Nomor 28 Tahun 1959  tentang Pembentukan Pemerintahan Daerah Tingkat II dan Kotapraja di Sumatra Selatan (Lembaran Negara RI Tahun 1959 Nomor 73, Tambahan lembaran Negara RI Nomor 1821)</p>
											<p>c.	Undang – Undang Nomor 25 Tahun 2007 tentang Penanaman Modal (Lembaran Negara Republik Indonesia Nomor 1821)</p>
											<p>d.	Undang-Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah (Lembaran Negara RI Tahun 2009 Nomor130, Tambahan Lembaran Negara RI Nomor 5049);</p>
											<p>e.	Peraturan Menteri Dalam Negeri Nomor 27 Tahun 2009 tentang Pedoman Penetapan Ijin Gangguan</p>
											<p>f.	Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010 tentang Ijin Mendirikan Bangunan (Lembaran Daerah Kota Palembang Tahun 2010 Nomor 5);</p>
											<p>g.	Peraturan Daerah Kota Palembang Nomor 3 Tahun 2013 tentang izin Lingkungan (Lembaran Daerah Kota Palembang Tahun 2013 Nomor 3);</p>
											<p>h.	Peraturan Daerah Kota Palembang Nomor 18 Tahun 2011 tentang Pembinaan dan Retribusi Ijin Gangguan (Lembaran Daerah Kota Palembang Tahun 2011 Nomor 18 Seri C);</p>
											<p>i.	Peraturan Daerah Kota Palembang Nomor 19 Tahun 2011 tentang Pembinaan di Bidang Industri dan Usaha Perdagangan (Lembaran Daerah Kota Palembang Tahun 2011 Nomor 19 Seri E);</p>
											<p>j.	Memperhatikan :</p>
											<ul>
												<li>Surat Permohonan Ijin Gangguan Saudara/i $nama_pemohon</li>
												<li>Hasil penelitian lapangan dan rekomendasi Tim Teknis Badan Penanaman Modal Dan Pelayanan Terpadu Satu Pintu Kota Palembang yang dituangkan dalam Berita Acara Pemeriksaan</li>
												<li>Bahwa berdasarkan pertimbangan sebagaimana tersebut di atas kepada pemohon dapat diberikan Surat Ijin Gangguan yang ditetapkan dengan Surat Ijin Walikota Palembang.</li>
											</ul>
										</td>
									</tr>
								</table>
								<table align="center" style="width:100%;text-align:left;font-size: 7px;">
									<tr style="height:30px;">
										<td width="10%">KEPADA </td>
										<td width="1%"> : </td>
										<td width="30%">Nama Pemilik / Penanggung Jawab</td>
										<td width="1%"> : </td>
										<td width="58%"><b><?php if($getData){echo $dataSk['first_name'].' '.$dataSk['last_name']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>&nbsp; </td>
										<td> &nbsp; </td>
										<td>Alamat Pemilik/ Penanggung Jawab</td>
										<td> : </td>
										<td><b><?php if($getData){echo $dataSk['alamat_user']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td colspan="5">&nbsp; </td>
									</tr>
									<tr style="height:30px;">
										<td width="10%">UNTUK </td>
										<td width="1%"> : </td>
										<td colspan="3">Untuk mendirikan Tempat Usaha, dengan data sebagai berikut:</td>
									</tr>
									<tr style="height:30px;">
										<td>&nbsp; </td>
										<td> &nbsp; </td>
										<td>1.	Nama Perusahaan</td>
										<td> : </td>
										<td><b><?php if($getData){echo $dataSk['nama_perusahaan']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>&nbsp; </td>
										<td> &nbsp; </td>
										<td>2.	Alamat Perusahaan</td>
										<td> : </td>
										<td><b><?php if($getData){echo $dataSk['alamat_perusahaan']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>&nbsp; </td>
										<td> &nbsp; </td>
										<td>3.	Jenis Usaha</td>
										<td> : </td>
										<td><b><?php if($getData){echo $dataSk['bidang_usaha']; } ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td>&nbsp; </td>
										<td> &nbsp; </td>
										<td>4.	Luas Tempat Usaha</td>
										<td> : </td>
										<td><b><?php if($getData){echo $dataSk['luas']; } ?> M</b></td>
									</tr>
									<tr style="height:30px;">
										<td>&nbsp; </td>
										<td> &nbsp; </td>
										<td>5.	Berlaku s.d tanggal</td>
										<td> : </td>
										<td><b><?php 
																	if($getData){
																		$original_start_date =  $dataSk['awal_masa_berlaku'];
																		$start_date = date("d M Y", strtotime($original_start_date));
																		echo $start_date; 
																		} 
																	
																	?> s/d <?php 
																				if($getData){
																					$original_end_date =  $dataSk['akhir_masa_berlaku'];
																					$end_date = date("d M Y", strtotime($original_end_date));
																					echo $end_date; 
																					} 
																			?></b></td>
									</tr>
								<table>
								<table align="center" style="margin-top:20px;width:100%;text-align:left;font-size: 7px;">
									<tr>
										<td width="100%">
											<p>Surat Ijin Gangguan ini berlaku sejak tanggal ditetapkan, dengan ketentuan sbb:</p>
											<p>1.	Pemegang Ijin wajib mentaati peraturan perundang-undangan yang berlaku;</p>
											<p>2.	Harus memasang Plat Nomor dan Petikan Surat Ijin Gangguan pada dinding depan yang mudah dibaca</p>
											<p>3.	Dalam pelaksanaan teknis pemasangan, pihak penyelenggara harus selalu menjaga keindahan, kebersihan dan ketertiban umum;</p>
											<p>4.	Surat Ijin Gangguan berlaku selama $lama_berlaku dan diwajibkan mendaftar ulang</p>
											<p>5.   Surat Ijin Gangguan akan ditinjau kembali dan disempurnakan sebagaimana mestinya, apabila dikemudian hari ternyata terdapat kekeliruan dalam penetapannya.</p>
										</td>
									</tr>
								</table>
								<table align="left" style="width:20%;text-align:center;font-size: 10px;">
									<tr>
										<td> <img src="<?php if($getData){ echo $dataSk['path_location_barcode'];} ?>" width="60px" valign="bottom" style="float:right;"> </td>
									</tr>
								</table>
								<table align="right" style="width:40%;text-align:left;font-size: 8px;">
									<tr>
										<td>
											<p> Ditetapkan di Palembang</p>
											<p>pada tanggal, <?php echo date('d M Y'); ?></p>
											<p><b> a.n. WALIKOTA PALEMBANG</b></p>
											<p><b> KEPALA BADAN PENANAMAN MODAL</b></p>
											<p><b> PELAYANAN TERPADU SATU PINTU,</b></p>
										</td>
									</tr>
									<tr>
										<td style="height:70px;"><img src="<?php echo $signature; ?>" width="80px" height="80px"></td>
									</tr>
									<tr>
										<td>
											<p><b> Drs. RATU DEWA, M.Si </b></p>
											<p> Pembina Tingkat I </p>
											<p> NIP. 196907071993031005 </p>
										</td>
									</tr>
									
								</table>
								
								<table align="center" style="margin-top:250px;width:100%;text-align:center;font-size: 10px;">
									<tr>
										<td>
											<p><b> KETENTUAN IJIN GANGGUAN </b></p>
										</td>
									</tr>
								</table>
								<table align="center" style="margin-top:-40px;width:100%;text-align:left;font-size: 7px;">
									<tr>
										<td>
											<p>Pemegang Ijin Gangguan pada Surat Ijin ini mempunyai kewajiban mematuhi Ketentuan-ketentuan sebagai berikut :</p>
											<p>A.	Ruangan Dan Lingkungan</p>
											<ul>
												<li>Memiliki Ijin Medirikan Bangunan (IMB)</li>
												<li>Memiliki Ijin Penggunaan Bangunan (IPB) / Sertifikat Laik Fungsi (SLF)</li>
												<li>Setiap memperluas dan merubah bangunan dan ruangan tempat usahanya harus mendapatkan ijin Walikota Palembang (Pasal 4 Perda No. 18 Tahun 2011)</li>
												<li>Memasang plat nomor dan petikan Ijin Gangguan pada dinding yang mudah dibaca (Pasal 7 Perda No. 18 Tahun 2011)</li>
												<li>Memasang nama perusahaan / merk usahanya</li>
												<li>Menjamin tempat usaha dalam keadaan bersih, rapi, dan indah</li>
												<li>Menjaga kebersihan got dan saluran pembuangan air</li>
												<li>Memelihara dengan baik dan bersih persilnya serta segala sesuatu dalam persilnya, termasuk memagar, mengecat dan mengapur setiap awal bulan Juni (pasal 2 Perda No. 3 Tahun 1981 jo. Perda No. 8 tahun 1987)</li>
												<li>Menyiapkan kotak sampah dan wajib mengijinkan petugas Pemerintah Daerah untuk memasuki pekarangan rumah dan persilnya untuk kepentingan umum (pasal 3 Perda No. 3 Tahun 1981 jo. Perda No. 8 tahun 1987)</li>
												<li>Dilarang menebang / merusak pohon-pohon pelindung dan tanaman yang tumbuh disepanjang jalan jalur hijau (Perda No. 44 Tahun 2002)</li>
												<li>Bangunan atau bagian bangunan yang mengalami perubahan bentuk dan ukuran bangunan yang sudah ada wajib mendapatkan Ijin Walikota  Kota Palembang (Pasal 2 Perda No. 5 Tahun 2010)</li>
												<li>Bangunan yang ada dalam lingkungan yang mengalami perubahan rencana kota, dapat melakukan perbaikan sesuai dengan peruntukan (Pasal 22 ayat (2) Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</li>
												<li>Pada lingkungan bangunan yang tertentu, dapat dilakukan perubahan penggunaan jenis bangunan yang ada, selama masih sesuai dengan golongan peruntukan rencana kota (Pasal 22 ayat (4) Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</li>
												<li>Perubahan rencana teknis karena perubahan pada arsitektur, struktur, utilitas (mekanikal dan elektrikal) serta perubahan rencana teknis karena perubahan fungsi harus melalui proses permohonan baru/revisi IMB dengan proses sesuai dengan penggolongan bangunan gedung untuk IMB (Pasal 26 huruf b dan huruf c Peraturan Daerah Kota Palembang Nomor 5 Tahun 2010)</li>
											</ul>
											<p>&nbsp;</p>
											<p>B.	Ketentuan Keselamatan Kerja</p>
											<ul>
												<li>Pemegang Ijin Gangguan wajib mematuhi ketentuan undang-undang Nomor 1 Tahun 1970 tentang Keselamatan Kerja</li>
												<li>Menjaga ketertiban & keamanan lingkungannya, menjaga kemungkinan timbulnya bahaya kebakaran dan menyediakan racun api yang telah diteliti oleh Dinas Penaggungan Bahaya Kebakaran. (Pasal 7 Keputusan Walikota Palembang No. 45 Tahun 2002)</li>
												<li>Untuk lingkungan bangunan perumahan dan lingkungan bangunan gedung harus dilengkapi hydrant atau sumur gali atau reservoar kebakaran dan apabila lingkungan bangunan yang berjarak lebih dari 100 (seratus) meter dari jalan lingkungan dilengkapi hydrant tersendiri (Pasal 5 Ayat 2 Perda Nomor 31 Tahun 2011) </li>
												<li>Untuk bangunan pabrik / mall / toko/ hotel, setiap 800 m2 diwajibkan menyediakan 1 (satu) titik hydrant conex macino 2,5 inchi.</li>
												<li>Untuk bangunan perumahan dengan tingkat kebakaran tinggi (Pom bensin), setiap 600 m2 diwajibkan menyediakan 1 (satu) titik hydrant conex macino 2,5 inchi.</li>
												<li>Untuk luas bangunan di bawah 150 m2 diwajibkan memiliki 1 (satu) tabung racun api ukuran 10 (sepuluh) liter dengan isi 3,5 kg dan untuk setiap kelipatan luas bangunan 150 m2 wajib merambah 1 (satu) tabung racun api dengan ukuran yang sama.</li>
												<li>Memenuhi persyaratan untuk mengajukan Ijin Gangguan  (Pasal 5 Perda No. 18 Tahun 2011)  </li>
											</ul>
											<p>&nbsp;</p>
											<p>C.	Ketentuan Retribusi</p>
											<ul>
												<li>Membayar Retribusi Ijin Gangguan berdasarkan Perda No. 18 Tahun 2011</li>
												<li>Wajib mendaftarkan ulang setiap $lama_berlaku sekali sejak tanggal Ijin Gangguan ditetapkan</li>
												<li>Dikenakan denda 2% setiap bulan dari retribusi apabila tidak membayar retribusi tepat pada waktunya (Pasal 34 Perda No. 18 Tahun 2011) </li>
											</ul>
											<p>&nbsp;</p>
											<p>D.	Ketentuan Larangan</p>
											<ul>
												<li>Jika Ijin Gangguan diperoleh secara tidak sah</li>
												<li>Tidak melakukan kegiatan-kegiatan pokok sesuai ijin yang diberikan</li>
												<li>Tidak memenuhi ketentuan-ketentuan yang ditetapkan dalam Surat Ijin Gangguan</li>
												<li>Mengadakan perluasan (kapasitas, volume dan luas) tempat usahanya tanpa ijin dari Walikota Palembang</li>
												<li>Memindahtangankan ijin tempat usahanya kepada pihak lain</li>
												<li>Tidak melakukan daftar ulang</li>
												<li>Memindahkan tempat usahanya</li>
											</ul>
											<p>&nbsp;</p>
											<p>Ijin Gangguan pada Surat Ijin ini, akan dicabut dan dinyatakan tidak berlaku apabila pemegang ijin memindah tangankan, merubah kegiatan usahanya, pindah lokasi dan memperluas tempat usaha dan tidak melaksanakan kewajiban pada ketentuan Keputusan ini.</p>
										</td>
									</tr>
								</table>