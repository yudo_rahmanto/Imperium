<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($getPetugas){
	$dataSession		= $this->session->userdata('user_data');	
	$nip_petugas		= $getPetugas[0]['nip'];
	$nama_petugas		= $getPetugas[0]['first_name'].' '.$getPetugas[0]['last_name'];
	$jabatan_petugas	= $dataSession['level_name'];
	$getKepalaDinas		= SingleFilter('m_admin','jabatan_id',5);
	$signature			= '';
	$nip_kepala_dinas		= '';
	$fullname_kepala_dinas	= '';
	$level_kepala			= '';
	
	if($getKepalaDinas){
		$nip_kepala_dinas		= $getKepalaDinas[0]['nip'];
		$fullname_kepala_dinas	= $getKepalaDinas[0]['fullname_with_gelar'];
		$level_id				= $getKepalaDinas[0]['level_id'];
		$signature				= $getKepalaDinas[0]['ttd'];
		$getLevel				= SingleFilter('m_level','level_id',$level_id);
		if($getLevel){
			$level_kepala		= $getLevel[0]['level_name'];
		}
	}
} else {
	$dataSession		= '';	
	$nip_petugas		= '';
	$nama_petugas		= '';
	$jabatan_petugas	= '';
	$signature			= '';
	$nip_kepala_dinas		= '';
	$fullname_kepala_dinas	= '';
	$level_kepala			= '';
}
if($getData){
	$permohonan_id		= $getData[0]['permohonan_id'];
	$izin_id			= $getData[0]['izin_id'];
	$izin_type_id		= $getData[0]['izin_type_id'];
	$izin_name			= $getData[0]['izin_name'];
	$izin_type_name		= $getData[0]['izin_type_name'];
	$ktp_user			= $getData[0]['ktp'];
	$full_name_user		= $getData[0]['first_name'].' '.$getData[0]['last_name'];
	$nama_perusahaan	= $getData[0]['nama_perusahaan'];
	$alamat_perusahaan	= $getData[0]['alamat_perusahaan'];
} else {
	$permohonan_id		= '';
	$izin_id			= '';
	$izin_type_id		= '';
	$izin_name			= '';
	$izin_type_name		= '';
	$ktp_user			= '';
	$full_name_user		= '';
	$nama_perusahaan	= '';
	$alamat_perusahaan	= '';
}
?>			
								<div align="center"><img src="<?php echo 'assets/images/logo/logo.png'; ?>"></div>
								<table align="center" style="width:100%;text-align:center;border-bottom: 1px solid black;">
									<tr>
										<td style="height:30px;"><div align="center"><b>PEMERINTAHAN KOTA PALEMBANG</b></div></td>
									</tr>
									<tr>
										<td><div align="center">DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU</div></td>
									</tr>
									<tr>
										<td><div align="center">Jl. Merdeka No. 1 Palembang, Provinsi Sumatera Selatan</div></td>
									</tr>
									<tr>
										<td><div align="center">Telp. (0711) 370679, 370681</div></td>
									</tr>
									<tr>
										<td><div align="center">PALEMBANG</div></td>
									</tr>
								</table>
							
								<table align="center" style="width:100%;text-align:center;">
									<tr>
										<td style="height:30px;"><b>SURAT PERINTAH KERJA</b></td>
									</tr>
									<tr>
										<td>No : <b><?php echo $permohonan_id.'/'.$izin_id.'/'.$izin_type_id.'/'.date('m').'/'.date('Y'); ?></b></td>
									</tr>
									
								</table>
							
								<table align="center" style="width:100%;text-align:left;">
									<tr style="height:30px;">
										<td colspan="3">Memberikan perintah kerja kepada :</td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">NIP</td><td width="1%"> : </td><td width="79%"><b><?php echo $nip_petugas; ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">Nama Petugas</td><td width="1%"> : </td><td width="79%"><b><?php echo $nama_petugas; ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">Jabatan</td><td width="1%"> : </td><td width="79%"><b><?php echo $jabatan_petugas; ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td colspan="3">&nbsp;</td>
									</tr>
								</table>
									
								<table align="center" style="width:100%;text-align:left;">
									<tr style="height:30px;">
										<td colspan="3">Untuk melakukan peninjauan langsung ke lokasi pemohon, berikut data pemohon :</td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">No KTP</td><td width="1%"> : </td><td width="79%"><b><?php echo $ktp_user; ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">Nama Pemohon</td><td width="1%"> : </td><td width="79%"><b><?php echo $full_name_user; ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">Nama Perusahaan</td><td width="1%"> : </td><td width="79%"><b><?php echo $nama_perusahaan;  ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">Nama Izin</td><td width="1%"> : </td><td width="79%"><b><?php echo $izin_name; ?></b></td>
									</tr>
									<tr style="height:30px;">
										<td width="20%">Type izin</td><td width="1%"> : </td><td width="79%"><b><?php echo $izin_type_name; ?></b></td>
									</tr>
								</table>
									
								<table align="center" style="width:100%;text-align:left;">
									<tr>
										<td colspan="3">
											<p> Surat perintah ini diserahkan kepada yang bersangkutan untuk dapat dilaksanakan sebagaimana mestinya.</p>
										</td>
									</tr>
								</table>
								<table align="right" style="width:40%;text-align:center;">
									<tr>
										<td>
											<p> Palembang, <?php echo date('d M Y'); ?></p>
											<p><b> WALIKOTA PALEMBANG</b></p>
										</td>
									</tr>
									<tr>
										<td style="height:70px;"><img src="<?php echo base_url($signature); ?>" height="70px" width="auto"></td>
									</tr>
									<tr>
										<td>
											<p><b> <?php echo $fullname_kepala_dinas; ?> </b></p>
											<p> <?php echo $level_kepala; ?> </p>
											<p> NIP. <?php echo $nip_kepala_dinas; ?> </p>
										</td>
									</tr>
									
								</table>
									