<?php
class authlogin{	
	var $CI;
	function __construct(){
		$this->CI =& get_instance();
	}	
	function check_admin_session(){
		$dataSession 	= $this->CI->session->userdata("user_data");
		$status			= $dataSession['level_id'];
		if(empty($dataSession)){
			redirect("login");
			exit;
		}		
		else if(($dataSession) && (!$status)){
			redirect("login/logout");
			exit;
			}
		
		return $dataSession;
	}
	
	
	public function del_session(){
		/* 
			Fungsi ini berfungsi untuk menghapus semua session
		*/
		$this->session->unset_userdata('user_data');
	}
}
?>