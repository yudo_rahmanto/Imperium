<?php
class Alert{	
	var $CI;
	function __construct(){
		$this->CI =& get_instance();
	}	
	function notice($no=null){
		if($no){
		$notice		= array();
		$notice[1][1]	= "Data has been saved";
		$notice[2][2]	= "Save data failed";
		$notice[3][2]	= "Login Failed";
		$notice[4][2]	= "Your Account is disabled, please contact your administrator";
		$notice[5][1]	= "Thank You For Login";
		$notice[6][1]	= "Delete Success";
		$notice[7][1]	= "Delete Failed";
		$notice[8][1]	= "Logout Success";
		$notice[9][2]	= "Data Already Exist";
		$notice[10][1]	= "Approval Success";
		$notice[11][2]	= "Approval Already Exist";
		$notice[12][1]	= "Reset your password success";
		$notice[13][2]	= "Upload image failed";
		$notice[14][2]	= "Sorry, your character is not allowed. Examples of permitted characters [1-9A-Z!_*,.@#]";
		$notice[15][1]	= "Your account is active, please login";
		$notice[16][1]	= "Register Data Success, please check your email for activation";
		$notice[17][2]	= "Register Data Failure";
		$group		= array();
		
		return $notice[$no];
		} else {
		return false;
		}
	}
	
}
?>