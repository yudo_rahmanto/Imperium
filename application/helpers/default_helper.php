<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* get head file */
if (!function_exists('debug')){
	function debug($data)
	{
		echo '<pre>',print_r($data,1),'</pre>';
	}
}
if (!function_exists('insert_log_activity')){
	function insert_log_activity($module = null,$action = null,$activity = null)
	{
		$CI 							= & get_instance();  //get instance, access the CI superobject
		$dataSession 					= $CI->session->userdata('user_data');
		$UserId_Session			= $dataSession['user_id'];
		$LevelId_Session						= $dataSession['status'];
		
		$CI->load->model('M_access');
		$data	= array(
						'datecreate'			=> date('Y-m-d H:i:s'),
						'user_management_id'	=> $getIdVendor_Session,
						'module'				=> $module,
						'action'				=> $action,
						'activity'				=> $activity
						);
		if($LevelId_Session	 == 1){
		$save	= $CI->M_access->add('log_activity_admin',$data);
		} else {
		$save	= $CI->M_access->add('log_activity_vendor',$data);	
		}
	}
}
if (!function_exists('file_log')){
	function file_log($activity = null)
	{
		$CI 							= & get_instance();  //get instance, access the CI superobject
		$CI->load->helper('file');
		$dataSession 					= $CI->session->userdata('user_data');
		$UserId_Session					= $dataSession['user_id'];
		$LevelId_Session				= $dataSession['status'];
		$fileLocation 					= $_SERVER['DOCUMENT_ROOT'].'assets/admin/log/log_file.txt';
		if(file_exists($fileLocation)){
			$content 	= "Date Created : ".date('Y-m-d H:i:s')." || ".$activity."\n";
			$read		= fopen($fileLocation, "a+");
			fwrite($read, $content);
			fclose($fileLocation);
		} else {
			$content 	= "Date Created : ".date('Y-m-d H:i:s')." || ".$activity."\n";
			write_file($fileLocation,$content);
			fclose($fileLocation);
		}
	}
}
if (!function_exists('random_string')){
	function random_string($length = 20,$value = null)
	{
		$value = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
		
		$charactersLength = strlen($value);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $value[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}

if (!function_exists('emailChecking')){
	function emailChecking($email = null)
	{
		$CI 	= & get_instance();  //get instance, access the CI superobject
		$user	= singleFilter('m_user_management','email',$email);
		$staff	= singleFilter('m_admin','email',$email);
		if((!$user) && (!$staff)){
			return true;
		} else {
			return false;
		}
	}
}
if (!function_exists('grabDataAlurIzin')){
	function grabDataAlurIzin($izin_id = null,$izin_type_id = null)
	{
		$CI 	= & get_instance();  //get instance, access the CI superobject
		$CI->load->model('M_alur_izin');
		$search							= array(
											"a.izin_id"			=> $izin_id,
											"a.izin_type_id"	=> $izin_type_id
										);
		$result		= $CI->M_alur_izin->getAlurIzin($search,'');
		$data		= $result['data'];
		$dataAlur	= '';
			
		if($data){
			foreach($data as $row){
				$dataAlur[$row['no_urut']]['jabatan_id']	= $row['jabatan_id'];
				$dataAlur[$row['no_urut']]['jabatan_name']	= $row['jabatan_name'];
			}
		}
		return $dataAlur;
	}
}
if (!function_exists('sendEmail')){
	function sendEmail($email = null,$subject = null,$body = null,$attach = null)
	{
		$ci = & get_instance();
        $ci->load->library('email');
        $config['protocol'] 	= "smtp";
        $config['smtp_host'] 	= "ssl://smtp.gmail.com";
        $config['smtp_port'] 	= "465";
        $config['smtp_user'] 	= "ugetuget80@gmail.com";#alamat email dibuat untuk test pake pribadi
        $config['smtp_pass'] 	= "S4landyou";#ini password email
        $config['charset'] 		= "iso-8859-1";
        $config['mailtype'] 	= "html";
        $config['newline'] 		= "\r\n";
		$ci->email->initialize($config);
		$ci->email->from("noreply@".$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
		$ci->email->to($email);
		 
		$ci->email->subject('[ IMPERIUM 1.0 ] '.$subject);
		$ci->email->message($body);
		if($attach){
		$ci->email->attach($attach);
		}
		if($ci->email->send()){
			return true;
		} else {
			return false;
		}
	}
}
if (!function_exists('terbilang')){
	function terbilang($nominal = null)
	{
		$angka = (float)$nominal;
		$bilangan = array(
				'',
				'satu',
				'dua',
				'tiga',
				'empat',
				'lima',
				'enam',
				'tujuh',
				'delapan',
				'sembilan',
				'sepuluh',
				'sebelas'
		);
	 
		if ($angka < 12) {
			return $bilangan[$angka];
		} else if ($angka < 20) {
			return $bilangan[$angka - 10] . ' belas';
		} else if ($angka < 100) {
			$hasil_bagi = (int)($angka / 10);
			$hasil_mod = $angka % 10;
			return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
		} else if ($angka < 200) {
			return sprintf('seratus %s', terbilang($angka - 100));
		} else if ($angka < 1000) {
			$hasil_bagi = (int)($angka / 100);
			$hasil_mod = $angka % 100;
			return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
		} else if ($angka < 2000) {
			return trim(sprintf('seribu %s', terbilang($angka - 1000)));
		} else if ($angka < 1000000) {
			$hasil_bagi = (int)($angka / 1000); // karena hasilnya bisa ratusan jadi langsung digunakan rekursif
			$hasil_mod = $angka % 1000;
			return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
		} else if ($angka < 1000000000) {
	 
			// hasil bagi bisa satuan, belasan, ratusan jadi langsung kita gunakan rekursif
			$hasil_bagi = (int)($angka / 1000000);
			$hasil_mod = $angka % 1000000;
			return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else if ($angka < 1000000000000) {
			// bilangan 'milyaran'
			$hasil_bagi = (int)($angka / 1000000000);
			$hasil_mod = fmod($angka, 1000000000);
			return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else if ($angka < 1000000000000000) {                         
		// bilangan 'triliun'                        
		$hasil_bagi = $angka / 1000000000000;                       
		$hasil_mod = fmod($angka, 1000000000000);                      
		return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else { return 'Wow...'; }   
	}
}
if (!function_exists('indonesia_date')){
	function indonesia_date($tanggal = null)
	{
		$CI 	= & get_instance();  //get instance, access the CI superobject
		$original_date 	=  $tanggal;
		$date 			= date("d F Y H:i:s", strtotime($original_date));	
		if(preg_match('/January/',$date)){
			$date 		= str_replace('January','Januari',$date);
		} 
		else if(preg_match('/February/',$date)){
			$date 		= str_replace('February','Februari',$date);
		}
		else if(preg_match('/March/',$date)){
			$date 		= str_replace('March','Maret',$date);
		}
		else if(preg_match('/April/',$date)){
			$date 		= str_replace('April','April',$date);
		}
		else if(preg_match('/May/',$date)){
			$date 		= str_replace('May','Mei',$date);
		}
		else if(preg_match('/June/',$date)){
			$date 		= str_replace('June','Juni',$date);
		}else if(preg_match('/July/',$date)){
			$date 		= str_replace('July','Juli',$date);
		}
		else if(preg_match('/August/',$date)){
			$date 		= str_replace('August','Agustus',$date);
		}
		else if(preg_match('/September/',$date)){
			$date 		= str_replace('September','September',$date);
		}
		else if(preg_match('/October/',$date)){
			$date 		= str_replace('October','Oktober',$date);
		}
		else if(preg_match('/November/',$date)){
			$date 		= str_replace('November','November',$date);
		}
		else if(preg_match('/December/',$date)){
			$date 		= str_replace('December','Desember',$date);
		}
		return $date;
	}
}
if (!function_exists('str_base64_encode')){
	function str_base64_encode($string = null)
	{
		$result =  strtr(base64_encode($string), '+/=', '-_,');
		return $result;
	}
}
if (!function_exists('str_base64_decode')){
	function str_base64_decode($string = null)
	{
		$result = base64_decode(strtr($string, '-_,', '+/='));
		return $result;
	}
}
?>