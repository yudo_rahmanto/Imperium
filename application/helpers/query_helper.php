<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter Directory Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Yudo Rahmanto
 * @link		https://codeigniter.com/user_guide/helpers/directory_helper.html
 */

// ------------------------------------------------------------------------
	if( !function_exists('SingleFilter') ) {
    	function SingleFilter($table = null,$field = null,$value = null,$order_by = null,$sort = null)
		{
			$CI 			= & get_instance();
			$CI->load->database();
			
			$filter	= '';
			if(($field) && ($value)){
				$filter	= " Where ".$field." = '".$value."' ";
			}
			$sql			= "
								Select * From ".$table." ".$filter."
							";
			if(($order_by) && ($sort)){
				$orderby	= " Order By ".$order_by." ".$sort;
				$sql		= $sql.$orderby;
			}
			$query			= $CI->db->query($sql);
			$data			= $query->result_array();
			if($data){
				return $data;
			} else {
				return false;
				}
		}
    }

	if( !function_exists('ManyFilter') ) {
    	function ManyFilter($table = null,$filter = null,$order_by = null,$sort = null)
		{
			$CI 			= & get_instance();
			$CI->load->database();
			
			$filter_array		= "";
			if($filter){
				$i 				= 1;
				$operator		= "";
				foreach($filter as $row_left=>$row_right){
					if($i == 1){$filter_array = " Where ";}
					if($i > 1){$operator = " And ";}
					$filter_array	.= $operator.$row_left." = '".$row_right."' ";
				$i++;
				}
			}
			$sql			= "
								Select * From ".$table." ".$filter_array."
							";
			if(($order_by) && ($sort)){
				$orderby	= " Order By ".$order_by." ".$sort;
				$sql		= $sql.$orderby;
			}
			$query			= $CI->db->query($sql);
			$data			= $query->result_array();
			if($data){
				return $data;
			} else {
				return false;
				}
		}
    }
	if( !function_exists('Id') ) {
    	function Id($table = null,$field = null)
		{
			$CI 			= & get_instance();
			$CI->load->database();
			
			$sql			= "Select case when max(".$field.") is not null then (max(".$field.") + 1) else '1' end as id From ".$table."";
			$query			= $CI->db->query($sql);
			$data			= $query->result_array();
			if($data){
				return $data[0]['id'];
			} else {
				return false;
				}
		}
    }
	if( !function_exists('Add') ) {
    	function Add($table = null,$data = null)
		{
			$CI 			= & get_instance();
			$CI->load->database();
			
			if($CI->db->insert($table, $data)){
				return true;
			}else {			
				return false;	
			}
		}
    }
	if( !function_exists('Edit') ) {
    	function Edit($table = null,$data= null,$field= null,$id= null)
		{
			$CI 			= & get_instance();
			$CI->load->database();
			
			$CI->db->where($field,$id);
			if($CI->db->update($table,$data)){
				return true;
			}else {			
				return false;	
			}
		}
    }
	if( !function_exists('DeleteTable') ) {
    	function DeleteTable($table = null,$field = null,$value = null)
		{
			$CI 			= & get_instance();
			$CI->load->database();
			
			$sql	= "	Delete from ".$table." where ".$field." = '".$value."' ";
			$query	= $CI->db->query($sql);
			if($query){
				return true;
				} else {
				return false;
				}
		}
    }