<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* get head file */

if (!function_exists('assets_css')){
	function assets_css($module = null)
	{
		$CI = & get_instance();  //get instance, access the CI superobject
		$CI->load->helper('file');
		$data		= '';
		$base_url	= $CI->config->config['base_url'];
		// if($module == 'Login'){
			// $data	= array(
								// $base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								// $base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								// $base_url.'assets/vendors/nprogress/nprogress.css',
								// $base_url.'assets/vendors/animate.css/animate.min.css',
								// $base_url.'assets/build/css/custom.min.css',
								// $base_url.'assets/vendors/pnotify/dist/pnotify.css',
								// $base_url.'assets/vendors/pnotify/dist/pnotify.buttons.css',
								// $base_url.'assets/vendors/pnotify/dist/pnotify.nonblock.css'
							// );
		// }
		if($module == 'Login'){
			$data	= array(
								$base_url.'assets/css/login/bootstrap.css',
								$base_url.'assets/css/login/bootstrap.min.css',
								$base_url.'assets/css/login/style.css',
								$base_url.'assets/css/login/style-responsive.css',
								$base_url.'assets/css/login/font-awesome/css/font-awesome.css',
								$base_url.'assets/css/login/bootstrap-reset.css'
							);
		}
		if($module == 'Home'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Menu'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Parent_menu'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Level'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Jabatan'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Menu_access'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Perusahaan'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Izin'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if(($module == 'Syarat') || ($module == 'Persyaratan')){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css',
								$base_url.'assets/vendors/select2/dist/css/select2.min.css'
							);
		}
		if(($module == 'Admin') || ($module == 'User') || ($module == 'Staff')){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css',
								$base_url.'assets/vendors/pnotify/dist/pnotify.css',
								$base_url.'assets/vendors/pnotify/dist/pnotify.buttons.css',
								$base_url.'assets/vendors/pnotify/dist/pnotify.nonblock.css'
							);
		}
		if($module == 'Alur_izin'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Permohonan'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Approval'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Archive'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css',
								$base_url.'assets/vendors/bootstrap-daterangepicker/daterangepicker.css'
							);
		}
		if($module == 'Surat'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css',
								$base_url.'assets/vendors/bootstrap-daterangepicker/daterangepicker.css',
								$base_url.'assets/vendors/select2/dist/css/select2.min.css'
							);
		}
		if($module == 'Template'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							
							);
		}
		if($module == 'Pengaduan'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							
							);
		}
		if($module == 'Penolakan'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							
							);
		}
		if($module == 'Jenis_usaha'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Jenis_perusahaan'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css'
							);
		}
		if($module == 'Finance'){
			$data	= array(
								$base_url.'assets/vendors/bootstrap/dist/css/bootstrap.min.css',
								$base_url.'assets/vendors/font-awesome/css/font-awesome.min.css',
								$base_url.'assets/vendors/nprogress/nprogress.css',
								$base_url.'assets/vendors/iCheck/skins/flat/green.css',
								$base_url.'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
								$base_url.'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
								$base_url.'assets/build/css/custom.min.css',
								$base_url.'assets/vendors/bootstrap-daterangepicker/daterangepicker.css',
								$base_url.'assets/vendors/select2/dist/css/select2.min.css'
							);
		}
		return $data;
	}
}
if (!function_exists('assets_js')){
	function assets_js($module = null)
	{
		$CI = & get_instance();  //get instance, access the CI superobject
		$CI->load->helper('file');
		$data		= '';
		$base_url	= $CI->config->config['base_url'];
		// if($module == 'Login'){
			// $data	= array(
								// $base_url.'assets/vendors/jquery/dist/jquery.min.js',
								// $base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								// $base_url.'assets/vendors/pnotify/dist/pnotify.js',
								// $base_url.'assets/vendors/pnotify/dist/pnotify.buttons.js',
								// $base_url.'assets/vendors/pnotify/dist/pnotify.nonblock.js',
								// $base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							// );
		// }
		if($module == 'Login'){
			$data	= array(
								$base_url.'assets/js/login/jquery.js',
								$base_url.'assets/js/login/jquery-1.8.3.min.js',
								$base_url.'assets/js/login/bootstrap.min.js',
							);
		}
		if($module == 'Home'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js'
							);
		}
		if($module == 'Menu'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js'
							);
		}
		if($module == 'Parent_menu'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js'
							);
		}
		if($module == 'Level'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							);
		}
		if($module == 'Jabatan'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							);
		}
		if($module == 'Menu_access'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js'
							);
		}
		if($module == 'Perusahaan'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							);
		}
		if($module == 'Izin'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							);
		}
		if(($module == 'Syarat') || ($module == 'Persyaratan')){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/vendors/select2/dist/js/select2.full.min.js'
							);
		}
		if(($module == 'Admin') || ($module == 'User') || ($module == 'Staff')){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/vendors/pnotify/dist/pnotify.js',
								$base_url.'assets/vendors/pnotify/dist/pnotify.buttons.js',
								$base_url.'assets/vendors/pnotify/dist/pnotify.nonblock.js',
							);
		}
		if($module == 'Alur_izin'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							);
		}
		if($module == 'Permohonan'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
								$base_url.'assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'
							);
		}
		if($module == 'Approval'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
								$base_url.'assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'
							);
		}
		if($module == 'Archive'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/moment/min/moment.min.js',
								$base_url.'assets/vendors/bootstrap-daterangepicker/daterangepicker.js',
								$base_url.'assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js',
								$base_url.'assets/vendors/jquery.hotkeys/jquery.hotkeys.js',
								$base_url.'assets/vendors/google-code-prettify/src/prettify.js',
								$base_url.'assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js',
								$base_url.'assets/vendors/switchery/dist/switchery.min.js',
								$base_url.'assets/vendors/select2/dist/js/select2.full.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/vendors/autosize/dist/autosize.min.js',
								$base_url.'assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js',
								$base_url.'assets/vendors/starrr/dist/starrr.js',
								$base_url.'assets/build/js/custom.min.js'
								
							);
							}
		if($module == 'Surat'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/moment/min/moment.min.js',
								$base_url.'assets/vendors/bootstrap-daterangepicker/daterangepicker.js',
								$base_url.'assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js',
								$base_url.'assets/vendors/jquery.hotkeys/jquery.hotkeys.js',
								$base_url.'assets/vendors/google-code-prettify/src/prettify.js',
								$base_url.'assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js',
								$base_url.'assets/vendors/switchery/dist/switchery.min.js',
								$base_url.'assets/vendors/select2/dist/js/select2.full.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/vendors/autosize/dist/autosize.min.js',
								$base_url.'assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js',
								$base_url.'assets/vendors/starrr/dist/starrr.js',
								$base_url.'assets/build/js/custom.min.js'
								
							);
		}
		if($module == 'Template'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/ckeditor/ckeditor.js',
								$base_url.'assets/ckeditor/samples/js/sample.js',
								$base_url.'assets/js/encrypt.js',
							);
		}
		if($module == 'Pengaduan'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/ckeditor/ckeditor.js',
								$base_url.'assets/ckeditor/samples/js/sample.js',
							);
		}
		if($module == 'Penolakan'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/ckeditor/ckeditor.js',
								$base_url.'assets/ckeditor/samples/js/sample.js',
							);
		}
		if($module == 'Jenis_usaha'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							);
		}
		if($module == 'Jenis_perusahaan'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/jszip/dist/jszip.min.js',
								$base_url.'assets/vendors/pdfmake/build/pdfmake.min.js',
								$base_url.'assets/vendors/pdfmake/build/vfs_fonts.js',
								$base_url.'assets/build/js/custom.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js'
							);
		}
		if($module == 'Finance'){
			$data	= array(
								$base_url.'assets/vendors/jquery/dist/jquery.min.js',
								$base_url.'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
								$base_url.'assets/vendors/fastclick/lib/fastclick.js',
								$base_url.'assets/vendors/nprogress/nprogress.js',
								$base_url.'assets/vendors/datatables.net/js/jquery.dataTables.min.js',
								$base_url.'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
								$base_url.'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js',
								$base_url.'assets/vendors/datatables.net-buttons/js/buttons.print.min.js',
								$base_url.'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
								$base_url.'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
								$base_url.'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
								$base_url.'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
								$base_url.'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',
								$base_url.'assets/vendors/iCheck/icheck.min.js',
								$base_url.'assets/vendors/moment/min/moment.min.js',
								$base_url.'assets/vendors/bootstrap-daterangepicker/daterangepicker.js',
								$base_url.'assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js',
								$base_url.'assets/vendors/jquery.hotkeys/jquery.hotkeys.js',
								$base_url.'assets/vendors/google-code-prettify/src/prettify.js',
								$base_url.'assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js',
								$base_url.'assets/vendors/switchery/dist/switchery.min.js',
								$base_url.'assets/vendors/select2/dist/js/select2.full.min.js',
								$base_url.'assets/vendors/parsleyjs/dist/parsley.min.js',
								$base_url.'assets/vendors/autosize/dist/autosize.min.js',
								$base_url.'assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js',
								$base_url.'assets/vendors/starrr/dist/starrr.js',
								$base_url.'assets/build/js/custom.min.js'
							);
		}
		return $data;
	}
}
?>